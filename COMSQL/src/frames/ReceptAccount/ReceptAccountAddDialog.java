/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * PayAccountAddDialog.java
 *
 * Created on 29/04/2009, 09:03:34
 */

package frames.ReceptAccount;

import components.CalendarComboBox;
import comsql.Customer;
import comsql.CustomerFunctions;
import util.Utilities;
import util.CalendarOperations;
import comsql.ReceptAccount;
import comsql.ReceptAccountFunctions;
import comsql.ReceptPlot;
import comsql.Sale;
import frames.Customer.CustomerAddEditDialog;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author usuario
 */
public class ReceptAccountAddDialog extends javax.swing.JDialog {
    CustomerFunctions cusfun = CustomerFunctions.getInstance();
    List<Customer> customers;
    DefaultTableModel dtm;
    List<ReceptPlot> receptPlots;
    Customer selectedCustomer;
    ReceptAccount receptAccount;
    Boolean isEditing = false;
    Sale sale;

    /** Creates new form PayAccountAddDialog */
    public ReceptAccountAddDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        Init();
    }

    public ReceptAccountAddDialog(java.awt.Frame parent, boolean modal, Customer c, Double inPlot, Double amount, Sale s) {
        super(parent, modal);
        initComponents();
        Init();
        this.selectedCustomer = c;
        this.sale = s;
        customerComboBox.setSelectedItem(this.selectedCustomer.getName());
        customerComboBox.setEnabled(false);
        customerAddButton.setEnabled(false);
        amountField.setValue(amount);
        amountField.setFocusable(false);
        inField.setValue(inPlot);
        inField.setFocusable(false);
        concerningField.setText("Referente à venda No. " + this.sale.getId());
        concerningField.setFocusable(false);
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        FillPlotTable();
    }

    public ReceptAccountAddDialog(java.awt.Frame parent, boolean modal, ReceptAccount r) {
        super(parent, modal);
        initComponents();
        Init();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        if (r != null) {
            isEditing = true;
            dateComboBox.setSelectedItem(sdf.format(r.getDate()));
            if (r.getCustomerId() != null) {
                selectedCustomer = r.getCustomerId();
                customerComboBox.setSelectedItem(selectedCustomer.getName());
            }
            payDayComboBox.setSelectedItem(sdf.format(r.getReceptPlotCollection().get(0).getDate()));
            plotSpinner.setValue(r.getPayPlotCount());
            amountField.setText(Utilities.doubleToMonetary(r.getTotal()));
            concerningField.setText(r.getConcerning());
            List<ReceptPlot> rp = r.getReceptPlotCollection();
            
            int initReceptPlot;
            if (r.hasInPlot()) {
                initReceptPlot = 1;
                inField.setText(Utilities.doubleToMonetary(r.getInPlot()));
            } else {
                initReceptPlot = 0;
            }

            for (int i = initReceptPlot; i < rp.size(); i++) {
                StringTokenizer st = new StringTokenizer(rp.get(i).getNumber(), "/");
                String plotNumber = null;
                while (st.hasMoreTokens()) {
                    plotNumber = st.nextToken();
                    break;
                }
                dtm.addRow(new Object[] {plotNumber, sdf.format(rp.get(i).getDate()),
                                            Utilities.doubleToMonetary(rp.get(i).getAmount())});
            }

        }
    }

    public ReceptAccountAddDialog(java.awt.Frame parent, boolean modal, Sale sale, Customer customer) {
        super(parent, modal);
        initComponents();
        Init();
        receptAccount = new ReceptAccount();
        if (sale != null) {
            concerningField.setText("Referente à Compra No: " + sale.getId().toString());
            concerningField.setEditable(false);
            //amountField.setValue(SaleFunctions.getInstance().getSaleTotal(sale.getId()));
            if (sale.getSaletypeId() != null) {
                if (sale.getSaletypeId().getId() == 2) {
                    inField.setEditable(false);
                }
            }
            receptAccount.setCustomerId(customer);
        }
        if (customer != null) {
            customerComboBox.setSelectedItem(customer.getName());
            customerComboBox.setEnabled(false);
            selectedCustomer = customer;
        }
        FillPlotTable();
    }

    private void Init() {
        FillCustomerComboBox();
        dtm =  (DefaultTableModel) plotTable.getModel();
        plotTable.setColumnSelectionAllowed(false);
        plotTable.setRowSelectionAllowed(false);
        dateComboBox.setSelectedItem(Utilities.dateToString(new Date()));
        payDayComboBox.setSelectedItem(CalendarOperations.SumMonths(new Date(), 1));
    }

    private void FillCustomerComboBox() {
        customers = cusfun.findCustomers();
        customerComboBox.removeAllItems();
        customerComboBox.addItem(String.valueOf(""));
        if (customers.size() > 0) {
            for (int i = 0; i < customers.size(); i++) {
                customerComboBox.addItem(customers.get(i).getName());
            }
        }
    }

    private void FillSupplierComboBox(Customer c) {
        customers = cusfun.findCustomers();
        customerComboBox.removeAllItems();
        customerComboBox.addItem(String.valueOf(""));
        if (customers.size() > 0) {
            for (int i = 0; i < customers.size(); i++) {
                customerComboBox.addItem(customers.get(i).getName());
            }
        }
        customerComboBox.setSelectedItem(c.getName());
    }

    private void FillPlotTable() {
        // Prestacoes = ao numero de prestacoes dividido por o valor menos a entrada
        Date payDay = Utilities.getFormatedDate((String) payDayComboBox.getSelectedItem());
        dtm.setRowCount(0);
        Double amount = Utilities.monetaryToDouble(amountField.getText());
        int numPlots = Integer.valueOf(String.valueOf(plotSpinner.getValue()));
        Double in = Utilities.monetaryToDouble(inField.getText());
        Double plotAmount = (amount - in) / numPlots;
        for (int i=1; i <= numPlots; i++) {
            int sum;
            if (i == 1)
                sum = 0;
            else
                sum = i - 1;
            Double balance = amountField.getValue() - inField.getValue();
            if (amountField.getValue() > 0.0 && balance != 0.0) {
                dtm.addRow(new Object[] {i, CalendarOperations.SumMonths(payDay, sum), Utilities.doubleToMonetary(plotAmount)});
            }
        }
        VerifyLastPlot();
    }

    private void VerifyLastPlot() {
        if (plotTable.getRowCount() > 0) {
            Double acumulated = 0.0;
            Double amount = Utilities.monetaryToDouble(amountField.getText()) - Utilities.monetaryToDouble(inField.getText());
            Double plot = Utilities.monetaryToDouble((String) plotTable.getValueAt(0, 2));

            for (int i = 0; i < plotTable.getRowCount(); i++) {
                acumulated += Utilities.monetaryToDouble((String) plotTable.getValueAt(i, 2));
            }

            if (acumulated != amount) {
                acumulated -= plot;
                plot = amount - acumulated;
            }
            
            plotTable.setValueAt(Utilities.doubleToMonetary(plot), plotTable.getRowCount() - 1 , 2);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        dateComboBox = new CalendarComboBox(true);
        jLabel2 = new javax.swing.JLabel();
        payDayComboBox = new CalendarComboBox(true);
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        plotTable = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        customerComboBox = new javax.swing.JComboBox();
        plotSpinner = new javax.swing.JSpinner();
        jLabel7 = new javax.swing.JLabel();
        concerningField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        amountField = new components.MonetaryJTextField(0.0);
        inField = new components.MonetaryJTextField(0.0);
        customerAddButton = new javax.swing.JButton();
        jToolBar1 = new javax.swing.JToolBar();
        saveButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Conta a Receber");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Data do Cadastro:");

        dateComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dateComboBoxKeyPressed(evt);
            }
        });

        jLabel2.setText("Prestações:");

        payDayComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                payDayComboBoxActionPerformed(evt);
            }
        });
        payDayComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                payDayComboBoxKeyPressed(evt);
            }
        });

        jLabel4.setText("Vencimento:");

        jLabel5.setText("Entrada:");

        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        plotTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Numero", "Vencimento", "Valor"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        plotTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(plotTable);
        plotTable.getColumnModel().getColumn(0).setResizable(false);
        plotTable.getColumnModel().getColumn(0).setPreferredWidth(80);
        plotTable.getColumnModel().getColumn(1).setResizable(false);
        plotTable.getColumnModel().getColumn(1).setPreferredWidth(120);
        plotTable.getColumnModel().getColumn(2).setResizable(false);
        plotTable.getColumnModel().getColumn(2).setPreferredWidth(120);

        jTabbedPane1.addTab("Visualizar Prestações", jScrollPane1);

        jLabel6.setText("Cliente:");

        customerComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customerComboBoxActionPerformed(evt);
            }
        });
        customerComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                customerComboBoxKeyPressed(evt);
            }
        });

        plotSpinner.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(1), Integer.valueOf(1), null, Integer.valueOf(1)));
        plotSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                plotSpinnerStateChanged(evt);
            }
        });
        plotSpinner.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                plotSpinnerKeyPressed(evt);
            }
        });

        jLabel7.setText("Referente à:");

        concerningField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                concerningFieldFocusGained(evt);
            }
        });
        concerningField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                concerningFieldKeyPressed(evt);
            }
        });

        jLabel3.setText("Valor:");

        amountField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                amountFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                amountFieldFocusLost(evt);
            }
        });
        amountField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                amountFieldKeyPressed(evt);
            }
        });

        inField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                inFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                inFieldFocusLost(evt);
            }
        });
        inField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                inFieldKeyPressed(evt);
            }
        });

        customerAddButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/user-add-16x16.png"))); // NOI18N
        customerAddButton.setToolTipText("Adicionar Fornecedor");
        customerAddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customerAddButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(concerningField, javax.swing.GroupLayout.DEFAULT_SIZE, 370, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(dateComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(customerComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(customerAddButton))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(payDayComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(plotSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(amountField, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(inField, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)))
                    .addComponent(jLabel7)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 370, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dateComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(customerComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(customerAddButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(payDayComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(plotSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(amountField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(inField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(concerningField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jToolBar1.setFloatable(false);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/accept-16x16.png"))); // NOI18N
        saveButton.setMnemonic('S');
        saveButton.setText("Salvar");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(saveButton);

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/remove-16x16.png"))); // NOI18N
        cancelButton.setMnemonic('C');
        cancelButton.setText("Cancelar");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(cancelButton);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        // TODO add your handling code here:
        this.dispose();
}//GEN-LAST:event_cancelButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        // TODO add your handling code here:
        if (amountField.getValue() <= 0.0) {
            JOptionPane.showMessageDialog(this, "Valor da Conta não pode ser zero.", "Mensagem", 2);
        } else if (dateComboBox.getSelectedItem().toString().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Data do cadastro inválida.", "Mensagem", 2);
        }else {
            receptPlots = new ArrayList<ReceptPlot>();

            if(inField.getValue() != 0.0) {
                ReceptPlot inPlot = new ReceptPlot();
                inPlot.setNumber("0");
                inPlot.setDate(Utilities.getFormatedDate(dateComboBox.getSelectedItem().toString()));
                inPlot.setAmount(inField.getValue());
                inPlot.setPenalty(0.0);
                inPlot.setInterestingrate(0.0);
                inPlot.setDiscount(0.0);
                receptPlots.add(inPlot);
            }

            for (int i = 0; i < plotTable.getRowCount(); i++) {
                ReceptPlot pp = new ReceptPlot();
                pp.setNumber(String.valueOf(i + 1));
                pp.setAmount(Utilities.monetaryToDouble((String) plotTable.getValueAt(i, 2)));
                pp.setDate(Utilities.getFormatedDate((String) plotTable.getValueAt(i, 1)));
                pp.setPenalty(0.0);
                pp.setInterestingrate(0.0);
                pp.setDiscount(0.0);
                receptPlots.add(pp);
            }

            if (receptAccount == null)
                receptAccount = new ReceptAccount();
            if (selectedCustomer != null)
                receptAccount.setCustomerId(selectedCustomer);
            
            receptAccount.setDate(Utilities.getFormatedDate((String) dateComboBox.getSelectedItem()));
            receptAccount.setConcerning(concerningField.getText());

            if (this.sale != null)
                receptAccount.setSaleId(this.sale);

            if (ReceptAccountFunctions.getInstance().create(receptAccount, receptPlots)) {
                this.dispose();
            } else {
                
            }
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void plotSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_plotSpinnerStateChanged
        // TODO add your handling code here:
        FillPlotTable();
    }//GEN-LAST:event_plotSpinnerStateChanged

    private void payDayComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_payDayComboBoxActionPerformed
        // TODO add your handling code here:
        FillPlotTable();
    }//GEN-LAST:event_payDayComboBoxActionPerformed

    private void amountFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_amountFieldFocusLost
        // TODO add your handling code here:
        FillPlotTable();
}//GEN-LAST:event_amountFieldFocusLost

    private void inFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_inFieldFocusLost
        // TODO add your handling code here:
        if (inField.getValue() > amountField.getValue()) {
            JOptionPane.showMessageDialog(this, "Este valor não pode ser maior que o valor da conta.", "Mensagem", 2);
            inField.setValue(0.0);
            inField.requestFocusInWindow();
        }
        FillPlotTable();
}//GEN-LAST:event_inFieldFocusLost

    private void customerComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customerComboBoxActionPerformed
        // TODO add your handling code here:
        if (customerComboBox.getSelectedIndex() > 0 ) {
            selectedCustomer = cusfun.findCustomerByName(String.valueOf(customerComboBox.getSelectedItem()));
        } else if (customerComboBox.getSelectedIndex() == 0) {
            selectedCustomer = null;
        }
}//GEN-LAST:event_customerComboBoxActionPerformed

    private void customerAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customerAddButtonActionPerformed
        // TODO add your handling code here:
        CustomerAddEditDialog customerDialog = new CustomerAddEditDialog(null, true);
        customerDialog.setLocationRelativeTo(null);
        customerDialog.setVisible(true);
        if (customerDialog.getCustomer() != null) {
            selectedCustomer = customerDialog.getCustomer();
            FillSupplierComboBox(selectedCustomer);
        }
}//GEN-LAST:event_customerAddButtonActionPerformed

    private void dateComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dateComboBoxKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            customerComboBox.requestFocusInWindow();
            //customerComboBox.selectAll();
        }
    }//GEN-LAST:event_dateComboBoxKeyPressed

    private void customerComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_customerComboBoxKeyPressed
        // TODO add your handling code here:if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
         if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            customerAddButton.requestFocusInWindow();
            //customerComboBox.selectAll();
        }

    }//GEN-LAST:event_customerComboBoxKeyPressed

    private void payDayComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_payDayComboBoxKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            plotSpinner.requestFocusInWindow();
            //customerComboBox.selectAll();
        }
    }//GEN-LAST:event_payDayComboBoxKeyPressed

    private void plotSpinnerKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_plotSpinnerKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            amountField.requestFocusInWindow();
            amountField.selectAll();
        }
    }//GEN-LAST:event_plotSpinnerKeyPressed

    private void amountFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_amountFieldKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            inField.requestFocusInWindow();
            inField.selectAll();
        }
    }//GEN-LAST:event_amountFieldKeyPressed

    private void inFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_inFieldKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            concerningField.requestFocusInWindow();
            concerningField.selectAll();
        }
    }//GEN-LAST:event_inFieldKeyPressed

    private void concerningFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_concerningFieldKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            saveButton.requestFocusInWindow();
            //customerComboBox.selectAll();
        }
    }//GEN-LAST:event_concerningFieldKeyPressed

    private void amountFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_amountFieldFocusGained
        // TODO add your handling code here:
        amountField.selectAll();
    }//GEN-LAST:event_amountFieldFocusGained

    private void inFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_inFieldFocusGained
        // TODO add your handling code here:
        inField.selectAll();
    }//GEN-LAST:event_inFieldFocusGained

    private void concerningFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_concerningFieldFocusGained
        // TODO add your handling code here:
        concerningField.selectAll();
    }//GEN-LAST:event_concerningFieldFocusGained

    /**
    * @param args the command line arguments
    */


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private components.MonetaryJTextField amountField;
    private javax.swing.JButton cancelButton;
    private javax.swing.JTextField concerningField;
    private javax.swing.JButton customerAddButton;
    private javax.swing.JComboBox customerComboBox;
    private javax.swing.JComboBox dateComboBox;
    private components.MonetaryJTextField inField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JComboBox payDayComboBox;
    private javax.swing.JSpinner plotSpinner;
    private javax.swing.JTable plotTable;
    private javax.swing.JButton saveButton;
    // End of variables declaration//GEN-END:variables

}
