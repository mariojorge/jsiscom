/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * PayAccountViewDialog.java
 *
 * Created on 10/05/2009, 21:17:43
 */

package frames.ReceptAccount;

import components.CalendarComboBox;
import comsql.Customer;
import util.Utilities;
import comsql.ReceptAccount;
import comsql.ReceptAccountFunctions;
import comsql.ReceptPlot;
import comsql.ReceptedPlot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author usuario
 */
public class ReceptAccountViewDialog extends javax.swing.JDialog {
    DefaultTableModel dtmReceptAccount;
    Double total, toReceptTotal, receptedTotal;
    Boolean isFiltered = false;
    List<ReceptAccount> receptAccounts;

    /** Creates new form PayAccountViewDialog */
    public ReceptAccountViewDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        Init();
        receptAccounts = new ArrayList<ReceptAccount>();
    }

    public ReceptAccountViewDialog(java.awt.Frame parent, boolean modal, Customer customer, int type) {
        super(parent, modal);
        initComponents();
        Init();
        if (type == 0) {
            isToReceptRadioButton.setSelected(true);
            isReceptedRadioButton.setEnabled(false);
        } else if (type == 1) {
            isReceptedRadioButton.setSelected(true);
            isToReceptRadioButton.setEnabled(false);
        }
        receptAccounts = new ArrayList<ReceptAccount>();
        filterValueField.setText(customer.getName());
        this.setTitle("Contas a Receber - " + customer.getName());
        filterComboBox.setSelectedIndex(1);
        filterButton.doClick();
        filterValueField.setEditable(false);
        filterComboBox.setEditable(false);
        filterButton.setEnabled(false);
        cleanFilterButton.setEnabled(false);
    }

    private void Init() {
        dtmReceptAccount =  (DefaultTableModel) receptAccountTable.getModel();
        receptAccountTable.setColumnSelectionAllowed(false);
        receptAccountTable.setRowSelectionAllowed(true);
        receptAccountTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        FillReceptAccountTable(ReceptAccountFunctions.getInstance().findReceptAccounts());
        filterComboBox.setSelectedIndex(1);
    }

    private void FillReceptAccountTable(List list) {
        dtmReceptAccount.setRowCount(0);
        total = 0.0;
        toReceptTotal = 0.0;
        receptedTotal = 0.0;
        List<ReceptAccount> accounts = null;

        if (list != null)
            accounts = list;

        if (accounts != null) {
            for (int i=0; i<accounts.size(); i++) {
                int id = accounts.get(i).getId();
                String concerning = accounts.get(i).getConcerning();
                String addDate = Utilities.dateToString(accounts.get(i).getDate());
                String strOriginTotal = Utilities.doubleToMonetary(accounts.get(i).getOriginTotal());
                String strPayedTotal = Utilities.doubleToMonetary(accounts.get(i).getPayedTotal());
                Double balance = accounts.get(i).getBalance();
                String balanceStr = Utilities.doubleToMonetary(accounts.get(i).getBalance());
                String customer = null;
                if (accounts.get(i).getCustomerId() != null)
                    customer = accounts.get(i).getCustomerId().getName();
                if (isToReceptRadioButton.isSelected()) {
                    if (balance != 0.0) {
                        total += accounts.get(i).getOriginTotal();
                        receptedTotal += accounts.get(i).getPayedTotal();
                        toReceptTotal += balance;
                        dtmReceptAccount.addRow(new Object[] {id, addDate, concerning, strOriginTotal, strPayedTotal, balanceStr, customer});
                    }
                }
                if (isReceptedRadioButton.isSelected()) {
                    if (balance <= 0.0) {
                        total += accounts.get(i).getOriginTotal();
                        receptedTotal += accounts.get(i).getPayedTotal();
                        toReceptTotal += balance;
                        dtmReceptAccount.addRow(new Object[] {id, addDate, concerning, strOriginTotal, strPayedTotal, balanceStr, customer});
                    }
                }
                totalField.setValue(total);
                toReceptTotalField.setValue(toReceptTotal);
                receptedTotalField.setValue(receptedTotal);
            }
        }
    }

    private void doFilter() {
        receptAccounts = null;
        if (!filterValueField.getText().isEmpty()) {
            if (filterComboBox.getSelectedIndex() == 0) {
                int id = 0;
                String sId = filterValueField.getText();
                try {
                    id = Integer.valueOf(sId);
                    receptAccounts = new ArrayList<ReceptAccount>();
                    receptAccounts.add(ReceptAccountFunctions.getInstance().getReceptAccount(id));
                    FillReceptAccountTable(receptAccounts);
                } catch (Exception e) {
                    FillReceptAccountTable(null);
                }
            } else if (filterComboBox.getSelectedIndex() == 1) {
                String s = filterValueField.getText();
                if (initialDateComboBox.getSelectedItem() != null && finalDateComboBox.getSelectedItem() != null &&
                        !initialDateComboBox.getSelectedItem().toString().isEmpty() && !finalDateComboBox.getSelectedItem().toString().isEmpty()) {
                    Date initialDate = Utilities.getFormatedDate((String) initialDateComboBox.getSelectedItem());
                    Date finalDate = Utilities.getFormatedDate((String) finalDateComboBox.getSelectedItem());
                    receptAccounts = ReceptAccountFunctions.getInstance().findReceptAccountsByCustomerName(s, initialDate, finalDate);
                    FillReceptAccountTable(receptAccounts);
                } else {
                    if (!filterValueField.getText().isEmpty()) {
                        receptAccounts = ReceptAccountFunctions.getInstance().findReceptAccountsByCustomerName(s);
                        FillReceptAccountTable(receptAccounts);
                    }
                    else
                        FillReceptAccountTable(null);
                }
           } else if (filterComboBox.getSelectedIndex() == 3) {
                String s = filterValueField.getText();
                receptAccounts = ReceptAccountFunctions.getInstance().findReceptAccountsByConcerning(s);
                FillReceptAccountTable(receptAccounts);
            }
            isFiltered = true;
        } else {
            FillReceptAccountTable(null);
        }

        if (filterComboBox.getSelectedIndex() == 2) {
           if (initialDateComboBox.getSelectedItem() != null && finalDateComboBox.getSelectedItem() != null &&
               !initialDateComboBox.getSelectedItem().toString().isEmpty() && !finalDateComboBox.getSelectedItem().toString().isEmpty()) {
               Date initialDate = Utilities.getFormatedDate((String) initialDateComboBox.getSelectedItem());
               Date finalDate = Utilities.getFormatedDate((String) finalDateComboBox.getSelectedItem());
               receptAccounts = ReceptAccountFunctions.getInstance().findReceptAccountsByPeriod(initialDate, finalDate);
               FillReceptAccountTable(receptAccounts);
            }  else {
                FillReceptAccountTable(null);
            }
        }
    }

    private void OpenDetails(int receptAccountId) {
        ReceptAccountDetailsDialog detailsDialog = new ReceptAccountDetailsDialog(null, true, ReceptAccountFunctions.getInstance().getReceptAccount(receptAccountId));
        detailsDialog.setLocationRelativeTo(null);
        detailsDialog.setVisible(true);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jScrollPane1 = new javax.swing.JScrollPane();
        receptAccountTable = new javax.swing.JTable();
        totalField = new components.MonetaryJTextField(0.0);
        jLabel5 = new javax.swing.JLabel();
        receptedTotalField = new components.MonetaryJTextField(0.0);
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        toReceptTotalField = new components.MonetaryJTextField(0.0);
        jToolBar1 = new javax.swing.JToolBar();
        addButton = new javax.swing.JButton();
        deletePayAccountButton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        isToReceptRadioButton = new javax.swing.JRadioButton();
        isReceptedRadioButton = new javax.swing.JRadioButton();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        initialDateComboBox = new CalendarComboBox(false);
        finalDateComboBox = new CalendarComboBox(false);
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        filterValueField = new javax.swing.JTextField();
        filterComboBox = new javax.swing.JComboBox();
        filterButton = new javax.swing.JButton();
        cleanFilterButton = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Contas a Receber");
        setResizable(false);

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        receptAccountTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Data", "Referente a", "Total Original", "Valor Pago", "Valor a Pagar", "Cliente"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        receptAccountTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        receptAccountTable.setRowHeight(20);
        receptAccountTable.getTableHeader().setReorderingAllowed(false);
        receptAccountTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                receptAccountTableMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                receptAccountTableMouseReleased(evt);
            }
        });
        receptAccountTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                receptAccountTableKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(receptAccountTable);
        receptAccountTable.getColumnModel().getColumn(0).setPreferredWidth(70);
        receptAccountTable.getColumnModel().getColumn(1).setPreferredWidth(70);
        receptAccountTable.getColumnModel().getColumn(2).setPreferredWidth(200);
        receptAccountTable.getColumnModel().getColumn(3).setPreferredWidth(90);
        receptAccountTable.getColumnModel().getColumn(4).setPreferredWidth(85);
        receptAccountTable.getColumnModel().getColumn(5).setPreferredWidth(85);
        receptAccountTable.getColumnModel().getColumn(6).setPreferredWidth(180);

        totalField.setFocusable(false);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel5.setText("Total Geral:");

        receptedTotalField.setFocusable(false);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel6.setText("Total Recebido:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel7.setText("Total a Receber:");

        toReceptTotalField.setFocusable(false);

        jToolBar1.setFloatable(false);

        addButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/add-16x16.png"))); // NOI18N
        addButton.setMnemonic('A');
        addButton.setText("Adicionar");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(addButton);

        deletePayAccountButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/remove-16x16.png"))); // NOI18N
        deletePayAccountButton.setMnemonic('x');
        deletePayAccountButton.setText("Excluir");
        deletePayAccountButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deletePayAccountButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(deletePayAccountButton);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Money-Calculator-16x16.png"))); // NOI18N
        jButton1.setMnemonic('D');
        jButton1.setText("Detalhes");
        jButton1.setFocusable(false);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/window-remove-16x16.png"))); // NOI18N
        jButton2.setMnemonic('F');
        jButton2.setText("Fechar");
        jButton2.setFocusable(false);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton2);

        buttonGroup1.add(isToReceptRadioButton);
        isToReceptRadioButton.setMnemonic('o');
        isToReceptRadioButton.setSelected(true);
        isToReceptRadioButton.setText("Contas a Receber");
        isToReceptRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                isToReceptRadioButtonActionPerformed(evt);
            }
        });

        buttonGroup1.add(isReceptedRadioButton);
        isReceptedRadioButton.setMnemonic('n');
        isReceptedRadioButton.setText("Contas Recebidas");
        isReceptedRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                isReceptedRadioButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(isToReceptRadioButton)
                    .addComponent(isReceptedRadioButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(isToReceptRadioButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(isReceptedRadioButton)
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Exibir", jPanel3);

        jLabel1.setText("Campo da Tabela:");

        jLabel2.setText("Data Inicial:");

        initialDateComboBox.setEnabled(false);
        initialDateComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                initialDateComboBoxKeyPressed(evt);
            }
        });

        finalDateComboBox.setEnabled(false);
        finalDateComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                finalDateComboBoxKeyPressed(evt);
            }
        });

        jLabel3.setText("Data Final:");

        jLabel4.setText("Valor à pesquisar:");

        filterValueField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filterValueFieldActionPerformed(evt);
            }
        });
        filterValueField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                filterValueFieldKeyPressed(evt);
            }
        });

        filterComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Código", "Cliente", "Período", "Referente a" }));
        filterComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filterComboBoxActionPerformed(evt);
            }
        });

        filterButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/search_16x16.png"))); // NOI18N
        filterButton.setMnemonic('l');
        filterButton.setText("Filtrar");
        filterButton.setToolTipText("Filtrar");
        filterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filterButtonActionPerformed(evt);
            }
        });

        cleanFilterButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/search_remover_16x16.png"))); // NOI18N
        cleanFilterButton.setMnemonic('v');
        cleanFilterButton.setText("Remover");
        cleanFilterButton.setToolTipText("Remover Filtro");
        cleanFilterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cleanFilterButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(filterComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(filterValueField, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(initialDateComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(finalDateComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(filterButton)
                        .addGap(6, 6, 6)
                        .addComponent(cleanFilterButton))
                    .addComponent(jLabel3))
                .addGap(73, 73, 73))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(filterValueField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cleanFilterButton)
                                .addComponent(filterButton))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(initialDateComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(finalDateComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addComponent(jLabel4)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addComponent(jLabel1)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(filterComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(32, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Filtrar", jPanel4);

        jMenu1.setMnemonic('C');
        jMenu1.setText("Cadastro");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/add-18x18.png"))); // NOI18N
        jMenuItem1.setMnemonic('A');
        jMenuItem1.setText("Adicionar Conta");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        jMenu2.setMnemonic('R');
        jMenu2.setText("Relatórios");

        jMenuItem4.setText("Carnê");
        jMenu2.add(jMenuItem4);

        jMenuItem5.setText("Contrato");
        jMenu2.add(jMenuItem5);

        jMenuItem2.setText("Contas a Receber");
        jMenu2.add(jMenuItem2);

        jMenuItem3.setText("Contas Recebidas");
        jMenu2.add(jMenuItem3);

        jMenuItem6.setText("Recibo");
        jMenu2.add(jMenuItem6);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 770, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 616, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(totalField, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(receptedTotalField, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(toReceptTotalField, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(240, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 750, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(toReceptTotalField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6)
                    .addComponent(receptedTotalField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(totalField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void deletePayAccountButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deletePayAccountButtonActionPerformed
        // TODO add your handling code here:
        if (receptAccountTable.getSelectedRowCount() > 0) {
            int option = JOptionPane.showConfirmDialog(this, "Tem certeza?", "Confirmação", 0);
            if (option == 0) {
                int id = (Integer) receptAccountTable.getValueAt(receptAccountTable.getSelectedRow(), 0);
                Boolean isDeleted = ReceptAccountFunctions.getInstance().delete(id);
                if (isDeleted) {
                    JOptionPane.showMessageDialog(this, "Operação concluida com sucesso.", "Mensagem", 1);
                    if (isFiltered)
                        doFilter();
                    else
                        FillReceptAccountTable(ReceptAccountFunctions.getInstance().findReceptAccounts());
                }
            }
        }
}//GEN-LAST:event_deletePayAccountButtonActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        addButton.doClick();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void isToReceptRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_isToReceptRadioButtonActionPerformed
        // TODO add your handling code here:
        if (isFiltered)
            doFilter();
        else
            FillReceptAccountTable(ReceptAccountFunctions.getInstance().findReceptAccounts());
}//GEN-LAST:event_isToReceptRadioButtonActionPerformed

    private void isReceptedRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_isReceptedRadioButtonActionPerformed
        // TODO add your handling code here:
        if (isFiltered)
            doFilter();
        else
            FillReceptAccountTable(ReceptAccountFunctions.getInstance().findReceptAccounts());
}//GEN-LAST:event_isReceptedRadioButtonActionPerformed

    private void receptAccountTableKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_receptAccountTableKeyReleased
        // TODO add your handling code here:
}//GEN-LAST:event_receptAccountTableKeyReleased

    private void filterComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filterComboBoxActionPerformed
        // TODO add your handling code here:
        if (filterComboBox.getSelectedItem().toString().compareTo("Período") == 0) {
            initialDateComboBox.setEnabled(true);
            finalDateComboBox.setEnabled(true);
            filterValueField.setEnabled(false);
            initialDateComboBox.requestFocusInWindow();
        } else if (filterComboBox.getSelectedItem().toString().compareTo("Fornecedor") == 0){
            initialDateComboBox.setEnabled(true);
            finalDateComboBox.setEnabled(true);
            filterValueField.setEnabled(true);
            filterValueField.requestFocusInWindow();
            filterValueField.selectAll();
        } else {
            initialDateComboBox.setEnabled(false);
            finalDateComboBox.setEnabled(false);
            filterValueField.setEnabled(true);
            filterValueField.requestFocusInWindow();
            filterValueField.selectAll();
        }
    }//GEN-LAST:event_filterComboBoxActionPerformed

    private void filterValueFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filterValueFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_filterValueFieldActionPerformed

    private void filterValueFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_filterValueFieldKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (initialDateComboBox.isEnabled())
                initialDateComboBox.requestFocusInWindow();
            else
                filterButton.requestFocusInWindow();
        }
    }//GEN-LAST:event_filterValueFieldKeyPressed

    private void filterButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filterButtonActionPerformed
        // TODO add your handling code here:
        doFilter();
    }//GEN-LAST:event_filterButtonActionPerformed

    private void cleanFilterButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cleanFilterButtonActionPerformed
        // TODO add your handling code here:
        filterValueField.setText("");
        initialDateComboBox.setSelectedItem("");
        initialDateComboBox.setSelectedItem(null);
        finalDateComboBox.setSelectedItem("");
        finalDateComboBox.setSelectedItem(null);
        filterValueField.requestFocusInWindow();
        isFiltered = false;
        FillReceptAccountTable(ReceptAccountFunctions.getInstance().findReceptAccounts());
}//GEN-LAST:event_cleanFilterButtonActionPerformed

    private void initialDateComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_initialDateComboBoxKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            finalDateComboBox.requestFocusInWindow();
        }
    }//GEN-LAST:event_initialDateComboBoxKeyPressed

    private void finalDateComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_finalDateComboBoxKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            filterButton.requestFocusInWindow();
        }
    }//GEN-LAST:event_finalDateComboBoxKeyPressed

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        // TODO add your handling code here:
        ReceptAccountAddDialog sform = new ReceptAccountAddDialog(null, true);
        sform.setLocationRelativeTo(null);
        sform.setVisible(true);
        if (isFiltered)
            doFilter();
        else
            FillReceptAccountTable(ReceptAccountFunctions.getInstance().findReceptAccounts());
}//GEN-LAST:event_addButtonActionPerformed

    private void receptAccountTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_receptAccountTableMouseReleased
        // TODO add your handling code here:
}//GEN-LAST:event_receptAccountTableMouseReleased

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        if (receptAccountTable.getSelectedRowCount() > 0) {
            int id = (Integer) receptAccountTable.getValueAt(receptAccountTable.getSelectedRow(), 0);
            OpenDetails(id);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void receptAccountTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_receptAccountTableMouseClicked
        // TODO add your handling code here:
        if (evt.getClickCount() >= 2) {
            if (receptAccountTable.getSelectedRowCount() > 0) {
                int id = (Integer) receptAccountTable.getValueAt(receptAccountTable.getSelectedRow(), 0);
                OpenDetails(id);
            }
        }
    }//GEN-LAST:event_receptAccountTableMouseClicked

    /**
    * @param args the command line arguments
    */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton cleanFilterButton;
    private javax.swing.JButton deletePayAccountButton;
    private javax.swing.JButton filterButton;
    private javax.swing.JComboBox filterComboBox;
    private javax.swing.JTextField filterValueField;
    private javax.swing.JComboBox finalDateComboBox;
    private javax.swing.JComboBox initialDateComboBox;
    private javax.swing.JRadioButton isReceptedRadioButton;
    private javax.swing.JRadioButton isToReceptRadioButton;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTable receptAccountTable;
    private components.MonetaryJTextField receptedTotalField;
    private components.MonetaryJTextField toReceptTotalField;
    private components.MonetaryJTextField totalField;
    // End of variables declaration//GEN-END:variables

}
