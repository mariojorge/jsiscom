/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ReceptAccountDetailsDialog.java
 *
 * Created on 21/06/2009, 08:36:35
 */

package frames.PayAccount;

import comsql.PayAccount;
import comsql.PayPlot;
import comsql.PayedPlot;
import comsql.Purchase;
import comsql.PurchaseProduct;
import java.util.List;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import util.Utilities;

/**
 *
 * @author usuario
 */
public class PayAccountDetailsDialog extends javax.swing.JDialog {
    DefaultTableModel dtmPayPlot;
    DefaultTableModel dtmPayedPlot;
    DefaultTableModel dtmProduct;
    PayAccount payAccount;
    Double penalty = 0.0;
    Double interestingRate = 0.0;
    Double discount = 0.0;

    /** Creates new form ReceptAccountDetailsDialog */
    public PayAccountDetailsDialog(java.awt.Frame parent, boolean modal, PayAccount pa) {
        super(parent, modal);
        initComponents();
        Init();
        this.payAccount = pa;
        FillPayPlotTable(this.payAccount.getPayPlotCollection());
        FillPayedPlotTable(this.payAccount.getPayedPlotCollection());
        FillForm();
        FillProductTable(this.payAccount.getPurchaseId());
    }

    private void Init() {
        dtmPayPlot = (DefaultTableModel) receptPlotTable.getModel();
        receptPlotTable.setColumnSelectionAllowed(false);
        receptPlotTable.setRowSelectionAllowed(false);
        receptPlotTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        dtmPayedPlot = (DefaultTableModel) receptedPlotTable.getModel();
        receptedPlotTable.setColumnSelectionAllowed(false);
        receptedPlotTable.setRowSelectionAllowed(false);
        receptedPlotTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        dtmProduct = (DefaultTableModel) productTable.getModel();
        productTable.setColumnSelectionAllowed(false);
        productTable.setRowSelectionAllowed(false);
        productTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    private void FillForm() {
        try {
            supplierName.setText(this.payAccount.getSupplierId().getName().toUpperCase());
        } catch (NullPointerException e) {
            supplierName.setText("");
        }
        plotsOriginTotalField.setValue(this.payAccount.getOriginTotal());
        balancePlotsTotalField.setValue(this.payAccount.getBalance());
        payedPlotsTotalField.setValue(this.payAccount.getPayedTotal());
        penaltyField.setValue(this.penalty);
        interestinRateField.setValue(this.interestingRate);
        discountField.setValue(this.discount);
        concerningLabel.setText(this.payAccount.getConcerning());
    }

    private void FillPayPlotTable(List<PayPlot> payPlots) {
        dtmPayPlot.setRowCount(0);
        int receptPlotCount = 0;
        if (payPlots != null) {
            for(int i=0; i <payPlots.size(); i++) {
                PayPlot rp = payPlots.get(i);
                String plotNumber = rp.getNumber();
                if (rp.getNumber().compareTo("0") == 0) {
                    plotNumber = "Entrada";
                } else {
                    receptPlotCount++;
                }
                String isPayed;
                if (rp.isPayed())
                    isPayed = "Sim";
                else
                    isPayed = "Não";
                this.penalty += rp.getPenalty();
                this.interestingRate += rp.getInterestingrate();
                this.discount += rp.getDiscount();
                dtmPayPlot.addRow(new Object[] {plotNumber, Utilities.dateToString(rp.getDate()), Utilities.doubleToMonetary(rp.getAmount()), Utilities.doubleToMonetary(rp.getTotal()), isPayed
                                    , Utilities.doubleToMonetary(rp.getInterestingrate()), Utilities.doubleToMonetary(rp.getPenalty()), Utilities.doubleToMonetary(rp.getDiscount())});
            }
            if (receptPlotCount == 0)
                receptPlotCountLabel.setText("Nenhuma parcela");
            else if (receptPlotCount == 1)
                receptPlotCountLabel.setText("1 parcela");
            else
                receptPlotCountLabel.setText(String.valueOf(receptPlotCount) + " parcelas");
        }
    }

    private void FillPayedPlotTable(List<PayedPlot> payedPlots) {
        dtmPayedPlot.setRowCount(0);
        if (payedPlots != null) {
            for(int i=0; i <payedPlots.size(); i++) {
                PayedPlot rp = payedPlots.get(i);
                String receivingType = rp.getReceivingtypeId().getDescription();
                String plotNumber = rp.getNumber();
                if (rp.getNumber().compareTo("0") == 0)
                    plotNumber = "Entrada";
                dtmPayedPlot.addRow(new Object[] {Utilities.dateToString(rp.getDate()), plotNumber,Utilities.doubleToMonetary(rp.getAmount()), receivingType});
            }
        }
    }

    private void FillProductTable(Purchase p) {
        Purchase purchase = null;
        try {
            purchase = p;
        } catch (NullPointerException e) {

        }
        if (purchase != null) {
            List<PurchaseProduct> products = purchase.getPurchaseProductCollection();
            dtmProduct.setRowCount(0);
            if (products != null) {
                for(int i=0; i < products.size(); i++) {
                    dtmProduct.addRow(new Object[] {products.get(i).getProduct().getName(), products.get(i).getQuantity(),
                        Utilities.doubleToMonetary(products.get(i).getPrice())});
                }
            }
        } else {
            dtmProduct.setRowCount(0);
        }
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        receptPlotTable = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        receptedPlotTable = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        productTable = new javax.swing.JTable();
        receptPlotCountLabel1 = new javax.swing.JLabel();
        receptPlotCountLabel = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        plotsOriginTotalField = new components.MonetaryJTextField(0.0);
        jLabel9 = new javax.swing.JLabel();
        balancePlotsTotalField = new components.MonetaryJTextField(0.0);
        interestinRateField = new components.MonetaryJTextField(0.0);
        jLabel12 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        penaltyField = new components.MonetaryJTextField(0.0);
        discountField = new components.MonetaryJTextField(0.0);
        jLabel14 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        supplierName = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        payedPlotsTotalField = new components.MonetaryJTextField(0.0);
        jLabel2 = new javax.swing.JLabel();
        concerningLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Detalhes");
        setResizable(false);

        jScrollPane2.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        receptPlotTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Parcela", "Vencimento", "Valor Original", "Valor Atual", "Pago", "Juros", "Multa", "Desconto"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        receptPlotTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        receptPlotTable.setRowHeight(20);
        receptPlotTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(receptPlotTable);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 621, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane2.addTab("Parcelas", jPanel1);

        jScrollPane3.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        receptedPlotTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Data", "Parcela", "Valor", "Forma"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        receptedPlotTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        receptedPlotTable.setRowHeight(20);
        jScrollPane3.setViewportView(receptedPlotTable);
        receptedPlotTable.getColumnModel().getColumn(2).setPreferredWidth(130);
        receptedPlotTable.getColumnModel().getColumn(3).setPreferredWidth(250);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 621, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane2.addTab("Pagamentos", jPanel2);

        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        productTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Nome", "Quantidade", "Preço"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        productTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        productTable.setRowHeight(20);
        jScrollPane1.setViewportView(productTable);
        productTable.getColumnModel().getColumn(0).setPreferredWidth(200);
        productTable.getColumnModel().getColumn(1).setPreferredWidth(80);
        productTable.getColumnModel().getColumn(2).setPreferredWidth(110);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 621, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane2.addTab("Produtos", jPanel3);

        receptPlotCountLabel1.setFont(new java.awt.Font("Tahoma", 1, 11));
        receptPlotCountLabel1.setText("Quantidade de Parcelas: ");

        receptPlotCountLabel.setText("02 parcelas");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel10.setText("Total:");

        plotsOriginTotalField.setFocusable(false);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel9.setText("A Receber:");

        balancePlotsTotalField.setFocusable(false);

        interestinRateField.setFocusable(false);

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel12.setText("Juros:");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel11.setText("Multa:");

        penaltyField.setFocusable(false);

        discountField.setFocusable(false);

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel14.setText("Desconto:");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("Fornecedor:");

        supplierName.setText("nome do cliente");

        jToolBar1.setFloatable(false);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/window-remove-16x16.png"))); // NOI18N
        jButton1.setMnemonic('F');
        jButton1.setText("Fechar");
        jButton1.setFocusable(false);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel8.setText("Total Recebido:");

        payedPlotsTotalField.setFocusable(false);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel2.setText("Referente a:");

        concerningLabel.setText("referente a");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 646, Short.MAX_VALUE)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 646, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(receptPlotCountLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9)
                            .addComponent(jLabel8)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(concerningLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 501, Short.MAX_VALUE)
                            .addComponent(supplierName, javax.swing.GroupLayout.DEFAULT_SIZE, 501, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(receptPlotCountLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 305, Short.MAX_VALUE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(payedPlotsTotalField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(balancePlotsTotalField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(plotsOriginTotalField, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE))
                                        .addGap(28, 28, 28)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel12)
                                                    .addComponent(jLabel11))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 76, Short.MAX_VALUE)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(penaltyField, javax.swing.GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)
                                                    .addComponent(interestinRateField, javax.swing.GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addComponent(jLabel14)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 56, Short.MAX_VALUE)
                                                .addComponent(discountField, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addGap(196, 196, 196)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(supplierName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(receptPlotCountLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(receptPlotCountLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(plotsOriginTotalField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(balancePlotsTotalField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9)
                            .addComponent(jLabel11)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(interestinRateField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(penaltyField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(discountField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(payedPlotsTotalField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel14)
                        .addComponent(jLabel8)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(concerningLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
    * @param args the command line arguments
    */


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private components.MonetaryJTextField balancePlotsTotalField;
    private javax.swing.JLabel concerningLabel;
    private components.MonetaryJTextField discountField;
    private components.MonetaryJTextField interestinRateField;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JToolBar jToolBar1;
    private components.MonetaryJTextField payedPlotsTotalField;
    private components.MonetaryJTextField penaltyField;
    private components.MonetaryJTextField plotsOriginTotalField;
    private javax.swing.JTable productTable;
    private javax.swing.JLabel receptPlotCountLabel;
    private javax.swing.JLabel receptPlotCountLabel1;
    private javax.swing.JTable receptPlotTable;
    private javax.swing.JTable receptedPlotTable;
    private javax.swing.JLabel supplierName;
    // End of variables declaration//GEN-END:variables

}
