/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * PayAccountAddDialog.java
 *
 * Created on 29/04/2009, 09:03:34
 */

package frames.PayAccount;

import components.CalendarComboBox;
import util.Utilities;
import util.CalendarOperations;
import comsql.PayAccount;
import comsql.PayAccountFunctions;
import comsql.PayPlot;
import comsql.Purchase;
import comsql.PurchaseFunctions;
import comsql.Supplier;
import comsql.SupplierFunctions;
import frames.Supplier.SupplierAddEditDialog;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author usuario
 */
public class PayAccountAddDialog extends javax.swing.JDialog {
    SupplierFunctions sf = SupplierFunctions.getInstance();
    PurchaseFunctions purf = PurchaseFunctions.getInstance();
    List<Supplier> suppliers;
    DefaultTableModel dtm;
    List<PayPlot> payPlots;
    Supplier selectedSupplier;
    PayAccount payAccount;
    Boolean isEditing = false;

    /** Creates new form PayAccountAddDialog */
    public PayAccountAddDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        Init();
    }

    public PayAccountAddDialog(java.awt.Frame parent, boolean modal, PayAccount p) {
        super(parent, modal);
        initComponents();
        Init();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        if (p != null) {
            isEditing = true;
            dateComboBox.setSelectedItem(sdf.format(p.getDate()));
            if (p.getSupplierId() != null) {
                selectedSupplier = p.getSupplierId();
                supplierComboBox.setSelectedItem(selectedSupplier.getName());
            }
            payDayComboBox.setSelectedItem(sdf.format(p.getPayPlotCollection().get(0).getDate()));
            plotSpinner.setValue(p.getPayPlotCount());
            amountField.setText(Utilities.doubleToMonetary(p.getTotal()));
            concerningField.setText(p.getConcerning());
            List<PayPlot> pp = p.getPayPlotCollection();
            
            int initPayPlot;
            if (p.hasInPlot()) {
                initPayPlot = 1;
                inField.setText(Utilities.doubleToMonetary(p.getInPlot()));
            } else {
                initPayPlot = 0;
            }

            for (int i = initPayPlot; i < pp.size(); i++) {
                StringTokenizer st = new StringTokenizer(pp.get(i).getNumber(), "/");
                String plotNumber = null;
                while (st.hasMoreTokens()) {
                    plotNumber = st.nextToken();
                    break;
                }
                dtm.addRow(new Object[] {plotNumber, sdf.format(pp.get(i).getDate()),
                                            Utilities.doubleToMonetary(pp.get(i).getAmount())});
            }

        }
    }

    public PayAccountAddDialog(java.awt.Frame parent, boolean modal, Purchase p, Supplier s) {
        super(parent, modal);
        initComponents();
        Init();
        payAccount = new PayAccount();
        if (p != null) {
            concerningField.setText("Referente à Compra No: " + p.getId().toString());
            concerningField.setEditable(false);
            amountField.setValue(PurchaseFunctions.getInstance().getPurchaseTotal(p.getId()));
            if (p.getPurchasetypeId() != null) {
                if (p.getPurchasetypeId().getId() == 2) {
                    inField.setEditable(false);
                }
            }
            payAccount.setPurchaseId(p);
        }
        if (s != null) {
            supplierComboBox.setSelectedItem(s.getName());
            supplierComboBox.setEnabled(false);
            selectedSupplier = s;
        }
        FillPlotTable();
    }

    private void Init() {
        FillSupplierComboBox();
        dtm =  (DefaultTableModel) plotTable.getModel();
        DefaultTableCellRenderer left = new DefaultTableCellRenderer();
        DefaultTableCellRenderer center = new DefaultTableCellRenderer();
        DefaultTableCellRenderer right = new DefaultTableCellRenderer();
        left.setHorizontalAlignment(SwingConstants.LEFT);
        center.setHorizontalAlignment(SwingConstants.CENTER);
        right.setHorizontalAlignment(SwingConstants.RIGHT);
        //plotTable.getColumnModel().getColumn(0).setCellRenderer(center);
        //plotTable.getColumnModel().getColumn(1).setCellRenderer(left);
        //plotTable.getColumnModel().getColumn(2).setCellRenderer(left);
        plotTable.setColumnSelectionAllowed(false);
        plotTable.setRowSelectionAllowed(false);
        dateComboBox.setSelectedItem(Utilities.dateToString(new Date()));
        payDayComboBox.setSelectedItem(CalendarOperations.SumMonths(new Date(), 1));
    }

    private void FillSupplierComboBox() {
        suppliers = sf.findSuppliers("name");
        supplierComboBox.removeAllItems();
        supplierComboBox.addItem(String.valueOf(""));
        if (suppliers.size() > 0) {
            for (int i = 0; i < suppliers.size(); i++) {
                supplierComboBox.addItem(suppliers.get(i).getName());
            }
        }
    }

    private void FillSupplierComboBox(Supplier s) {
        suppliers = sf.findSuppliers("name");
        supplierComboBox.removeAllItems();
        supplierComboBox.addItem(String.valueOf(""));
        if (suppliers.size() > 0) {
            for (int i = 0; i < suppliers.size(); i++) {
                supplierComboBox.addItem(suppliers.get(i).getName());
            }
        }
        supplierComboBox.setSelectedItem(s.getName());
    }

    private void FillPlotTable() {
        // Prestacoes = ao numero de prestacoes dividido por o valor menos a entrada
        Date payDay = Utilities.getFormatedDate((String) payDayComboBox.getSelectedItem());
        dtm.setRowCount(0);
        Double amount = Utilities.monetaryToDouble(amountField.getText());
        int numPlots = Integer.valueOf(String.valueOf(plotSpinner.getValue()));
        Double in = Utilities.monetaryToDouble(inField.getText());
        Double plotAmount = (amount - in) / numPlots;
        for (int i=1; i <= numPlots; i++) {
            int sum;
            if (i == 1)
                sum = 0;
            else
                sum = i - 1;
            Double balance = amountField.getValue() - inField.getValue();
            if (amountField.getValue() > 0.0 && balance != 0.0) {
                dtm.addRow(new Object[] {i, CalendarOperations.SumMonths(payDay, sum), Utilities.doubleToMonetary(plotAmount)});
            }
        }
        VerifyLastPlot();
    }

    private void VerifyLastPlot() {
        if (plotTable.getRowCount() > 0) {
            Double acumulated = 0.0;
            Double amount = Utilities.monetaryToDouble(amountField.getText()) - Utilities.monetaryToDouble(inField.getText());
            Double plot = Utilities.monetaryToDouble((String) plotTable.getValueAt(0, 2));

            for (int i = 0; i < plotTable.getRowCount(); i++) {
                acumulated += Utilities.monetaryToDouble((String) plotTable.getValueAt(i, 2));
            }

            if (acumulated != amount) {
                acumulated -= plot;
                plot = amount - acumulated;
            }
            
            plotTable.setValueAt(Utilities.doubleToMonetary(plot), plotTable.getRowCount() - 1 , 2);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        dateComboBox = new CalendarComboBox(true);
        jLabel2 = new javax.swing.JLabel();
        payDayComboBox = new CalendarComboBox(true);
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        plotTable = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        supplierComboBox = new javax.swing.JComboBox();
        plotSpinner = new javax.swing.JSpinner();
        jLabel7 = new javax.swing.JLabel();
        concerningField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        amountField = new components.MonetaryJTextField(0.0);
        inField = new components.MonetaryJTextField(0.0);
        jToolBar2 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jToolBar1 = new javax.swing.JToolBar();
        saveButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Conta a Pagar");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Data do Cadastro:");

        dateComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dateComboBoxKeyPressed(evt);
            }
        });

        jLabel2.setText("Prestações:");

        payDayComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                payDayComboBoxActionPerformed(evt);
            }
        });
        payDayComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                payDayComboBoxKeyPressed(evt);
            }
        });

        jLabel4.setText("Vencimento:");

        jLabel5.setText("Entrada:");

        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        plotTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Numero", "Vencimento", "Valor"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        plotTable.setRowHeight(20);
        plotTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(plotTable);
        plotTable.getColumnModel().getColumn(0).setResizable(false);
        plotTable.getColumnModel().getColumn(0).setPreferredWidth(80);
        plotTable.getColumnModel().getColumn(1).setResizable(false);
        plotTable.getColumnModel().getColumn(1).setPreferredWidth(120);
        plotTable.getColumnModel().getColumn(2).setResizable(false);
        plotTable.getColumnModel().getColumn(2).setPreferredWidth(120);

        jTabbedPane1.addTab("Visualizar Prestações", jScrollPane1);

        jLabel6.setText("Fornecedor:");

        supplierComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                supplierComboBoxActionPerformed(evt);
            }
        });
        supplierComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                supplierComboBoxKeyPressed(evt);
            }
        });

        plotSpinner.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(1), Integer.valueOf(1), null, Integer.valueOf(1)));
        plotSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                plotSpinnerStateChanged(evt);
            }
        });
        plotSpinner.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                plotSpinnerKeyPressed(evt);
            }
        });

        jLabel7.setText("Referente à:");

        concerningField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                concerningFieldFocusGained(evt);
            }
        });
        concerningField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                concerningFieldKeyPressed(evt);
            }
        });

        jLabel3.setText("Valor:");

        amountField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                amountFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                amountFieldFocusLost(evt);
            }
        });

        inField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                inFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                inFieldFocusLost(evt);
            }
        });
        inField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                inFieldKeyPressed(evt);
            }
        });

        jToolBar2.setFloatable(false);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/user-add-24x24.png"))); // NOI18N
        jButton1.setMnemonic('A');
        jButton1.setText("Adicionar Fornecedor");
        jButton1.setToolTipText("Adicionar Fornecedor");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar2.add(jButton1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dateComboBox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(payDayComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(amountField, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addGap(4, 4, 4)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(plotSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(jLabel5))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(inField, javax.swing.GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE))))
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(supplierComboBox, 0, 236, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(concerningField, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dateComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(payDayComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel5))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(amountField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(plotSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(inField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(supplierComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(concerningField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jToolBar1.setFloatable(false);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/accept-16x16.png"))); // NOI18N
        saveButton.setMnemonic('S');
        saveButton.setText("Salvar");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(saveButton);

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/remove-16x16.png"))); // NOI18N
        cancelButton.setMnemonic('C');
        cancelButton.setText("Cancelar");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(cancelButton);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        // TODO create your handling code here:
        this.dispose();
}//GEN-LAST:event_cancelButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        // TODO create your handling code here:
        if (amountField.getValue() <= 0.0) {
            JOptionPane.showMessageDialog(this, "Valor da Conta não pode ser zero.", "Mensagem", 2);
        } else if (dateComboBox.getSelectedItem().toString().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Data do cadastro inválida.", "Mensagem", 2);
        }else {
            payPlots = new ArrayList<PayPlot>();

            if(inField.getValue() != 0.0) {
                PayPlot inPlot = new PayPlot();
                inPlot.setNumber("0");
                inPlot.setDate(Utilities.getFormatedDate(dateComboBox.getSelectedItem().toString()));
                inPlot.setAmount(inField.getValue());
                inPlot.setPenalty(0.0);
                inPlot.setInterestingrate(0.0);
                inPlot.setDiscount(0.0);
                payPlots.add(inPlot);
            }

            for (int i = 0; i < plotTable.getRowCount(); i++) {
                PayPlot pp = new PayPlot();
                pp.setNumber(String.valueOf(i + 1));
                pp.setAmount(Utilities.monetaryToDouble((String) plotTable.getValueAt(i, 2)));
                pp.setDate(Utilities.getFormatedDate((String) plotTable.getValueAt(i, 1)));
                pp.setPenalty(0.0);
                pp.setInterestingrate(0.0);
                pp.setDiscount(0.0);
                payPlots.add(pp);
            }

            if (payAccount == null)
                payAccount = new PayAccount();
            if (selectedSupplier != null)
                payAccount.setSupplierId(selectedSupplier);
            
            payAccount.setDate(Utilities.getFormatedDate((String) dateComboBox.getSelectedItem()));
            payAccount.setConcerning(concerningField.getText());

            if (PayAccountFunctions.getInstance().create(payAccount, payPlots)) {
                this.dispose();
            } else {
                
            }
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void plotSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_plotSpinnerStateChanged
        // TODO create your handling code here:
        FillPlotTable();
    }//GEN-LAST:event_plotSpinnerStateChanged

    private void payDayComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_payDayComboBoxActionPerformed
        // TODO create your handling code here:
        FillPlotTable();
    }//GEN-LAST:event_payDayComboBoxActionPerformed

    private void amountFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_amountFieldFocusLost
        // TODO create your handling code here:
        FillPlotTable();
}//GEN-LAST:event_amountFieldFocusLost

    private void inFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_inFieldFocusLost
        // TODO create your handling code here:
        if (inField.getValue() > amountField.getValue()) {
            JOptionPane.showMessageDialog(this, "Este valor não pode ser maior que o valor da conta.", "Mensagem", 2);
            inField.setValue(0.0);
            inField.requestFocusInWindow();
        }
        FillPlotTable();
}//GEN-LAST:event_inFieldFocusLost

    private void supplierComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_supplierComboBoxActionPerformed
        // TODO create your handling code here:
        if (supplierComboBox.getSelectedIndex() > 0 ) {
            selectedSupplier = sf.getSupplierByName(String.valueOf(supplierComboBox.getSelectedItem()));
        } else if (supplierComboBox.getSelectedIndex() == 0) {
            selectedSupplier = null;
        }
    }//GEN-LAST:event_supplierComboBoxActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO create your handling code here:
        SupplierAddEditDialog supplierDialog = new SupplierAddEditDialog(null, true);
        supplierDialog.setLocationRelativeTo(null);
        supplierDialog.setVisible(true);
        if (supplierDialog.getSupplier() != null) {
            selectedSupplier = supplierDialog.getSupplier();
            FillSupplierComboBox(selectedSupplier);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void dateComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dateComboBoxKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            payDayComboBox.requestFocus();
        }
    }//GEN-LAST:event_dateComboBoxKeyPressed

    private void payDayComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_payDayComboBoxKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            amountField.requestFocusInWindow();
            //customerComboBox.selectAll();
        }
    }//GEN-LAST:event_payDayComboBoxKeyPressed

    private void plotSpinnerKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_plotSpinnerKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            inField.requestFocusInWindow();
            inField.selectAll();
        }
    }//GEN-LAST:event_plotSpinnerKeyPressed

    private void inFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_inFieldKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            supplierComboBox.requestFocusInWindow();
            //customerComboBox.selectAll();
        }
    }//GEN-LAST:event_inFieldKeyPressed

    private void supplierComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_supplierComboBoxKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jButton1.requestFocusInWindow();
            //customerComboBox.selectAll();
        }
    }//GEN-LAST:event_supplierComboBoxKeyPressed

    private void concerningFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_concerningFieldFocusGained
        // TODO add your handling code here:
        concerningField.selectAll();

    }//GEN-LAST:event_concerningFieldFocusGained

    private void concerningFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_concerningFieldKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            saveButton.requestFocusInWindow();
            //customerComboBox.selectAll();
        }
    }//GEN-LAST:event_concerningFieldKeyPressed

    private void amountFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_amountFieldFocusGained
        // TODO add your handling code here:
        amountField.selectAll();
    }//GEN-LAST:event_amountFieldFocusGained

    private void inFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_inFieldFocusGained
        // TODO add your handling code here:
        inField.selectAll();
    }//GEN-LAST:event_inFieldFocusGained

    /**
    * @param args the command line arguments
    */


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private components.MonetaryJTextField amountField;
    private javax.swing.JButton cancelButton;
    private javax.swing.JTextField concerningField;
    private javax.swing.JComboBox dateComboBox;
    private components.MonetaryJTextField inField;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JComboBox payDayComboBox;
    private javax.swing.JSpinner plotSpinner;
    private javax.swing.JTable plotTable;
    private javax.swing.JButton saveButton;
    private javax.swing.JComboBox supplierComboBox;
    // End of variables declaration//GEN-END:variables

}
