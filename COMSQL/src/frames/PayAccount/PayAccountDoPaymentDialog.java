/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * PayAccountDoPaymentDialog.java
 *
 * Created on 12/05/2009, 13:11:09
 */

package frames.PayAccount;

import components.CalendarComboBox;
import util.CalendarOperations;
import util.Utilities;
import comsql.JPA.ReceivingTypeJpaController;
import comsql.PayAccount;
import comsql.PayAccountFunctions;
import comsql.PayPlot;
import comsql.PayedPlot;
import comsql.ReceivingType;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author usuario
 */
public class PayAccountDoPaymentDialog extends javax.swing.JDialog {
    private DefaultTableModel dtmPayPlot;
    private PayAccount payAccount;
    private List<PayPlot> payPlotsNotPayed;
    private Boolean isPayed = false;

    /** Creates new form PayAccountDoPaymentDialog */
    public PayAccountDoPaymentDialog(java.awt.Frame parent, boolean modal, PayAccount p) {
        super(parent, modal);
        initComponents();
        Init();
        payPlotsNotPayed = null;
        payAccount = p;
        FilterPayPlots(payAccount.getPayPlotCollection());
        FillPayPlotTable(payPlotsNotPayed);
        FillOthersObjects();
        FillReceivingTypeComboBox();
        payDayComboBox.setSelectedItem(Utilities.dateToString(new Date()));
    }

    private void Init() {
        DefaultTableCellRenderer left = new DefaultTableCellRenderer();
        DefaultTableCellRenderer center = new DefaultTableCellRenderer();
        DefaultTableCellRenderer right = new DefaultTableCellRenderer();
        left.setHorizontalAlignment(SwingConstants.LEFT);
        center.setHorizontalAlignment(SwingConstants.CENTER);
        right.setHorizontalAlignment(SwingConstants.RIGHT);

        dtmPayPlot = (DefaultTableModel) payPlotTable.getModel();
        //payPlotTable.getColumnModel().getColumn(0).setCellRenderer(center);
        //payPlotTable.getColumnModel().getColumn(1).setCellRenderer(center);
        //payPlotTable.getColumnModel().getColumn(2).setCellRenderer(left);
        //payPlotTable.getColumnModel().getColumn(3).setCellRenderer(left);
        //payPlotTable.getColumnModel().getColumn(4).setCellRenderer(left);
        //payPlotTable.getColumnModel().getColumn(5).setCellRenderer(left);
        payPlotTable.setColumnSelectionAllowed(false);
        payPlotTable.setRowSelectionAllowed(true);
        payPlotTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    }

    private void FilterPayPlots(List<PayPlot> payPlots) {
        payPlotsNotPayed = null;
        if (payPlotsNotPayed == null)
            payPlotsNotPayed = new ArrayList<PayPlot>();
        for(int i=0; i <payPlots.size(); i++) {
            if (!payPlots.get(i).isPayed())
                payPlotsNotPayed.add(payPlots.get(i));
        }
    }

    private void FillPayPlotTable(List<PayPlot> payPlots) {
        dtmPayPlot.setRowCount(0);
        int payPlotCount = 0;
        if (payPlots != null) {
            for(int i=0; i <payPlots.size(); i++) {
                PayPlot pp = (PayPlot) payPlots.get(i);
                String plotNumber = pp.getNumber();
                if (pp.getNumber().compareTo("0") == 0) {
                    plotNumber = "Entrada";
                } else {
                    payPlotCount++;
                }
                if (CalendarOperations.VerifyTwoDates(new Date(), pp.getDate()) < 0) {
                    // IMPLEMENTAR
                }

                dtmPayPlot.addRow(new Object[] {plotNumber, Utilities.dateToString(pp.getDate()), Utilities.doubleToMonetary(pp.getAmount())
                                    , Utilities.doubleToMonetary(pp.getInterestingrate()), Utilities.doubleToMonetary(pp.getPenalty()), Utilities.doubleToMonetary(pp.getTotal())});
            }
        }
    }

    private void FillOthersObjects() {
        originTotalField.setValue(payAccount.getOriginTotal());
        payedTotalField.setValue(payAccount.getPayedTotal());
        toPayTotalField.setValue(payAccount.getBalance());
    }

    private void FillReceivingTypeComboBox() {
        List<ReceivingType> types = ReceivingTypeJpaController.getInstance().findReceivingTypeEntities();
        for (int i=0; i<types.size(); i++) {
            receivingTypeComboBox.addItem(types.get(i).getId() + " - " + types.get(i).getDescription());
        }
    }

    private ReceivingType getSelectedReceivingType() {
        StringTokenizer token = new StringTokenizer((String) receivingTypeComboBox.getSelectedItem(), "-");
        String sId = null;
        while (token.hasMoreTokens()) {
            sId = token.nextToken();
            break;
        }
        sId = sId.replace(" ", "");
        int id = -1;
        try {
            id = Integer.valueOf(sId);
        } catch (Exception e) {

        }
        ReceivingType rt = ReceivingTypeJpaController.getInstance().findReceivingType(id);
        if (rt != null)
            return rt;
        else
            return null;
    }


    private void deselectRows(int undeselectableRow) {
        for (int i=0; i<payPlotTable.getRowCount(); i++) {
            if (i != undeselectableRow)
                payPlotTable.setValueAt(false, i, 6);
        }
    }

    private Boolean isOneCellSelected() {
        for (int i=0; i<payPlotTable.getRowCount(); i++) {
            if (payPlotTable.getValueAt(i, 6) != null) {
                if ((Boolean) payPlotTable.getValueAt(i, 6) == true)
                    return true;
            }
        }
        return false;
    }

    private int getSelectedRowToPay() {
        if (isOneCellSelected()) {
            for (int i=0; i<payPlotTable.getRowCount(); i++) {
                if (payPlotTable.getValueAt(i, 6) != null) {
                    if ((Boolean) payPlotTable.getValueAt(i, 6) == true)
                        return i;
                }
            }
        }
        return -1;
    }

    private void calculateToPayTotal() {
        Double d = 0.0;
        d += originField.getValue();
        d += interestingRateField.getValue();
        d += penaltyField.getValue();
        d -= discountField.getValue();
        toPayField.setValue(d);
    }

    public Boolean getIsPayed() {
        return isPayed;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        payPlotTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        originTotalField = new components.MonetaryJTextField();
        payedTotalField = new components.MonetaryJTextField();
        jLabel2 = new javax.swing.JLabel();
        toPayTotalField = new components.MonetaryJTextField();
        jLabel3 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        toPayField = new components.MonetaryJTextField(0.0);
        jLabel5 = new javax.swing.JLabel();
        receivingTypeComboBox = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        payDayComboBox = new CalendarComboBox(true);
        originField = new components.MonetaryJTextField(0.0);
        jLabel7 = new javax.swing.JLabel();
        interestingRateField = new components.MonetaryJTextField(0.0);
        jLabel8 = new javax.swing.JLabel();
        penaltyField = new components.MonetaryJTextField(0.0);
        jLabel9 = new javax.swing.JLabel();
        discountField = new components.MonetaryJTextField(0.0);
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        confirmButton = new javax.swing.JButton();
        closeButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Efetuar Pagamento");
        setModal(true);
        setResizable(false);

        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        payPlotTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Parcela", "Vencimento", "Valor Original", "Juros", "Multa", "Total", "Selecionar"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        payPlotTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        payPlotTable.setRowHeight(20);
        payPlotTable.getTableHeader().setReorderingAllowed(false);
        payPlotTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                payPlotTableMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(payPlotTable);
        payPlotTable.getColumnModel().getColumn(0).setPreferredWidth(70);
        payPlotTable.getColumnModel().getColumn(1).setPreferredWidth(80);
        payPlotTable.getColumnModel().getColumn(2).setPreferredWidth(90);
        payPlotTable.getColumnModel().getColumn(3).setPreferredWidth(80);
        payPlotTable.getColumnModel().getColumn(4).setPreferredWidth(80);
        payPlotTable.getColumnModel().getColumn(5).setPreferredWidth(90);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel1.setText("Total Geral:");

        originTotalField.setEditable(false);
        originTotalField.setFocusable(false);

        payedTotalField.setEditable(false);
        payedTotalField.setFocusable(false);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel2.setText("Total Pago:");

        toPayTotalField.setEditable(false);
        toPayTotalField.setFocusable(false);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel3.setText("Total a Pagar:");

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 14));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel4.setText("Total a Pagar:");

        toPayField.setEditable(false);
        toPayField.setFocusable(false);
        toPayField.setFont(new java.awt.Font("Tahoma", 0, 14));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel5.setText("Forma de Pagamento:");

        receivingTypeComboBox.setFont(new java.awt.Font("Tahoma", 0, 14));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel6.setText("Data do Pagamento:");

        payDayComboBox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        originField.setEditable(false);
        originField.setFocusable(false);
        originField.setFont(new java.awt.Font("Tahoma", 0, 14));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel7.setText("Valor Original:");

        interestingRateField.setFont(new java.awt.Font("Tahoma", 0, 14));
        interestingRateField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                interestingRateFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                interestingRateFieldFocusLost(evt);
            }
        });
        interestingRateField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                interestingRateFieldKeyPressed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel8.setText("Multa:");

        penaltyField.setFont(new java.awt.Font("Tahoma", 0, 14));
        penaltyField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                penaltyFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                penaltyFieldFocusLost(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel9.setText("Juros:");

        discountField.setFont(new java.awt.Font("Tahoma", 0, 14));
        discountField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                discountFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                discountFieldFocusLost(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel10.setText("Desconto:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(payDayComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(receivingTypeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(originField, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(interestingRateField, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(penaltyField, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(discountField, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(toPayField, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(payDayComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(receivingTypeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(originField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(interestingRateField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel4))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(penaltyField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(toPayField, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE))))
                        .addGap(42, 42, 42))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(discountField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );

        jTabbedPane1.addTab("Pagamento", jPanel1);

        jLabel11.setText("Selecione a parcela que deseja efetuar o pagamento.");

        jToolBar1.setRollover(true);

        confirmButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/accept-16x16.png"))); // NOI18N
        confirmButton.setMnemonic('S');
        confirmButton.setText("Salvar");
        confirmButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(confirmButton);

        closeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/remove-16x16.png"))); // NOI18N
        closeButton.setMnemonic('C');
        closeButton.setText("Cancelar");
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(closeButton);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 594, Short.MAX_VALUE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 594, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 594, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(originTotalField, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 81, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(payedTotalField, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(toPayTotalField, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(originTotalField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(toPayTotalField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(payedTotalField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void confirmButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmButtonActionPerformed
        // TODO create your handling code here:
        if (!isOneCellSelected()) {
            JOptionPane.showMessageDialog(this, "Nenhuma parcela selecionada.", "Mensagem", 2);
        } else if (payDayComboBox.getSelectedItem() == null) {
            JOptionPane.showMessageDialog(this, "Data de pagamento inválida.", "Mensagem", 2);
        } else if (getSelectedReceivingType() == null) {
            JOptionPane.showMessageDialog(this, "Forma de Pagamento inválida.", "Mensagem", 2);
        } else {
            int i = JOptionPane.showConfirmDialog(this, "Confirma o pagamento?", "Confirmação", 0);
            if (i == 0) {
                PayedPlot pp = new PayedPlot();
                String plotNumber;
                if (payPlotTable.getValueAt(this.getSelectedRowToPay(), 0).toString().compareTo("Entrada") == 0)
                    plotNumber = "0";
                else
                    plotNumber = payPlotTable.getValueAt(this.getSelectedRowToPay(), 0).toString();
                pp.setNumber(plotNumber);
                Date d = Utilities.getFormatedDate(payDayComboBox.getSelectedItem().toString());
                pp.setDate(d);
                pp.setAmount(toPayField.getValue());
                pp.setPayaccountId(payAccount);
                pp.setReceivingtypeId(this.getSelectedReceivingType());
                PayPlot payPlotToUpdate = payPlotsNotPayed.get(this.getSelectedRowToPay());
                payPlotToUpdate.setInterestingrate(interestingRateField.getValue());
                payPlotToUpdate.setPenalty(penaltyField.getValue());
                payPlotToUpdate.setDiscount(discountField.getValue());
                if (PayAccountFunctions.getInstance().toPayAccount(payAccount, pp) &&
                        PayAccountFunctions.getInstance().actualizePayPlot(payPlotToUpdate)) {
                    payAccount = PayAccountFunctions.getInstance().getPayAccount(payAccount.getId());
                    if (payAccount.getBalance() == 0.0)
                        this.isPayed = true;
                    this.dispose();
                } else
                    JOptionPane.showMessageDialog(this, "Erro.", "Mensagem", 0);
            }
        }
        
}//GEN-LAST:event_confirmButtonActionPerformed

    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
        // TODO create your handling code here:
        this.dispose();
}//GEN-LAST:event_closeButtonActionPerformed

    private void payPlotTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_payPlotTableMouseReleased
        // TODO create your handling code here:
        Double originTotal = 0.0;
        Double interestingRate = 0.0;
        Double penalty = 0.0;
        Double total = 0.0;
        int selectedRow;
        Boolean isSelectedToPay = false;
        if (payPlotTable.getSelectedRowCount() > 0){
            selectedRow = payPlotTable.getSelectedRow();
            if (payPlotTable.getValueAt(selectedRow, 6) != null) {
                if ((Boolean) payPlotTable.getValueAt(selectedRow, 6) == true)
                    isSelectedToPay = true;
                if (isSelectedToPay)
                    deselectRows(selectedRow);
            }
        }
        for (int i=0; i<payPlotTable.getRowCount(); i++) {
            Boolean b = false;
            if (payPlotTable.getValueAt(i, 6) != null) {
                if ((Boolean) payPlotTable.getValueAt(i, 6) == true)
                    b = true;
            }
            if (b) {
                originTotal += payPlotsNotPayed.get(i).getOriginTotal();
                interestingRate += payPlotsNotPayed.get(i).getInterestingrate();
                penalty += payPlotsNotPayed.get(i).getPenalty();
                total += payPlotsNotPayed.get(i).getTotal();
            }
        }
        originField.setValue(originTotal);
        interestingRateField.setValue(interestingRate);
        penaltyField.setValue(penalty);
        toPayField.setValue(total);
        discountField.setValue(0.0);
    }//GEN-LAST:event_payPlotTableMouseReleased

    private void interestingRateFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_interestingRateFieldFocusLost
        // TODO create your handling code here:
        calculateToPayTotal();
    }//GEN-LAST:event_interestingRateFieldFocusLost

    private void penaltyFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_penaltyFieldFocusLost
        // TODO create your handling code here:
        calculateToPayTotal();
    }//GEN-LAST:event_penaltyFieldFocusLost

    private void discountFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_discountFieldFocusLost
        // TODO create your handling code here:
        calculateToPayTotal();
    }//GEN-LAST:event_discountFieldFocusLost

    private void interestingRateFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_interestingRateFieldKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            penaltyField.requestFocusInWindow();
            penaltyField.selectAll();
        }
    }//GEN-LAST:event_interestingRateFieldKeyPressed

    private void interestingRateFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_interestingRateFieldFocusGained
        // TODO add your handling code here:
        interestingRateField.selectAll();
    }//GEN-LAST:event_interestingRateFieldFocusGained

    private void penaltyFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_penaltyFieldFocusGained
        // TODO add your handling code here:
        penaltyField.selectAll();
    }//GEN-LAST:event_penaltyFieldFocusGained

    private void discountFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_discountFieldFocusGained
        // TODO add your handling code here:
        discountField.selectAll();
    }//GEN-LAST:event_discountFieldFocusGained

    /**
    * @param args the command line arguments
    */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton closeButton;
    private javax.swing.JButton confirmButton;
    private components.MonetaryJTextField discountField;
    private components.MonetaryJTextField interestingRateField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JToolBar jToolBar1;
    private components.MonetaryJTextField originField;
    private components.MonetaryJTextField originTotalField;
    private javax.swing.JComboBox payDayComboBox;
    private javax.swing.JTable payPlotTable;
    private components.MonetaryJTextField payedTotalField;
    private components.MonetaryJTextField penaltyField;
    private javax.swing.JComboBox receivingTypeComboBox;
    private components.MonetaryJTextField toPayField;
    private components.MonetaryJTextField toPayTotalField;
    // End of variables declaration//GEN-END:variables

}
