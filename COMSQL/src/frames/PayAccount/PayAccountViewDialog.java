/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * PayAccountViewDialog.java
 *
 * Created on 10/05/2009, 21:17:43
 */

package frames.PayAccount;

import frames.PayAccount.PayedPlot.PayedPlotDeleteDialog;
import components.CalendarComboBox;
import util.Utilities;
import comsql.PayAccount;
import comsql.PayAccountFunctions;
import comsql.PayPlot;
import comsql.PayedPlot;
import comsql.Supplier;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author usuario
 */
public class PayAccountViewDialog extends javax.swing.JDialog {
    DefaultTableModel dtmPayAccount;
    Double total, toPayTotal, payedTotal;
    Boolean isFiltered = false;
    List<PayAccount> payAccounts;

    /** Creates new form PayAccountViewDialog */
    public PayAccountViewDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        Init();
        payAccounts = new ArrayList<PayAccount>();
    }

    public PayAccountViewDialog(java.awt.Frame parent, boolean modal, Supplier s, int type) {
        super(parent, modal);
        initComponents();
        Init();
        if (type == 0) {
            isToPayRadioButton.setSelected(true);
            isPayedRadioButton.setEnabled(false);
        } else if (type == 1) {
            isPayedRadioButton.setSelected(true);
            isToPayRadioButton.setEnabled(false);
        }
        payAccounts = new ArrayList<PayAccount>();
        filterValueField.setText(s.getName());
        this.setTitle("Contas a Pagar - " + s.getName());
        filterComboBox.setSelectedIndex(1);
        filterButton.doClick();
        filterValueField.setEditable(false);
        filterComboBox.setEditable(false);
        filterButton.setEnabled(false);
        cleanFilterButton.setEnabled(false);
    }

    private void Init() {
        DefaultTableCellRenderer left = new DefaultTableCellRenderer();
        DefaultTableCellRenderer center = new DefaultTableCellRenderer();
        DefaultTableCellRenderer right = new DefaultTableCellRenderer();

        left.setHorizontalAlignment(SwingConstants.LEFT);
        center.setHorizontalAlignment(SwingConstants.CENTER);
        right.setHorizontalAlignment(SwingConstants.RIGHT);

        DefaultTableCellRenderer red = new DefaultTableCellRenderer();
        red.setHorizontalAlignment(SwingConstants.CENTER);
        red.setForeground(Color.RED);

        dtmPayAccount =  (DefaultTableModel) payAccountTable.getModel();
        payAccountTable.setColumnSelectionAllowed(false);
        payAccountTable.setRowSelectionAllowed(true);
        payAccountTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        FillPayAccountTable(PayAccountFunctions.getInstance().findPayAccounts());

        filterComboBox.setSelectedIndex(1);
    }

    private void FillPayAccountTable(List list) {
        dtmPayAccount.setRowCount(0);
        total = 0.0;
        toPayTotal = 0.0;
        payedTotal = 0.0;
        List<PayAccount> accounts = null;

        if (list != null)
            accounts = list;

        if (accounts != null) {
            for (int i=0; i<accounts.size(); i++) {
                int id = accounts.get(i).getId();
                String concerning = accounts.get(i).getConcerning();
                String addDate = Utilities.dateToString(accounts.get(i).getDate());
                String strOriginTotal = Utilities.doubleToMonetary(accounts.get(i).getOriginTotal());
                String strPayedTotal = Utilities.doubleToMonetary(accounts.get(i).getPayedTotal());
                Double balance = accounts.get(i).getBalance();
                String balanceStr = Utilities.doubleToMonetary(accounts.get(i).getBalance());
                String supplier = null;
                if (accounts.get(i).getSupplierId() != null)
                    supplier = accounts.get(i).getSupplierId().getName();
                if (isToPayRadioButton.isSelected()) {
                    if (balance != 0.0) {
                        total += accounts.get(i).getOriginTotal();
                        payedTotal += accounts.get(i).getPayedTotal();
                        toPayTotal += balance;
                        dtmPayAccount.addRow(new Object[] {id, addDate, concerning, strOriginTotal, strPayedTotal, balanceStr, supplier});
                    }
                }
                if (isPayedRadioButton.isSelected()) {
                    if (balance <= 0.0) {
                        total += accounts.get(i).getOriginTotal();
                        payedTotal += accounts.get(i).getPayedTotal();
                        toPayTotal += balance;
                        dtmPayAccount.addRow(new Object[] {id, addDate, concerning, strOriginTotal, strPayedTotal, balanceStr, supplier});
                    }
                }
                totalField.setValue(total);
                toPayTotalField.setValue(toPayTotal);
                payedTotalField.setValue(payedTotal);
            }
        }
    }

    private void doFilter() {
        payAccounts = null;
        if (!filterValueField.getText().isEmpty()) {
            if (filterComboBox.getSelectedIndex() == 0) {
                int id = 0;
                String sId = filterValueField.getText();
                try {
                    id = Integer.valueOf(sId);
                    payAccounts = new ArrayList<PayAccount>();
                    payAccounts.add(PayAccountFunctions.getInstance().getPayAccount(id));
                    FillPayAccountTable(payAccounts);
                } catch (Exception e) {
                    FillPayAccountTable(null);
                }
            } else if (filterComboBox.getSelectedIndex() == 1) {
                String s = filterValueField.getText();
                if (initialDateComboBox.getSelectedItem() != null && finalDateComboBox.getSelectedItem() != null &&
                        !initialDateComboBox.getSelectedItem().toString().isEmpty() && !finalDateComboBox.getSelectedItem().toString().isEmpty()) {
                    Date initialDate = Utilities.getFormatedDate((String) initialDateComboBox.getSelectedItem());
                    Date finalDate = Utilities.getFormatedDate((String) finalDateComboBox.getSelectedItem());
                    payAccounts = PayAccountFunctions.getInstance().findPayAccountsBySupplierName(s, initialDate, finalDate);
                    FillPayAccountTable(payAccounts);
                } else {
                    if (!filterValueField.getText().isEmpty()) {
                        payAccounts = PayAccountFunctions.getInstance().findPayAccountsBySupplierName(s);
                        FillPayAccountTable(payAccounts);
                    }
                    else
                        FillPayAccountTable(null);
                }
           } else if (filterComboBox.getSelectedIndex() == 3) {
                String s = filterValueField.getText();
                payAccounts = PayAccountFunctions.getInstance().findPayAccountsByConcerning(s);
                FillPayAccountTable(payAccounts);
            }
            isFiltered = true;
        } else {
            FillPayAccountTable(null);
        }

        if (filterComboBox.getSelectedIndex() == 2) {
           if (initialDateComboBox.getSelectedItem() != null && finalDateComboBox.getSelectedItem() != null &&
               !initialDateComboBox.getSelectedItem().toString().isEmpty() && !finalDateComboBox.getSelectedItem().toString().isEmpty()) {
               Date initialDate = Utilities.getFormatedDate((String) initialDateComboBox.getSelectedItem());
               Date finalDate = Utilities.getFormatedDate((String) finalDateComboBox.getSelectedItem());
               payAccounts = PayAccountFunctions.getInstance().findPayAccountsByPeriod(initialDate, finalDate);
               FillPayAccountTable(payAccounts);
            }  else {
                FillPayAccountTable(null);
            }
        }
    }

    private void OpenDetails(int payAccountId) {
        PayAccountDetailsDialog detailsDialog = new PayAccountDetailsDialog(null, true, PayAccountFunctions.getInstance().getPayAccount(payAccountId));
        detailsDialog.setLocationRelativeTo(null);
        detailsDialog.setVisible(true);
    }
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jScrollPane1 = new javax.swing.JScrollPane();
        payAccountTable = new javax.swing.JTable();
        totalField = new components.MonetaryJTextField(0.0);
        jLabel5 = new javax.swing.JLabel();
        payedTotalField = new components.MonetaryJTextField(0.0);
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        toPayTotalField = new components.MonetaryJTextField(0.0);
        jToolBar1 = new javax.swing.JToolBar();
        addButton = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        deletePayAccountButton = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JToolBar.Separator();
        doPaymentButton = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JToolBar.Separator();
        deletePayedPlot = new javax.swing.JButton();
        jSeparator6 = new javax.swing.JToolBar.Separator();
        jButton2 = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JToolBar.Separator();
        jButton1 = new javax.swing.JButton();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        isToPayRadioButton = new javax.swing.JRadioButton();
        isPayedRadioButton = new javax.swing.JRadioButton();
        jTabbedPane4 = new javax.swing.JTabbedPane();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        initialDateComboBox = new CalendarComboBox(false);
        finalDateComboBox = new CalendarComboBox(false);
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        filterValueField = new javax.swing.JTextField();
        filterComboBox = new javax.swing.JComboBox();
        filterButton = new javax.swing.JButton();
        cleanFilterButton = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Contas a Pagar");
        setResizable(false);

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        payAccountTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Data", "Referente a", "Total Original", "Valor Pago", "Valor a Pagar", "Fornecedor"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        payAccountTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        payAccountTable.setRowHeight(20);
        payAccountTable.getTableHeader().setReorderingAllowed(false);
        payAccountTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                payAccountTableMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                payAccountTableMouseReleased(evt);
            }
        });
        payAccountTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                payAccountTableKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(payAccountTable);
        payAccountTable.getColumnModel().getColumn(0).setPreferredWidth(60);
        payAccountTable.getColumnModel().getColumn(1).setPreferredWidth(70);
        payAccountTable.getColumnModel().getColumn(2).setPreferredWidth(200);
        payAccountTable.getColumnModel().getColumn(3).setPreferredWidth(90);
        payAccountTable.getColumnModel().getColumn(4).setPreferredWidth(85);
        payAccountTable.getColumnModel().getColumn(5).setPreferredWidth(85);
        payAccountTable.getColumnModel().getColumn(6).setPreferredWidth(180);

        totalField.setEditable(false);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel5.setText("Total Geral:");

        payedTotalField.setEditable(false);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel6.setText("Total Pago:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel7.setText("Total a Pagar:");

        toPayTotalField.setEditable(false);

        jToolBar1.setFloatable(false);

        addButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/add-16x16.png"))); // NOI18N
        addButton.setMnemonic('A');
        addButton.setText("Adicionar");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(addButton);
        jToolBar1.add(jSeparator1);

        deletePayAccountButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/remove-16x16.png"))); // NOI18N
        deletePayAccountButton.setMnemonic('x');
        deletePayAccountButton.setText("Excluir");
        deletePayAccountButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deletePayAccountButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(deletePayAccountButton);
        jToolBar1.add(jSeparator3);

        doPaymentButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Money-24x24.png"))); // NOI18N
        doPaymentButton.setMnemonic('p');
        doPaymentButton.setText("Pagar");
        doPaymentButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                doPaymentButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(doPaymentButton);
        jToolBar1.add(jSeparator4);

        deletePayedPlot.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/payment-18x18.png"))); // NOI18N
        deletePayedPlot.setMnemonic('t');
        deletePayedPlot.setText("Extornar");
        deletePayedPlot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deletePayedPlotActionPerformed(evt);
            }
        });
        jToolBar1.add(deletePayedPlot);
        jToolBar1.add(jSeparator6);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Money-Calculator-16x16.png"))); // NOI18N
        jButton2.setMnemonic('D');
        jButton2.setText("Detalhes");
        jButton2.setFocusable(false);
        jButton2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton2);
        jToolBar1.add(jSeparator5);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/window-remove-16x16.png"))); // NOI18N
        jButton1.setMnemonic('F');
        jButton1.setText("Fechar");
        jButton1.setFocusable(false);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        buttonGroup1.add(isToPayRadioButton);
        isToPayRadioButton.setMnemonic('o');
        isToPayRadioButton.setSelected(true);
        isToPayRadioButton.setText("Contas a Pagar");
        isToPayRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                isToPayRadioButtonActionPerformed(evt);
            }
        });

        buttonGroup1.add(isPayedRadioButton);
        isPayedRadioButton.setMnemonic('n');
        isPayedRadioButton.setText("Contas Pagas");
        isPayedRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                isPayedRadioButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(isToPayRadioButton)
                    .addComponent(isPayedRadioButton))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(isToPayRadioButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(isPayedRadioButton))
        );

        jTabbedPane3.addTab("Exibir", jPanel3);

        jLabel1.setText("Campo:");

        jLabel2.setText("Data Inicial:");

        initialDateComboBox.setEnabled(false);
        initialDateComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                initialDateComboBoxKeyPressed(evt);
            }
        });

        finalDateComboBox.setEnabled(false);
        finalDateComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                finalDateComboBoxKeyPressed(evt);
            }
        });

        jLabel3.setText("Data Final:");

        jLabel4.setText("Valor:");

        filterValueField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filterValueFieldActionPerformed(evt);
            }
        });
        filterValueField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                filterValueFieldKeyPressed(evt);
            }
        });

        filterComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Código", "Fornecedor", "Período", "Referente a" }));
        filterComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filterComboBoxActionPerformed(evt);
            }
        });

        filterButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/search_16x16.png"))); // NOI18N
        filterButton.setMnemonic('l');
        filterButton.setText("Filtrar");
        filterButton.setToolTipText("Filtrar");
        filterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filterButtonActionPerformed(evt);
            }
        });

        cleanFilterButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/search_remover_16x16.png"))); // NOI18N
        cleanFilterButton.setMnemonic('v');
        cleanFilterButton.setText("Remover");
        cleanFilterButton.setToolTipText("Remover Filtro");
        cleanFilterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cleanFilterButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(filterComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(filterValueField, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(initialDateComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(finalDateComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(filterButton)
                .addGap(6, 6, 6)
                .addComponent(cleanFilterButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(filterComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(filterValueField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(finalDateComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel3)
                                .addComponent(jLabel2))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(initialDateComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cleanFilterButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(filterButton, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane4.addTab("Filtrar", jPanel4);

        jMenu1.setMnemonic('C');
        jMenu1.setText("Cadastro");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/add-18x18.png"))); // NOI18N
        jMenuItem1.setText("Adicionar Conta");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        jMenu2.setMnemonic('r');
        jMenu2.setText("Relatórios");

        jMenuItem2.setText("Contas a Pagar");
        jMenu2.add(jMenuItem2);

        jMenuItem3.setText("Contas Pagas");
        jMenu2.add(jMenuItem3);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 769, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(totalField, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(payedTotalField, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(toPayTotalField, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(277, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 634, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 749, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(toPayTotalField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6)
                    .addComponent(payedTotalField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(totalField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE)
                    .addComponent(jTabbedPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void deletePayAccountButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deletePayAccountButtonActionPerformed
        // TODO create your handling code here:
        if (payAccountTable.getSelectedRowCount() > 0) {
            int option = JOptionPane.showConfirmDialog(this, "Tem certeza?", "Confirmação", 0);
            if (option == 0) {
                int id = (Integer) payAccountTable.getValueAt(payAccountTable.getSelectedRow(), 0);
                Boolean isDeleted = PayAccountFunctions.getInstance().delete(id);
                if (isDeleted) {
                    JOptionPane.showMessageDialog(this, "Operação concluida com sucesso.", "Mensagem", 1);
                    if (isFiltered)
                        doFilter();
                    else
                        FillPayAccountTable(PayAccountFunctions.getInstance().findPayAccounts());
                }
            }
        }
}//GEN-LAST:event_deletePayAccountButtonActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO create your handling code here:
        addButton.doClick();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void isToPayRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_isToPayRadioButtonActionPerformed
        // TODO create your handling code here:
        if (isFiltered)
            doFilter();
        else
            FillPayAccountTable(PayAccountFunctions.getInstance().findPayAccounts());
    }//GEN-LAST:event_isToPayRadioButtonActionPerformed

    private void isPayedRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_isPayedRadioButtonActionPerformed
        // TODO create your handling code here:
        if (isFiltered)
            doFilter();
        else
            FillPayAccountTable(PayAccountFunctions.getInstance().findPayAccounts());
    }//GEN-LAST:event_isPayedRadioButtonActionPerformed

    private void doPaymentButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_doPaymentButtonActionPerformed
        // TODO create your handling code here:
        if (isToPayRadioButton.isSelected()) {
            if (payAccountTable.getSelectedRowCount() > 0) {
                int id = (Integer) payAccountTable.getValueAt(payAccountTable.getSelectedRow(), 0);
                int selectedRow = payAccountTable.getSelectedRow();
                PayAccountDoPaymentDialog sform = new PayAccountDoPaymentDialog(null, true, PayAccountFunctions.getInstance().getPayAccount(id));
                sform.setLocationRelativeTo(null);
                sform.setVisible(true);
                if (isFiltered)
                    doFilter();
                else
                    FillPayAccountTable(PayAccountFunctions.getInstance().findPayAccounts());
                if (!sform.getIsPayed())
                    payAccountTable.setRowSelectionInterval(selectedRow, selectedRow);
            } else {
                JOptionPane.showMessageDialog(this, "Você deve selecionar uma conta primeiro.", "Mensagem", 2);
            }
        }
}//GEN-LAST:event_doPaymentButtonActionPerformed

    private void payAccountTableKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_payAccountTableKeyReleased
        // TODO create your handling code here:
    }//GEN-LAST:event_payAccountTableKeyReleased

    private void filterComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filterComboBoxActionPerformed
        // TODO create your handling code here:
        if (filterComboBox.getSelectedItem().toString().compareTo("Período") == 0) {
            initialDateComboBox.setEnabled(true);
            finalDateComboBox.setEnabled(true);
            filterValueField.setEnabled(false);
            initialDateComboBox.requestFocusInWindow();
        } else if (filterComboBox.getSelectedItem().toString().compareTo("Fornecedor") == 0){
            initialDateComboBox.setEnabled(true);
            finalDateComboBox.setEnabled(true);
            filterValueField.setEnabled(true);
            filterValueField.requestFocusInWindow();
            filterValueField.selectAll();
        } else {
            initialDateComboBox.setEnabled(false);
            finalDateComboBox.setEnabled(false);
            filterValueField.setEnabled(true);
            filterValueField.requestFocusInWindow();
            filterValueField.selectAll();
        }
    }//GEN-LAST:event_filterComboBoxActionPerformed

    private void filterValueFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filterValueFieldActionPerformed
        // TODO create your handling code here:
    }//GEN-LAST:event_filterValueFieldActionPerformed

    private void filterValueFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_filterValueFieldKeyPressed
        // TODO create your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (initialDateComboBox.isEnabled())
                initialDateComboBox.requestFocusInWindow();
            else
                filterButton.requestFocusInWindow();
        }
    }//GEN-LAST:event_filterValueFieldKeyPressed

    private void filterButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filterButtonActionPerformed
        // TODO create your handling code here:
        doFilter();
    }//GEN-LAST:event_filterButtonActionPerformed

    private void cleanFilterButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cleanFilterButtonActionPerformed
        // TODO create your handling code here:
        filterValueField.setText("");
        initialDateComboBox.setSelectedItem("");
        initialDateComboBox.setSelectedItem(null);
        finalDateComboBox.setSelectedItem("");
        finalDateComboBox.setSelectedItem(null);
        filterValueField.requestFocusInWindow();
        isFiltered = false;
        FillPayAccountTable(PayAccountFunctions.getInstance().findPayAccounts());
}//GEN-LAST:event_cleanFilterButtonActionPerformed

    private void initialDateComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_initialDateComboBoxKeyPressed
        // TODO create your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            finalDateComboBox.requestFocusInWindow();
        }
    }//GEN-LAST:event_initialDateComboBoxKeyPressed

    private void finalDateComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_finalDateComboBoxKeyPressed
        // TODO create your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            filterButton.requestFocusInWindow();
        }
    }//GEN-LAST:event_finalDateComboBoxKeyPressed

    private void deletePayedPlotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deletePayedPlotActionPerformed
        // TODO create your handling code here:
        if (payAccountTable.getSelectedRowCount() > 0) {
            int id = (Integer) payAccountTable.getValueAt(payAccountTable.getSelectedRow(), 0);
            int selectedRow = payAccountTable.getSelectedRow();
            PayedPlotDeleteDialog sform = new PayedPlotDeleteDialog(null, true, PayAccountFunctions.getInstance().getPayAccount(id));
            sform.setLocationRelativeTo(null);
            sform.setVisible(true);
            if (sform.getIsToUpdate()) {
                if (isFiltered)
                    doFilter();
                else
                    FillPayAccountTable(PayAccountFunctions.getInstance().findPayAccounts());
            }
                if (isToPayRadioButton.isSelected())
                    payAccountTable.setRowSelectionInterval(selectedRow, selectedRow);
        }   else {
            JOptionPane.showMessageDialog(this, "Você deve selecionar uma conta primeiro.", "Mensagem", 2);
        }
}//GEN-LAST:event_deletePayedPlotActionPerformed

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        // TODO create your handling code here:
        PayAccountAddDialog sform = new PayAccountAddDialog(null, true);
        sform.setLocationRelativeTo(null);
        sform.setVisible(true);
        if (isFiltered)
            doFilter();
        else
            FillPayAccountTable(PayAccountFunctions.getInstance().findPayAccounts());
}//GEN-LAST:event_addButtonActionPerformed

    private void payAccountTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_payAccountTableMouseReleased
        // TODO create your handling code here:
    }//GEN-LAST:event_payAccountTableMouseReleased

    private void payAccountTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_payAccountTableMouseClicked
        // TODO add your handling code here:
        if (evt.getClickCount() >= 2) {
            if (payAccountTable.getSelectedRowCount() > 0) {
                int id = (Integer) payAccountTable.getValueAt(payAccountTable.getSelectedRow(), 0);
                OpenDetails(id);
            }
        }
    }//GEN-LAST:event_payAccountTableMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        if (payAccountTable.getSelectedRowCount() > 0) {
            int id = (Integer) payAccountTable.getValueAt(payAccountTable.getSelectedRow(), 0);
            OpenDetails(id);
        } else {
            JOptionPane.showMessageDialog(this, "Você deve selecionar uma conta primeiro.", "Mensagem", 2);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
    * @param args the command line arguments
    */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton cleanFilterButton;
    private javax.swing.JButton deletePayAccountButton;
    private javax.swing.JButton deletePayedPlot;
    private javax.swing.JButton doPaymentButton;
    private javax.swing.JButton filterButton;
    private javax.swing.JComboBox filterComboBox;
    private javax.swing.JTextField filterValueField;
    private javax.swing.JComboBox finalDateComboBox;
    private javax.swing.JComboBox initialDateComboBox;
    private javax.swing.JRadioButton isPayedRadioButton;
    private javax.swing.JRadioButton isToPayRadioButton;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar.Separator jSeparator3;
    private javax.swing.JToolBar.Separator jSeparator4;
    private javax.swing.JToolBar.Separator jSeparator5;
    private javax.swing.JToolBar.Separator jSeparator6;
    private javax.swing.JTabbedPane jTabbedPane3;
    private javax.swing.JTabbedPane jTabbedPane4;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTable payAccountTable;
    private components.MonetaryJTextField payedTotalField;
    private components.MonetaryJTextField toPayTotalField;
    private components.MonetaryJTextField totalField;
    // End of variables declaration//GEN-END:variables

}
