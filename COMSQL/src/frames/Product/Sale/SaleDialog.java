/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * SaleDialog.java
 *
 * Created on 07/06/2009, 08:44:29
 */

package frames.Product.Sale;

import comsql.Customer;
import comsql.JPA.SaleTypeJpaController;
import comsql.Product;
import comsql.ProductFunctions;
import comsql.Sale;
import comsql.SaleFunctions;
import comsql.SaleProduct;
import comsql.SaleType;
import frames.Product.ProductSearchDialog;
import frames.Product.Reports.PrintSale;
import frames.ReceptAccount.ReceptAccountAddDialog;
import java.awt.Cursor;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import util.ReadWritePropertiesFile;
import util.Utilities;

/**
 *
 * @author usuario
 */
public class SaleDialog extends javax.swing.JDialog {
    DefaultTableModel dtm;
    List<SaleProduct> productsList;
    int multiplier = 1;
    Sale sale;

    /** Creates new form SaleDialog */
    public SaleDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        Init();
    }

    private void Init() {
        dtm = (DefaultTableModel) productsTable.getModel();
        dtm.setRowCount(0);
        productsTable.setColumnSelectionAllowed(false);
        productsTable.setRowSelectionAllowed(true);
        productsTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        productsList = new ArrayList<SaleProduct>();
        productTextField.requestFocusInWindow();
        boolean isToPrintSaleProof = Boolean.valueOf(ReadWritePropertiesFile.ReadProperty("config.properties", "isToPrintSaleProof"));
        if (isToPrintSaleProof)
            isToPrintSaleCheckBox.setSelected(true);
    }

    private void AddProduct(Product p) {
        if (!isInList(p)) {
            String salePrice = Utilities.doubleToMonetary(p.getSalesprice());
            String saleTotal = Utilities.doubleToMonetary(p.getSalesprice() * multiplier);
            dtm.addRow(new Object[] {multiplier, p.getName(), salePrice, saleTotal});
            SaleProduct sp = new SaleProduct();
            sp.setProduct(p);
            sp.setQuantity(multiplier);
            sp.setPrice(p.getSalesprice());
            productsList.add(sp);
            jScrollPane1.getViewport().repaint();
            jScrollPane1.getVerticalScrollBar().setValue(jScrollPane1.getVerticalScrollBar().getMaximum());
        } else {
            int id = LocateInList(p);
            int quantity = productsList.get(id).getQuantity() + multiplier;
            Double total = quantity * p.getSalesprice();
            productsTable.setValueAt(quantity, id, 0);
            productsTable.setValueAt(Utilities.doubleToMonetary(total), id, 3);
            productsList.get(id).setQuantity(quantity);
        }
        setTotalLabel(CalculateTotal());
        multiplier = 1;
        multiplierLabel.setText("x 1");
    }

    private Boolean isInList(Product p) {
        for (int i=0; i<productsList.size(); i++) {
            if (productsList.get(i).getProduct().equals(p))
                return true;
        }
        return false;
    }

    private int LocateInList(Product p) {
        for (int i=0; i<productsList.size(); i++) {
            if (productsList.get(i).getProduct().equals(p))
                return i;
        }
        return -1;
    }

    private void setStatus(String text) {
        if (text.length() > 40)
            statusLabel.setText(text.substring(0, 39));
        else
            statusLabel.setText(text);
    }

    private void setTotalLabel(Double value) {
        totalLabel.setText(Utilities.doubleToMonetary(value));
    }

    private Double CalculateTotal() {
        if (productsList.size() > 0) {
            Double total = 0.0;
            for (int i=0; i<productsList.size(); i++) {
                total += productsList.get(i).getQuantity() * productsList.get(i).getPrice();
            }
            return total;
        }
        return 0.0;
    }

    private Boolean SaveSale(SaleType st, Double originTotal, Double discount, Double total, Double inPlotValue, Customer c) {
        sale = new Sale();
        sale.setDate(new Date());
        sale.setOrigintotal(originTotal);
        sale.setDiscount(discount);
        sale.setTotal(total);
        sale.setSaletypeId(st);
        if (c != null)
            sale.setCustomerId(c);
        if (SaleFunctions.getInstance().Create(sale, this.productsList, inPlotValue)) {
            PrintSale();
            return true;
        }
        else
            return false;
    }

    private void PrintSale() {
        if (isToPrintSaleCheckBox.isSelected()) {
            PrintSale ps = new PrintSale(SaleFunctions.getInstance().getSale(sale.getId()));
            ps.Print();
        }
    }
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        productsPanel = new javax.swing.JPanel();
        productTextField = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        productsTable = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        statusLabel = new javax.swing.JLabel();
        totalLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        multiplierLabel = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        confirmButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        isToPrintSaleCheckBox = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Vendas");

        productTextField.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                productTextFieldMouseClicked(evt);
            }
        });
        productTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                productTextFieldKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                productTextFieldKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                productTextFieldKeyTyped(evt);
            }
        });

        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        productsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Quantidade", "Produto", "Preço Unitário", "Preço Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        productsTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        productsTable.setRowHeight(20);
        productsTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                productsTableKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(productsTable);
        productsTable.getColumnModel().getColumn(0).setResizable(false);
        productsTable.getColumnModel().getColumn(0).setPreferredWidth(80);
        productsTable.getColumnModel().getColumn(1).setResizable(false);
        productsTable.getColumnModel().getColumn(1).setPreferredWidth(310);
        productsTable.getColumnModel().getColumn(2).setResizable(false);
        productsTable.getColumnModel().getColumn(2).setPreferredWidth(100);
        productsTable.getColumnModel().getColumn(3).setResizable(false);
        productsTable.getColumnModel().getColumn(3).setPreferredWidth(100);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        statusLabel.setFont(new java.awt.Font("Tahoma", 1, 24));
        statusLabel.setForeground(new java.awt.Color(255, 51, 51));
        statusLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(statusLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 591, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(statusLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        totalLabel.setFont(new java.awt.Font("Tahoma", 0, 36));
        totalLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        totalLabel.setText("R$ 0,00");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel1.setText("(F1) Ajuda - (F2) Pesquisar - (F3) Configuração - (F4) Concluir Venda");

        multiplierLabel.setText("x 1");

        javax.swing.GroupLayout productsPanelLayout = new javax.swing.GroupLayout(productsPanel);
        productsPanel.setLayout(productsPanelLayout);
        productsPanelLayout.setHorizontalGroup(
            productsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, productsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(productsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 615, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, productsPanelLayout.createSequentialGroup()
                        .addGroup(productsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(productsPanelLayout.createSequentialGroup()
                                .addComponent(productTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(multiplierLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(totalLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        productsPanelLayout.setVerticalGroup(
            productsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(productsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(productsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(productsPanelLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(productsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(productTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(multiplierLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(totalLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Itens da Venda", productsPanel);

        jToolBar1.setFloatable(false);

        confirmButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/accept-16x16.png"))); // NOI18N
        confirmButton.setMnemonic('V');
        confirmButton.setText("Concluir Venda");
        confirmButton.setFocusable(false);
        confirmButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        confirmButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(confirmButton);

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/remove-16x16.png"))); // NOI18N
        cancelButton.setMnemonic('C');
        cancelButton.setText("Cancelar");
        cancelButton.setFocusable(false);
        cancelButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(cancelButton);

        isToPrintSaleCheckBox.setMnemonic('I');
        isToPrintSaleCheckBox.setText("Imprimir Comprovante de Venda");
        isToPrintSaleCheckBox.setFocusable(false);
        isToPrintSaleCheckBox.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        isToPrintSaleCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                isToPrintSaleCheckBoxActionPerformed(evt);
            }
        });
        jToolBar1.add(isToPrintSaleCheckBox);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 640, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 590, Short.MAX_VALUE)
                        .addContainerGap(60, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 412, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void confirmButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmButtonActionPerformed
        // TODO add your handling code here:
        if (productsList.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Nenhum produto vendido.", "Mensagem", 2);
        } else {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            SalePaymentWayDialog paymentDialog = new SalePaymentWayDialog(null, true);
            paymentDialog.setLocationRelativeTo(null);
            paymentDialog.setVisible(true);

            // Pagamento a Vista
            if (paymentDialog.getSelectWay() == 0) {
                SaleType st = SaleTypeJpaController.getInstance().findSaleType(1);
                // A vista com dinheiro
                if (paymentDialog.getSelectedReceivingType().getDescription().compareTo("DINHEIRO") == 0) {
                    SaleMoneyReceptDialog moneyDialog = new SaleMoneyReceptDialog(null, true, CalculateTotal());
                    moneyDialog.setLocationRelativeTo(null);
                    moneyDialog.setVisible(true);
                    if (this.SaveSale(st, moneyDialog.getOriginTotal(), moneyDialog.getDiscountValue(), moneyDialog.getTotalValue(), 0.0, paymentDialog.getSelectedCustomer())) {
                        JOptionPane.showMessageDialog(this, "Venda Concluída.", "Mensagem", 1);
                        this.dispose();
                    }
                }
            // Pagamento a Prazo com entrada
            } else if (paymentDialog.getSelectWay() == 1) {
                SaleType st = SaleTypeJpaController.getInstance().findSaleType(2);
                // Pegar valor da entrada
                SaleInPlotValueDialog inPlotDialog = new SaleInPlotValueDialog(null, true);
                inPlotDialog.setLocationRelativeTo(null);
                inPlotDialog.setVisible(true);
                // Se entrada maior que zero continua
                if (inPlotDialog.getInPlot() > 0.0) {
                    Double originTotal = this.CalculateTotal();
                    SaleMoneyReceptDialog moneyDialog = new SaleMoneyReceptDialog(null, true, inPlotDialog.getInPlot());
                    moneyDialog.setTitle("Recebimento em Dinheiro - Entrada");
                    moneyDialog.setLocationRelativeTo(null);
                    moneyDialog.setVisible(true);
                    Double total = originTotal - moneyDialog.getDiscountValue();
                    if (this.SaveSale(st, originTotal, moneyDialog.getDiscountValue(), total, moneyDialog.getReceptedValue(), paymentDialog.getSelectedCustomer())) {
                        ReceptAccountAddDialog receptAccoutDialog = new ReceptAccountAddDialog(null, true, paymentDialog.getSelectedCustomer(),  moneyDialog.getReceptedValue(), total, this.sale);
                        receptAccoutDialog.setLocationRelativeTo(null);
                        receptAccoutDialog.setVisible(true);
                        JOptionPane.showMessageDialog(this, "Venda Concluída.", "Mensagem", 1);
                        this.dispose();
                    }
                }
            // Pagamento a Prazo sem entrada
            } else if (paymentDialog.getSelectWay() == 2) {
                SaleType st = SaleTypeJpaController.getInstance().findSaleType(3);
                // Se entrada maior que zero continua
                Double total = this.CalculateTotal();
                if (this.SaveSale(st, total, 0.0, total, 0.0, paymentDialog.getSelectedCustomer())) {
                    ReceptAccountAddDialog receptAccoutDialog = new ReceptAccountAddDialog(null, true, paymentDialog.getSelectedCustomer(),  0.0, total, this.sale);
                    receptAccoutDialog.setLocationRelativeTo(null);
                    receptAccoutDialog.setVisible(true);
                    JOptionPane.showMessageDialog(this, "Venda Concluída.", "Mensagem", 1);
                    this.dispose();
                }
            }
            this.setCursor(null);
        }
}//GEN-LAST:event_confirmButtonActionPerformed

    private void productsTableKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_productsTableKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            if (productsTable.getSelectedRowCount() > 0) {
                int selectedRow = productsTable.getSelectedRow();
                dtm.removeRow(selectedRow);
                productsList.remove(selectedRow);
            }
            setTotalLabel(CalculateTotal());
            productTextField.requestFocusInWindow();
        }
}//GEN-LAST:event_productsTableKeyPressed

    private void productTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_productTextFieldKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_productTextFieldKeyTyped

    private void productTextFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_productTextFieldKeyReleased
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_MULTIPLY) {
            String t = productTextField.getText();
            if (t.contains("*")) {
                String number = t.substring(0, t.indexOf("*", 0));
                try {
                    multiplier = Integer.valueOf(number);
                    multiplierLabel.setText("x " + String.valueOf(multiplier));
                } catch (Exception e) {
                    multiplier = 1;
                }
            }
            productTextField.setText("");
            productTextField.setCaretPosition(0);
        }
}//GEN-LAST:event_productTextFieldKeyReleased

    private void productTextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_productTextFieldKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_F2) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            ProductSearchDialog psd = new ProductSearchDialog(null, true);
            psd.setLocationRelativeTo(null);
            psd.setVisible(true);
            if (psd.getSelected() != null) {
                setStatus(psd.getSelected().getName());
                AddProduct(psd.getSelected());
            }
            this.setCursor(null);
        }
        if (evt.getKeyCode() == KeyEvent.VK_F3) {
            SaleConfigDialog scd = new SaleConfigDialog(null, true);
            scd.setLocationRelativeTo(null);
            scd.setVisible(true);
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!productTextField.getText().isEmpty()) {
                this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                Product p = null;
                try {
                    p = ProductFunctions.getInstance().SearchByBarCode(productTextField.getText());
                } catch (Exception e) {
                    p = null;
                }
                if (p != null) {
                    setStatus(p.getName());
                    AddProduct(p);
                } else {
                    JOptionPane.showMessageDialog(null, "Produto não encontrado", "Mensagem", 2);
                }
                productTextField.setText("");
                productTextField.setCaretPosition(0);
                this.setCursor(null);
            }
        }
        if (evt.getKeyCode() == KeyEvent.VK_F4) {
            confirmButton.doClick();
        }
}//GEN-LAST:event_productTextFieldKeyPressed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        // TODO add your handling code here:
        int i = JOptionPane.showConfirmDialog(this, "Tem certeza?", "Confirmação", 0);
        if (i == 0)
            this.dispose();
}//GEN-LAST:event_cancelButtonActionPerformed

    private void productTextFieldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_productTextFieldMouseClicked
        // TODO add your handling code here:
        if (evt.getClickCount() >= 2) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            ProductSearchDialog psd = new ProductSearchDialog(null, true);
            psd.setLocationRelativeTo(null);
            psd.setVisible(true);
            if (psd.getSelected() != null) {
                setStatus(psd.getSelected().getName());
                AddProduct(psd.getSelected());
            }
            this.setCursor(null);
        }
    }//GEN-LAST:event_productTextFieldMouseClicked

    private void isToPrintSaleCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_isToPrintSaleCheckBoxActionPerformed
        // TODO add your handling code here:
        if (isToPrintSaleCheckBox.isSelected())
            ReadWritePropertiesFile.WriteProperty("config.properties", "isToPrintSaleProof", "true");
        else
            ReadWritePropertiesFile.WriteProperty("config.properties", "isToPrintSaleProof", "false");
    }//GEN-LAST:event_isToPrintSaleCheckBoxActionPerformed

    /**
    * @param args the command line arguments
    */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JButton confirmButton;
    private javax.swing.JCheckBox isToPrintSaleCheckBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JLabel multiplierLabel;
    private javax.swing.JTextField productTextField;
    private javax.swing.JPanel productsPanel;
    private javax.swing.JTable productsTable;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JLabel totalLabel;
    // End of variables declaration//GEN-END:variables

}
