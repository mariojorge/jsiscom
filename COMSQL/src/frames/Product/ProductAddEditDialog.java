/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ProductAddEditDialog.java
 *
 * Created on 16/04/2009, 10:11:09
 */

package frames.Product;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.swing.AutoCompleteSupport;
import comsql.Manufacturer;
import util.FixedLengthDocument;
import util.FramesUtilities;
import util.Utilities;
import comsql.Product;
import comsql.ProductFunctions;
import comsql.Unit;
import frames.Product.Purchase.PurchasePriceCalculateDialog;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author usuario
 */
public class ProductAddEditDialog extends javax.swing.JDialog {
    Product product;
    Boolean isNew = true;
    EventList glasedList = new BasicEventList();
    EventList manufacturerList = new BasicEventList();
    List<Unit> units;
    List<Manufacturer> manufactures;
    private Boolean isAddedProduct = false;
    Boolean hasBarCode = false;

    /** Creates new form ProductAddEditDialog */
    public ProductAddEditDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        initFields();
        product = new Product();
        product.setQuantity(0);
    }

    public ProductAddEditDialog(java.awt.Frame parent, boolean modal, Product product) {
        super(parent, modal);
        this.product = product;
        initComponents();
        initFields();
        isNew = false;
        nameField.setText(product.getName());
        barCodeField.setText(product.getBarcode());
        if (!product.getBarcode().isEmpty())
            hasBarCode = true;
        unitComboBox.setSelectedItem(product.getUnit());
        quantityLabel.setText(product.getQuantity().toString());
        purchasePriceField.setText(Utilities.doubleToMonetary(product.getPurchaseprice()));
        salesPriceField.setText(Utilities.doubleToMonetary(product.getSalesprice()));
        setMiddlePrice();
        setMargin();
        manufacturerComboBox.setSelectedItem(product.getManufacturer());
    }

    public void initFields() {
        barCodeField.setDocument(new FixedLengthDocument(15));
        nameField.setDocument(new FixedLengthDocument(45));
        purchasePriceField.setText("R$ 0,00");
        salesPriceField.setText("R$ 0,00");
        middlePriceField.setText("R$ 0,00");
        marginField.setText("0,0 %");
        units = ProductFunctions.getInstance().findUnits();
        FillUnitComboBox(units);
        manufactures = ProductFunctions.getInstance().findManufacturers();
        FillManufacturerComboBox(manufactures);
    }

    private void setMiddlePrice() {
        Double p1 = Utilities.monetaryToDouble(purchasePriceField.getText());
        Double p2 = Utilities.monetaryToDouble(salesPriceField.getText());
        Double p3 = 0.0;
        if (p1 != 0 || p2 != 0)
            p3 = (p1 + p2) / 2;
        middlePriceField.setText(Utilities.doubleToMonetary(p3));
    }

    private void setMargin() {
        Double p1 = Utilities.monetaryToDouble(purchasePriceField.getText());
        Double p2 = Utilities.monetaryToDouble(salesPriceField.getText());
        Double p3 = 0.0;
        if (p1 != 0 && p2 != 0)
            p3 = ((p2 * 100) / p1) - 100;
        DecimalFormat decimal = new DecimalFormat("0.00");
        marginField.setText(decimal.format(p3).replace(".", ",") + " %");
    }

    private void FillUnitComboBox(List<Unit> l) {
        AutoCompleteSupport autoComplete = AutoCompleteSupport.install(unitComboBox,glasedList);
        autoComplete.setStrict(false);
        glasedList.add("");
        for(int i=0; i<l.size(); i++) {
            glasedList.add(l.get(i).getDescription());
        }
    }

    private void FillManufacturerComboBox(List<Manufacturer> l) {
        AutoCompleteSupport acs = AutoCompleteSupport.install(manufacturerComboBox, manufacturerList);
        acs.setStrict(false);
        manufacturerList.add("");
        for(int i=0; i<l.size(); i++) {
            manufacturerList.add(l.get(i).getDescription());
        }
    }

    private void VerifyIfUnitExist(String description) {
        if (!description.isEmpty()) {
            Boolean exist = false;
            for(int i=0; i<units.size(); i++) {
                if (units.get(i).getDescription().compareTo(description) == 0)
                    exist = true;
            }
            if (!exist)
                ProductFunctions.getInstance().CreateUnit(description);
        }
    }

    private void VerifyIfManufacturerExist(String description) {
        if (!description.isEmpty()) {
            Boolean exist = false;
            for(int i=0; i<manufactures.size(); i++) {
                if (manufactures.get(i).getDescription().compareTo(description) == 0)
                    exist = true;
            }
            if (!exist)
                ProductFunctions.getInstance().CreateManufacturer(description);
        }
    }

    public Boolean IsAddedProduct() {
        if (isAddedProduct)
            return true;
        else
            return false;
    }

    public void setIsAddedProduct(Boolean isAdded) {
        this.isAddedProduct = isAdded;
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        nameField = new javax.swing.JTextField();
        barCodeField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        quantityLabel = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        salesPriceField = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        middlePriceField = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        marginField = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        purchasePriceField = new components.MonetaryJTextField();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        unitComboBox = new javax.swing.JComboBox();
        jToolBar1 = new javax.swing.JToolBar();
        saveButton = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        cancelButton = new javax.swing.JButton();
        manufacturerComboBox = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Produtos");
        setResizable(false);

        jLabel1.setText("Descrição:");

        nameField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                nameFieldFocusGained(evt);
            }
        });
        nameField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                nameFieldKeyPressed(evt);
            }
        });

        barCodeField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                barCodeFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                barCodeFieldFocusLost(evt);
            }
        });
        barCodeField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                barCodeFieldKeyPressed(evt);
            }
        });

        jLabel2.setText("Código de Barra:");

        jLabel3.setText("Unidade:");

        jLabel4.setText("Quantidade:");

        quantityLabel.setFont(new java.awt.Font("Tahoma", 1, 11));
        quantityLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        quantityLabel.setText("0");

        jLabel10.setText("Fabricante:");

        jLabel6.setText("Preço de Compra:");

        salesPriceField.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        salesPriceField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                salesPriceFieldFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                salesPriceFieldFocusLost(evt);
            }
        });
        salesPriceField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                salesPriceFieldKeyPressed(evt);
            }
        });

        jLabel7.setText("Preço de Venda:");

        middlePriceField.setEditable(false);
        middlePriceField.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        middlePriceField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                middlePriceFieldKeyPressed(evt);
            }
        });

        jLabel8.setText("Preço Médio:");

        marginField.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        marginField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                marginFieldFocusGained(evt);
            }
        });
        marginField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                marginFieldKeyPressed(evt);
            }
        });

        jLabel9.setText("Margem de Lucro:");

        purchasePriceField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                purchasePriceFieldKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
                    .addComponent(jLabel9)
                    .addComponent(marginField, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
                    .addComponent(purchasePriceField, 0, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(salesPriceField, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(middlePriceField, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(32, 32, 32)
                        .addComponent(jLabel9))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel8)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(middlePriceField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(salesPriceField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(purchasePriceField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(marginField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Valores", jPanel1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 368, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 108, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Impostos", jPanel2);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 368, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 108, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Imagem", jPanel3);

        unitComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                unitComboBoxKeyPressed(evt);
            }
        });

        jToolBar1.setFloatable(false);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/accept-16x16.png"))); // NOI18N
        saveButton.setMnemonic('S');
        saveButton.setText("Salvar");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(saveButton);
        jToolBar1.add(jSeparator1);

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/remove-16x16.png"))); // NOI18N
        cancelButton.setMnemonic('C');
        cancelButton.setText("Cancelar");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(cancelButton);

        manufacturerComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                manufacturerComboBoxKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(nameField, javax.swing.GroupLayout.PREFERRED_SIZE, 257, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(barCodeField, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                            .addComponent(jLabel2)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(unitComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel4))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(quantityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(manufacturerComboBox, 0, 246, Short.MAX_VALUE)
                            .addComponent(jLabel10)))
                    .addComponent(jToolBar1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 373, Short.MAX_VALUE)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 373, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(barCodeField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(quantityLabel)
                            .addComponent(unitComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(manufacturerComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel10)))
                .addGap(13, 13, 13)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void salesPriceFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_salesPriceFieldFocusGained
        // TODO add your handling code here:
        FramesUtilities.focusGainedTextField(salesPriceField);
    }//GEN-LAST:event_salesPriceFieldFocusGained

    private void salesPriceFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_salesPriceFieldFocusLost
        // TODO add your handling code here:
        FramesUtilities.focusLostTextField(salesPriceField);
        setMiddlePrice();
        setMargin();
    }//GEN-LAST:event_salesPriceFieldFocusLost

    private void nameFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nameFieldFocusGained
        // TODO add your handling code here:
        nameField.selectAll();
    }//GEN-LAST:event_nameFieldFocusGained

    private void barCodeFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_barCodeFieldFocusGained
        // TODO add your handling code here:
        barCodeField.selectAll();
    }//GEN-LAST:event_barCodeFieldFocusGained

    private void marginFieldFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_marginFieldFocusGained
        // TODO add your handling code here:
        marginField.selectAll();
    }//GEN-LAST:event_marginFieldFocusGained

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        // TODO add your handling code here:
        if (nameField.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Campo descrição não pode ser vazio.", "Mensagem", 2);
        } else {
            product.setName(nameField.getText());
            product.setBarcode(barCodeField.getText());
            product.setPurchaseprice(Utilities.monetaryToDouble(purchasePriceField.getText()));
            product.setSalesprice(Utilities.monetaryToDouble(salesPriceField.getText()));
            try {
                VerifyIfManufacturerExist((String) manufacturerComboBox.getSelectedItem());
                product.setManufacturer((String) manufacturerComboBox.getSelectedItem());
            } catch (Exception e) {
                product.setManufacturer("");
            }
            try {
                VerifyIfUnitExist((String) unitComboBox.getSelectedItem());
                product.setUnit((String) unitComboBox.getSelectedItem());
            } catch (Exception e) {
                product.setUnit("");
            }
            if (isNew) {
                Boolean productExist = ProductFunctions.getInstance().VerifyIfProductExist(product.getBarcode());
                if (!productExist) {
                    if (ProductFunctions.getInstance().Create(product)) {
                        this.setIsAddedProduct(true);
                        this.dispose();
                    } else {
                        JOptionPane.showMessageDialog(this, "Erro ao salvar dados.", "Mensagem", 0);
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Este produto já está cadastro.", "Mensagem", 0);
                }
            } else {
                if (ProductFunctions.getInstance().Edit(this.product)) {
                    this.dispose();
                } else {
                    JOptionPane.showMessageDialog(this, "Erro ao salvar dados.", "Mensagem", 0);
                }
            }
        }
}//GEN-LAST:event_saveButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        // TODO add your handling code here:
        this.dispose();
}//GEN-LAST:event_cancelButtonActionPerformed

    private void middlePriceFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_middlePriceFieldKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            marginField.requestFocusInWindow();
        }
    }//GEN-LAST:event_middlePriceFieldKeyPressed

    private void salesPriceFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_salesPriceFieldKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            marginField.requestFocusInWindow();
        }
    }//GEN-LAST:event_salesPriceFieldKeyPressed

    private void barCodeFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_barCodeFieldFocusLost
        // TODO add your handling code here:
        if (!hasBarCode) {
            if (ProductFunctions.getInstance().VerifyIfProductExist(barCodeField.getText())) {
                JOptionPane.showMessageDialog(this, "Este código de barra está cadastrado para outro produto.", "Mensagem", 2);
            }
        }
    }//GEN-LAST:event_barCodeFieldFocusLost

    private void purchasePriceFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_purchasePriceFieldKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_F2) {
            PurchasePriceCalculateDialog ppcd = new PurchasePriceCalculateDialog(null, true);
            ppcd.setLocationRelativeTo(null);
            ppcd.setVisible(true);
            if (ppcd.getPurchasePrice() != 0.0) {
                purchasePriceField.setValue(ppcd.getPurchasePrice());
            }
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            salesPriceField.requestFocusInWindow();
        }
}//GEN-LAST:event_purchasePriceFieldKeyPressed

    private void nameFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nameFieldKeyPressed
        // TODO add your handling code here:
         if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            barCodeField.requestFocusInWindow();
            barCodeField.selectAll();
        }
    }//GEN-LAST:event_nameFieldKeyPressed

    private void barCodeFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_barCodeFieldKeyPressed
        // TODO add your handling code here:
         if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            unitComboBox.requestFocusInWindow();
        }
    }//GEN-LAST:event_barCodeFieldKeyPressed

    private void unitComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_unitComboBoxKeyPressed
        // TODO add your handling code here:
         if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            manufacturerComboBox.requestFocusInWindow();
        }
    }//GEN-LAST:event_unitComboBoxKeyPressed

    private void manufacturerComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_manufacturerComboBoxKeyPressed
        // TODO add your handling code here:
         if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            purchasePriceField.requestFocusInWindow();
            purchasePriceField.selectAll();
        }
    }//GEN-LAST:event_manufacturerComboBoxKeyPressed

    private void marginFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_marginFieldKeyPressed
        // TODO add your handling code here:
        saveButton.requestFocusInWindow();
    }//GEN-LAST:event_marginFieldKeyPressed

    /**
    * @param args the command line arguments
    */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField barCodeField;
    private javax.swing.JButton cancelButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JComboBox manufacturerComboBox;
    private javax.swing.JTextField marginField;
    private javax.swing.JTextField middlePriceField;
    private javax.swing.JTextField nameField;
    private components.MonetaryJTextField purchasePriceField;
    private javax.swing.JLabel quantityLabel;
    private javax.swing.JTextField salesPriceField;
    private javax.swing.JButton saveButton;
    private javax.swing.JComboBox unitComboBox;
    // End of variables declaration//GEN-END:variables

}
