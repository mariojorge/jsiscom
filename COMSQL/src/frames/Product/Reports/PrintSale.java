/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package frames.Product.Reports;

import comsql.Corporation;
import comsql.CorporationFunctions;
import comsql.Customer;
import comsql.Product;
import comsql.Sale;
import comsql.SaleProduct;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import util.PrintLpt;
import util.ReadWritePropertiesFile;
import util.Utilities;

/**
 *
 * @author usuario
 */
public class PrintSale {
    private Sale sale;
    PrintLpt printer;

    public PrintSale(Sale s) {
        this.sale = s;
        String printSaleIn = ReadWritePropertiesFile.ReadProperty("config.properties", "printSaleIn");
        String printLPTPort = ReadWritePropertiesFile.ReadProperty("config.properties", "printLPTPort");
        if (printSaleIn.compareTo("FILE") == 0) {
            printer = new PrintLpt();
            this.Print();
            printer.Close();
        } else if (printSaleIn.compareTo("PRINTER") == 0) {
            printer = new PrintLpt(printLPTPort);
            this.Print();
            printer.Close();
        } else if (printSaleIn.compareTo("REPORT") == 0) {
            // TODO
        }
    }

    public void Print() {
        PrintHeader();
        printer.PrintLine("==================================");
        if (sale.getSaleProductCollection() != null)
            PrintProducts();
        printer.PrintLine("----------------------------------");
        PrintTotal();
        printer.PrintLine("");
        printer.PrintLine("");
        if (sale.getCustomerId() != null) {
            printer.PrintLine("==================================");
            PrintCustomerInfo();
            printer.PrintLine("----------------------------------");
        }
        if (sale.getSaletypeId().getId() == 2 || sale.getSaletypeId().getId() == 3) {
            PrintSignature();
        }
        PrintFooter();
    }

    private void PrintHeader() {
        Corporation corp = CorporationFunctions.getInstance().getCorporation();
        printer.PrintLine("");
        printer.PrintLine("");
        printer.PrintLine("      COMPROVANTE DE VENDA      ");
        printer.PrintLine("");
        printer.PrintLine("    --- SEM VALOR FISCAL ---    ");
        printer.PrintLine("");
        printer.PrintLine(corp.getName());
        printer.PrintLine(corp.getAddressId().getStreet() + "," + corp.getAddressId().getNumber());
        printer.PrintLine("Fone: " + corp.getPhone());
        printer.PrintLine("");
        printer.PrintLine("Data: " + Utilities.dateToString(new Date()) + "  Hora: " + Utilities.timeToString(new Date()));
        printer.PrintLine("");
        printer.PrintLine("Venda No:" + this.sale.getId());
        printer.PrintLine("");
        printer.PrintLine("Cod. Produto");
        printer.PrintLine("          Quant.   Valor   Total  ");
    }

    private void PrintProducts() {
        List<SaleProduct> list = this.sale.getSaleProductCollection();
        for (int i=0; i<list.size(); i++ ) {
            Product p = list.get(i).getProduct();
            printer.PrintLine(getProductId(p) + "" + getProductName(p));
            printer.PrintLine("            " + getQuantity(list.get(i).getQuantity()) + getPrice(list.get(i).getPrice()) +
                    getTotal(list.get(i).getTotal()));
        }
    }

    private void PrintTotal() {
        printer.PrintLine("## SUBTOTAL: " + Utilities.doubleToMonetary(this.sale.getOrigintotal()));
        printer.PrintLine("## DESCONTO: " + Utilities.doubleToMonetary(this.sale.getDiscount()));
        printer.PrintLine("## TOTAL:    " + Utilities.doubleToMonetary(this.sale.getTotal()));
        printer.PrintLine("");
        printer.PrintLine("## Forma de Pagamento:");
        printer.PrintLine("   " + this.sale.getSaletypeId().getDescription());
    }

    private void PrintCustomerInfo() {
        Customer c = this.sale.getCustomerId();
        printer.PrintLine("Cliente:");
        printer.PrintLine(c.getName());
        if (c.getType().compareTo("Física") == 0) {
            printer.PrintLine("CPF:  " + c.getCpf());
            printer.PrintLine("RG:   " + c.getRg());
        } else {
            printer.PrintLine("CNPJ: " + c.getCnpj());
            printer.PrintLine("I.E.: " + c.getIe());
        }
        printer.PrintLine("Endereço:");
        printer.PrintLine(c.getAddressId().getStreet() + ", " + c.getAddressId().getNumber());
        printer.PrintLine("Bairro: " + c.getAddressId().getDistrict());
        printer.PrintLine("CEP:    " + c.getAddressId().getZipcode());
        printer.PrintLine("Cidade: " + c.getAddressId().getCity() + "-" + c.getAddressId().getFederation());
    }

    private void PrintSignature() {
        printer.PrintLine("");
        printer.PrintLine("");
        printer.PrintLine("");
        printer.PrintLine("      ______________________     ");
        printer.PrintLine("       Reconheco e paguarei      ");
        printer.PrintLine("    a divida aqui discriminada   ");
        printer.PrintLine("");
    }

    private void PrintFooter() {
        printer.PrintLine(" ");
        printer.PrintLine(" ");
        printer.PrintLine("    --- SEM VALOR FISCAL ---    ");
        printer.PrintLine(" ");
        printer.PrintLine(" ");
        printer.PrintLine("     OBRIGADO, VOLTE SEMPRE!    ");
    }
    private String getProductName(Product p) {
        if (p.getName().length() > 29)
            return p.getName().substring(0, 28);
        else
            return p.getName();
    }

    private String getProductId(Product p) {
        String s = String.valueOf(p.getId());
        return getFullString(s, 5);
    }

    private String getQuantity(int quantity) {
        String s = String.valueOf(quantity);
        return getFullString(s, 8);
    }

    private String getPrice(double price) {
        String s = Utilities.doubleToMonetary(price);
        s = s.substring(3, s.length());
        return getFullString(s, 8);
    }

    private String getTotal(double total) {
        String s = Utilities.doubleToMonetary(total);
        s = s.substring(3, s.length());
        return getFullString(s, 8);
    }

    private String getFullString(String in, int size) {
        int zeros = size - in.length();
        if (zeros > 0) {
            for (int i=0; i<zeros; i++) {
                in += " ";
            }
        } else {
            in = in.substring(0, size);
        }
        return in;
    }
}
