/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * PBMainFrame.java
 *
 * Created on 09/03/2009, 20:53:56
 */

package frames.PayBox;

import comsql.CorporationFunctions;
import comsql.Customer;
import javax.swing.Icon;
import util.*;
import comsql.PayBox;
import comsql.PayBoxFunctions;
import comsql.JPA.PayBoxJpaController;
import comsql.LogFunctions;
import comsql.ReceptAccount;
import comsql.ReceptAccountFunctions;
import comsql.ReceptPlot;
import comsql.Sale;
import comsql.SaleFunctions;
import frames.Customer.CustomerSearchDialog;
import frames.DbConfigDialog;
import frames.Product.Sale.SaleDialog;
import frames.Product.Sale.SaleSelectDialog;
import frames.Product.Sale.SaleResumedViewDialog;
import frames.ReceptAccount.ReceptAccountSelectDialog;
import frames.ReceptAccount.ReceptAccountToReceptDialog;
import frames.ReceptAccount.ReceptPlotSelectDialog;
import frames.ReceptAccount.ReceptedPlotViewDialog;
import frames.User.UserLoginDialog;
import java.awt.Cursor;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JOptionPane;
/**
 *
 * @author usuario
 */
public class PayBoxMainFrame extends javax.swing.JFrame {
    PayBoxFunctions pbf = PayBoxFunctions.getInstance();
    String hdserial;
    private boolean isInitingSystem = true;
    
    /** Creates new form PBMainFrame */
    public PayBoxMainFrame() {
        initComponents();
        isInitingSystem = true;
        this.setTitle(CorporationFunctions.getInstance().getCorporation().getName() + " - Caixa - SoftEasy");
        this.setExtendedState(MAXIMIZED_BOTH);
        //this.setSize(800, 600);
        this.setIconImage(getToolkit().getImage(getClass().getResource("/resources/Money-Calculator-32x32.png")));
        this.dateLabel.setText(Utilities.dateToString(new Date()));
        changeButtonsState();
        PbAddDialog cpbdialog = new PbAddDialog(this, true);
        if (!Verify()) {
            int i = JOptionPane.showConfirmDialog(this, "Caixa não cadastrado, deseja cadastrar agora?", "ATENÇÃO", 2);
            if (i == 2) {
                System.exit(1);
            } else if (i == 0) {
                //CreatePayBoxDialog cpbdialog = new CreatePayBoxDialog(this, true);
                cpbdialog.setVisible(true);
            }
        }
        initClock();
        if (!LoggedUser.isLogged()) {
            UserLoginDialog sform;
            try {
                sform = new UserLoginDialog(this, true, LoggedUser.getLoggedUser());
            } catch (Exception e) {
                sform = new UserLoginDialog(this, true);
            }
            sform.setLocationRelativeTo(null);
            sform.setVisible(true);
        } else {
            loggedUserLabel.setText(LoggedUser.getLoggedUser().getFullname());
        }
    }

    private void changeButtonsState() {
        if(PayBoxFunctions.getInstance().isOpen()) {
            openPayBoxButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Comic Icons 3 button_ok_grey.png")));
            openPayBoxButton.setEnabled(false);
            openMenuItem.setEnabled(false);
            closePayBoxButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Comic Icons 3 button_cancel.png")));
            closePayBoxButton.setEnabled(true);
            closeMenuItem.setEnabled(true);
        } else {
            closePayBoxButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Comic Icons 3 button_cancel_gray.png")));
            closePayBoxButton.setEnabled(false);
            closeMenuItem.setEnabled(false);
            openPayBoxButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Comic Icons 3 button_ok.png")));
            openPayBoxButton.setEnabled(true);
            openMenuItem.setEnabled(true);
        }
    }

    @SuppressWarnings("static-access")
    public Boolean Verify() {
        Boolean retorno = false;
        hdserial = new HdSerial().getHDSerial("C");
        PayBoxJpaController pbjpa = PayBoxJpaController.getInstance();
        List<PayBox> payboxs = pbjpa.findPayBoxEntities();
        for (int i=0; i < payboxs.size(); i++) {
            if (hdserial.compareTo(payboxs.get(i).getCpuserial()) == 0)
                return true;
        }
        return retorno;
    }

    private void initClock() {
        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {
                Date actual = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
                String sActual = sdf.format(actual);
                timerLabel.setText(sActual);
            }
        };
        timer.scheduleAtFixedRate(timerTask, 0, 1000);
    }

    public void PayBoxOpenDialog() {
        if(!pbf.isOpen()) {
            PbOpenDialog pbopen = new PbOpenDialog(this, true);
            pbopen.setLocationRelativeTo(null);
            pbopen.setVisible(true);
            changeButtonsState();
        } else {
            JOptionPane.showMessageDialog(this, "O caixa já está aberto.", "Mensagem", 2);
        }
    }

    public void PayBoxCloseDialog() {
        if(pbf.isOpen()) {
            PbCloseDialog pbclose = new PbCloseDialog(this, true);
            pbclose.setLocationRelativeTo(null);
            pbclose.setVisible(true);
            changeButtonsState();
        } else {
            JOptionPane.showMessageDialog(this, "O caixa não está aberto.", "Mensagem", 2);
        }
    }

    public void PayBoxMoviments() {
        PbLastMovesDialog pbmd = new PbLastMovesDialog(this, true);
        pbmd.setLocationRelativeTo(null);
        pbmd.setVisible(true);
    }

    public void PayBoxReforceDialog() {
        PbReforceDialog reforce = new PbReforceDialog(this, true);
        reforce.setLocationRelativeTo(null);
        reforce.setVisible(true);
    }

    public void PayBoxRetire() {
        PbRetireDialog retire = new PbRetireDialog(this, true);
        retire.setLocationRelativeTo(null);
        retire.setVisible(true);
    }

    public void DBConfigDialog() {
        if (LoggedUser.isLogged()) {
            DbConfigDialog dbcofig = new DbConfigDialog(this, true, LoggedUser.getLoggedUser());
            dbcofig.setLocationRelativeTo(null);
            dbcofig.setVisible(true);
        }
    }

    public void toReceptAccount() {
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        // Selecionar Cliente
        CustomerSearchDialog csd = new CustomerSearchDialog(null, true);
        csd.setLocationRelativeTo(null);
        csd.setVisible(true);
        Customer customer = csd.getSelected();
        if (customer != null) {
            // Selecionar Conta
            ReceptAccountSelectDialog radialog = new ReceptAccountSelectDialog(null, true, customer.getId());
            radialog.setLocationRelativeTo(null);
            radialog.setVisible(true);
            if (radialog.getSelectedAccount() != null) {
                ReceptAccount receptAccount = radialog.getSelectedAccount();
                if (receptAccount.getReceptPlotCollection().size() == 1) {
                    ReceptAccountToReceptDialog receptDialog = new ReceptAccountToReceptDialog(null, true, receptAccount);
                    receptDialog.setLocationRelativeTo(null);
                    receptDialog.setVisible(true);
                } else if (receptAccount.getReceptPlotCollection().size() > 1) {
                    ReceptPlotSelectDialog rprdialog = new ReceptPlotSelectDialog(null, true, receptAccount);
                    rprdialog.setLocationRelativeTo(null);
                    rprdialog.setVisible(true);
                    if (rprdialog.getSelectedPlot() != null) {
                        ReceptPlot receptPlot = rprdialog.getSelectedPlot();
                        ReceptAccountToReceptDialog receptDialog = new ReceptAccountToReceptDialog(null, true, receptPlot);
                        receptDialog.setLocationRelativeTo(null);
                        receptDialog.setVisible(true);  
                    }
                }
            }
        }
        setCursor(null);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar2 = new javax.swing.JToolBar();
        jButton3 = new javax.swing.JButton();
        jToolBar1 = new javax.swing.JToolBar();
        jLabel1 = new javax.swing.JLabel();
        dateLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        jLabel3 = new javax.swing.JLabel();
        timerLabel = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JToolBar.Separator();
        jLabel4 = new javax.swing.JLabel();
        loggedUserLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jToolBar3 = new javax.swing.JToolBar();
        openPayBoxButton = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JToolBar.Separator();
        closePayBoxButton = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JToolBar.Separator();
        saleButton = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JToolBar.Separator();
        receptButton = new javax.swing.JButton();
        jSeparator6 = new javax.swing.JToolBar.Separator();
        movesButton = new javax.swing.JButton();
        jSeparator9 = new javax.swing.JToolBar.Separator();
        returnButton = new javax.swing.JButton();
        jSeparator7 = new javax.swing.JToolBar.Separator();
        logoutButton = new javax.swing.JButton();
        jSeparator8 = new javax.swing.JToolBar.Separator();
        closeButton = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        openMenuItem = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        closeMenuItem = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem14 = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem19 = new javax.swing.JMenuItem();
        jMenuItem17 = new javax.swing.JMenuItem();
        jMenuItem20 = new javax.swing.JMenuItem();
        jMenuItem18 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem15 = new javax.swing.JMenuItem();
        jMenuItem16 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem21 = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem13 = new javax.swing.JMenuItem();

        jToolBar2.setRollover(true);

        jButton3.setText("jButton3");

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Sistema Comercial - Caixa");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowDeactivated(java.awt.event.WindowEvent evt) {
                formWindowDeactivated(evt);
            }
        });

        jToolBar1.setFloatable(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel1.setText(" Data: ");
        jToolBar1.add(jLabel1);

        dateLabel.setText("22/05/2009");
        jToolBar1.add(dateLabel);
        jToolBar1.add(jSeparator1);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel3.setText("Hora: ");
        jToolBar1.add(jLabel3);

        timerLabel.setText("00:00:00");
        jToolBar1.add(timerLabel);
        jToolBar1.add(jSeparator2);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel4.setText("Usuário Logado: ");
        jToolBar1.add(jLabel4);

        loggedUserLabel.setText(" ");
        jToolBar1.add(loggedUserLabel);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 102, 102));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/bar-chart-128x128.png"))); // NOI18N
        jLabel2.setText("Sistema Comercial");
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jToolBar3.setBackground(new java.awt.Color(255, 255, 255));
        jToolBar3.setFloatable(false);

        openPayBoxButton.setBackground(new java.awt.Color(255, 255, 255));
        openPayBoxButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Comic Icons 3 button_ok.png"))); // NOI18N
        openPayBoxButton.setText("F4 - ABERTURA");
        openPayBoxButton.setFocusable(false);
        openPayBoxButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        openPayBoxButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        openPayBoxButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openPayBoxButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(openPayBoxButton);
        jToolBar3.add(jSeparator3);

        closePayBoxButton.setBackground(new java.awt.Color(255, 255, 255));
        closePayBoxButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Comic Icons 3 button_cancel.png"))); // NOI18N
        closePayBoxButton.setText("F5 - ENCERRAMENTO");
        closePayBoxButton.setFocusable(false);
        closePayBoxButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        closePayBoxButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        closePayBoxButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closePayBoxButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(closePayBoxButton);
        jToolBar3.add(jSeparator4);

        saleButton.setBackground(new java.awt.Color(255, 255, 255));
        saleButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/7459_32x32.png"))); // NOI18N
        saleButton.setText("F6 - ATENDIMENTO");
        saleButton.setFocusable(false);
        saleButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        saleButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        saleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saleButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(saleButton);
        jToolBar3.add(jSeparator5);

        receptButton.setBackground(new java.awt.Color(255, 255, 255));
        receptButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/piggy-bank-32x32.png"))); // NOI18N
        receptButton.setText("F7 - RECEBIMENTO");
        receptButton.setFocusable(false);
        receptButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        receptButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        receptButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                receptButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(receptButton);
        jToolBar3.add(jSeparator6);

        movesButton.setBackground(new java.awt.Color(255, 255, 255));
        movesButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/line-chart-32x32.png"))); // NOI18N
        movesButton.setText("F8 - MOVIMENTO");
        movesButton.setFocusable(false);
        movesButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        movesButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        movesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                movesButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(movesButton);
        jToolBar3.add(jSeparator9);

        returnButton.setBackground(new java.awt.Color(255, 255, 255));
        returnButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/calculator-32x32.png"))); // NOI18N
        returnButton.setText("F9 - TROCO");
        returnButton.setFocusable(false);
        returnButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        returnButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        returnButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                returnButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(returnButton);
        jToolBar3.add(jSeparator7);

        logoutButton.setBackground(new java.awt.Color(255, 255, 255));
        logoutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/lock-off-32x32.png"))); // NOI18N
        logoutButton.setText("F11 - TRAVAR");
        logoutButton.setFocusable(false);
        logoutButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        logoutButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        logoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(logoutButton);
        jToolBar3.add(jSeparator8);

        closeButton.setBackground(new java.awt.Color(255, 255, 255));
        closeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/window-remove-32x32.png"))); // NOI18N
        closeButton.setText("F12 - SAIR");
        closeButton.setFocusable(false);
        closeButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        closeButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });
        jToolBar3.add(closeButton);

        jMenu1.setMnemonic('a');
        jMenu1.setText("Controle de Caixa");

        openMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, 0));
        openMenuItem.setMnemonic('A');
        openMenuItem.setText("Abrir Caixa");
        openMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(openMenuItem);

        jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem4.setText("Retirada");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem4);

        jMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem5.setText("Reforço");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem5);

        closeMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F5, 0));
        closeMenuItem.setText("Fechar Caixa");
        closeMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(closeMenuItem);

        jMenuBar1.add(jMenu1);

        jMenu5.setMnemonic('V');
        jMenu5.setText("Vendas");

        jMenuItem7.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F6, 0));
        jMenuItem7.setMnemonic('I');
        jMenuItem7.setText("Iniciar Venda");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem7);

        jMenuItem11.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F9, 0));
        jMenuItem11.setMnemonic('T');
        jMenuItem11.setText("Calcular Troco");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem11);

        jMenuItem14.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem14.setMnemonic('x');
        jMenuItem14.setText("Extornar Venda");
        jMenuItem14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem14ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem14);

        jMenuBar1.add(jMenu5);

        jMenu6.setMnemonic('b');
        jMenu6.setText("Recebimentos");

        jMenuItem10.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F7, 0));
        jMenuItem10.setMnemonic('C');
        jMenuItem10.setText("Por Cliente");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem10);

        jMenuBar1.add(jMenu6);

        jMenu2.setMnemonic('C');
        jMenu2.setText("Consultas");

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F8, 0));
        jMenuItem3.setText("Movimento Atual");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenuItem19.setText("Vendas");
        jMenuItem19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem19ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem19);

        jMenuItem17.setText("Vendas do Dia");
        jMenuItem17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem17ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem17);

        jMenuItem20.setText("Recebimentos");
        jMenuItem20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem20ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem20);

        jMenuItem18.setText("Recebimentos do Dia");
        jMenuItem18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem18ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem18);

        jMenuBar1.add(jMenu2);

        jMenu3.setMnemonic('R');
        jMenu3.setText("Relatórios");

        jMenuItem15.setText("Vendas");
        jMenu3.add(jMenuItem15);

        jMenuItem16.setText("Recebimentos");
        jMenu3.add(jMenuItem16);

        jMenuItem8.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem8.setMnemonic('C');
        jMenuItem8.setText("Imprimir Carnê");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem8);

        jMenuItem21.setText("Imprimir Recibo");
        jMenu3.add(jMenuItem21);

        jMenuBar1.add(jMenu3);

        jMenu7.setText("Sistema");

        jMenu4.setMnemonic('F');
        jMenu4.setText("Ferramentas");

        jMenuItem6.setText("Configurar Conexão");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem6);

        jMenu7.add(jMenu4);

        jMenuItem12.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F11, 0));
        jMenuItem12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/lock-off-18x18.png"))); // NOI18N
        jMenuItem12.setText("Travar");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem12);

        jMenuItem13.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F12, 0));
        jMenuItem13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/window-remove-16x16.png"))); // NOI18N
        jMenuItem13.setText("Sair");
        jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem13ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem13);

        jMenuBar1.add(jMenu7);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 689, Short.MAX_VALUE)
            .addComponent(jToolBar3, javax.swing.GroupLayout.DEFAULT_SIZE, 689, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 669, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jToolBar3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 397, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void openMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openMenuItemActionPerformed
        // TODO add your handling code here:
        PayBoxOpenDialog();
}//GEN-LAST:event_openMenuItemActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
        PayBoxMoviments();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        // TODO add your handling code here:
        DBConfigDialog();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void closeMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeMenuItemActionPerformed
        // TODO add your handling code here:
        PayBoxCloseDialog();
}//GEN-LAST:event_closeMenuItemActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        // TODO add your handling code here:
        PayBoxReforceDialog();
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        // TODO add your handling code here:
        if (VerifyPermission.hasPermissionToOpenDialog()) {
            PayBoxRetire();
        }
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        // TODO add your handling code here:
        saleButton.doClick();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        // TODO add your handling code here:
        if(!pbf.isOpen()) {
            int i = JOptionPane.showConfirmDialog(this, "O Caixa não está aberto. Deseja efetuar a abertura?", "Mensagem", 0);
            if (i==0)
                PayBoxOpenDialog();
        }
        if (pbf.isOpen()) {
            toReceptAccount();
        }
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        // TODO add your handling code here:
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        // Selecionar Cliente
        CustomerSearchDialog csd = new CustomerSearchDialog(null, true);
        csd.setLocationRelativeTo(null);
        csd.setVisible(true);
        Customer customer = csd.getSelected();
        if (customer != null) {
            // Selecionar Conta
            ReceptAccountSelectDialog radialog = new ReceptAccountSelectDialog(null, true, customer.getId());
            radialog.setLocationRelativeTo(null);
            radialog.setVisible(true);
            if (radialog.getSelectedAccount() != null) {
                // Exibir relatorio de carne
            }
        }
        setCursor(null);
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        // TODO add your handling code here:
        returnButton.doClick();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed
        // TODO add your handling code here:
        logoutButton.doClick();
    }//GEN-LAST:event_jMenuItem12ActionPerformed

    private void jMenuItem13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem13ActionPerformed
        // TODO add your handling code here:
        closeButton.doClick();
    }//GEN-LAST:event_jMenuItem13ActionPerformed

    private void jMenuItem14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem14ActionPerformed
        // TODO add your handling code here:
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        if (VerifyPermission.hasPermissionToOpenDialog()) {
            SaleSelectDialog saleResumeDialog = new SaleSelectDialog(this, true, true);
            saleResumeDialog.setLocationRelativeTo(null);
            saleResumeDialog.setVisible(true);
            Sale s;
            if (saleResumeDialog.getSelectedSale() != null) {
                s = saleResumeDialog.getSelectedSale();
                if (saleResumeDialog.getSelectedSale() != null) {
                    int x = JOptionPane.showConfirmDialog(this, "Confirma extorno de venda?", "Confirmação", 0);
                    if (x == 0) {
                        // Procura por uma conta a receber relacionada a esta venda
                        ReceptAccount ra = null;
                        try {
                            ra = ReceptAccountFunctions.getInstance().getReceptAccount(s);
                        } catch (Exception e) {

                        }
                        // Apaga a venda
                        if (SaleFunctions.getInstance().destroy(s)) {
                            JOptionPane.showMessageDialog(this, "A venda foi extornada.", "Mensagem", 1);
                        }
                        if (ra != null) {
                            int i = JOptionPane.showConfirmDialog(this, "Deseja tambem excluir a conta referente a esta venda?", "Confirmação", 0);
                            if (i == 0) {
                                // Apaga a conta a receber
                                if (ReceptAccountFunctions.getInstance().delete(ra.getId())) {
                                    JOptionPane.showMessageDialog(this, "A venda foi extornada.", "Mensagem", 1);
                                }
                            }
                        }
                    }
                }
            }
        }
        setCursor(null);
    }//GEN-LAST:event_jMenuItem14ActionPerformed

    private void jMenuItem17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem17ActionPerformed
        // TODO add your handling code here:
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        SaleResumedViewDialog svd = new SaleResumedViewDialog(this, true, true);
        svd.setLocationRelativeTo(null);
        svd.setVisible(true);
        setCursor(null);
    }//GEN-LAST:event_jMenuItem17ActionPerformed

    private void jMenuItem19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem19ActionPerformed
        // TODO add your handling code here:
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        SaleResumedViewDialog svd = new SaleResumedViewDialog(this, true, false);
        svd.setLocationRelativeTo(null);
        svd.setVisible(true);
        setCursor(null);
    }//GEN-LAST:event_jMenuItem19ActionPerformed

    private void jMenuItem20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem20ActionPerformed
        // TODO add your handling code here:
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        ReceptedPlotViewDialog rpvd = new ReceptedPlotViewDialog(this, true);
        rpvd.setLocationRelativeTo(null);
        rpvd.setVisible(true);
        setCursor(null);
    }//GEN-LAST:event_jMenuItem20ActionPerformed

    private void jMenuItem18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem18ActionPerformed
        // TODO add your handling code here:
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        ReceptedPlotViewDialog rpvd = new ReceptedPlotViewDialog(this, true, new Date(), new Date());
        rpvd.setLocationRelativeTo(null);
        rpvd.setVisible(true);
        setCursor(null);
    }//GEN-LAST:event_jMenuItem18ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        int i = JOptionPane.showConfirmDialog(this, "Deseja realmente sair?", "Confirmação", 0);
        if (i == 0) {
            if(pbf.isOpen()) {
                int x = JOptionPane.showConfirmDialog(this, "O Caixa está aberto. Deseja encerra-lo?", "Confirmação", 0);
                if (x==0) {
                    PbCloseDialog pbclose = new PbCloseDialog(this, true);
                    pbclose.setLocationRelativeTo(null);
                    pbclose.setVisible(true);
                }
            }
            System.exit(0);
        }
    }//GEN-LAST:event_formWindowClosing

    private void formWindowDeactivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowDeactivated
        // TODO add your handling code here:

    }//GEN-LAST:event_formWindowDeactivated

    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
        // TODO add your handling code here:
        int i = JOptionPane.showConfirmDialog(this, "Deseja realmente sair?", "Confirmação", 0);
        if (i == 0) {
            if(pbf.isOpen()) {
                int x = JOptionPane.showConfirmDialog(this, "O Caixa está aberto. Deseja encerra-lo?", "Confirmação", 0);
                if (x==0) {
                    PbCloseDialog pbclose = new PbCloseDialog(this, true);
                    pbclose.setLocationRelativeTo(null);
                    pbclose.setVisible(true);
                }
            }
            System.exit(0);
        }
}//GEN-LAST:event_closeButtonActionPerformed

    private void logoutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logoutButtonActionPerformed
        // TODO add your handling code here:
        logoutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/lock-32x32.png"))); // NOI18N
        logoutButton.setText("F11 - LOGIN");
        if (!isInitingSystem)
            LogFunctions.getInstance().Insert("Saiu do Adminitrador");
        else
            isInitingSystem = false;

        UserLoginDialog sform;
        try {
            sform = new UserLoginDialog(this, true, LoggedUser.getLoggedUser());
        } catch (Exception e) {
            sform = new UserLoginDialog(this, true);
        }
        sform.setLocationRelativeTo(null);
        sform.setVisible(true);

        if (LoggedUser.isLogged()) {
            loggedUserLabel.setText(LoggedUser.getLoggedUser().getFullname());
        }

        logoutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/lock-off-32x32.png"))); // NOI18N
        logoutButton.setText("F11 - LOGOUT");
}//GEN-LAST:event_logoutButtonActionPerformed

    private void returnButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_returnButtonActionPerformed
        // TODO add your handling code here:
        CalculateReturnDialog crdialog = new CalculateReturnDialog(this, true, 0.0);
        crdialog.setLocationRelativeTo(null);
        crdialog.setVisible(true);
}//GEN-LAST:event_returnButtonActionPerformed

    private void movesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_movesButtonActionPerformed
        // TODO add your handling code here:
        PayBoxMoviments();
}//GEN-LAST:event_movesButtonActionPerformed

    private void saleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saleButtonActionPerformed
        // TODO add your handling code here:
        if(!pbf.isOpen()) {
            int i = JOptionPane.showConfirmDialog(this, "O Caixa não está aberto. Deseja efetuar a abertura?", "Mensagem", 0);
            if (i==0)
                PayBoxOpenDialog();
        }
        if (pbf.isOpen()) {
                SaleDialog sdialog = new SaleDialog(this, true);
                sdialog.setLocationRelativeTo(null);
                sdialog.setVisible(true);
        }
}//GEN-LAST:event_saleButtonActionPerformed

    private void receptButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_receptButtonActionPerformed
        // TODO add your handling code here:
        if(!pbf.isOpen()) {
            int i = JOptionPane.showConfirmDialog(this, "O Caixa não está aberto. Deseja efetuar a abertura?", "Mensagem", 0);
            if (i==0)
                PayBoxOpenDialog();
        }
        if (pbf.isOpen()) {
            toReceptAccount();
        }

    }//GEN-LAST:event_receptButtonActionPerformed

    private void closePayBoxButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closePayBoxButtonActionPerformed
        // TODO add your handling code here:
        PayBoxCloseDialog();
}//GEN-LAST:event_closePayBoxButtonActionPerformed

    private void openPayBoxButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openPayBoxButtonActionPerformed
        // TODO add your handling code here:
        PayBoxOpenDialog();
}//GEN-LAST:event_openPayBoxButtonActionPerformed

    /**
    * @param args the command line arguments
    */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton closeButton;
    private javax.swing.JMenuItem closeMenuItem;
    private javax.swing.JButton closePayBoxButton;
    private javax.swing.JLabel dateLabel;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem16;
    private javax.swing.JMenuItem jMenuItem17;
    private javax.swing.JMenuItem jMenuItem18;
    private javax.swing.JMenuItem jMenuItem19;
    private javax.swing.JMenuItem jMenuItem20;
    private javax.swing.JMenuItem jMenuItem21;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar.Separator jSeparator2;
    private javax.swing.JToolBar.Separator jSeparator3;
    private javax.swing.JToolBar.Separator jSeparator4;
    private javax.swing.JToolBar.Separator jSeparator5;
    private javax.swing.JToolBar.Separator jSeparator6;
    private javax.swing.JToolBar.Separator jSeparator7;
    private javax.swing.JToolBar.Separator jSeparator8;
    private javax.swing.JToolBar.Separator jSeparator9;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JToolBar jToolBar3;
    private javax.swing.JLabel loggedUserLabel;
    private javax.swing.JButton logoutButton;
    private javax.swing.JButton movesButton;
    private javax.swing.JMenuItem openMenuItem;
    private javax.swing.JButton openPayBoxButton;
    private javax.swing.JButton receptButton;
    private javax.swing.JButton returnButton;
    private javax.swing.JButton saleButton;
    private javax.swing.JLabel timerLabel;
    // End of variables declaration//GEN-END:variables

}
