/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package frames.Customer.Reports;

import comsql.Corporation;
import comsql.CorporationFunctions;
import util.LoadingReport;

/**
 *
 * @author usuario
 */
public class CustomerRegistrationReport {
    Corporation corp = CorporationFunctions.getInstance().getCorporation();

    public CustomerRegistrationReport() {

    }

    public void ShowRegistrationReport(int customerId) {
        String query = "SELECT customer.`id` AS customer_id, customer.`registrationdate` AS customer_registrationdate," +
                "customer.`name` AS customer_name, customer.`fantasyname` AS customer_fantasyname," +
                "customer.`type` AS customer_type, customer.`cpf` AS customer_cpf, customer.`rg` AS customer_rg," +
                "customer.`cnpj` AS customer_cnpj, customer.`ie` AS customer_ie, customer.`address_id` AS customer_address_id," +
                "customer.`phone` AS customer_phone, customer.`cellphone` AS customer_cellphone, customer.`fax` AS customer_fax," +
                "customer.`email` AS customer_email, customer.`birthday` AS customer_birthday, customer.`father` AS customer_father," +
                "customer.`mather` AS customer_mather, customer.`ocupation` AS customer_ocupation, customer.`photo` AS customer_photo," +
                "customer.`islocked` AS customer_islocked, address.`street` AS address_street, address.`number` AS address_number," +
                "address.`district` AS address_district, address.`zipcode` AS address_zipcode, address.`city` AS address_city," +
                "address.`federation` AS address_federation FROM `address` address INNER JOIN `customer` customer ON address.`id` = customer.`address_id`";
        query = query + " WHERE customer.`id` = " + String.valueOf(customerId) + ";";
        LoadingReport lr = new LoadingReport(null, true, query, "CustomerRegistration");
        lr.setVisible(true);
   }

    public void ShowAllRegistrationsReport() {
        String query = "SELECT customer.`id` AS customer_id, customer.`registrationdate` AS customer_registrationdate," +
                "customer.`name` AS customer_name, customer.`fantasyname` AS customer_fantasyname," +
                "customer.`type` AS customer_type, customer.`cpf` AS customer_cpf, customer.`rg` AS customer_rg," +
                "customer.`cnpj` AS customer_cnpj, customer.`ie` AS customer_ie, customer.`address_id` AS customer_address_id," +
                "customer.`phone` AS customer_phone, customer.`cellphone` AS customer_cellphone, customer.`fax` AS customer_fax," +
                "customer.`email` AS customer_email, customer.`birthday` AS customer_birthday, customer.`father` AS customer_father," +
                "customer.`mather` AS customer_mather, customer.`ocupation` AS customer_ocupation, customer.`photo` AS customer_photo," +
                "customer.`islocked` AS customer_islocked, address.`street` AS address_street, address.`number` AS address_number," +
                "address.`district` AS address_district, address.`zipcode` AS address_zipcode, address.`city` AS address_city," +
                "address.`federation` AS address_federation FROM `address` address INNER JOIN `customer` customer ON address.`id` = customer.`address_id`;";
        LoadingReport lr = new LoadingReport(null, true, query, "CustomerRegistration");
        lr.setVisible(true);
   }
}
