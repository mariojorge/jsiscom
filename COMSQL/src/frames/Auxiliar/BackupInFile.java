/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package frames.Auxiliar;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class BackupInFile {
    private FileOutputStream fos = null;
    private PrintStream ps = null;
    private final String file = "backup/backup.txt";

    public BackupInFile() {
        try {
            fos = new FileOutputStream(file, false);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BackupInFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            ps = new PrintStream(fos);
        } catch (Exception e) {

        }
    }

    public BackupInFile(String port) {
        try {
            fos = new FileOutputStream(port);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BackupInFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            ps = new PrintStream(fos);
        } catch (Exception e) {

        }
    }
    
    public void PrintLine(String s) {
        ps.println(s);
    }
    
    public void Close() {
        ps.close();
        try {
            fos.close();
        } catch (IOException ex) {
            Logger.getLogger(BackupInFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }



}
