/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package components;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.JFormattedTextField;

/**
 *
 * @author Gilderlan
 */
public class PhoneFormatted extends JFormattedTextField {

    /**
     * Creates a new instance of <code>PhoneFormatted</code> without detail message.
     */
    public PhoneFormatted() {
        this.addFocusListener(new FocusAdapter() {
            public void focusGained (FocusEvent evt) {
                phoneFormattedFocusGained(evt);
            }

            public void focusLost (FocusEvent evt) {
                phoneFormattedFocusLost(evt);
            }
        });

    }

    private void phoneFormattedFocusGained (FocusEvent evt) {
        try {
            super.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##)####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
    }

    private void phoneFormattedFocusLost (FocusEvent evt) {
        String s = super.getText();
        if (s.compareTo("(  )    -    ") == 0) {
            try {
                super.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("")));
            } catch (java.text.ParseException ex) {
                ex.printStackTrace();
            }
        } else if (s.contains(" ")) {
            super.setText("");
            try {
                super.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("")));
            } catch (java.text.ParseException ex) {
                ex.printStackTrace();
            }
        }
    }
}
