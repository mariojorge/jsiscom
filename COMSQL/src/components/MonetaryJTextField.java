/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package components;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 *
 * @author usuario
 */
public class MonetaryJTextField extends javax.swing.JTextField {

    public MonetaryJTextField() {
        super();
        this.addFocusListener();
    }

    public MonetaryJTextField(Double value) {
        super();
        super.setText(NumberFormat.getCurrencyInstance(Locale.getDefault()).format(value));
        this.addFocusListener();
    }

   public synchronized void addFocusListener() {
        super.addFocusListener(new java.awt.event.FocusAdapter() {
            @Override
            public void focusGained(java.awt.event.FocusEvent evt) {
                MonetaryJTextFieldFocusGained(evt);
            }
            @Override
            public void focusLost(java.awt.event.FocusEvent evt) {
                MonetaryJTextFieldFocusLost(evt);
            }
        });
    }

    private void MonetaryJTextFieldFocusGained(java.awt.event.FocusEvent evt) {
        String s = super.getText();
        if (!s.isEmpty()) {
            Number n = null;
            Double d = null;

            s = s.substring(3, s.length());
            try {
                n = NumberFormat.getNumberInstance(Locale.getDefault()).parse(s);
            } catch (ParseException ex) {
                //Logger.getLogger(FramesUtilities.class.getName()).log(Level.SEVERE, null, ex);
            }
            d = n.doubleValue();
            s = String.valueOf(NumberFormat.getNumberInstance(Locale.getDefault()).format(d));
            s = s.replace(".", "");
        } else {
            s = "";
        }
        super.setText(s);
        super.selectAll();
    }

    private void MonetaryJTextFieldFocusLost(java.awt.event.FocusEvent evt) {
        String j = super.getText();
        j = j.replace(".", ",");
        Number n = null;
        try {
            n = NumberFormat.getNumberInstance(Locale.getDefault()).parse(j);
        } catch (ParseException ex) {
            n = 0;
        }
        Double d = n.doubleValue();
        if (d <= 0) {
            d = d * -1;
            if (d == -0)
                d = 0.0;
        }
        super.setText(NumberFormat.getCurrencyInstance(Locale.getDefault()).format(d));
    }

    public Double getValue() {
        String s = super.getText();
        s = s.substring(3, s.length());
        Number n = null;
        try {
            n = NumberFormat.getNumberInstance(Locale.getDefault()).parse(s);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return n.doubleValue();
    }

    public void setValue(Double d) {
        String s = NumberFormat.getCurrencyInstance(Locale.getDefault()).format(d);
        super.setText(s);
    }
}
