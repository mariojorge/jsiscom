/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package test.Imports;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "produto")
@NamedQueries({@NamedQuery(name = "Produto.findAll", query = "SELECT p FROM Produto p"), @NamedQuery(name = "Produto.findByCodigo", query = "SELECT p FROM Produto p WHERE p.codigo = :codigo"), @NamedQuery(name = "Produto.findByCodbar", query = "SELECT p FROM Produto p WHERE p.codbar = :codbar"), @NamedQuery(name = "Produto.findByGrupo", query = "SELECT p FROM Produto p WHERE p.grupo = :grupo"), @NamedQuery(name = "Produto.findByFornecedor", query = "SELECT p FROM Produto p WHERE p.fornecedor = :fornecedor"), @NamedQuery(name = "Produto.findByDescricao", query = "SELECT p FROM Produto p WHERE p.descricao = :descricao"), @NamedQuery(name = "Produto.findByUnidade", query = "SELECT p FROM Produto p WHERE p.unidade = :unidade"), @NamedQuery(name = "Produto.findByPrecoVenda", query = "SELECT p FROM Produto p WHERE p.precoVenda = :precoVenda"), @NamedQuery(name = "Produto.findByPrecoCusto", query = "SELECT p FROM Produto p WHERE p.precoCusto = :precoCusto"), @NamedQuery(name = "Produto.findByCustoMedio", query = "SELECT p FROM Produto p WHERE p.custoMedio = :custoMedio"), @NamedQuery(name = "Produto.findByMargem", query = "SELECT p FROM Produto p WHERE p.margem = :margem"), @NamedQuery(name = "Produto.findBySt", query = "SELECT p FROM Produto p WHERE p.st = :st"), @NamedQuery(name = "Produto.findByLucro", query = "SELECT p FROM Produto p WHERE p.lucro = :lucro"), @NamedQuery(name = "Produto.findByEstofi", query = "SELECT p FROM Produto p WHERE p.estofi = :estofi"), @NamedQuery(name = "Produto.findById", query = "SELECT p FROM Produto p WHERE p.id = :id")})
public class Produto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "CODIGO")
    private Double codigo;
    @Column(name = "CODBAR")
    private String codbar;
    @Column(name = "GRUPO")
    private Double grupo;
    @Column(name = "FORNECEDOR")
    private Double fornecedor;
    @Column(name = "DESCRICAO")
    private String descricao;
    @Column(name = "UNIDADE")
    private String unidade;
    @Column(name = "PRECO_VENDA")
    private Double precoVenda;
    @Column(name = "PRECO_CUSTO")
    private Double precoCusto;
    @Column(name = "CUSTO_MEDIO")
    private Double custoMedio;
    @Column(name = "MARGEM")
    private Double margem;
    @Column(name = "ST")
    private Double st;
    @Column(name = "LUCRO")
    private Double lucro;
    @Column(name = "ESTOFI")
    private Double estofi;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    public Produto() {
    }

    public Produto(Integer id) {
        this.id = id;
    }

    public Double getCodigo() {
        return codigo;
    }

    public void setCodigo(Double codigo) {
        this.codigo = codigo;
    }

    public String getCodbar() {
        return codbar;
    }

    public void setCodbar(String codbar) {
        this.codbar = codbar;
    }

    public Double getGrupo() {
        return grupo;
    }

    public void setGrupo(Double grupo) {
        this.grupo = grupo;
    }

    public Double getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Double fornecedor) {
        this.fornecedor = fornecedor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public Double getPrecoVenda() {
        return precoVenda;
    }

    public void setPrecoVenda(Double precoVenda) {
        this.precoVenda = precoVenda;
    }

    public Double getPrecoCusto() {
        return precoCusto;
    }

    public void setPrecoCusto(Double precoCusto) {
        this.precoCusto = precoCusto;
    }

    public Double getCustoMedio() {
        return custoMedio;
    }

    public void setCustoMedio(Double custoMedio) {
        this.custoMedio = custoMedio;
    }

    public Double getMargem() {
        return margem;
    }

    public void setMargem(Double margem) {
        this.margem = margem;
    }

    public Double getSt() {
        return st;
    }

    public void setSt(Double st) {
        this.st = st;
    }

    public Double getLucro() {
        return lucro;
    }

    public void setLucro(Double lucro) {
        this.lucro = lucro;
    }

    public Double getEstofi() {
        return estofi;
    }

    public void setEstofi(Double estofi) {
        this.estofi = estofi;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produto)) {
            return false;
        }
        Produto other = (Produto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test.Produto[id=" + id + "]";
    }

}
