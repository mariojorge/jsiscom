/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package test.Imports;

import java.util.Observable;
import java.util.Observer;

public class Observador extends Observable implements Runnable {

    public Observador(Observer observador) {
        // Adiciona o objeto observador a lista de observadores
        addObserver(observador);
    }

    public void run() {
        int i;
        for (i = 0; i <= 10; i++) {
        // Notifica o processamento a cada 10 iterações
            if ((i % 10 == 0)) {
                notifyObservers(new Integer(i));
                setChanged();
            }
        }

        // Notifica fim do processo
        notifyObservers(new Boolean(true));
        setChanged();
    }
}