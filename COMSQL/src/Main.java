/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import comsql.JPA.EntityManagerProvider;
import frames.Auxiliar.LoginDialog;
import frames.DbConfigDialog;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.jvnet.substance.skin.BusinessBlackSteelSkin;
import org.jvnet.substance.skin.ChallengerDeepSkin;
import org.jvnet.substance.skin.CremeSkin;
import org.jvnet.substance.skin.DustCoffeeSkin;
import org.jvnet.substance.skin.EmeraldDuskSkin;
import org.jvnet.substance.skin.MagmaSkin;
import org.jvnet.substance.skin.MistAquaSkin;
import org.jvnet.substance.skin.ModerateSkin;
import org.jvnet.substance.skin.OfficeBlue2007Skin;
import org.jvnet.substance.skin.SubstanceTwilightLookAndFeel;
import org.jvnet.substance.skin.TwilightSkin;
import util.ReadWritePropertiesFile;

/**
 *
 * @author usuario
 */
public class Main {
    /**
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        
        // Inicializa icone da barra do relogio
        int defaultSkin = 0;
        
        Properties config = ReadWritePropertiesFile.Read("config.properties");
        try {
            defaultSkin = Integer.valueOf(config.getProperty("defaultSkin"));
        } catch (Exception e) {
            defaultSkin = 0;
        }

        if (defaultSkin == 0) {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedLookAndFeelException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if (defaultSkin > 0) {
            try {
                UIManager.setLookAndFeel(new SubstanceTwilightLookAndFeel());
                if (defaultSkin == 1)
                    SubstanceTwilightLookAndFeel.setSkin(new BusinessBlackSteelSkin());
                if (defaultSkin == 2)
                    SubstanceTwilightLookAndFeel.setSkin(new CremeSkin());
                if (defaultSkin == 3)
                    SubstanceTwilightLookAndFeel.setSkin(new ModerateSkin());
                if (defaultSkin == 4)
                    SubstanceTwilightLookAndFeel.setSkin(new TwilightSkin());
                if (defaultSkin == 5)
                    SubstanceTwilightLookAndFeel.setSkin(new DustCoffeeSkin());
                if (defaultSkin == 6)
                    SubstanceTwilightLookAndFeel.setSkin(new ChallengerDeepSkin());
                if (defaultSkin == 7)
                    SubstanceTwilightLookAndFeel.setSkin(new EmeraldDuskSkin());
                if (defaultSkin == 8)
                    SubstanceTwilightLookAndFeel.setSkin(new MagmaSkin());
                if (defaultSkin == 9)
                    SubstanceTwilightLookAndFeel.setSkin(new MistAquaSkin());
                if (defaultSkin == 10)
                    SubstanceTwilightLookAndFeel.setSkin(new OfficeBlue2007Skin());
                JFrame.setDefaultLookAndFeelDecorated(true);
                JDialog.setDefaultLookAndFeelDecorated(true);
            } catch (UnsupportedLookAndFeelException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                
                //Licence licence = new Licence();
                //licence.Verify();
                
                try {
                    EntityManagerProvider.getEntityManagerFactory();
                } catch (Exception e) {
                    e.printStackTrace();
                    DbConfigDialog dbcdialog = new DbConfigDialog(null, true, null);
                    dbcdialog.setLocationRelativeTo(null);
                    dbcdialog.setVisible(true);
                }

                LoginDialog login = new LoginDialog(null, false);
                login.setLocationRelativeTo(null);
                login.setVisible(true);

            }
        });
    }
}
