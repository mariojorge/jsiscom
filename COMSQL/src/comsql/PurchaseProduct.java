/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "purchase_product")
@NamedQueries({@NamedQuery(name = "PurchaseProduct.findAll", query = "SELECT p FROM PurchaseProduct p"), @NamedQuery(name = "PurchaseProduct.findByPurchaseId", query = "SELECT p FROM PurchaseProduct p WHERE p.purchaseProductPK.purchaseId = :purchaseId"), @NamedQuery(name = "PurchaseProduct.findByProductId", query = "SELECT p FROM PurchaseProduct p WHERE p.purchaseProductPK.productId = :productId"), @NamedQuery(name = "PurchaseProduct.findByQuantity", query = "SELECT p FROM PurchaseProduct p WHERE p.quantity = :quantity"), @NamedQuery(name = "PurchaseProduct.findByPrice", query = "SELECT p FROM PurchaseProduct p WHERE p.price = :price")})
public class PurchaseProduct implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PurchaseProductPK purchaseProductPK;
    @Column(name = "quantity")
    private Integer quantity;
    @Column(name = "price")
    private Double price;
    @JoinColumn(name = "product_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Product product;
    @JoinColumn(name = "purchase_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Purchase purchase;

    public PurchaseProduct() {
    }

    public PurchaseProduct(PurchaseProductPK purchaseProductPK) {
        this.purchaseProductPK = purchaseProductPK;
    }

    public PurchaseProduct(int purchaseId, int productId) {
        this.purchaseProductPK = new PurchaseProductPK(purchaseId, productId);
    }

    public PurchaseProductPK getPurchaseProductPK() {
        return purchaseProductPK;
    }

    public void setPurchaseProductPK(PurchaseProductPK purchaseProductPK) {
        this.purchaseProductPK = purchaseProductPK;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Purchase getPurchase() {
        return purchase;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (purchaseProductPK != null ? purchaseProductPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PurchaseProduct)) {
            return false;
        }
        PurchaseProduct other = (PurchaseProduct) object;
        if ((this.purchaseProductPK == null && other.purchaseProductPK != null) || (this.purchaseProductPK != null && !this.purchaseProductPK.equals(other.purchaseProductPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "comsql.PurchaseProduct[purchaseProductPK=" + purchaseProductPK + "]";
    }

}
