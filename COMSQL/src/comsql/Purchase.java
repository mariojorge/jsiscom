/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "purchase")
@NamedQueries({@NamedQuery(name = "Purchase.findAll", query = "SELECT p FROM Purchase p"), @NamedQuery(name = "Purchase.findById", query = "SELECT p FROM Purchase p WHERE p.id = :id"), @NamedQuery(name = "Purchase.findByNotenumber", query = "SELECT p FROM Purchase p WHERE p.notenumber = :notenumber"), @NamedQuery(name = "Purchase.findByDate", query = "SELECT p FROM Purchase p WHERE p.date = :date"), @NamedQuery(name = "Purchase.findByIcms", query = "SELECT p FROM Purchase p WHERE p.icms = :icms"), @NamedQuery(name = "Purchase.findBySubicms", query = "SELECT p FROM Purchase p WHERE p.subicms = :subicms"), @NamedQuery(name = "Purchase.findByFreight", query = "SELECT p FROM Purchase p WHERE p.freight = :freight"), @NamedQuery(name = "Purchase.findBySafe", query = "SELECT p FROM Purchase p WHERE p.safe = :safe"), @NamedQuery(name = "Purchase.findByIpi", query = "SELECT p FROM Purchase p WHERE p.ipi = :ipi")})
public class Purchase implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "notenumber")
    private String notenumber;
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Column(name = "icms")
    private Double icms;
    @Column(name = "subicms")
    private Double subicms;
    @Column(name = "freight")
    private Double freight;
    @Column(name = "safe")
    private Double safe;
    @Column(name = "ipi")
    private Double ipi;
    @Column(name = "total")
    private Double total;
    @JoinColumn(name = "purchasetype_id", referencedColumnName = "id")
    @ManyToOne
    private PurchaseType purchasetypeId;
    @JoinColumn(name = "supplier_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Supplier supplierId;
    @OneToMany(mappedBy = "purchaseId")
    private List<PayAccount> payAccountCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "purchase")
    private List<PurchaseProduct> purchaseProductCollection;

    public Purchase() {
    }

    public Purchase(Integer id) {
        this.id = id;
    }

    public Purchase(Integer id, String notenumber) {
        this.id = id;
        this.notenumber = notenumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNotenumber() {
        return notenumber;
    }

    public void setNotenumber(String notenumber) {
        this.notenumber = notenumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getIcms() {
        return icms;
    }

    public void setIcms(Double icms) {
        this.icms = icms;
    }

    public Double getSubicms() {
        return subicms;
    }

    public void setSubicms(Double subicms) {
        this.subicms = subicms;
    }

    public Double getFreight() {
        return freight;
    }

    public void setFreight(Double freight) {
        this.freight = freight;
    }

    public Double getSafe() {
        return safe;
    }

    public void setSafe(Double safe) {
        this.safe = safe;
    }

    public Double getIpi() {
        return ipi;
    }

    public void setIpi(Double ipi) {
        this.ipi = ipi;
    }

    public PurchaseType getPurchasetypeId() {
        return purchasetypeId;
    }

    public void setPurchasetypeId(PurchaseType purchasetypeId) {
        this.purchasetypeId = purchasetypeId;
    }

    public Supplier getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Supplier supplierId) {
        this.supplierId = supplierId;
    }

    public List<PayAccount> getPayAccountCollection() {
        return payAccountCollection;
    }

    public void setPayAccountCollection(List<PayAccount> payAccountCollection) {
        this.payAccountCollection = payAccountCollection;
    }

    public List<PurchaseProduct> getPurchaseProductCollection() {
        return purchaseProductCollection;
    }

    public void setPurchaseProductCollection(List<PurchaseProduct> purchaseProductCollection) {
        this.purchaseProductCollection = purchaseProductCollection;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getTaxesTotal() {
        return this.getIcms() + this.getIpi() + this.getSubicms();
    }
    
    public Double getProductsTotal() {
        Double t = 0.0;
        List<PurchaseProduct> products = this.getPurchaseProductCollection();
        if (products != null) {
            for (int i=0; i<products.size(); i++)
                t += products.get(i).getPrice() * products.get(i).getQuantity();
        }
        return t;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Purchase)) {
            return false;
        }
        Purchase other = (Purchase) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "comsql.Purchase[id=" + id + "]";
    }

}
