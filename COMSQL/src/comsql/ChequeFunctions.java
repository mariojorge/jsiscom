/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import comsql.JPA.ChequeJpaController;

/**
 *
 * @author Gilderlan
 */
public class ChequeFunctions {
    private static ChequeFunctions cf;
    private ChequeJpaController cjpac;

    private ChequeFunctions () {
        cjpac = ChequeJpaController.getInstance();
    }

    public static ChequeFunctions getInstance () {
        if (cf == null) {
            cf = new ChequeFunctions();
        }
        return cf;
    }

    public Boolean Create (Cheque c) {
        cjpac.create(c);
        return true;
    }

}
