/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import comsql.JPA.AddressJpaController;
import comsql.JPA.OfficialJpaController;
import comsql.JPA.exceptions.NonexistentEntityException;
import java.util.List;

/**
 *
 * @author Gilderlan
 */
public class OfficialFunctions {
    private static OfficialFunctions ff;
    private OfficialJpaController ojpac;

    private OfficialFunctions () {
        ojpac = OfficialJpaController.getInstance();
    }

    public static OfficialFunctions getInstance () {
        if (ff == null) {
            ff = new OfficialFunctions();
        }
        return ff;
    }

    public Boolean Create (Official o, Address a) {
        try {
            AddressJpaController.getInstance().create(a);
            o.setAddressId(a);
            ojpac.create(o);
            LogFunctions.getInstance().Insert("Cadastrou o funcionário com código = " + String.valueOf(o.getId()));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public Boolean Edit(Official o, Address a) {
        try {
            AddressJpaController.getInstance().edit(a);
        } catch (NonexistentEntityException ex) {
            return false;
        } catch (Exception ex) {
            return false;
        }
        try {
            ojpac.edit(o);
            LogFunctions.getInstance().Insert("Modificou o cadastro do funcionário com código = " + String.valueOf(o.getId()));
        } catch (comsql.exceptions.NonexistentEntityException ex) {
            return false;
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public Boolean Destroy(int officialId) {
        try {
            ojpac.destroy(officialId);
            LogFunctions.getInstance().Insert("Apagou o cadastro do funcionário com código = " + String.valueOf(officialId));
        } catch (comsql.exceptions.NonexistentEntityException ex) {
            return false;
        }
        return true;
    }

    public List FindOfficials() {
        return ojpac.findOfficialEntities();
    }
}
