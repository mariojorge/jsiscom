/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "receivingtype")
@NamedQueries({@NamedQuery(name = "ReceivingType.findAll", query = "SELECT r FROM ReceivingType r"), @NamedQuery(name = "ReceivingType.findById", query = "SELECT r FROM ReceivingType r WHERE r.id = :id"), @NamedQuery(name = "ReceivingType.findByDescription", query = "SELECT r FROM ReceivingType r WHERE r.description = :description")})
public class ReceivingType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "description")
    private String description;
    @Column(name = "type")
    private Integer type;    
    @OneToMany(mappedBy = "receivingtypeId")
    private List<PayedPlot> payedPlotCollection;
    @OneToMany(mappedBy = "receivingtypeId")
    private List<ReceptedPlot> receptedPlotCollection;

    public ReceivingType() {
    }

    public ReceivingType(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<PayedPlot> getPayedPlotCollection() {
        return payedPlotCollection;
    }

    public void setPayedPlotCollection(List<PayedPlot> payedPlotCollection) {
        this.payedPlotCollection = payedPlotCollection;
    }

    public List<ReceptedPlot> getReceptedPlotCollection() {
        return receptedPlotCollection;
    }

    public void setReceptedPlotCollection(List<ReceptedPlot> receptedPlotCollection) {
        this.receptedPlotCollection = receptedPlotCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReceivingType)) {
            return false;
        }
        ReceivingType other = (ReceivingType) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "comsql.ReceivingType[id=" + id + "]";
    }

}
