/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "payplot")
@NamedQueries({@NamedQuery(name = "PayPlot.findAll", query = "SELECT p FROM PayPlot p"), @NamedQuery(name = "PayPlot.findById", query = "SELECT p FROM PayPlot p WHERE p.id = :id"), @NamedQuery(name = "PayPlot.findByNumber", query = "SELECT p FROM PayPlot p WHERE p.number = :number"), @NamedQuery(name = "PayPlot.findByDate", query = "SELECT p FROM PayPlot p WHERE p.date = :date"), @NamedQuery(name = "PayPlot.findByAmount", query = "SELECT p FROM PayPlot p WHERE p.amount = :amount")})
public class PayPlot implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "number")
    private String number;
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Column(name = "amount")
    private Double amount;
    @Column(name = "penalty")
    private Double penalty;
    @Column(name = "interestingrate")
    private Double interestingrate;
    @Column(name = "discount")
    private Double discount;
    @JoinColumn(name = "payaccount_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private PayAccount payaccountId;

    public PayPlot() {
    }

    public PayPlot(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getInterestingrate() {
        return interestingrate;
    }

    public void setInterestingrate(Double interestingrate) {
        this.interestingrate = interestingrate;
    }

    public Double getPenalty() {
        return penalty;
    }

    public void setPenalty(Double penalty) {
        this.penalty = penalty;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public PayAccount getPayaccountId() {
        return payaccountId;
    }

    public void setPayaccountId(PayAccount payaccountId) {
        this.payaccountId = payaccountId;
    }

    public Double getTotal() {
        return this.getAmount() + this.getPenalty() + this.getInterestingrate() - this.getDiscount();
    }

    public Double getOriginTotal() {
        return this.getAmount();
    }

    public Boolean isPayed() {
        List<PayedPlot> payedPlots = this.payaccountId.getPayedPlotCollection();
        for (int i=0; i<payedPlots.size(); i++) {
            if (payedPlots.get(i).getNumber().compareTo(this.getNumber()) == 0)
                return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PayPlot)) {
            return false;
        }
        PayPlot other = (PayPlot) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "comsql.PayPlot[id=" + id + "]";
    }

}
