/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import comsql.JPA.UserJpaController;
import comsql.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gilderlan
 */
public class UserFuctions {
    private static UserFuctions uf;
    private UserJpaController ujpac;

    private UserFuctions() {
        ujpac = UserJpaController.getInstance();
    }

    public static UserFuctions getInstance (){
        if (uf == null) {
            uf = new UserFuctions();
        }
        return uf;
    }

    public Boolean Create(User u) {
        ujpac.create(u);
        return true;
    }

    public Boolean Edit(User u) {
        try {
            ujpac.edit(u);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(UserFuctions.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(UserFuctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    public Boolean Destroy(User u) {
        try {
            ujpac.destroy(u.getId());
        } catch (NonexistentEntityException ex) {
            return false;
        }
        return true;
    }

    public Boolean Destroy(int UserId) {
        try {
            ujpac.destroy(UserId);
        } catch (NonexistentEntityException ex) {
            return false;
        }
        return true;
    }

    public User findUser(int userId) {
        return ujpac.findUser(userId);
    }

    public List findUsers() {
        return ujpac.findUserEntities();
    }
}
