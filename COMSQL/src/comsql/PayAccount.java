/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import oracle.toplink.essentials.config.HintValues;
import oracle.toplink.essentials.config.TopLinkQueryHints;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "payaccount")
@NamedQueries({@NamedQuery(name = "PayAccount.findAll", query = "SELECT p FROM PayAccount p", hints={@QueryHint(name=TopLinkQueryHints.REFRESH,value=HintValues.TRUE)}), @NamedQuery(name = "PayAccount.findById", query = "SELECT p FROM PayAccount p WHERE p.id = :id", hints={@QueryHint(name=TopLinkQueryHints.REFRESH,value=HintValues.TRUE)}), @NamedQuery(name = "PayAccount.findByDate", query = "SELECT p FROM PayAccount p WHERE p.date = :date", hints={@QueryHint(name=TopLinkQueryHints.REFRESH,value=HintValues.TRUE)})})
public class PayAccount implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @JoinColumn(name = "supplier_id", referencedColumnName = "id")
    @ManyToOne
    private Supplier supplierId;
    @JoinColumn(name = "purchase_id", referencedColumnName = "id")
    @ManyToOne
    private Purchase purchaseId;
    @OneToMany(mappedBy = "payaccountId")
    private List<PayedPlot> payedPlotCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "payaccountId")
    private List<PayPlot> payPlotCollection;
    @Column(name = "concerning")
    private String concerning;

    public PayAccount() {
    }

    public PayAccount(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        Integer oldId = this.id;
        this.id = id;
        changeSupport.firePropertyChange("id", oldId, id);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        Date oldDate = this.date;
        this.date = date;
        changeSupport.firePropertyChange("date", oldDate, date);
    }

    public Supplier getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Supplier supplierId) {
        Supplier oldSupplierId = this.supplierId;
        this.supplierId = supplierId;
        changeSupport.firePropertyChange("supplierId", oldSupplierId, supplierId);
    }

    public Purchase getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(Purchase purchaseId) {
        Purchase oldPurchaseId = this.purchaseId;
        this.purchaseId = purchaseId;
        changeSupport.firePropertyChange("purchaseId", oldPurchaseId, purchaseId);
    }

    public List<PayedPlot> getPayedPlotCollection() {
        return payedPlotCollection;
    }

    public void setPayedPlotCollection(List<PayedPlot> payedPlotCollection) {
        this.payedPlotCollection = payedPlotCollection;
    }

    public List<PayPlot> getPayPlotCollection() {
        return payPlotCollection;
    }

    public void setPayPlotCollection(List<PayPlot> payPlotCollection) {
        this.payPlotCollection = payPlotCollection;
    }

    public String getConcerning() {
        return concerning;
    }

    public void setConcerning(String concerning) {
        this.concerning = concerning;
    }

    public Double getTotal() {
        Double total = 0.0;
        for (int i = 0; i < this.getPayPlotCollection().size(); i++) {
            total += this.getPayPlotCollection().get(i).getTotal();
        }
        return total;
    }

    public Double getOriginTotal() {
        Double total = 0.0;
        for (int i = 0; i < this.getPayPlotCollection().size(); i++) {
            total += this.getPayPlotCollection().get(i).getOriginTotal();
        }
        return total;
    }

    public Double getInPlot() {
        if (this.getPayPlotCollection().get(0).getNumber().compareTo("0") == 0)
            return this.getPayPlotCollection().get(0).getAmount();
        else
            return 0.0;
    }

    public Boolean hasInPlot() {
        if (this.getPayPlotCollection().get(0).getNumber().compareTo("0") == 0)
            return true;
        else
            return false;
    }

    public Double getPayedTotal() {
        Double total = 0.0;
        for (int i = 0; i < this.getPayedPlotCollection().size(); i++) {
            total += this.getPayedPlotCollection().get(i).getAmount();
        }
        return total;
    }

    public Double getBalance() {
        return this.getTotal() - this.getPayedTotal();
    }

    public int getPayPlotCount() {
        if (hasInPlot())
            return this.getPayPlotCollection().size() - 1;
        else
            return this.getPayPlotCollection().size();
    }

    public Boolean isPayed() {
        if (getBalance() == 0.0)
            return true;
        else
            return false;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PayAccount)) {
            return false;
        }
        PayAccount other = (PayAccount) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "comsql.PayAccount[id=" + id + "]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

}
