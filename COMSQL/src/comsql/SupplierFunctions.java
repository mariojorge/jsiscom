/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import comsql.JPA.AddressJpaController;
import comsql.JPA.SupplierJpaController;
import comsql.exceptions.IllegalOrphanException;
import comsql.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author usuario
 */
public class SupplierFunctions {
    private static SupplierFunctions sf;
    private AddressJpaController ajpa;
    private SupplierJpaController sjpa;

    private SupplierFunctions() {
        ajpa = AddressJpaController.getInstance();
        sjpa = SupplierJpaController.getInstance();
    }

    public static SupplierFunctions getInstance() {
        if (sf == null) {
            sf = new SupplierFunctions();
        }
        return sf;
    }

    public Boolean Create(Supplier s, Address a) {
        ajpa.create(a);
        s.setAddressId(a);
        sjpa.create(s);
        return true;
    }

    public Boolean Edit(Supplier s, Address a) {
        try {
            ajpa.edit(a);
        } catch (comsql.JPA.exceptions.NonexistentEntityException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            sjpa.edit(s);
        } catch (IllegalOrphanException ex) {
            ex.printStackTrace();
            return false;
        } catch (NonexistentEntityException ex) {
            ex.printStackTrace();
            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return true;
    }

    public Boolean Destroy(int supplierId) {
        try {
            SupplierJpaController.getInstance().destroy(supplierId);
            LogFunctions.getInstance().Insert("Apagou o cadastro do fornecedor de código = " + String.valueOf(supplierId));
        } catch (comsql.JPA.exceptions.IllegalOrphanException ex) {
            return false;
        } catch (comsql.JPA.exceptions.NonexistentEntityException ex) {
            return false;
        }
        return true;
    }

    public Supplier getSupplier(int i) {
        return sjpa.findSupplier(i);
    }

    public Supplier getSupplierByName(String name) {
        String strQuery = null;
        strQuery = "select s from Supplier as s WHERE s.name = :name";
        Query query = sjpa.getEntityManager().createQuery(strQuery);
        query.setParameter("name", name);
        query.setMaxResults(1);
        query.setHint("toplink.refresh", "true");
        return (Supplier) query.getSingleResult();
    }

    public List findSuppliers() {
        return sjpa.findSupplierEntities();
    }

    public List findSuppliersByName(String name) {
        String strQuery = null;
        strQuery = "select s from Supplier as s WHERE s.name LIKE :name";
        Query query = sjpa.getEntityManager().createQuery(strQuery);
        query.setParameter("name", '%' + name + '%');
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findSuppliers(String order) {
        String strQuery = null;
        if (order.compareTo("name") == 0)
             strQuery = "select s from Supplier as s ORDER BY s.name";
        Query query = sjpa.getEntityManager().createQuery(strQuery);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }
    
    public List findSuppliersByFantasyName(String fantasyName) {
        String strQuery = null;
        strQuery = "select s from Supplier as s WHERE s.fantasyname LIKE :name";
        Query query = sjpa.getEntityManager().createQuery(strQuery);
        query.setParameter("name", '%' + fantasyName + '%');
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findSuppliersByCPF(String cpf) {
        String strQuery = null;
        strQuery = "select s from Supplier as s WHERE s.cpf = :cpf";
        Query query = sjpa.getEntityManager().createQuery(strQuery);
        query.setParameter("cpf", cpf);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findSuppliersByCNPJ(String cnpj) {
        String strQuery = null;
        strQuery = "select s from Supplier as s WHERE s.cnpj = :cnpj";
        Query query = sjpa.getEntityManager().createQuery(strQuery);
        query.setParameter("cnpj", cnpj);
        return query.getResultList();
    }

    public List findSuppliersByRG(String rg) {
        String strQuery = null;
        strQuery = "select s from Supplier as s WHERE s.rg = :rg";
        Query query = sjpa.getEntityManager().createQuery(strQuery);
        query.setParameter("rg", rg);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findSuppliersByIE(String ie) {
        String strQuery = null;
        strQuery = "select s from Supplier as s WHERE s.ie = :ie";
        Query query = sjpa.getEntityManager().createQuery(strQuery);
        query.setParameter("ie", ie);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

}
