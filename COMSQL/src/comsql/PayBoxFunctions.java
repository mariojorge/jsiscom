/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import comsql.JPA.PBMoveJpaController;
import comsql.JPA.PBOpenJpaController;
import comsql.JPA.PayBoxJpaController;
import util.*;
import comsql.exceptions.NonexistentEntityException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author usuario
 */
public class PayBoxFunctions {
    PayBox pb = null;
    private static PayBoxFunctions pbfunctions;
    
    public PayBoxFunctions() {
        pb = getPayBox();
    }

    public PayBox getPayBox() {
        try {
            String hdserial = HdSerial.getHDSerial("C");
            String strQuery = "select p from PayBox as p WHERE p.cpuserial = :hdserial";
            PayBoxJpaController pbjpa = PayBoxJpaController.getInstance();
            Query query = pbjpa.getEntityManager().createQuery(strQuery);
            query.setParameter("hdserial", hdserial);
            query.setHint("toplink.refresh", "true");
            return (PayBox) query.getSingleResult();
        } catch (NoResultException e) {
            //e.printStackTrace();
            return null;
        }
    }

    public PayBox getPayBox(int payBoxId) {
        return PayBoxJpaController.getInstance().findPayBox(payBoxId);
    }

    public List findPayBoxs() {
        return PayBoxJpaController.getInstance().findPayBoxEntities();
    }

    public String getPayBoxName (){
        if (this.pb != null) {
            return this.pb.getName();
        }
        return null;
    }

    public PBOpen getLastPBOpen() {
        try {
            String strQuery = "SELECT p FROM PBOpen p WHERE p.pbId = :paybox ORDER BY p.id DESC";
            PayBoxJpaController pbjpa = PayBoxJpaController.getInstance();
            Query query = pbjpa.getEntityManager().createQuery(strQuery);
            query.setParameter("paybox", pb);
            query.setHint("toplink.refresh", "true");
            query.setMaxResults(1);
            return (PBOpen) query.getSingleResult();
        } catch (NoResultException noe) {
            return null;
        }
    }

    public PBOpen getLastPBOpen(PayBox payBox) {
        try {
            String strQuery = "SELECT p FROM PBOpen p WHERE p.pbId = :paybox ORDER BY p.id DESC";
            PayBoxJpaController pbjpa = PayBoxJpaController.getInstance();
            Query query = pbjpa.getEntityManager().createQuery(strQuery);
            query.setParameter("paybox", payBox);
            query.setHint("toplink.refresh", "true");
            query.setMaxResults(1);
            return (PBOpen) query.getSingleResult();
        } catch (NoResultException noe) {
            return null;
        }
    }

    public Boolean isOpen() {
        pb = getPayBox();
        if (pb != null) {
            if (pb.getIsopen() == 1)
                return true;
        }
        return false;
    }

    // Abrir o caixa
    public Boolean toOpen(Date openDate, Date openTime, Double amount) {
        PayBoxJpaController pbjpa = PayBoxJpaController.getInstance();
        if (this.pb != null && !isOpen()) {
            pb.setIsopen(1);
            try {
                pbjpa.edit(pb);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(PayBoxFunctions.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(PayBoxFunctions.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }

            if (isOpen()) {
                // Salva a abertura no bd
                PBOpen pbopen = new PBOpen();
                pbopen.setOpeneddate(openDate);
                pbopen.setOpenedtime(openTime);
                pbopen.setPbId(pb);
                PBOpenJpaController pbojpa = PBOpenJpaController.getInstance();
                pbojpa.create(pbopen);
                // Salva o movimento no bd
                PBMove pbmove = new PBMove();
                pbmove.setPbopenId(pbopen);
                pbmove.setMovimentdate(openDate);
                pbmove.setMovimenttime(openTime);
                pbmove.setDescription("ABERTURA DE CAIXA");
                pbmove.setDocnumber("");
                pbmove.setObservation("");
                pbmove.setAmount(amount);
                PBMoveJpaController pbmjpa = PBMoveJpaController.getInstance();
                pbmjpa.create(pbmove);
                return true;
            }
        }
        return false;
    }

    // Fechar o caixa
    public Boolean toClose(Date closeDate, Date closeTime, Double amount) {
        PayBoxJpaController pbjpa = PayBoxJpaController.getInstance();
        if (this.isOpen() && amount <= getBalance()) {
            pb.setIsopen(0);
            try {
                pbjpa.edit(pb);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(PayBoxFunctions.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(PayBoxFunctions.class.getName()).log(Level.SEVERE, null, ex);
            }

            // Salva o movimento no bd
            PBOpen pbopen = getLastPBOpen();
            if (pbopen != null) {
                PBMove pbmove = new PBMove();
                pbmove.setPbopenId(pbopen);
                pbmove.setMovimentdate(closeDate);
                pbmove.setMovimenttime(closeTime);
                pbmove.setDescription("ENCERRAMENTO DE CAIXA");
                pbmove.setDocnumber("");
                pbmove.setObservation("");
                Double transform = amount * -1;
                pbmove.setAmount(transform);
                PBMoveJpaController pbmjpa = PBMoveJpaController.getInstance();
                pbmjpa.create(pbmove);
                return true;
            }
        }
        return false;
    }

    // Recebimento
    public Boolean toRecept(Date pagamentDate, Date pagamentTime, String description,
            String docNumber, Double amount, String observation) {
        PBOpen pbopen = getLastPBOpen();
        if (pbopen != null && isOpen()) {
            PBMove pbmove = new PBMove();
            pbmove.setPbopenId(pbopen);
            pbmove.setMovimentdate(pagamentDate);
            pbmove.setMovimenttime(pagamentTime);
            pbmove.setDescription(description);
            pbmove.setDocnumber(docNumber);
            pbmove.setAmount(amount);
            pbmove.setObservation(observation);
            PBMoveJpaController pbmjpa = PBMoveJpaController.getInstance();
            pbmjpa.create(pbmove);
            return true;
        }
        return false;
    }

    // Pagamento
    public Boolean toPay(Date pagamentDate, Date pagamentTime, String description,
            String docNumber, Double amount, String observation) {
        if (this.pb != null && (getBalance() - amount) >= 0 && isOpen()) {
            PBOpen pbopen = getLastPBOpen();
            PBMove pbmove = new PBMove();
            pbmove.setPbopenId(pbopen);
            pbmove.setMovimentdate(pagamentDate);
            pbmove.setMovimenttime(pagamentTime);
            pbmove.setDescription(description);
            pbmove.setDocnumber(docNumber);
            Double transform = (amount * -1);
            pbmove.setAmount(transform);
            pbmove.setObservation(observation);
            PBMoveJpaController pbmjpa = PBMoveJpaController.getInstance();
            pbmjpa.create(pbmove);
            return true;
        }
        return false;
    }

    // Retornar Saldo
    public Double getBalance() {
        PBOpen pbopen = getLastPBOpen();
        Double balance = 0.0;
        if (pbopen != null) {
            List<PBMove> pbmoves = pbopen.getPBMoveCollection();
            for (int i=0; i<pbmoves.size(); i++ ) {
                balance += pbmoves.get(i).getAmount();
            }
        }
        return balance;
    }

    public Double getBalance(PBOpen open) {
        PBOpen pbopen = open;
        Double balance = 0.0;
        if (pbopen != null) {
            List<PBMove> pbmoves = pbopen.getPBMoveCollection();
            for (int i=0; i<pbmoves.size(); i++ ) {
                balance += pbmoves.get(i).getAmount();
            }
        }
        return balance;
    }
    
    // Reforço do caixa
    public Boolean toReforce(Date pagamentDate, Date pagamentTime, Double amount) {
        if (toRecept(pagamentDate, pagamentTime, "REFORÇO DE CAIXA", "", amount, ""))
            return true;
        else
            return false;
    }

    // Sangria
    public Boolean toBlood(Date pagamentDate, Date pagamentTime, Double amount) {
        if (toPay(pagamentDate, pagamentTime, "SANGRIA", "", amount, ""))
            return true;
        else
            return false;
    }

    public static PayBoxFunctions getInstance() {
        if (pbfunctions == null) {
            pbfunctions = new PayBoxFunctions();
        }
        return pbfunctions;
    }

    public void addPayBox(PayBox pb) {
        PayBoxJpaController pbjpa = PayBoxJpaController.getInstance();
        pbjpa.create(pb);
    }
}
