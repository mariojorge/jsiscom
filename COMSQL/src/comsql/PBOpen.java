/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "pbopen")
@NamedQueries({@NamedQuery(name = "PBOpen.findAll", query = "SELECT p FROM PBOpen p"), @NamedQuery(name = "PBOpen.findById", query = "SELECT p FROM PBOpen p WHERE p.id = :id"), @NamedQuery(name = "PBOpen.findByOpeneddate", query = "SELECT p FROM PBOpen p WHERE p.openeddate = :openeddate"), @NamedQuery(name = "PBOpen.findByOpenedtime", query = "SELECT p FROM PBOpen p WHERE p.openedtime = :openedtime")})
public class PBOpen implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "openeddate")
    @Temporal(TemporalType.DATE)
    private Date openeddate;
    @Column(name = "openedtime")
    @Temporal(TemporalType.TIME)
    private Date openedtime;
    @JoinColumn(name = "pb_id", referencedColumnName = "id")
    @ManyToOne
    private PayBox pbId;
    @OneToMany(mappedBy = "pbopenId")
    private List<PBMove> pBMoveCollection;

    public PBOpen() {
    }

    public PBOpen(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getOpeneddate() {
        return openeddate;
    }

    public void setOpeneddate(Date openeddate) {
        this.openeddate = openeddate;
    }

    public Date getOpenedtime() {
        return openedtime;
    }

    public void setOpenedtime(Date openedtime) {
        this.openedtime = openedtime;
    }

    public PayBox getPbId() {
        return pbId;
    }

    public void setPbId(PayBox pbId) {
        this.pbId = pbId;
    }

    public List<PBMove> getPBMoveCollection() {
        return pBMoveCollection;
    }

    public void setPBMoveCollection(List<PBMove> pBMoveCollection) {
        this.pBMoveCollection = pBMoveCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PBOpen)) {
            return false;
        }
        PBOpen other = (PBOpen) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "comsql.PBOpen[id=" + id + "]";
    }

}
