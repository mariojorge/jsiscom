/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "receptaccount")
@NamedQueries({@NamedQuery(name = "ReceptAccount.findAll", query = "SELECT r FROM ReceptAccount r"), @NamedQuery(name = "ReceptAccount.findById", query = "SELECT r FROM ReceptAccount r WHERE r.id = :id"), @NamedQuery(name = "ReceptAccount.findByDate", query = "SELECT r FROM ReceptAccount r WHERE r.date = :date")})
public class ReceptAccount implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Column(name = "concerning")
    private String concerning;
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    @ManyToOne
    private Customer customerId;
    @JoinColumn(name = "sale_id", referencedColumnName = "id")
    @ManyToOne
    private Sale saleId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "receptaccountId")
    private List<ReceptPlot> receptPlotCollection;
    @OneToMany(mappedBy = "receptaccountId")
    private List<ReceptedPlot> receptedPlotCollection;

    public ReceptAccount() {
    }

    public ReceptAccount(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getConcerning() {
        return concerning;
    }

    public void setConcerning(String concerning) {
        this.concerning = concerning;
    }

    public Double getTotal() {
        Double total = 0.0;
        for (int i = 0; i < this.getReceptPlotCollection().size(); i++) {
            total += this.getReceptPlotCollection().get(i).getTotal();
        }
        return total;
    }

    public Double getOriginTotal() {
        Double total = 0.0;
        for (int i = 0; i < this.getReceptPlotCollection().size(); i++) {
            total += this.getReceptPlotCollection().get(i).getOriginTotal();
        }
        return total;
    }

    public Double getPayedTotal() {
        Double total = 0.0;
        for (int i = 0; i < this.getReceptedPlotCollection().size(); i++) {
            total += this.getReceptedPlotCollection().get(i).getAmount();
        }
        return total;
    }

    public Double getBalance() {
        return this.getTotal() - this.getPayedTotal();
    }

    public int getPayPlotCount() {
        if (hasInPlot())
            return this.getReceptPlotCollection().size() - 1;
        else
            return this.getReceptPlotCollection().size();
    }

    public Boolean isPayed() {
        if (getBalance() == 0.0)
            return true;
        else
            return false;
    }

    public Boolean hasInPlot() {
        if (this.getReceptPlotCollection().get(0).getNumber().compareTo("0") == 0)
            return true;
        else
            return false;
    }

    public Double getInPlot() {
        if (this.getReceptPlotCollection().get(0).getNumber().compareTo("0") == 0)
            return this.getReceptPlotCollection().get(0).getAmount();
        else
            return 0.0;
    }
    public Customer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Customer customerId) {
        this.customerId = customerId;
    }

    public Sale getSaleId() {
        return saleId;
    }

    public void setSaleId(Sale saleId) {
        this.saleId = saleId;
    }

    public List<ReceptPlot> getReceptPlotCollection() {
        return receptPlotCollection;
    }

    public void setReceptPlotCollection(List<ReceptPlot> receptPlotCollection) {
        this.receptPlotCollection = receptPlotCollection;
    }

    public List<ReceptedPlot> getReceptedPlotCollection() {
        return receptedPlotCollection;
    }

    public void setReceptedPlotCollection(List<ReceptedPlot> receptedPlotCollection) {
        this.receptedPlotCollection = receptedPlotCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReceptAccount)) {
            return false;
        }
        ReceptAccount other = (ReceptAccount) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "comsql.ReceptAccount[id=" + id + "]";
    }

}
