/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "sale")
@NamedQueries({@NamedQuery(name = "Sale.findAll", query = "SELECT s FROM Sale s"), @NamedQuery(name = "Sale.findById", query = "SELECT s FROM Sale s WHERE s.id = :id"), @NamedQuery(name = "Sale.findByDate", query = "SELECT s FROM Sale s WHERE s.date = :date"), @NamedQuery(name = "Sale.findByDiscount", query = "SELECT s FROM Sale s WHERE s.discount = :discount")})
public class Sale implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Column(name = "origintotal")
    private Double originTotal;
    @Column(name = "discount")
    private Double discount;
    @Column(name = "total")
    private Double total;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sale")
    private List<SaleProduct> saleProductCollection;
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    @ManyToOne
    private Customer customerId;
    @JoinColumn(name = "saletype_id", referencedColumnName = "id")
    @ManyToOne
    private SaleType saletypeId;
    @OneToMany(mappedBy = "saleId")
    private List<ReceptAccount> receptAccountCollection;

    public Sale() {
    }

    public Sale(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getOrigintotal() {
        return originTotal;
    }

    public void setOrigintotal(Double origintotal) {
        this.originTotal = origintotal;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public List<SaleProduct> getSaleProductCollection() {
        return saleProductCollection;
    }

    public void setSaleProductCollection(List<SaleProduct> saleProductCollection) {
        this.saleProductCollection = saleProductCollection;
    }

    public Customer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Customer customerId) {
        this.customerId = customerId;
    }

    public SaleType getSaletypeId() {
        return saletypeId;
    }

    public void setSaletypeId(SaleType saletypeId) {
        this.saletypeId = saletypeId;
    }

    public List<ReceptAccount> getReceptAccountCollection() {
        return receptAccountCollection;
    }

    public void setReceptAccountCollection(List<ReceptAccount> receptAccountCollection) {
        this.receptAccountCollection = receptAccountCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sale)) {
            return false;
        }
        Sale other = (Sale) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "comsql.Sale[id=" + id + "]";
    }

}
