/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import comsql.JPA.ReceivingTypeJpaController;
import comsql.JPA.ReceptAccountJpaController;
import comsql.JPA.ReceptPlotJpaController;
import comsql.JPA.ReceptedPlotJpaController;
import comsql.JPA.exceptions.IllegalOrphanException;
import comsql.JPA.exceptions.NonexistentEntityException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Query;

/**
 *
 * @author usuario
 */
public class ReceptAccountFunctions {
    private static ReceptAccountFunctions raf;

    private ReceptAccountFunctions () {

    }

    public static ReceptAccountFunctions getInstance() {
        if (raf == null) {
            raf = new ReceptAccountFunctions();
        }
        return raf;
    }

    public Boolean create(ReceptAccount r, List<ReceptPlot> receptPlots ) {
        try {
            ReceptAccountJpaController.getInstance().create(r);
        } catch (Exception e) {
            return false;
        }
        for (int i = 0; i < receptPlots.size(); i++) {
            receptPlots.get(i).setReceptaccountId(r);
            ReceptPlotJpaController.getInstance().create(receptPlots.get(i));
        }

        if (receptPlots.get(0).getNumber().compareTo("0") == 0) {
            ReceptPlot inReceptPlot = receptPlots.get(0);
            ReceptedPlot inReceptedPlot = new ReceptedPlot();
            inReceptedPlot.setAmount(inReceptPlot.getAmount());
            inReceptedPlot.setDate(inReceptPlot.getDate());
            inReceptedPlot.setNumber(inReceptPlot.getNumber());
            inReceptedPlot.setReceivingtypeId(ReceivingTypeJpaController.getInstance().findReceivingType(1));
            inReceptedPlot.setReceptaccountId(r);
            ReceptedPlotJpaController.getInstance().create(inReceptedPlot);
        }
        return true;
    }

        public Boolean delete(int id) {
        ReceptAccount receptAccount = ReceptAccountJpaController.getInstance().findReceptAccount(id);
        int i = 0;
        if (receptAccount.getReceptedPlotCollection() != null) {
            List<ReceptedPlot> receptedPlots = receptAccount.getReceptedPlotCollection();
            for (i=0; i<receptedPlots.size(); i++) {
                try {
                    ReceptedPlotJpaController.getInstance().destroy(receptedPlots.get(i).getId());
                } catch (comsql.exceptions.NonexistentEntityException ex) {
                    ex.printStackTrace();
                }
            }
        }
        if (receptAccount.getReceptPlotCollection() != null) {
            List<ReceptPlot> receptPlots = receptAccount.getReceptPlotCollection();
            for (i=0; i<receptPlots.size(); i++) {
                try {
                    ReceptPlotJpaController.getInstance().destroy(receptPlots.get(i).getId());
                } catch (comsql.exceptions.NonexistentEntityException ex) {
                    ex.printStackTrace();
                }

            }
        }
        try {
            ReceptAccountJpaController.getInstance().destroy(receptAccount.getId());
        } catch (IllegalOrphanException ex) {
            ex.printStackTrace();
        } catch (NonexistentEntityException ex) {
            ex.printStackTrace();
        }
        return true;
    }

    public List findReceptAccounts() {
        return ReceptAccountJpaController.getInstance().findReceptAccountEntities();
    }

    public ReceptAccount getReceptAccount(Sale s) {
        String strQuery = null;
        strQuery = "select p from ReceptAccount as p WHERE p.saleId = :sale";
        Query query = ReceptAccountJpaController.getInstance().getEntityManager().createQuery(strQuery);
        query.setParameter("sale", s);
        query.setHint("toplink.refresh", "true");
        return (ReceptAccount) query.getSingleResult();
    }

    public List findReceptAccountsByCustomerName(String customerName) {
        String strQuery = null;
        strQuery = "select p from ReceptAccount as p WHERE p.customerId.name LIKE :customerName ORDER BY p.id";
        Query query = ReceptAccountJpaController.getInstance().getEntityManager().createQuery(strQuery);
        query.setParameter("customerName", '%' + customerName + '%');
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findReceptAccountsByCustomerName(String customerName, Date initialDate, Date finalDate) {
        String strQuery = null;
        strQuery = "select p from ReceptAccount as p WHERE p.customerId.name LIKE :customerName AND p.date BETWEEN :initial AND :final ORDER BY p.id";
        Query query = ReceptAccountJpaController.getInstance().getEntityManager().createQuery(strQuery);
        query.setParameter("customerName", '%' + customerName + '%');
        query.setParameter("initial", initialDate);
        query.setParameter("final", finalDate);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findReceptAccountsByConcerning(String concerning) {
        String strQuery = null;
        strQuery = "select p from ReceptAccount as p WHERE p.concerning LIKE :concerning ORDER BY p.id";
        Query query = ReceptAccountJpaController.getInstance().getEntityManager().createQuery(strQuery);
        query.setParameter("concerning", '%' + concerning + '%');
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findReceptAccountsByPeriod(Date initialDate, Date finalDate) {
        String strQuery = null;
        strQuery = "select p from ReceptAccount as p WHERE p.date BETWEEN :initial AND :final ORDER BY p.id";
        Query query = ReceptAccountJpaController.getInstance().getEntityManager().createQuery(strQuery);
        query.setParameter("initial", initialDate);
        query.setParameter("final", finalDate);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public ReceptAccount getReceptAccount(int receptAccountId) {
        return ReceptAccountJpaController.getInstance().findReceptAccount(receptAccountId);
    }

    public List getReceptPlots(int receptAccountId) {
        ReceptAccount p = ReceptAccountJpaController.getInstance().findReceptAccount(receptAccountId);
        return p.getReceptPlotCollection();
    }

    public List getReceptedPlots(int receptAccountId) {
        ReceptAccount p = ReceptAccountJpaController.getInstance().findReceptAccount(receptAccountId);
        return p.getReceptedPlotCollection();
    }

    public Boolean toReceptAccount(ReceptPlot receptPlot, ReceptedPlot receptedPlot) {
        try {
            ReceptPlotJpaController.getInstance().edit(receptPlot);
        } catch (comsql.exceptions.NonexistentEntityException ex) {
            Logger.getLogger(ReceptAccountFunctions.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (Exception ex) {
            Logger.getLogger(ReceptAccountFunctions.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        try {
            ReceptedPlotJpaController.getInstance().create(receptedPlot);
            String description = "RECEBIMENTO - CÓD.: " + String.valueOf(receptedPlot.getReceptaccountId().getId()) +
                                    " - PAR.: " + receptedPlot.getNumber();
            PayBoxFunctions.getInstance().toRecept(new Date(), new Date(), description, String.valueOf(receptedPlot.getReceptaccountId().getId())
                                                        , receptedPlot.getAmount(), "");
        } catch (Exception ex) {
            Logger.getLogger(ReceptAccountFunctions.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public List findReceptedPlots() {
        return ReceptedPlotJpaController.getInstance().findReceptedPlotEntities();
    }

    public List findReceptedPlots(Boolean ordenaded) {
        String strQuery = null;
        if (ordenaded)
            strQuery = "select r from ReceptedPlot as r ORDER BY r.date DESC";
        else
            strQuery = "select r from ReceptedPlot as r";
        Query query = ReceptAccountJpaController.getInstance().getEntityManager().createQuery(strQuery);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findReceptedPlots(Date dInitial, Date dFinal, Boolean ordenaded) {
        String strQuery = null;
        if (ordenaded)
            strQuery = "select r from ReceptedPlot as r WHERE r.date BETWEEN :initial AND :final ORDER BY r.date DESC";
        else
            strQuery = "select r from ReceptedPlot as r WHERE r.date BETWEEN :initial AND :final";
        Query query = ReceptAccountJpaController.getInstance().getEntityManager().createQuery(strQuery);
        query.setParameter("initial", dInitial);
        query.setParameter("final", dFinal);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findReceptedPlots(Date dInitial, Date dFinal, Customer customer, Boolean ordenaded) {
        String strQuery = null;
        if (ordenaded)
            strQuery = "select r from ReceptedPlot as r WHERE r.receptaccountId.customerId = :customer AND r.date BETWEEN :initial AND :final ORDER BY r.date DESC";
        else
            strQuery = "select r from ReceptedPlot as r WHERE r.receptaccountId.customerId = :customer AND r.date BETWEEN :initial AND :final";
        Query query = ReceptAccountJpaController.getInstance().getEntityManager().createQuery(strQuery);
        query.setParameter("initial", dInitial);
        query.setParameter("final", dFinal);
        query.setParameter("customer", customer);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }
}
