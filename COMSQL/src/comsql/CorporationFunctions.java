/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import comsql.JPA.AddressJpaController;
import comsql.JPA.CorporationJpaController;
import comsql.exceptions.NonexistentEntityException;

/**
 *
 * @author usuario
 */
public class CorporationFunctions {
    private static CorporationFunctions corpfunctions;
    CorporationJpaController cjpa;
    AddressJpaController ajpa;
    
    private CorporationFunctions() {
        cjpa = CorporationJpaController.getInstance();
        ajpa = AddressJpaController.getInstance();
    }

    public static CorporationFunctions getInstance() {
        if (corpfunctions == null) {
            corpfunctions = new CorporationFunctions();
        }
        return corpfunctions;
    }

    public Corporation getCorporation() {
        return cjpa.findCorporation(1);
    }

    public Boolean edit(Corporation c, Address a) {
        try {
            ajpa.edit(a);
        } catch (NonexistentEntityException ex) {
            ex.printStackTrace();
            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            cjpa.edit(c);
            LogFunctions.getInstance().Insert("Modificou os dados sobre a empresa.");
        } catch (NonexistentEntityException ex) {
            ex.printStackTrace();
            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return true;
    }

    public String getPass() {
        return cjpa.findCorporation(1).getPass();
    }
}
