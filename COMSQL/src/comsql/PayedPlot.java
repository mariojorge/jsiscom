/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "payedplot")
@NamedQueries({@NamedQuery(name = "PayedPlot.findAll", query = "SELECT p FROM PayedPlot p"), @NamedQuery(name = "PayedPlot.findById", query = "SELECT p FROM PayedPlot p WHERE p.id = :id"), @NamedQuery(name = "PayedPlot.findByNumber", query = "SELECT p FROM PayedPlot p WHERE p.number = :number"), @NamedQuery(name = "PayedPlot.findByDate", query = "SELECT p FROM PayedPlot p WHERE p.date = :date"), @NamedQuery(name = "PayedPlot.findByAmount", query = "SELECT p FROM PayedPlot p WHERE p.amount = :amount")})
public class PayedPlot implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "number")
    private String number;
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Column(name = "amount")
    private Double amount;
    @JoinColumn(name = "payaccount_id", referencedColumnName = "id")
    @ManyToOne
    private PayAccount payaccountId;
    @JoinColumn(name = "receivingtype_id", referencedColumnName = "id")
    @ManyToOne
    private ReceivingType receivingtypeId;

    public PayedPlot() {
    }

    public PayedPlot(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        Integer oldId = this.id;
        this.id = id;
        changeSupport.firePropertyChange("id", oldId, id);
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        String oldNumber = this.number;
        this.number = number;
        changeSupport.firePropertyChange("number", oldNumber, number);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        Date oldDate = this.date;
        this.date = date;
        changeSupport.firePropertyChange("date", oldDate, date);
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        Double oldAmount = this.amount;
        this.amount = amount;
        changeSupport.firePropertyChange("amount", oldAmount, amount);
    }

    public PayAccount getPayaccountId() {
        return payaccountId;
    }

    public void setPayaccountId(PayAccount payaccountId) {
        PayAccount oldPayaccountId = this.payaccountId;
        this.payaccountId = payaccountId;
        changeSupport.firePropertyChange("payaccountId", oldPayaccountId, payaccountId);
    }

    public ReceivingType getReceivingtypeId() {
        return receivingtypeId;
    }

    public void setReceivingtypeId(ReceivingType receivingtypeId) {
        ReceivingType oldReceivingtypeId = this.receivingtypeId;
        this.receivingtypeId = receivingtypeId;
        changeSupport.firePropertyChange("receivingtypeId", oldReceivingtypeId, receivingtypeId);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PayedPlot)) {
            return false;
        }
        PayedPlot other = (PayedPlot) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "comsql.PayedPlot[id=" + id + "]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

}
