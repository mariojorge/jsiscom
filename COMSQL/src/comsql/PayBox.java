/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "paybox")
@NamedQueries({@NamedQuery(name = "PayBox.findAll", query = "SELECT p FROM PayBox p"), @NamedQuery(name = "PayBox.findById", query = "SELECT p FROM PayBox p WHERE p.id = :id"), @NamedQuery(name = "PayBox.findByName", query = "SELECT p FROM PayBox p WHERE p.name = :name"), @NamedQuery(name = "PayBox.findByIsopen", query = "SELECT p FROM PayBox p WHERE p.isopen = :isopen"), @NamedQuery(name = "PayBox.findByCpuserial", query = "SELECT p FROM PayBox p WHERE p.cpuserial = :cpuserial")})
public class PayBox implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "isopen")
    private Integer isopen;
    @Basic(optional = false)
    @Column(name = "cpuserial")
    private String cpuserial;
    @OneToMany(mappedBy = "pbId")
    private List<PBOpen> pBOpenCollection;

    public PayBox() {
    }

    public PayBox(Integer id) {
        this.id = id;
    }

    public PayBox(Integer id, String cpuserial) {
        this.id = id;
        this.cpuserial = cpuserial;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIsopen() {
        return isopen;
    }

    public void setIsopen(Integer isopen) {
        this.isopen = isopen;
    }

    public String getCpuserial() {
        return cpuserial;
    }

    public void setCpuserial(String cpuserial) {
        this.cpuserial = cpuserial;
    }

    public Boolean isOpen() {
        if (this.getIsopen() == 1)
            return true;
        else
            return false;
    }
    public List<PBOpen> getPBOpenCollection() {
        return pBOpenCollection;
    }

    public void setPBOpenCollection(List<PBOpen> pBOpenCollection) {
        this.pBOpenCollection = pBOpenCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PayBox)) {
            return false;
        }
        PayBox other = (PayBox) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "comsql.PayBox[id=" + id + "]";
    }

}
