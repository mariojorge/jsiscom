/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import comsql.JPA.ProductJpaController;
import comsql.JPA.PurchaseJpaController;
import comsql.JPA.PurchaseProductJpaController;
import comsql.exceptions.IllegalOrphanException;
import comsql.exceptions.NonexistentEntityException;
import comsql.exceptions.PreexistingEntityException;
import java.util.Date;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author usuario
 */
public class PurchaseFunctions {
    private static PurchaseFunctions pf;
    private PurchaseJpaController pjpa;

    private PurchaseFunctions() {
        this.pjpa = PurchaseJpaController.getInstance();
    }

    public static PurchaseFunctions getInstance() {
        if (pf == null) {
            pf = new PurchaseFunctions();
        }
        return pf;
    }

    public Purchase getPurchase(int id) {
        return this.pjpa.findPurchase(id);
    }

    public Double getPurchaseTotal(int id) {
        Purchase purchase = this.getPurchase(id);
        Double total = 0.0;
        List<PurchaseProduct> products = purchase.getPurchaseProductCollection();
        if (products.size() > 0) {
            for (int i = 0; i < products.size(); i++) {
                total += products.get(i).getPrice() * products.get(i).getQuantity();
            }
        }
        total += purchase.getFreight() + purchase.getIcms() + purchase.getIpi()
                 + purchase.getSafe() + purchase.getSubicms();
        return total;
    }

    public Boolean addPurchase(Purchase p, List<PurchaseProduct> pplist) {
        PurchaseProductJpaController ppjpa = PurchaseProductJpaController.getInstance();
        ProductJpaController prodjpa = ProductJpaController.getInstance();
        Boolean isInsertedPurchaseProduct;
        try {
            pjpa.create(p);
        } catch (Exception e) {
            return false;
        }
        for (int i = 0; i < pplist.size(); i++) {
            pplist.get(i).setPurchase(p);
            try {
                ppjpa.create(pplist.get(i));
                isInsertedPurchaseProduct = true;
            } catch (PreexistingEntityException ex) {
                ex.printStackTrace();
                isInsertedPurchaseProduct = false;
            } catch (Exception ex) {
                ex.printStackTrace();
                isInsertedPurchaseProduct = false;
            }
            if (isInsertedPurchaseProduct) {
                Product product = pplist.get(i).getProduct();
                int quantity = product.getQuantity() + pplist.get(i).getQuantity();
                product.setQuantity(quantity);
                try {
                    prodjpa.edit(product);
                } catch (IllegalOrphanException ex) {
                    ex.printStackTrace();
                } catch (NonexistentEntityException ex) {
                    ex.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            isInsertedPurchaseProduct = false;
        }
        return true;
    }

    public List findPurchases() {
        return PurchaseJpaController.getInstance().findPurchaseEntities();
    }

    public List findPurchases(Date dInicial, Date dFinal) {
        String strQuery = null;
        strQuery = "select p from Purchase as p WHERE p.date BETWEEN :initial AND :final ORDER BY p.date DESC";
        Query query = PurchaseJpaController.getInstance().getEntityManager().createQuery(strQuery);
        query.setParameter("initial", dInicial);
        query.setParameter("final", dFinal);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findPurchases(Date dInicial, Date dFinal, Supplier supplier) {
        String strQuery = null;
        strQuery = "select p from Purchase as p WHERE p.supplierId = :supplier AND p.date BETWEEN :initial AND :final ORDER BY p.id DESC";
        Query query = PurchaseJpaController.getInstance().getEntityManager().createQuery(strQuery);
        query.setParameter("initial", dInicial);
        query.setParameter("final", dFinal);
        query.setParameter("supplier", supplier);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }
}
