/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import comsql.JPA.AddressJpaController;
import comsql.JPA.CustomerJpaController;
import comsql.JPA.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author usuario
 */
public class CustomerFunctions {
    private static CustomerFunctions customerfunctions = null;
    CustomerJpaController cjpa;
    AddressJpaController ajpa;

    private CustomerFunctions() {
        cjpa = CustomerJpaController.getInstance();
        ajpa = AddressJpaController.getInstance();
    }
    
    public static CustomerFunctions getInstance() {
        if (customerfunctions == null) {
            customerfunctions = new CustomerFunctions();
        }
        return customerfunctions;
    }

    public Boolean Create(Customer customer, Address address) {
        try {
            ajpa.create(address);
            customer.setAddressId(address);
            cjpa.create(customer);
            LogFunctions.getInstance().Insert("Cadastrou o cliente com código = " + String.valueOf(customer.getId()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public Boolean Edit(Customer customer, Address address) {
        try {
            ajpa.edit(address);
            cjpa.edit(customer);
            LogFunctions.getInstance().Insert("Modificou o cadastro do cliente de código = " + String.valueOf(customer.getId()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public Boolean Destroy(int customerId) {
        try {
            cjpa.destroy(customerId);
            LogFunctions.getInstance().Insert("Apagou o cadastro do cliente de código = " + String.valueOf(customerId));
        } catch (NonexistentEntityException ex) {
            return false;
        }
        return true;
    }

    public Customer getCustomer(int i) {
        return CustomerJpaController.getInstance().findCustomer(i);
    }

    public Customer findCustomerByName(String name) {
        String strQuery = null;
        strQuery = "select c from Customer as c WHERE c.name = :name";
        Query query = ajpa.getEntityManager().createQuery(strQuery);
        query.setParameter("name", name);
        query.setHint("toplink.refresh", "true");
        return (Customer) query.getSingleResult();
    }

    public List findCustomers() {
        return CustomerJpaController.getInstance().findCustomerEntities();
    }

    public List findCustomers(String order) {
        String strQuery = null;
        if (order.compareTo("name") == 0)
            strQuery = "select c from Customer as c ORDER BY c.name";
        else
            strQuery = "select c from Customer as c";
        Query query = ajpa.getEntityManager().createQuery(strQuery);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findCustomersByName(String name) {
        String strQuery = null;
        strQuery = "select c from Customer as c WHERE c.name LIKE :name";
        Query query = ajpa.getEntityManager().createQuery(strQuery);
        query.setParameter("name", '%' + name + '%');
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findCustomersByFantasyName(String fantasyName) {
        String strQuery = null;
        strQuery = "select c from Customer as c WHERE c.fantasyname LIKE :name";
        Query query = cjpa.getEntityManager().createQuery(strQuery);
        query.setParameter("name", '%' + fantasyName + '%');
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findCustomersByCPF(String cpf) {
        String strQuery = null;
        strQuery = "select c from Customer as c WHERE c.cpf = :cpf";
        Query query = ajpa.getEntityManager().createQuery(strQuery);
        query.setParameter("cpf", cpf);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findCustomersByCNPJ(String cnpj) {
        String strQuery = null;
        strQuery = "select c from Customer as c WHERE c.cnpj = :cnpj";
        Query query = ajpa.getEntityManager().createQuery(strQuery);
        query.setParameter("cnpj", cnpj);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findCustomersByRG(String rg) {
        String strQuery = null;
        strQuery = "select c from Customer as c WHERE c.rg = :rg";
        Query query = ajpa.getEntityManager().createQuery(strQuery);
        query.setParameter("rg", rg);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findCustomersByIE(String ie) {
        String strQuery = null;
        strQuery = "select c from Customer as c WHERE c.ie = :ie";
        Query query = ajpa.getEntityManager().createQuery(strQuery);
        query.setParameter("ie", ie);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }
}
