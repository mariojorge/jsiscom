/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import comsql.JPA.PayAccountJpaController;
import comsql.JPA.PayPlotJpaController;
import comsql.JPA.PayedPlotJpaController;
import comsql.JPA.ReceivingTypeJpaController;
import comsql.JPA.exceptions.IllegalOrphanException;
import comsql.JPA.exceptions.NonexistentEntityException;
import java.util.Date;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author usuario
 */
public class PayAccountFunctions {
    private static PayAccountFunctions paf;

    private PayAccountFunctions() {

    }

    public static PayAccountFunctions getInstance() {
        if (paf == null) {
            paf = new PayAccountFunctions();
        }
        return paf;
    }

    public Boolean create(PayAccount p, List<PayPlot> payplots ) {
        try {
            PayAccountJpaController.getInstance().create(p);
        } catch (Exception e) {
            return false;
        }
        for (int i = 0; i < payplots.size(); i++) {
            payplots.get(i).setPayaccountId(p);
            PayPlotJpaController.getInstance().create(payplots.get(i));
        }

        if (payplots.get(0).getNumber().compareTo("0") == 0) {
            PayPlot inPayPlot = payplots.get(0);
            PayedPlot inPayedPlot = new PayedPlot();
            inPayedPlot.setAmount(inPayPlot.getAmount());
            inPayedPlot.setDate(inPayPlot.getDate());
            inPayedPlot.setNumber(inPayPlot.getNumber());
            inPayedPlot.setReceivingtypeId(ReceivingTypeJpaController.getInstance().findReceivingType(1));
            inPayedPlot.setPayaccountId(p);
            PayedPlotJpaController.getInstance().create(inPayedPlot);
        }
        return true;
    }

    public Boolean delete(int id) {
        PayAccount p = PayAccountJpaController.getInstance().findPayAccount(id);
        int i = 0;
        if (p.getPayedPlotCollection() != null) {
            List<PayedPlot> payedPlots = p.getPayedPlotCollection();
            for (i=0; i<payedPlots.size(); i++) {
                try {
                    PayedPlotJpaController.getInstance().destroy(payedPlots.get(i).getId());
                } catch (NonexistentEntityException ex) {
                    ex.printStackTrace();
                }
            }
        }
        if (p.getPayPlotCollection() != null) {
            List<PayPlot> payPlots = p.getPayPlotCollection();
            for (i=0; i<payPlots.size(); i++) {
                try {
                    PayPlotJpaController.getInstance().destroy(payPlots.get(i).getId());
                } catch (comsql.exceptions.NonexistentEntityException ex) {
                    ex.printStackTrace();
                }

            }
        }
        try {
            PayAccountJpaController.getInstance().destroy(p.getId());
        } catch (IllegalOrphanException ex) {
            ex.printStackTrace();
        } catch (NonexistentEntityException ex) {
            ex.printStackTrace();
        }
        return true;
    }

    public PayAccount getPayAccount(int id) {
        return PayAccountJpaController.getInstance().findPayAccount(id);
    }

    public List findPayAccounts() {
        return PayAccountJpaController.getInstance().findPayAccountEntities();
    }

    public List findPayAccountsBySupplierName(String supplierName) {
        String strQuery = null;
        strQuery = "select p from PayAccount as p WHERE p.supplierId.name LIKE :supplierName ORDER BY p.id";
        Query query = PayAccountJpaController.getInstance().getEntityManager().createQuery(strQuery);
        query.setParameter("supplierName", '%' + supplierName + '%');
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findPayAccountsBySupplierName(String supplierName, Date initialDate, Date finalDate) {
        String strQuery = null;
        strQuery = "select p from PayAccount as p WHERE p.supplierId.name LIKE :supplierName AND p.date BETWEEN :initial AND :final ORDER BY p.id";
        Query query = PayAccountJpaController.getInstance().getEntityManager().createQuery(strQuery);
        query.setParameter("supplierName", '%' + supplierName + '%');
        query.setParameter("initial", initialDate);
        query.setParameter("final", finalDate);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findPayAccountsByConcerning(String concerning) {
        String strQuery = null;
        strQuery = "select p from PayAccount as p WHERE p.concerning LIKE :concerning ORDER BY p.id";
        Query query = PayAccountJpaController.getInstance().getEntityManager().createQuery(strQuery);
        query.setParameter("concerning", '%' + concerning + '%');
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findPayAccountsByPeriod(Date initialDate, Date finalDate) {
        String strQuery = null;
        strQuery = "select p from PayAccount as p WHERE p.date BETWEEN :initial AND :final ORDER BY p.id";
        Query query = PayAccountJpaController.getInstance().getEntityManager().createQuery(strQuery);
        query.setParameter("initial", initialDate);
        query.setParameter("final", finalDate);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List getPayPlots(int payAccountId) {
        PayAccount p = PayAccountJpaController.getInstance().findPayAccount(payAccountId);
        return p.getPayPlotCollection();
    }

    public List getPayedPlots(int payAccountId) {
        PayAccount p = PayAccountJpaController.getInstance().findPayAccount(payAccountId);
        return p.getPayedPlotCollection();
    }

    public PayedPlot getPayedPlot(PayAccount p, String plotNumber) {
        for (int i =0; i<p.getPayedPlotCollection().size(); i++) {
            if (p.getPayedPlotCollection().get(i).getNumber().compareTo(plotNumber) == 0)
                return p.getPayedPlotCollection().get(i);
        }
        return null;
    }

    public Boolean destroyPayedPlots(List<PayedPlot> l) {
        for (int i=0; i<l.size(); i++) {
            try {
                PayedPlotJpaController.getInstance().destroy(l.get(i).getId());
            } catch (NonexistentEntityException ex) {
                return false;
            }
        }
        return true;
    }

    public List getAllPayAccounts() {
        return PayAccountJpaController.getInstance().findPayAccountEntities();
    }

    public Boolean toPayAccount(PayAccount payAccount, PayedPlot payedPlot) {
        try {
            PayedPlotJpaController.getInstance().create(payedPlot);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public Boolean actualizePayPlot(PayPlot payPlot) {
        try {
            PayPlotJpaController.getInstance().edit(payPlot);
        } catch (comsql.exceptions.NonexistentEntityException ex) {
            return false;
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    public Boolean actualizePayPlots(List<PayPlot> l) {
        for (int i=0; i<l.size(); i++) {
            try {
                PayPlotJpaController.getInstance().edit(l.get(i));
            } catch (comsql.exceptions.NonexistentEntityException ex) {
                return false;
            } catch (Exception ex) {
                return false;
            }
        }
        return true;
    }

    public PayPlot getSimilarPayPlot(PayedPlot payedPlot) {
        String strQuery = null;
        strQuery = "select p from PayPlot as p WHERE p.number = :number AND p.payaccountId = :payaccountId ORDER BY p.id";
        Query query = PayAccountJpaController.getInstance().getEntityManager().createQuery(strQuery);
        query.setParameter("number", payedPlot.getNumber());
        query.setParameter("payaccountId", payedPlot.getPayaccountId());
        query.setHint("toplink.refresh", "true");
        return (PayPlot) query.getSingleResult();
    }
}
