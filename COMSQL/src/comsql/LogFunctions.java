/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import comsql.JPA.LogJpaController;
import java.util.Date;
import java.util.List;
import javax.persistence.Query;
import util.LoggedUser;

/**
 *
 * @author usuario
 */
public class LogFunctions {
    private static LogFunctions logFunctions;

    private LogFunctions() {

    }

    public static LogFunctions getInstance() {
        if (logFunctions == null) {
            logFunctions = new LogFunctions();
        }
        return logFunctions;
    }

    public Boolean Insert(String description) {
        String user;
        Boolean isLogged = false;
        try {
            user = LoggedUser.getLoggedUser().getUser();
        } catch (Exception e) {
            user = "";
        }
        if (user.compareTo("SUPORTE") != 0) {
            Log l = new Log();
            l.setDate(new Date());
            l.setTime(new Date());
            l.setDescription(description);
            if (LoggedUser.getLoggedUser() != null) {
                l.setUser(LoggedUser.getLoggedUser().getUser());
                try {
                    LogJpaController.getInstance().create(l);
                } catch (Exception e) {
                    isLogged = false;
                }
            } else {
                isLogged = false;
            }
                isLogged = true;
        }
        return isLogged;
    }

    public List findLogs() {
        String strQuery = "select l from Log as l ORDER BY l.date DESC, l.time DESC";
        Query query = LogJpaController.getInstance().getEntityManager().createQuery(strQuery);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }
}
