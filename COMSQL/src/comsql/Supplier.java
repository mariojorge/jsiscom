/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "supplier")
@NamedQueries({@NamedQuery(name = "Supplier.findAll", query = "SELECT s FROM Supplier s"), @NamedQuery(name = "Supplier.findById", query = "SELECT s FROM Supplier s WHERE s.id = :id"), @NamedQuery(name = "Supplier.findByRegistrationdate", query = "SELECT s FROM Supplier s WHERE s.registrationdate = :registrationdate"), @NamedQuery(name = "Supplier.findByName", query = "SELECT s FROM Supplier s WHERE s.name = :name"), @NamedQuery(name = "Supplier.findByFantasyname", query = "SELECT s FROM Supplier s WHERE s.fantasyname = :fantasyname"), @NamedQuery(name = "Supplier.findByCpf", query = "SELECT s FROM Supplier s WHERE s.cpf = :cpf"), @NamedQuery(name = "Supplier.findByRg", query = "SELECT s FROM Supplier s WHERE s.rg = :rg"), @NamedQuery(name = "Supplier.findByCnpj", query = "SELECT s FROM Supplier s WHERE s.cnpj = :cnpj"), @NamedQuery(name = "Supplier.findByIe", query = "SELECT s FROM Supplier s WHERE s.ie = :ie"), @NamedQuery(name = "Supplier.findByPhone", query = "SELECT s FROM Supplier s WHERE s.phone = :phone"), @NamedQuery(name = "Supplier.findByCellphone", query = "SELECT s FROM Supplier s WHERE s.cellphone = :cellphone"), @NamedQuery(name = "Supplier.findByPhonefax", query = "SELECT s FROM Supplier s WHERE s.phonefax = :phonefax"), @NamedQuery(name = "Supplier.findByEmail", query = "SELECT s FROM Supplier s WHERE s.email = :email"), @NamedQuery(name = "Supplier.findByHomepage", query = "SELECT s FROM Supplier s WHERE s.homepage = :homepage")})
public class Supplier implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "registrationdate")
    @Temporal(TemporalType.DATE)
    private Date registrationdate;
    @Column(name = "name")
    private String name;
    @Column(name = "fantasyname")
    private String fantasyname;
    @Column(name = "type")
    private String type;
    @Column(name = "cpf")
    private String cpf;
    @Column(name = "rg")
    private String rg;
    @Column(name = "cnpj")
    private String cnpj;
    @Column(name = "ie")
    private String ie;
    @Column(name = "phone")
    private String phone;
    @Column(name = "cellphone")
    private String cellphone;
    @Column(name = "phonefax")
    private String phonefax;
    @Column(name = "email")
    private String email;
    @Column(name = "homepage")
    private String homepage;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "supplierId")
    private List<Purchase> purchaseCollection;
    @OneToMany(mappedBy = "supplierId")
    private List<PayAccount> payAccountCollection;
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    @ManyToOne
    private Address addressId;

    public Supplier() {
    }

    public Supplier(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        Integer oldId = this.id;
        this.id = id;
        changeSupport.firePropertyChange("id", oldId, id);
    }

    public Date getRegistrationdate() {
        return registrationdate;
    }

    public void setRegistrationdate(Date registrationdate) {
        Date oldRegistrationdate = this.registrationdate;
        this.registrationdate = registrationdate;
        changeSupport.firePropertyChange("registrationdate", oldRegistrationdate, registrationdate);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldName = this.name;
        this.name = name;
        changeSupport.firePropertyChange("name", oldName, name);
    }

    public String getFantasyname() {
        return fantasyname;
    }

    public void setFantasyname(String fantasyname) {
        String oldFantasyname = this.fantasyname;
        this.fantasyname = fantasyname;
        changeSupport.firePropertyChange("fantasyname", oldFantasyname, fantasyname);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        String oldType = this.type;
        this.type = type;
        changeSupport.firePropertyChange("type", oldType, type);
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        String oldCpf = this.cpf;
        this.cpf = cpf;
        changeSupport.firePropertyChange("cpf", oldCpf, cpf);
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        String oldRg = this.rg;
        this.rg = rg;
        changeSupport.firePropertyChange("rg", oldRg, rg);
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        String oldCnpj = this.cnpj;
        this.cnpj = cnpj;
        changeSupport.firePropertyChange("cnpj", oldCnpj, cnpj);
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        String oldIe = this.ie;
        this.ie = ie;
        changeSupport.firePropertyChange("ie", oldIe, ie);
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        String oldPhone = this.phone;
        this.phone = phone;
        changeSupport.firePropertyChange("phone", oldPhone, phone);
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        String oldCellphone = this.cellphone;
        this.cellphone = cellphone;
        changeSupport.firePropertyChange("cellphone", oldCellphone, cellphone);
    }

    public String getPhonefax() {
        return phonefax;
    }

    public void setPhonefax(String phonefax) {
        String oldPhonefax = this.phonefax;
        this.phonefax = phonefax;
        changeSupport.firePropertyChange("phonefax", oldPhonefax, phonefax);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        String oldEmail = this.email;
        this.email = email;
        changeSupport.firePropertyChange("email", oldEmail, email);
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        String oldHomepage = this.homepage;
        this.homepage = homepage;
        changeSupport.firePropertyChange("homepage", oldHomepage, homepage);
    }

    public List<Purchase> getPurchaseCollection() {
        return purchaseCollection;
    }

    public void setPurchaseCollection(List<Purchase> purchaseCollection) {
        this.purchaseCollection = purchaseCollection;
    }

    public List<PayAccount> getPayAccountCollection() {
        return payAccountCollection;
    }

    public void setPayAccountCollection(List<PayAccount> payAccountCollection) {
        this.payAccountCollection = payAccountCollection;
    }

    public Address getAddressId() {
        return addressId;
    }

    public void setAddressId(Address addressId) {
        Address oldAddressId = this.addressId;
        this.addressId = addressId;
        changeSupport.firePropertyChange("addressId", oldAddressId, addressId);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Supplier)) {
            return false;
        }
        Supplier other = (Supplier) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "comsql.Supplier[id=" + id + "]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

}
