/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.JPA.exceptions.IllegalOrphanException;
import comsql.JPA.exceptions.NonexistentEntityException;
import comsql.PayAccount;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import comsql.Supplier;
import comsql.Purchase;
import comsql.PayedPlot;
import java.util.ArrayList;
import java.util.List;
import comsql.PayPlot;

/**
 *
 * @author usuario
 */
public class PayAccountJpaController {

    private static PayAccountJpaController pajpa;

    public static PayAccountJpaController getInstance() {
        if (pajpa == null) {
            pajpa = new PayAccountJpaController();
        }
        return pajpa;
    }

    private PayAccountJpaController() {

    }

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(PayAccount payAccount) {
        if (payAccount.getPayedPlotCollection() == null) {
            payAccount.setPayedPlotCollection(new ArrayList<PayedPlot>());
        }
        if (payAccount.getPayPlotCollection() == null) {
            payAccount.setPayPlotCollection(new ArrayList<PayPlot>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Supplier supplierId = payAccount.getSupplierId();
            if (supplierId != null) {
                supplierId = em.getReference(supplierId.getClass(), supplierId.getId());
                payAccount.setSupplierId(supplierId);
            }
            Purchase purchaseId = payAccount.getPurchaseId();
            if (purchaseId != null) {
                purchaseId = em.getReference(purchaseId.getClass(), purchaseId.getId());
                payAccount.setPurchaseId(purchaseId);
            }
            List<PayedPlot> attachedPayedPlotCollection = new ArrayList<PayedPlot>();
            for (PayedPlot payedPlotCollectionPayedPlotToAttach : payAccount.getPayedPlotCollection()) {
                payedPlotCollectionPayedPlotToAttach = em.getReference(payedPlotCollectionPayedPlotToAttach.getClass(), payedPlotCollectionPayedPlotToAttach.getId());
                attachedPayedPlotCollection.add(payedPlotCollectionPayedPlotToAttach);
            }
            payAccount.setPayedPlotCollection(attachedPayedPlotCollection);
            List<PayPlot> attachedPayPlotCollection = new ArrayList<PayPlot>();
            for (PayPlot payPlotCollectionPayPlotToAttach : payAccount.getPayPlotCollection()) {
                payPlotCollectionPayPlotToAttach = em.getReference(payPlotCollectionPayPlotToAttach.getClass(), payPlotCollectionPayPlotToAttach.getId());
                attachedPayPlotCollection.add(payPlotCollectionPayPlotToAttach);
            }
            payAccount.setPayPlotCollection(attachedPayPlotCollection);
            em.persist(payAccount);
            if (supplierId != null) {
                supplierId.getPayAccountCollection().add(payAccount);
                supplierId = em.merge(supplierId);
            }
            if (purchaseId != null) {
                purchaseId.getPayAccountCollection().add(payAccount);
                purchaseId = em.merge(purchaseId);
            }
            for (PayedPlot payedPlotCollectionPayedPlot : payAccount.getPayedPlotCollection()) {
                PayAccount oldPayaccountIdOfPayedPlotCollectionPayedPlot = payedPlotCollectionPayedPlot.getPayaccountId();
                payedPlotCollectionPayedPlot.setPayaccountId(payAccount);
                payedPlotCollectionPayedPlot = em.merge(payedPlotCollectionPayedPlot);
                if (oldPayaccountIdOfPayedPlotCollectionPayedPlot != null) {
                    oldPayaccountIdOfPayedPlotCollectionPayedPlot.getPayedPlotCollection().remove(payedPlotCollectionPayedPlot);
                    oldPayaccountIdOfPayedPlotCollectionPayedPlot = em.merge(oldPayaccountIdOfPayedPlotCollectionPayedPlot);
                }
            }
            for (PayPlot payPlotCollectionPayPlot : payAccount.getPayPlotCollection()) {
                PayAccount oldPayaccountIdOfPayPlotCollectionPayPlot = payPlotCollectionPayPlot.getPayaccountId();
                payPlotCollectionPayPlot.setPayaccountId(payAccount);
                payPlotCollectionPayPlot = em.merge(payPlotCollectionPayPlot);
                if (oldPayaccountIdOfPayPlotCollectionPayPlot != null) {
                    oldPayaccountIdOfPayPlotCollectionPayPlot.getPayPlotCollection().remove(payPlotCollectionPayPlot);
                    oldPayaccountIdOfPayPlotCollectionPayPlot = em.merge(oldPayaccountIdOfPayPlotCollectionPayPlot);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PayAccount payAccount) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PayAccount persistentPayAccount = em.find(PayAccount.class, payAccount.getId());
            Supplier supplierIdOld = persistentPayAccount.getSupplierId();
            Supplier supplierIdNew = payAccount.getSupplierId();
            Purchase purchaseIdOld = persistentPayAccount.getPurchaseId();
            Purchase purchaseIdNew = payAccount.getPurchaseId();
            List<PayedPlot> payedPlotCollectionOld = persistentPayAccount.getPayedPlotCollection();
            List<PayedPlot> payedPlotCollectionNew = payAccount.getPayedPlotCollection();
            List<PayPlot> payPlotCollectionOld = persistentPayAccount.getPayPlotCollection();
            List<PayPlot> payPlotCollectionNew = payAccount.getPayPlotCollection();
            List<String> illegalOrphanMessages = null;
            for (PayPlot payPlotCollectionOldPayPlot : payPlotCollectionOld) {
                if (!payPlotCollectionNew.contains(payPlotCollectionOldPayPlot)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain PayPlot " + payPlotCollectionOldPayPlot + " since its payaccountId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (supplierIdNew != null) {
                supplierIdNew = em.getReference(supplierIdNew.getClass(), supplierIdNew.getId());
                payAccount.setSupplierId(supplierIdNew);
            }
            if (purchaseIdNew != null) {
                purchaseIdNew = em.getReference(purchaseIdNew.getClass(), purchaseIdNew.getId());
                payAccount.setPurchaseId(purchaseIdNew);
            }
            List<PayedPlot> attachedPayedPlotCollectionNew = new ArrayList<PayedPlot>();
            for (PayedPlot payedPlotCollectionNewPayedPlotToAttach : payedPlotCollectionNew) {
                payedPlotCollectionNewPayedPlotToAttach = em.getReference(payedPlotCollectionNewPayedPlotToAttach.getClass(), payedPlotCollectionNewPayedPlotToAttach.getId());
                attachedPayedPlotCollectionNew.add(payedPlotCollectionNewPayedPlotToAttach);
            }
            payedPlotCollectionNew = attachedPayedPlotCollectionNew;
            payAccount.setPayedPlotCollection(payedPlotCollectionNew);
            List<PayPlot> attachedPayPlotCollectionNew = new ArrayList<PayPlot>();
            for (PayPlot payPlotCollectionNewPayPlotToAttach : payPlotCollectionNew) {
                payPlotCollectionNewPayPlotToAttach = em.getReference(payPlotCollectionNewPayPlotToAttach.getClass(), payPlotCollectionNewPayPlotToAttach.getId());
                attachedPayPlotCollectionNew.add(payPlotCollectionNewPayPlotToAttach);
            }
            payPlotCollectionNew = attachedPayPlotCollectionNew;
            payAccount.setPayPlotCollection(payPlotCollectionNew);
            payAccount = em.merge(payAccount);
            if (supplierIdOld != null && !supplierIdOld.equals(supplierIdNew)) {
                supplierIdOld.getPayAccountCollection().remove(payAccount);
                supplierIdOld = em.merge(supplierIdOld);
            }
            if (supplierIdNew != null && !supplierIdNew.equals(supplierIdOld)) {
                supplierIdNew.getPayAccountCollection().add(payAccount);
                supplierIdNew = em.merge(supplierIdNew);
            }
            if (purchaseIdOld != null && !purchaseIdOld.equals(purchaseIdNew)) {
                purchaseIdOld.getPayAccountCollection().remove(payAccount);
                purchaseIdOld = em.merge(purchaseIdOld);
            }
            if (purchaseIdNew != null && !purchaseIdNew.equals(purchaseIdOld)) {
                purchaseIdNew.getPayAccountCollection().add(payAccount);
                purchaseIdNew = em.merge(purchaseIdNew);
            }
            for (PayedPlot payedPlotCollectionOldPayedPlot : payedPlotCollectionOld) {
                if (!payedPlotCollectionNew.contains(payedPlotCollectionOldPayedPlot)) {
                    payedPlotCollectionOldPayedPlot.setPayaccountId(null);
                    payedPlotCollectionOldPayedPlot = em.merge(payedPlotCollectionOldPayedPlot);
                }
            }
            for (PayedPlot payedPlotCollectionNewPayedPlot : payedPlotCollectionNew) {
                if (!payedPlotCollectionOld.contains(payedPlotCollectionNewPayedPlot)) {
                    PayAccount oldPayaccountIdOfPayedPlotCollectionNewPayedPlot = payedPlotCollectionNewPayedPlot.getPayaccountId();
                    payedPlotCollectionNewPayedPlot.setPayaccountId(payAccount);
                    payedPlotCollectionNewPayedPlot = em.merge(payedPlotCollectionNewPayedPlot);
                    if (oldPayaccountIdOfPayedPlotCollectionNewPayedPlot != null && !oldPayaccountIdOfPayedPlotCollectionNewPayedPlot.equals(payAccount)) {
                        oldPayaccountIdOfPayedPlotCollectionNewPayedPlot.getPayedPlotCollection().remove(payedPlotCollectionNewPayedPlot);
                        oldPayaccountIdOfPayedPlotCollectionNewPayedPlot = em.merge(oldPayaccountIdOfPayedPlotCollectionNewPayedPlot);
                    }
                }
            }
            for (PayPlot payPlotCollectionNewPayPlot : payPlotCollectionNew) {
                if (!payPlotCollectionOld.contains(payPlotCollectionNewPayPlot)) {
                    PayAccount oldPayaccountIdOfPayPlotCollectionNewPayPlot = payPlotCollectionNewPayPlot.getPayaccountId();
                    payPlotCollectionNewPayPlot.setPayaccountId(payAccount);
                    payPlotCollectionNewPayPlot = em.merge(payPlotCollectionNewPayPlot);
                    if (oldPayaccountIdOfPayPlotCollectionNewPayPlot != null && !oldPayaccountIdOfPayPlotCollectionNewPayPlot.equals(payAccount)) {
                        oldPayaccountIdOfPayPlotCollectionNewPayPlot.getPayPlotCollection().remove(payPlotCollectionNewPayPlot);
                        oldPayaccountIdOfPayPlotCollectionNewPayPlot = em.merge(oldPayaccountIdOfPayPlotCollectionNewPayPlot);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = payAccount.getId();
                if (findPayAccount(id) == null) {
                    throw new NonexistentEntityException("The payAccount with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PayAccount payAccount;
            try {
                payAccount = em.getReference(PayAccount.class, id);
                payAccount.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The payAccount with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<PayPlot> payPlotCollectionOrphanCheck = payAccount.getPayPlotCollection();
            for (PayPlot payPlotCollectionOrphanCheckPayPlot : payPlotCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This PayAccount (" + payAccount + ") cannot be destroyed since the PayPlot " + payPlotCollectionOrphanCheckPayPlot + " in its payPlotCollection field has a non-nullable payaccountId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Supplier supplierId = payAccount.getSupplierId();
            if (supplierId != null) {
                supplierId.getPayAccountCollection().remove(payAccount);
                supplierId = em.merge(supplierId);
            }
            Purchase purchaseId = payAccount.getPurchaseId();
            if (purchaseId != null) {
                purchaseId.getPayAccountCollection().remove(payAccount);
                purchaseId = em.merge(purchaseId);
            }
            List<PayedPlot> payedPlotCollection = payAccount.getPayedPlotCollection();
            for (PayedPlot payedPlotCollectionPayedPlot : payedPlotCollection) {
                payedPlotCollectionPayedPlot.setPayaccountId(null);
                payedPlotCollectionPayedPlot = em.merge(payedPlotCollectionPayedPlot);
            }
            em.remove(payAccount);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PayAccount> findPayAccountEntities() {
        return findPayAccountEntities(true, -1, -1);
    }

    public List<PayAccount> findPayAccountEntities(int maxResults, int firstResult) {
        return findPayAccountEntities(false, maxResults, firstResult);
    }

    private List<PayAccount> findPayAccountEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from PayAccount as o");
            q.setHint("toplink.refresh", "true");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PayAccount findPayAccount(Integer id) {
        EntityManager em = getEntityManager();
        PayAccount p;
        try {
            p = em.find(PayAccount.class, id);
            em.refresh(p);
            return p;
        } finally {
            em.close();
        }
    }

    public int getPayAccountCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from PayAccount as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
