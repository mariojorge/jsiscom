/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.*;
import comsql.exceptions.IllegalOrphanException;
import comsql.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author usuario
 */
public class ProductJpaController {
    private static ProductJpaController pjpa;

    private ProductJpaController() {
        
    }

    public static ProductJpaController getInstance() {
        if (pjpa == null) {
            pjpa = new ProductJpaController();
        }
        return pjpa;
    }
    //private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(Product product) {
        if (product.getSaleProductCollection() == null) {
            product.setSaleProductCollection(new ArrayList<SaleProduct>());
        }
        if (product.getPurchaseProductCollection() == null) {
            product.setPurchaseProductCollection(new ArrayList<PurchaseProduct>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<SaleProduct> attachedSaleProductCollection = new ArrayList<SaleProduct>();
            for (SaleProduct saleProductCollectionSaleProductToAttach : product.getSaleProductCollection()) {
                saleProductCollectionSaleProductToAttach = em.getReference(saleProductCollectionSaleProductToAttach.getClass(), saleProductCollectionSaleProductToAttach.getSaleProductPK());
                attachedSaleProductCollection.add(saleProductCollectionSaleProductToAttach);
            }
            product.setSaleProductCollection(attachedSaleProductCollection);
            List<PurchaseProduct> attachedPurchaseProductCollection = new ArrayList<PurchaseProduct>();
            for (PurchaseProduct purchaseProductCollectionPurchaseProductToAttach : product.getPurchaseProductCollection()) {
                purchaseProductCollectionPurchaseProductToAttach = em.getReference(purchaseProductCollectionPurchaseProductToAttach.getClass(), purchaseProductCollectionPurchaseProductToAttach.getPurchaseProductPK());
                attachedPurchaseProductCollection.add(purchaseProductCollectionPurchaseProductToAttach);
            }
            product.setPurchaseProductCollection(attachedPurchaseProductCollection);
            em.persist(product);
            for (SaleProduct saleProductCollectionSaleProduct : product.getSaleProductCollection()) {
                Product oldProductOfSaleProductCollectionSaleProduct = saleProductCollectionSaleProduct.getProduct();
                saleProductCollectionSaleProduct.setProduct(product);
                saleProductCollectionSaleProduct = em.merge(saleProductCollectionSaleProduct);
                if (oldProductOfSaleProductCollectionSaleProduct != null) {
                    oldProductOfSaleProductCollectionSaleProduct.getSaleProductCollection().remove(saleProductCollectionSaleProduct);
                    oldProductOfSaleProductCollectionSaleProduct = em.merge(oldProductOfSaleProductCollectionSaleProduct);
                }
            }
            for (PurchaseProduct purchaseProductCollectionPurchaseProduct : product.getPurchaseProductCollection()) {
                Product oldProductOfPurchaseProductCollectionPurchaseProduct = purchaseProductCollectionPurchaseProduct.getProduct();
                purchaseProductCollectionPurchaseProduct.setProduct(product);
                purchaseProductCollectionPurchaseProduct = em.merge(purchaseProductCollectionPurchaseProduct);
                if (oldProductOfPurchaseProductCollectionPurchaseProduct != null) {
                    oldProductOfPurchaseProductCollectionPurchaseProduct.getPurchaseProductCollection().remove(purchaseProductCollectionPurchaseProduct);
                    oldProductOfPurchaseProductCollectionPurchaseProduct = em.merge(oldProductOfPurchaseProductCollectionPurchaseProduct);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Product product) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Product persistentProduct = em.find(Product.class, product.getId());
            List<SaleProduct> saleProductCollectionOld = persistentProduct.getSaleProductCollection();
            List<SaleProduct> saleProductCollectionNew = product.getSaleProductCollection();
            List<PurchaseProduct> purchaseProductCollectionOld = persistentProduct.getPurchaseProductCollection();
            List<PurchaseProduct> purchaseProductCollectionNew = product.getPurchaseProductCollection();
            List<String> illegalOrphanMessages = null;
            for (SaleProduct saleProductCollectionOldSaleProduct : saleProductCollectionOld) {
                if (!saleProductCollectionNew.contains(saleProductCollectionOldSaleProduct)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain SaleProduct " + saleProductCollectionOldSaleProduct + " since its product field is not nullable.");
                }
            }
            for (PurchaseProduct purchaseProductCollectionOldPurchaseProduct : purchaseProductCollectionOld) {
                if (!purchaseProductCollectionNew.contains(purchaseProductCollectionOldPurchaseProduct)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain PurchaseProduct " + purchaseProductCollectionOldPurchaseProduct + " since its product field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<SaleProduct> attachedSaleProductCollectionNew = new ArrayList<SaleProduct>();
            for (SaleProduct saleProductCollectionNewSaleProductToAttach : saleProductCollectionNew) {
                saleProductCollectionNewSaleProductToAttach = em.getReference(saleProductCollectionNewSaleProductToAttach.getClass(), saleProductCollectionNewSaleProductToAttach.getSaleProductPK());
                attachedSaleProductCollectionNew.add(saleProductCollectionNewSaleProductToAttach);
            }
            saleProductCollectionNew = attachedSaleProductCollectionNew;
            product.setSaleProductCollection(saleProductCollectionNew);
            List<PurchaseProduct> attachedPurchaseProductCollectionNew = new ArrayList<PurchaseProduct>();
            for (PurchaseProduct purchaseProductCollectionNewPurchaseProductToAttach : purchaseProductCollectionNew) {
                purchaseProductCollectionNewPurchaseProductToAttach = em.getReference(purchaseProductCollectionNewPurchaseProductToAttach.getClass(), purchaseProductCollectionNewPurchaseProductToAttach.getPurchaseProductPK());
                attachedPurchaseProductCollectionNew.add(purchaseProductCollectionNewPurchaseProductToAttach);
            }
            purchaseProductCollectionNew = attachedPurchaseProductCollectionNew;
            product.setPurchaseProductCollection(purchaseProductCollectionNew);
            product = em.merge(product);
            for (SaleProduct saleProductCollectionNewSaleProduct : saleProductCollectionNew) {
                if (!saleProductCollectionOld.contains(saleProductCollectionNewSaleProduct)) {
                    Product oldProductOfSaleProductCollectionNewSaleProduct = saleProductCollectionNewSaleProduct.getProduct();
                    saleProductCollectionNewSaleProduct.setProduct(product);
                    saleProductCollectionNewSaleProduct = em.merge(saleProductCollectionNewSaleProduct);
                    if (oldProductOfSaleProductCollectionNewSaleProduct != null && !oldProductOfSaleProductCollectionNewSaleProduct.equals(product)) {
                        oldProductOfSaleProductCollectionNewSaleProduct.getSaleProductCollection().remove(saleProductCollectionNewSaleProduct);
                        oldProductOfSaleProductCollectionNewSaleProduct = em.merge(oldProductOfSaleProductCollectionNewSaleProduct);
                    }
                }
            }
            for (PurchaseProduct purchaseProductCollectionNewPurchaseProduct : purchaseProductCollectionNew) {
                if (!purchaseProductCollectionOld.contains(purchaseProductCollectionNewPurchaseProduct)) {
                    Product oldProductOfPurchaseProductCollectionNewPurchaseProduct = purchaseProductCollectionNewPurchaseProduct.getProduct();
                    purchaseProductCollectionNewPurchaseProduct.setProduct(product);
                    purchaseProductCollectionNewPurchaseProduct = em.merge(purchaseProductCollectionNewPurchaseProduct);
                    if (oldProductOfPurchaseProductCollectionNewPurchaseProduct != null && !oldProductOfPurchaseProductCollectionNewPurchaseProduct.equals(product)) {
                        oldProductOfPurchaseProductCollectionNewPurchaseProduct.getPurchaseProductCollection().remove(purchaseProductCollectionNewPurchaseProduct);
                        oldProductOfPurchaseProductCollectionNewPurchaseProduct = em.merge(oldProductOfPurchaseProductCollectionNewPurchaseProduct);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = product.getId();
                if (findProduct(id) == null) {
                    throw new NonexistentEntityException("The product with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Product product;
            try {
                product = em.getReference(Product.class, id);
                product.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The product with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<SaleProduct> saleProductCollectionOrphanCheck = product.getSaleProductCollection();
            for (SaleProduct saleProductCollectionOrphanCheckSaleProduct : saleProductCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Product (" + product + ") cannot be destroyed since the SaleProduct " + saleProductCollectionOrphanCheckSaleProduct + " in its saleProductCollection field has a non-nullable product field.");
            }
            List<PurchaseProduct> purchaseProductCollectionOrphanCheck = product.getPurchaseProductCollection();
            for (PurchaseProduct purchaseProductCollectionOrphanCheckPurchaseProduct : purchaseProductCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Product (" + product + ") cannot be destroyed since the PurchaseProduct " + purchaseProductCollectionOrphanCheckPurchaseProduct + " in its purchaseProductCollection field has a non-nullable product field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(product);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Product> findProductEntities() {
        return findProductEntities(true, -1, -1);
    }

    public List<Product> findProductEntities(int maxResults, int firstResult) {
        return findProductEntities(false, maxResults, firstResult);
    }

    private List<Product> findProductEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Product as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
                q.setHint("toplink.refresh", "true");
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Product findProduct(Integer id) {
        EntityManager em = getEntityManager();
        Product p;
        try {
            p = em.find(Product.class, id);
            em.refresh(p);
            return p;
        } finally {
            em.close();
        }
    }

    public int getProductCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from Product as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
