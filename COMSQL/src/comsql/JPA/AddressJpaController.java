/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.Address;
import comsql.JPA.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import comsql.Customer;
import java.util.ArrayList;
import java.util.List;
import comsql.Official;
import comsql.Supplier;

/**
 *
 * @author usuario
 */
public class AddressJpaController {
    private static AddressJpaController jpa;

    private AddressJpaController() {

    }

    public static AddressJpaController getInstance() {
        if (jpa == null) {
            jpa = new AddressJpaController();
        }
        return jpa;
    }

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(Address address) {
        if (address.getCustomerCollection() == null) {
            address.setCustomerCollection(new ArrayList<Customer>());
        }
        if (address.getOfficialCollection() == null) {
            address.setOfficialCollection(new ArrayList<Official>());
        }
        if (address.getSupplierCollection() == null) {
            address.setSupplierCollection(new ArrayList<Supplier>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Customer> attachedCustomerCollection = new ArrayList<Customer>();
            for (Customer customerCollectionCustomerToAttach : address.getCustomerCollection()) {
                customerCollectionCustomerToAttach = em.getReference(customerCollectionCustomerToAttach.getClass(), customerCollectionCustomerToAttach.getId());
                attachedCustomerCollection.add(customerCollectionCustomerToAttach);
            }
            address.setCustomerCollection(attachedCustomerCollection);
            List<Official> attachedOfficialCollection = new ArrayList<Official>();
            for (Official officialCollectionOfficialToAttach : address.getOfficialCollection()) {
                officialCollectionOfficialToAttach = em.getReference(officialCollectionOfficialToAttach.getClass(), officialCollectionOfficialToAttach.getId());
                attachedOfficialCollection.add(officialCollectionOfficialToAttach);
            }
            address.setOfficialCollection(attachedOfficialCollection);
            List<Supplier> attachedSupplierCollection = new ArrayList<Supplier>();
            for (Supplier supplierCollectionSupplierToAttach : address.getSupplierCollection()) {
                supplierCollectionSupplierToAttach = em.getReference(supplierCollectionSupplierToAttach.getClass(), supplierCollectionSupplierToAttach.getId());
                attachedSupplierCollection.add(supplierCollectionSupplierToAttach);
            }
            address.setSupplierCollection(attachedSupplierCollection);
            em.persist(address);
            for (Customer customerCollectionCustomer : address.getCustomerCollection()) {
                Address oldAddressIdOfCustomerCollectionCustomer = customerCollectionCustomer.getAddressId();
                customerCollectionCustomer.setAddressId(address);
                customerCollectionCustomer = em.merge(customerCollectionCustomer);
                if (oldAddressIdOfCustomerCollectionCustomer != null) {
                    oldAddressIdOfCustomerCollectionCustomer.getCustomerCollection().remove(customerCollectionCustomer);
                    oldAddressIdOfCustomerCollectionCustomer = em.merge(oldAddressIdOfCustomerCollectionCustomer);
                }
            }
            for (Official officialCollectionOfficial : address.getOfficialCollection()) {
                Address oldAddressIdOfOfficialCollectionOfficial = officialCollectionOfficial.getAddressId();
                officialCollectionOfficial.setAddressId(address);
                officialCollectionOfficial = em.merge(officialCollectionOfficial);
                if (oldAddressIdOfOfficialCollectionOfficial != null) {
                    oldAddressIdOfOfficialCollectionOfficial.getOfficialCollection().remove(officialCollectionOfficial);
                    oldAddressIdOfOfficialCollectionOfficial = em.merge(oldAddressIdOfOfficialCollectionOfficial);
                }
            }
            for (Supplier supplierCollectionSupplier : address.getSupplierCollection()) {
                Address oldAddressIdOfSupplierCollectionSupplier = supplierCollectionSupplier.getAddressId();
                supplierCollectionSupplier.setAddressId(address);
                supplierCollectionSupplier = em.merge(supplierCollectionSupplier);
                if (oldAddressIdOfSupplierCollectionSupplier != null) {
                    oldAddressIdOfSupplierCollectionSupplier.getSupplierCollection().remove(supplierCollectionSupplier);
                    oldAddressIdOfSupplierCollectionSupplier = em.merge(oldAddressIdOfSupplierCollectionSupplier);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Address address) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Address persistentAddress = em.find(Address.class, address.getId());
            List<Customer> customerCollectionOld = persistentAddress.getCustomerCollection();
            List<Customer> customerCollectionNew = address.getCustomerCollection();
            List<Official> officialCollectionOld = persistentAddress.getOfficialCollection();
            List<Official> officialCollectionNew = address.getOfficialCollection();
            List<Supplier> supplierCollectionOld = persistentAddress.getSupplierCollection();
            List<Supplier> supplierCollectionNew = address.getSupplierCollection();
            List<Customer> attachedCustomerCollectionNew = new ArrayList<Customer>();
            for (Customer customerCollectionNewCustomerToAttach : customerCollectionNew) {
                customerCollectionNewCustomerToAttach = em.getReference(customerCollectionNewCustomerToAttach.getClass(), customerCollectionNewCustomerToAttach.getId());
                attachedCustomerCollectionNew.add(customerCollectionNewCustomerToAttach);
            }
            customerCollectionNew = attachedCustomerCollectionNew;
            address.setCustomerCollection(customerCollectionNew);
            List<Official> attachedOfficialCollectionNew = new ArrayList<Official>();
            for (Official officialCollectionNewOfficialToAttach : officialCollectionNew) {
                officialCollectionNewOfficialToAttach = em.getReference(officialCollectionNewOfficialToAttach.getClass(), officialCollectionNewOfficialToAttach.getId());
                attachedOfficialCollectionNew.add(officialCollectionNewOfficialToAttach);
            }
            officialCollectionNew = attachedOfficialCollectionNew;
            address.setOfficialCollection(officialCollectionNew);
            List<Supplier> attachedSupplierCollectionNew = new ArrayList<Supplier>();
            for (Supplier supplierCollectionNewSupplierToAttach : supplierCollectionNew) {
                supplierCollectionNewSupplierToAttach = em.getReference(supplierCollectionNewSupplierToAttach.getClass(), supplierCollectionNewSupplierToAttach.getId());
                attachedSupplierCollectionNew.add(supplierCollectionNewSupplierToAttach);
            }
            supplierCollectionNew = attachedSupplierCollectionNew;
            address.setSupplierCollection(supplierCollectionNew);
            address = em.merge(address);
            for (Customer customerCollectionOldCustomer : customerCollectionOld) {
                if (!customerCollectionNew.contains(customerCollectionOldCustomer)) {
                    customerCollectionOldCustomer.setAddressId(null);
                    customerCollectionOldCustomer = em.merge(customerCollectionOldCustomer);
                }
            }
            for (Customer customerCollectionNewCustomer : customerCollectionNew) {
                if (!customerCollectionOld.contains(customerCollectionNewCustomer)) {
                    Address oldAddressIdOfCustomerCollectionNewCustomer = customerCollectionNewCustomer.getAddressId();
                    customerCollectionNewCustomer.setAddressId(address);
                    customerCollectionNewCustomer = em.merge(customerCollectionNewCustomer);
                    if (oldAddressIdOfCustomerCollectionNewCustomer != null && !oldAddressIdOfCustomerCollectionNewCustomer.equals(address)) {
                        oldAddressIdOfCustomerCollectionNewCustomer.getCustomerCollection().remove(customerCollectionNewCustomer);
                        oldAddressIdOfCustomerCollectionNewCustomer = em.merge(oldAddressIdOfCustomerCollectionNewCustomer);
                    }
                }
            }
            for (Official officialCollectionOldOfficial : officialCollectionOld) {
                if (!officialCollectionNew.contains(officialCollectionOldOfficial)) {
                    officialCollectionOldOfficial.setAddressId(null);
                    officialCollectionOldOfficial = em.merge(officialCollectionOldOfficial);
                }
            }
            for (Official officialCollectionNewOfficial : officialCollectionNew) {
                if (!officialCollectionOld.contains(officialCollectionNewOfficial)) {
                    Address oldAddressIdOfOfficialCollectionNewOfficial = officialCollectionNewOfficial.getAddressId();
                    officialCollectionNewOfficial.setAddressId(address);
                    officialCollectionNewOfficial = em.merge(officialCollectionNewOfficial);
                    if (oldAddressIdOfOfficialCollectionNewOfficial != null && !oldAddressIdOfOfficialCollectionNewOfficial.equals(address)) {
                        oldAddressIdOfOfficialCollectionNewOfficial.getOfficialCollection().remove(officialCollectionNewOfficial);
                        oldAddressIdOfOfficialCollectionNewOfficial = em.merge(oldAddressIdOfOfficialCollectionNewOfficial);
                    }
                }
            }
            for (Supplier supplierCollectionOldSupplier : supplierCollectionOld) {
                if (!supplierCollectionNew.contains(supplierCollectionOldSupplier)) {
                    supplierCollectionOldSupplier.setAddressId(null);
                    supplierCollectionOldSupplier = em.merge(supplierCollectionOldSupplier);
                }
            }
            for (Supplier supplierCollectionNewSupplier : supplierCollectionNew) {
                if (!supplierCollectionOld.contains(supplierCollectionNewSupplier)) {
                    Address oldAddressIdOfSupplierCollectionNewSupplier = supplierCollectionNewSupplier.getAddressId();
                    supplierCollectionNewSupplier.setAddressId(address);
                    supplierCollectionNewSupplier = em.merge(supplierCollectionNewSupplier);
                    if (oldAddressIdOfSupplierCollectionNewSupplier != null && !oldAddressIdOfSupplierCollectionNewSupplier.equals(address)) {
                        oldAddressIdOfSupplierCollectionNewSupplier.getSupplierCollection().remove(supplierCollectionNewSupplier);
                        oldAddressIdOfSupplierCollectionNewSupplier = em.merge(oldAddressIdOfSupplierCollectionNewSupplier);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = address.getId();
                if (findAddress(id) == null) {
                    throw new NonexistentEntityException("The address with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Address address;
            try {
                address = em.getReference(Address.class, id);
                address.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The address with id " + id + " no longer exists.", enfe);
            }
            List<Customer> customerCollection = address.getCustomerCollection();
            for (Customer customerCollectionCustomer : customerCollection) {
                customerCollectionCustomer.setAddressId(null);
                customerCollectionCustomer = em.merge(customerCollectionCustomer);
            }
            List<Official> officialCollection = address.getOfficialCollection();
            for (Official officialCollectionOfficial : officialCollection) {
                officialCollectionOfficial.setAddressId(null);
                officialCollectionOfficial = em.merge(officialCollectionOfficial);
            }
            List<Supplier> supplierCollection = address.getSupplierCollection();
            for (Supplier supplierCollectionSupplier : supplierCollection) {
                supplierCollectionSupplier.setAddressId(null);
                supplierCollectionSupplier = em.merge(supplierCollectionSupplier);
            }
            em.remove(address);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Address> findAddressEntities() {
        return findAddressEntities(true, -1, -1);
    }

    public List<Address> findAddressEntities(int maxResults, int firstResult) {
        return findAddressEntities(false, maxResults, firstResult);
    }

    private List<Address> findAddressEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Address as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Address findAddress(Integer id) {
        EntityManager em = getEntityManager();
        Address a;
        try {
            a = em.find(Address.class, id);
            em.refresh(a);
            return a;
        } finally {
            em.close();
        }
    }

    public int getAddressCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from Address as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
