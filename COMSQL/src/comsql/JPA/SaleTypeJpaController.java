/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.exceptions.NonexistentEntityException;
import comsql.SaleType;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import comsql.Sale;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author usuario
 */
public class SaleTypeJpaController {
    private static SaleTypeJpaController jpa;

    private SaleTypeJpaController() {

    }

    public static SaleTypeJpaController getInstance() {
        if (jpa == null) {
            jpa = new SaleTypeJpaController();
        }
        return jpa;
    }

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }
    
    public void create(SaleType saleType) {
        if (saleType.getSaleCollection() == null) {
            saleType.setSaleCollection(new ArrayList<Sale>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Sale> attachedSaleCollection = new ArrayList<Sale>();
            for (Sale saleCollectionSaleToAttach : saleType.getSaleCollection()) {
                saleCollectionSaleToAttach = em.getReference(saleCollectionSaleToAttach.getClass(), saleCollectionSaleToAttach.getId());
                attachedSaleCollection.add(saleCollectionSaleToAttach);
            }
            saleType.setSaleCollection(attachedSaleCollection);
            em.persist(saleType);
            for (Sale saleCollectionSale : saleType.getSaleCollection()) {
                SaleType oldSaletypeIdOfSaleCollectionSale = saleCollectionSale.getSaletypeId();
                saleCollectionSale.setSaletypeId(saleType);
                saleCollectionSale = em.merge(saleCollectionSale);
                if (oldSaletypeIdOfSaleCollectionSale != null) {
                    oldSaletypeIdOfSaleCollectionSale.getSaleCollection().remove(saleCollectionSale);
                    oldSaletypeIdOfSaleCollectionSale = em.merge(oldSaletypeIdOfSaleCollectionSale);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SaleType saleType) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SaleType persistentSaleType = em.find(SaleType.class, saleType.getId());
            List<Sale> saleCollectionOld = persistentSaleType.getSaleCollection();
            List<Sale> saleCollectionNew = saleType.getSaleCollection();
            List<Sale> attachedSaleCollectionNew = new ArrayList<Sale>();
            for (Sale saleCollectionNewSaleToAttach : saleCollectionNew) {
                saleCollectionNewSaleToAttach = em.getReference(saleCollectionNewSaleToAttach.getClass(), saleCollectionNewSaleToAttach.getId());
                attachedSaleCollectionNew.add(saleCollectionNewSaleToAttach);
            }
            saleCollectionNew = attachedSaleCollectionNew;
            saleType.setSaleCollection(saleCollectionNew);
            saleType = em.merge(saleType);
            for (Sale saleCollectionOldSale : saleCollectionOld) {
                if (!saleCollectionNew.contains(saleCollectionOldSale)) {
                    saleCollectionOldSale.setSaletypeId(null);
                    saleCollectionOldSale = em.merge(saleCollectionOldSale);
                }
            }
            for (Sale saleCollectionNewSale : saleCollectionNew) {
                if (!saleCollectionOld.contains(saleCollectionNewSale)) {
                    SaleType oldSaletypeIdOfSaleCollectionNewSale = saleCollectionNewSale.getSaletypeId();
                    saleCollectionNewSale.setSaletypeId(saleType);
                    saleCollectionNewSale = em.merge(saleCollectionNewSale);
                    if (oldSaletypeIdOfSaleCollectionNewSale != null && !oldSaletypeIdOfSaleCollectionNewSale.equals(saleType)) {
                        oldSaletypeIdOfSaleCollectionNewSale.getSaleCollection().remove(saleCollectionNewSale);
                        oldSaletypeIdOfSaleCollectionNewSale = em.merge(oldSaletypeIdOfSaleCollectionNewSale);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = saleType.getId();
                if (findSaleType(id) == null) {
                    throw new NonexistentEntityException("The saleType with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SaleType saleType;
            try {
                saleType = em.getReference(SaleType.class, id);
                saleType.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The saleType with id " + id + " no longer exists.", enfe);
            }
            List<Sale> saleCollection = saleType.getSaleCollection();
            for (Sale saleCollectionSale : saleCollection) {
                saleCollectionSale.setSaletypeId(null);
                saleCollectionSale = em.merge(saleCollectionSale);
            }
            em.remove(saleType);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SaleType> findSaleTypeEntities() {
        return findSaleTypeEntities(true, -1, -1);
    }

    public List<SaleType> findSaleTypeEntities(int maxResults, int firstResult) {
        return findSaleTypeEntities(false, maxResults, firstResult);
    }

    private List<SaleType> findSaleTypeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from SaleType as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SaleType findSaleType(Integer id) {
        EntityManager em = getEntityManager();
        SaleType p;
        try {
            p = em.find(SaleType.class, id);
            em.refresh(p);
            return p;
        } finally {
            em.close();
        }
    }

    public int getSaleTypeCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from SaleType as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
