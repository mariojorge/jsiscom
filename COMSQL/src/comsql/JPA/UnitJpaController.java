/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.JPA.exceptions.NonexistentEntityException;
import comsql.Unit;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author usuario
 */
public class UnitJpaController {
    private static UnitJpaController jpa;

    private UnitJpaController() {

    }

    public static UnitJpaController getInstance() {
        if (jpa == null) {
            jpa = new UnitJpaController();
        }
        return jpa;
    }
    //private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(Unit unit) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(unit);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Unit unit) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            unit = em.merge(unit);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = unit.getId();
                if (findUnit(id) == null) {
                    throw new NonexistentEntityException("The unit with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Unit unit;
            try {
                unit = em.getReference(Unit.class, id);
                unit.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The unit with id " + id + " no longer exists.", enfe);
            }
            em.remove(unit);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Unit> findUnitEntities() {
        return findUnitEntities(true, -1, -1);
    }

    public List<Unit> findUnitEntities(int maxResults, int firstResult) {
        return findUnitEntities(false, maxResults, firstResult);
    }

    private List<Unit> findUnitEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Unit as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Unit findUnit(Integer id) {
        EntityManager em = getEntityManager();
        Unit u;
        try {
            u = em.find(Unit.class, id);
            em.refresh(u);
            return u;
        } finally {
            em.close();
        }
    }

    public int getUnitCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from Unit as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
