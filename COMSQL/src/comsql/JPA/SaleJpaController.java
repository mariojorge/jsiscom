/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.JPA.exceptions.IllegalOrphanException;
import comsql.JPA.exceptions.NonexistentEntityException;
import comsql.Sale;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import comsql.Customer;
import comsql.SaleType;
import comsql.SaleProduct;
import java.util.ArrayList;
import java.util.List;
import comsql.ReceptAccount;

/**
 *
 * @author usuario
 */
public class SaleJpaController {
    private static SaleJpaController jpa;

    private SaleJpaController() {

    }

    public static SaleJpaController getInstance() {
        if (jpa == null) {
            jpa = new SaleJpaController();
        }
        return jpa;
    }

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(Sale sale) {
        if (sale.getSaleProductCollection() == null) {
            sale.setSaleProductCollection(new ArrayList<SaleProduct>());
        }
        if (sale.getReceptAccountCollection() == null) {
            sale.setReceptAccountCollection(new ArrayList<ReceptAccount>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Customer customerId = sale.getCustomerId();
            if (customerId != null) {
                customerId = em.getReference(customerId.getClass(), customerId.getId());
                sale.setCustomerId(customerId);
            }
            SaleType saletypeId = sale.getSaletypeId();
            if (saletypeId != null) {
                saletypeId = em.getReference(saletypeId.getClass(), saletypeId.getId());
                sale.setSaletypeId(saletypeId);
            }
            List<SaleProduct> attachedSaleProductCollection = new ArrayList<SaleProduct>();
            for (SaleProduct saleProductCollectionSaleProductToAttach : sale.getSaleProductCollection()) {
                saleProductCollectionSaleProductToAttach = em.getReference(saleProductCollectionSaleProductToAttach.getClass(), saleProductCollectionSaleProductToAttach.getSaleProductPK());
                attachedSaleProductCollection.add(saleProductCollectionSaleProductToAttach);
            }
            sale.setSaleProductCollection(attachedSaleProductCollection);
            List<ReceptAccount> attachedReceptAccountCollection = new ArrayList<ReceptAccount>();
            for (ReceptAccount receptAccountCollectionReceptAccountToAttach : sale.getReceptAccountCollection()) {
                receptAccountCollectionReceptAccountToAttach = em.getReference(receptAccountCollectionReceptAccountToAttach.getClass(), receptAccountCollectionReceptAccountToAttach.getId());
                attachedReceptAccountCollection.add(receptAccountCollectionReceptAccountToAttach);
            }
            sale.setReceptAccountCollection(attachedReceptAccountCollection);
            em.persist(sale);
            if (customerId != null) {
                customerId.getSaleCollection().add(sale);
                customerId = em.merge(customerId);
            }
            if (saletypeId != null) {
                saletypeId.getSaleCollection().add(sale);
                saletypeId = em.merge(saletypeId);
            }
            for (SaleProduct saleProductCollectionSaleProduct : sale.getSaleProductCollection()) {
                Sale oldSaleOfSaleProductCollectionSaleProduct = saleProductCollectionSaleProduct.getSale();
                saleProductCollectionSaleProduct.setSale(sale);
                saleProductCollectionSaleProduct = em.merge(saleProductCollectionSaleProduct);
                if (oldSaleOfSaleProductCollectionSaleProduct != null) {
                    oldSaleOfSaleProductCollectionSaleProduct.getSaleProductCollection().remove(saleProductCollectionSaleProduct);
                    oldSaleOfSaleProductCollectionSaleProduct = em.merge(oldSaleOfSaleProductCollectionSaleProduct);
                }
            }
            for (ReceptAccount receptAccountCollectionReceptAccount : sale.getReceptAccountCollection()) {
                Sale oldSaleIdOfReceptAccountCollectionReceptAccount = receptAccountCollectionReceptAccount.getSaleId();
                receptAccountCollectionReceptAccount.setSaleId(sale);
                receptAccountCollectionReceptAccount = em.merge(receptAccountCollectionReceptAccount);
                if (oldSaleIdOfReceptAccountCollectionReceptAccount != null) {
                    oldSaleIdOfReceptAccountCollectionReceptAccount.getReceptAccountCollection().remove(receptAccountCollectionReceptAccount);
                    oldSaleIdOfReceptAccountCollectionReceptAccount = em.merge(oldSaleIdOfReceptAccountCollectionReceptAccount);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Sale sale) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sale persistentSale = em.find(Sale.class, sale.getId());
            Customer customerIdOld = persistentSale.getCustomerId();
            Customer customerIdNew = sale.getCustomerId();
            SaleType saletypeIdOld = persistentSale.getSaletypeId();
            SaleType saletypeIdNew = sale.getSaletypeId();
            List<SaleProduct> saleProductCollectionOld = persistentSale.getSaleProductCollection();
            List<SaleProduct> saleProductCollectionNew = sale.getSaleProductCollection();
            List<ReceptAccount> receptAccountCollectionOld = persistentSale.getReceptAccountCollection();
            List<ReceptAccount> receptAccountCollectionNew = sale.getReceptAccountCollection();
            List<String> illegalOrphanMessages = null;
            for (SaleProduct saleProductCollectionOldSaleProduct : saleProductCollectionOld) {
                if (!saleProductCollectionNew.contains(saleProductCollectionOldSaleProduct)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain SaleProduct " + saleProductCollectionOldSaleProduct + " since its sale field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (customerIdNew != null) {
                customerIdNew = em.getReference(customerIdNew.getClass(), customerIdNew.getId());
                sale.setCustomerId(customerIdNew);
            }
            if (saletypeIdNew != null) {
                saletypeIdNew = em.getReference(saletypeIdNew.getClass(), saletypeIdNew.getId());
                sale.setSaletypeId(saletypeIdNew);
            }
            List<SaleProduct> attachedSaleProductCollectionNew = new ArrayList<SaleProduct>();
            for (SaleProduct saleProductCollectionNewSaleProductToAttach : saleProductCollectionNew) {
                saleProductCollectionNewSaleProductToAttach = em.getReference(saleProductCollectionNewSaleProductToAttach.getClass(), saleProductCollectionNewSaleProductToAttach.getSaleProductPK());
                attachedSaleProductCollectionNew.add(saleProductCollectionNewSaleProductToAttach);
            }
            saleProductCollectionNew = attachedSaleProductCollectionNew;
            sale.setSaleProductCollection(saleProductCollectionNew);
            List<ReceptAccount> attachedReceptAccountCollectionNew = new ArrayList<ReceptAccount>();
            for (ReceptAccount receptAccountCollectionNewReceptAccountToAttach : receptAccountCollectionNew) {
                receptAccountCollectionNewReceptAccountToAttach = em.getReference(receptAccountCollectionNewReceptAccountToAttach.getClass(), receptAccountCollectionNewReceptAccountToAttach.getId());
                attachedReceptAccountCollectionNew.add(receptAccountCollectionNewReceptAccountToAttach);
            }
            receptAccountCollectionNew = attachedReceptAccountCollectionNew;
            sale.setReceptAccountCollection(receptAccountCollectionNew);
            sale = em.merge(sale);
            if (customerIdOld != null && !customerIdOld.equals(customerIdNew)) {
                customerIdOld.getSaleCollection().remove(sale);
                customerIdOld = em.merge(customerIdOld);
            }
            if (customerIdNew != null && !customerIdNew.equals(customerIdOld)) {
                customerIdNew.getSaleCollection().add(sale);
                customerIdNew = em.merge(customerIdNew);
            }
            if (saletypeIdOld != null && !saletypeIdOld.equals(saletypeIdNew)) {
                saletypeIdOld.getSaleCollection().remove(sale);
                saletypeIdOld = em.merge(saletypeIdOld);
            }
            if (saletypeIdNew != null && !saletypeIdNew.equals(saletypeIdOld)) {
                saletypeIdNew.getSaleCollection().add(sale);
                saletypeIdNew = em.merge(saletypeIdNew);
            }
            for (SaleProduct saleProductCollectionNewSaleProduct : saleProductCollectionNew) {
                if (!saleProductCollectionOld.contains(saleProductCollectionNewSaleProduct)) {
                    Sale oldSaleOfSaleProductCollectionNewSaleProduct = saleProductCollectionNewSaleProduct.getSale();
                    saleProductCollectionNewSaleProduct.setSale(sale);
                    saleProductCollectionNewSaleProduct = em.merge(saleProductCollectionNewSaleProduct);
                    if (oldSaleOfSaleProductCollectionNewSaleProduct != null && !oldSaleOfSaleProductCollectionNewSaleProduct.equals(sale)) {
                        oldSaleOfSaleProductCollectionNewSaleProduct.getSaleProductCollection().remove(saleProductCollectionNewSaleProduct);
                        oldSaleOfSaleProductCollectionNewSaleProduct = em.merge(oldSaleOfSaleProductCollectionNewSaleProduct);
                    }
                }
            }
            for (ReceptAccount receptAccountCollectionOldReceptAccount : receptAccountCollectionOld) {
                if (!receptAccountCollectionNew.contains(receptAccountCollectionOldReceptAccount)) {
                    receptAccountCollectionOldReceptAccount.setSaleId(null);
                    receptAccountCollectionOldReceptAccount = em.merge(receptAccountCollectionOldReceptAccount);
                }
            }
            for (ReceptAccount receptAccountCollectionNewReceptAccount : receptAccountCollectionNew) {
                if (!receptAccountCollectionOld.contains(receptAccountCollectionNewReceptAccount)) {
                    Sale oldSaleIdOfReceptAccountCollectionNewReceptAccount = receptAccountCollectionNewReceptAccount.getSaleId();
                    receptAccountCollectionNewReceptAccount.setSaleId(sale);
                    receptAccountCollectionNewReceptAccount = em.merge(receptAccountCollectionNewReceptAccount);
                    if (oldSaleIdOfReceptAccountCollectionNewReceptAccount != null && !oldSaleIdOfReceptAccountCollectionNewReceptAccount.equals(sale)) {
                        oldSaleIdOfReceptAccountCollectionNewReceptAccount.getReceptAccountCollection().remove(receptAccountCollectionNewReceptAccount);
                        oldSaleIdOfReceptAccountCollectionNewReceptAccount = em.merge(oldSaleIdOfReceptAccountCollectionNewReceptAccount);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = sale.getId();
                if (findSale(id) == null) {
                    throw new NonexistentEntityException("The sale with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sale sale;
            try {
                sale = em.getReference(Sale.class, id);
                sale.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The sale with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<SaleProduct> saleProductCollectionOrphanCheck = sale.getSaleProductCollection();
            for (SaleProduct saleProductCollectionOrphanCheckSaleProduct : saleProductCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sale (" + sale + ") cannot be destroyed since the SaleProduct " + saleProductCollectionOrphanCheckSaleProduct + " in its saleProductCollection field has a non-nullable sale field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Customer customerId = sale.getCustomerId();
            if (customerId != null) {
                customerId.getSaleCollection().remove(sale);
                customerId = em.merge(customerId);
            }
            SaleType saletypeId = sale.getSaletypeId();
            if (saletypeId != null) {
                saletypeId.getSaleCollection().remove(sale);
                saletypeId = em.merge(saletypeId);
            }
            List<ReceptAccount> receptAccountCollection = sale.getReceptAccountCollection();
            for (ReceptAccount receptAccountCollectionReceptAccount : receptAccountCollection) {
                receptAccountCollectionReceptAccount.setSaleId(null);
                receptAccountCollectionReceptAccount = em.merge(receptAccountCollectionReceptAccount);
            }
            em.remove(sale);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Sale> findSaleEntities() {
        return findSaleEntities(true, -1, -1);
    }

    public List<Sale> findSaleEntities(int maxResults, int firstResult) {
        return findSaleEntities(false, maxResults, firstResult);
    }

    private List<Sale> findSaleEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Sale as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Sale findSale(Integer id) {
        EntityManager em = getEntityManager();
        Sale s;
        try {
            s = em.find(Sale.class, id);
            em.refresh(s);
            return s;
        } finally {
            em.close();
        }
    }

    public int getSaleCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from Sale as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
