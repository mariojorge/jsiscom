/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.JPA.exceptions.IllegalOrphanException;
import comsql.JPA.exceptions.NonexistentEntityException;
import comsql.Supplier;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import comsql.Address;
import comsql.Purchase;
import java.util.ArrayList;
import java.util.List;
import comsql.PayAccount;

/**
 *
 * @author usuario
 */
public class SupplierJpaController {
    private static SupplierJpaController jpa;

    private SupplierJpaController() {

    }

    public static SupplierJpaController getInstance() {
        if (jpa == null) {
            jpa = new SupplierJpaController();
        }
        return jpa;
    }

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(Supplier supplier) {
        if (supplier.getPurchaseCollection() == null) {
            supplier.setPurchaseCollection(new ArrayList<Purchase>());
        }
        if (supplier.getPayAccountCollection() == null) {
            supplier.setPayAccountCollection(new ArrayList<PayAccount>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Address addressId = supplier.getAddressId();
            if (addressId != null) {
                addressId = em.getReference(addressId.getClass(), addressId.getId());
                supplier.setAddressId(addressId);
            }
            List<Purchase> attachedPurchaseCollection = new ArrayList<Purchase>();
            for (Purchase purchaseCollectionPurchaseToAttach : supplier.getPurchaseCollection()) {
                purchaseCollectionPurchaseToAttach = em.getReference(purchaseCollectionPurchaseToAttach.getClass(), purchaseCollectionPurchaseToAttach.getId());
                attachedPurchaseCollection.add(purchaseCollectionPurchaseToAttach);
            }
            supplier.setPurchaseCollection(attachedPurchaseCollection);
            List<PayAccount> attachedPayAccountCollection = new ArrayList<PayAccount>();
            for (PayAccount payAccountCollectionPayAccountToAttach : supplier.getPayAccountCollection()) {
                payAccountCollectionPayAccountToAttach = em.getReference(payAccountCollectionPayAccountToAttach.getClass(), payAccountCollectionPayAccountToAttach.getId());
                attachedPayAccountCollection.add(payAccountCollectionPayAccountToAttach);
            }
            supplier.setPayAccountCollection(attachedPayAccountCollection);
            em.persist(supplier);
            if (addressId != null) {
                addressId.getSupplierCollection().add(supplier);
                addressId = em.merge(addressId);
            }
            for (Purchase purchaseCollectionPurchase : supplier.getPurchaseCollection()) {
                Supplier oldSupplierIdOfPurchaseCollectionPurchase = purchaseCollectionPurchase.getSupplierId();
                purchaseCollectionPurchase.setSupplierId(supplier);
                purchaseCollectionPurchase = em.merge(purchaseCollectionPurchase);
                if (oldSupplierIdOfPurchaseCollectionPurchase != null) {
                    oldSupplierIdOfPurchaseCollectionPurchase.getPurchaseCollection().remove(purchaseCollectionPurchase);
                    oldSupplierIdOfPurchaseCollectionPurchase = em.merge(oldSupplierIdOfPurchaseCollectionPurchase);
                }
            }
            for (PayAccount payAccountCollectionPayAccount : supplier.getPayAccountCollection()) {
                Supplier oldSupplierIdOfPayAccountCollectionPayAccount = payAccountCollectionPayAccount.getSupplierId();
                payAccountCollectionPayAccount.setSupplierId(supplier);
                payAccountCollectionPayAccount = em.merge(payAccountCollectionPayAccount);
                if (oldSupplierIdOfPayAccountCollectionPayAccount != null) {
                    oldSupplierIdOfPayAccountCollectionPayAccount.getPayAccountCollection().remove(payAccountCollectionPayAccount);
                    oldSupplierIdOfPayAccountCollectionPayAccount = em.merge(oldSupplierIdOfPayAccountCollectionPayAccount);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Supplier supplier) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Supplier persistentSupplier = em.find(Supplier.class, supplier.getId());
            Address addressIdOld = persistentSupplier.getAddressId();
            Address addressIdNew = supplier.getAddressId();
            List<Purchase> purchaseCollectionOld = persistentSupplier.getPurchaseCollection();
            List<Purchase> purchaseCollectionNew = supplier.getPurchaseCollection();
            List<PayAccount> payAccountCollectionOld = persistentSupplier.getPayAccountCollection();
            List<PayAccount> payAccountCollectionNew = supplier.getPayAccountCollection();
            List<String> illegalOrphanMessages = null;
            for (Purchase purchaseCollectionOldPurchase : purchaseCollectionOld) {
                if (!purchaseCollectionNew.contains(purchaseCollectionOldPurchase)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Purchase " + purchaseCollectionOldPurchase + " since its supplierId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (addressIdNew != null) {
                addressIdNew = em.getReference(addressIdNew.getClass(), addressIdNew.getId());
                supplier.setAddressId(addressIdNew);
            }
            List<Purchase> attachedPurchaseCollectionNew = new ArrayList<Purchase>();
            for (Purchase purchaseCollectionNewPurchaseToAttach : purchaseCollectionNew) {
                purchaseCollectionNewPurchaseToAttach = em.getReference(purchaseCollectionNewPurchaseToAttach.getClass(), purchaseCollectionNewPurchaseToAttach.getId());
                attachedPurchaseCollectionNew.add(purchaseCollectionNewPurchaseToAttach);
            }
            purchaseCollectionNew = attachedPurchaseCollectionNew;
            supplier.setPurchaseCollection(purchaseCollectionNew);
            List<PayAccount> attachedPayAccountCollectionNew = new ArrayList<PayAccount>();
            for (PayAccount payAccountCollectionNewPayAccountToAttach : payAccountCollectionNew) {
                payAccountCollectionNewPayAccountToAttach = em.getReference(payAccountCollectionNewPayAccountToAttach.getClass(), payAccountCollectionNewPayAccountToAttach.getId());
                attachedPayAccountCollectionNew.add(payAccountCollectionNewPayAccountToAttach);
            }
            payAccountCollectionNew = attachedPayAccountCollectionNew;
            supplier.setPayAccountCollection(payAccountCollectionNew);
            supplier = em.merge(supplier);
            if (addressIdOld != null && !addressIdOld.equals(addressIdNew)) {
                addressIdOld.getSupplierCollection().remove(supplier);
                addressIdOld = em.merge(addressIdOld);
            }
            if (addressIdNew != null && !addressIdNew.equals(addressIdOld)) {
                addressIdNew.getSupplierCollection().add(supplier);
                addressIdNew = em.merge(addressIdNew);
            }
            for (Purchase purchaseCollectionNewPurchase : purchaseCollectionNew) {
                if (!purchaseCollectionOld.contains(purchaseCollectionNewPurchase)) {
                    Supplier oldSupplierIdOfPurchaseCollectionNewPurchase = purchaseCollectionNewPurchase.getSupplierId();
                    purchaseCollectionNewPurchase.setSupplierId(supplier);
                    purchaseCollectionNewPurchase = em.merge(purchaseCollectionNewPurchase);
                    if (oldSupplierIdOfPurchaseCollectionNewPurchase != null && !oldSupplierIdOfPurchaseCollectionNewPurchase.equals(supplier)) {
                        oldSupplierIdOfPurchaseCollectionNewPurchase.getPurchaseCollection().remove(purchaseCollectionNewPurchase);
                        oldSupplierIdOfPurchaseCollectionNewPurchase = em.merge(oldSupplierIdOfPurchaseCollectionNewPurchase);
                    }
                }
            }
            for (PayAccount payAccountCollectionOldPayAccount : payAccountCollectionOld) {
                if (!payAccountCollectionNew.contains(payAccountCollectionOldPayAccount)) {
                    payAccountCollectionOldPayAccount.setSupplierId(null);
                    payAccountCollectionOldPayAccount = em.merge(payAccountCollectionOldPayAccount);
                }
            }
            for (PayAccount payAccountCollectionNewPayAccount : payAccountCollectionNew) {
                if (!payAccountCollectionOld.contains(payAccountCollectionNewPayAccount)) {
                    Supplier oldSupplierIdOfPayAccountCollectionNewPayAccount = payAccountCollectionNewPayAccount.getSupplierId();
                    payAccountCollectionNewPayAccount.setSupplierId(supplier);
                    payAccountCollectionNewPayAccount = em.merge(payAccountCollectionNewPayAccount);
                    if (oldSupplierIdOfPayAccountCollectionNewPayAccount != null && !oldSupplierIdOfPayAccountCollectionNewPayAccount.equals(supplier)) {
                        oldSupplierIdOfPayAccountCollectionNewPayAccount.getPayAccountCollection().remove(payAccountCollectionNewPayAccount);
                        oldSupplierIdOfPayAccountCollectionNewPayAccount = em.merge(oldSupplierIdOfPayAccountCollectionNewPayAccount);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = supplier.getId();
                if (findSupplier(id) == null) {
                    throw new NonexistentEntityException("The supplier with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Supplier supplier;
            try {
                supplier = em.getReference(Supplier.class, id);
                supplier.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The supplier with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Purchase> purchaseCollectionOrphanCheck = supplier.getPurchaseCollection();
            for (Purchase purchaseCollectionOrphanCheckPurchase : purchaseCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Supplier (" + supplier + ") cannot be destroyed since the Purchase " + purchaseCollectionOrphanCheckPurchase + " in its purchaseCollection field has a non-nullable supplierId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Address addressId = supplier.getAddressId();
            if (addressId != null) {
                addressId.getSupplierCollection().remove(supplier);
                addressId = em.merge(addressId);
            }
            List<PayAccount> payAccountCollection = supplier.getPayAccountCollection();
            for (PayAccount payAccountCollectionPayAccount : payAccountCollection) {
                payAccountCollectionPayAccount.setSupplierId(null);
                payAccountCollectionPayAccount = em.merge(payAccountCollectionPayAccount);
            }
            em.remove(supplier);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Supplier> findSupplierEntities() {
        return findSupplierEntities(true, -1, -1);
    }

    public List<Supplier> findSupplierEntities(int maxResults, int firstResult) {
        return findSupplierEntities(false, maxResults, firstResult);
    }

    private List<Supplier> findSupplierEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Supplier as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Supplier findSupplier(Integer id) {
        EntityManager em = getEntityManager();
        Supplier s;
        try {
            s = em.find(Supplier.class, id);
            em.refresh(s);
            return s;
        } finally {
            em.close();
        }
    }

    public int getSupplierCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from Supplier as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
