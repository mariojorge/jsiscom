/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import util.DbConfig;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author usuario
 */
public class EntityManagerProvider {

    private static EntityManagerFactory emf = null;

	private EntityManagerProvider() {

	}

	public static EntityManager getEntityManagerFactory()
	{
		if (emf == null)
		{
			emf = Persistence.createEntityManagerFactory("COMSQLPU", DbConfig.getMap());
		}
		return emf.createEntityManager();		
	}

    public Boolean isConected() {
        return emf.isOpen();
    }

}
