/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.exceptions.NonexistentEntityException;
import comsql.PurchaseType;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import comsql.Purchase;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author usuario
 */
public class PurchaseTypeJpaController {
    private static PurchaseTypeJpaController jpa;

    private PurchaseTypeJpaController() {

    }

    public static PurchaseTypeJpaController getInstance() {
        if (jpa == null) {
            jpa = new PurchaseTypeJpaController();
        }
        return jpa;
    }

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(PurchaseType purchaseType) {
        if (purchaseType.getPurchaseCollection() == null) {
            purchaseType.setPurchaseCollection(new ArrayList<Purchase>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Purchase> attachedPurchaseCollection = new ArrayList<Purchase>();
            for (Purchase purchaseCollectionPurchaseToAttach : purchaseType.getPurchaseCollection()) {
                purchaseCollectionPurchaseToAttach = em.getReference(purchaseCollectionPurchaseToAttach.getClass(), purchaseCollectionPurchaseToAttach.getId());
                attachedPurchaseCollection.add(purchaseCollectionPurchaseToAttach);
            }
            purchaseType.setPurchaseCollection(attachedPurchaseCollection);
            em.persist(purchaseType);
            for (Purchase purchaseCollectionPurchase : purchaseType.getPurchaseCollection()) {
                PurchaseType oldPurchasetypeIdOfPurchaseCollectionPurchase = purchaseCollectionPurchase.getPurchasetypeId();
                purchaseCollectionPurchase.setPurchasetypeId(purchaseType);
                purchaseCollectionPurchase = em.merge(purchaseCollectionPurchase);
                if (oldPurchasetypeIdOfPurchaseCollectionPurchase != null) {
                    oldPurchasetypeIdOfPurchaseCollectionPurchase.getPurchaseCollection().remove(purchaseCollectionPurchase);
                    oldPurchasetypeIdOfPurchaseCollectionPurchase = em.merge(oldPurchasetypeIdOfPurchaseCollectionPurchase);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PurchaseType purchaseType) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PurchaseType persistentPurchaseType = em.find(PurchaseType.class, purchaseType.getId());
            List<Purchase> purchaseCollectionOld = persistentPurchaseType.getPurchaseCollection();
            List<Purchase> purchaseCollectionNew = purchaseType.getPurchaseCollection();
            List<Purchase> attachedPurchaseCollectionNew = new ArrayList<Purchase>();
            for (Purchase purchaseCollectionNewPurchaseToAttach : purchaseCollectionNew) {
                purchaseCollectionNewPurchaseToAttach = em.getReference(purchaseCollectionNewPurchaseToAttach.getClass(), purchaseCollectionNewPurchaseToAttach.getId());
                attachedPurchaseCollectionNew.add(purchaseCollectionNewPurchaseToAttach);
            }
            purchaseCollectionNew = attachedPurchaseCollectionNew;
            purchaseType.setPurchaseCollection(purchaseCollectionNew);
            purchaseType = em.merge(purchaseType);
            for (Purchase purchaseCollectionOldPurchase : purchaseCollectionOld) {
                if (!purchaseCollectionNew.contains(purchaseCollectionOldPurchase)) {
                    purchaseCollectionOldPurchase.setPurchasetypeId(null);
                    purchaseCollectionOldPurchase = em.merge(purchaseCollectionOldPurchase);
                }
            }
            for (Purchase purchaseCollectionNewPurchase : purchaseCollectionNew) {
                if (!purchaseCollectionOld.contains(purchaseCollectionNewPurchase)) {
                    PurchaseType oldPurchasetypeIdOfPurchaseCollectionNewPurchase = purchaseCollectionNewPurchase.getPurchasetypeId();
                    purchaseCollectionNewPurchase.setPurchasetypeId(purchaseType);
                    purchaseCollectionNewPurchase = em.merge(purchaseCollectionNewPurchase);
                    if (oldPurchasetypeIdOfPurchaseCollectionNewPurchase != null && !oldPurchasetypeIdOfPurchaseCollectionNewPurchase.equals(purchaseType)) {
                        oldPurchasetypeIdOfPurchaseCollectionNewPurchase.getPurchaseCollection().remove(purchaseCollectionNewPurchase);
                        oldPurchasetypeIdOfPurchaseCollectionNewPurchase = em.merge(oldPurchasetypeIdOfPurchaseCollectionNewPurchase);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = purchaseType.getId();
                if (findPurchaseType(id) == null) {
                    throw new NonexistentEntityException("The purchaseType with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PurchaseType purchaseType;
            try {
                purchaseType = em.getReference(PurchaseType.class, id);
                purchaseType.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The purchaseType with id " + id + " no longer exists.", enfe);
            }
            List<Purchase> purchaseCollection = purchaseType.getPurchaseCollection();
            for (Purchase purchaseCollectionPurchase : purchaseCollection) {
                purchaseCollectionPurchase.setPurchasetypeId(null);
                purchaseCollectionPurchase = em.merge(purchaseCollectionPurchase);
            }
            em.remove(purchaseType);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PurchaseType> findPurchaseTypeEntities() {
        return findPurchaseTypeEntities(true, -1, -1);
    }

    public List<PurchaseType> findPurchaseTypeEntities(int maxResults, int firstResult) {
        return findPurchaseTypeEntities(false, maxResults, firstResult);
    }

    private List<PurchaseType> findPurchaseTypeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from PurchaseType as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PurchaseType findPurchaseType(Integer id) {
        EntityManager em = getEntityManager();
        PurchaseType p;
        try {
            p = em.find(PurchaseType.class, id);
            em.refresh(p);
            return p;
        } finally {
            em.close();
        }
    }

    public int getPurchaseTypeCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from PurchaseType as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
