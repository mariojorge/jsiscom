/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.*;
import comsql.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author usuario
 */
public class PayPlotJpaController {
    private static PayPlotJpaController jpa;

    public static PayPlotJpaController getInstance() {
        if (jpa == null) {
            jpa = new PayPlotJpaController();
        }
        return jpa;
    }

    private PayPlotJpaController() {

    }

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(PayPlot payPlot) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PayAccount payaccountId = payPlot.getPayaccountId();
            if (payaccountId != null) {
                payaccountId = em.getReference(payaccountId.getClass(), payaccountId.getId());
                payPlot.setPayaccountId(payaccountId);
            }
            em.persist(payPlot);
            if (payaccountId != null) {
                payaccountId.getPayPlotCollection().add(payPlot);
                payaccountId = em.merge(payaccountId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PayPlot payPlot) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PayPlot persistentPayPlot = em.find(PayPlot.class, payPlot.getId());
            PayAccount payaccountIdOld = persistentPayPlot.getPayaccountId();
            PayAccount payaccountIdNew = payPlot.getPayaccountId();
            if (payaccountIdNew != null) {
                payaccountIdNew = em.getReference(payaccountIdNew.getClass(), payaccountIdNew.getId());
                payPlot.setPayaccountId(payaccountIdNew);
            }
            payPlot = em.merge(payPlot);
            if (payaccountIdOld != null && !payaccountIdOld.equals(payaccountIdNew)) {
                payaccountIdOld.getPayPlotCollection().remove(payPlot);
                payaccountIdOld = em.merge(payaccountIdOld);
            }
            if (payaccountIdNew != null && !payaccountIdNew.equals(payaccountIdOld)) {
                payaccountIdNew.getPayPlotCollection().add(payPlot);
                payaccountIdNew = em.merge(payaccountIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = payPlot.getId();
                if (findPayPlot(id) == null) {
                    throw new NonexistentEntityException("The payPlot with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PayPlot payPlot;
            try {
                payPlot = em.getReference(PayPlot.class, id);
                payPlot.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The payPlot with id " + id + " no longer exists.", enfe);
            }
            PayAccount payaccountId = payPlot.getPayaccountId();
            if (payaccountId != null) {
                payaccountId.getPayPlotCollection().remove(payPlot);
                payaccountId = em.merge(payaccountId);
            }
            em.remove(payPlot);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PayPlot> findPayPlotEntities() {
        return findPayPlotEntities(true, -1, -1);
    }

    public List<PayPlot> findPayPlotEntities(int maxResults, int firstResult) {
        return findPayPlotEntities(false, maxResults, firstResult);
    }

    private List<PayPlot> findPayPlotEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from PayPlot as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PayPlot findPayPlot(Integer id) {
        EntityManager em = getEntityManager();
        PayPlot p;
        try {
            p = em.find(PayPlot.class, id);
            em.refresh(p);
            return p;
        } finally {
            em.close();
        }
    }

    public int getPayPlotCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from PayPlot as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
