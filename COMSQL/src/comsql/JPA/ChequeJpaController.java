/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import util.exceptions.NonexistentEntityException;
import comsql.Cheque;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;

/**
 *
 * @author Gilderlan
 */
public class ChequeJpaController {
    private static ChequeJpaController cjpac;

    private ChequeJpaController () {

    }

    public static ChequeJpaController getInstance () {
        if (cjpac == null) {
            cjpac = new ChequeJpaController();
        }
        return cjpac;
    }

    public EntityManager getEntityManager () {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(Cheque cheque) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(cheque);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Cheque cheque) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            cheque = em.merge(cheque);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cheque.getId();
                if (findUser(id) == null) {
                    throw new NonexistentEntityException("The user with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cheque cheque;
            try {
                cheque = em.getReference(Cheque.class, id);
                cheque.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The user with id " + id + " no longer exists.", enfe);
            }
            em.remove(cheque);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Cheque> findUserEntities() {
        return findUserEntities(true, -1, -1);
    }

    public List<Cheque> findUserEntities(int maxResults, int firstResult) {
        return findUserEntities(false, maxResults, firstResult);
    }

    private List<Cheque> findUserEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from User as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Cheque findUser(Integer id) {
        EntityManager em = getEntityManager();
        Cheque c;
        try {
            c = em.find(Cheque.class, id);
            em.refresh(c);
            return c;
        } finally {
            em.close();
        }
    }

    public int getUserCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from User as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
}
