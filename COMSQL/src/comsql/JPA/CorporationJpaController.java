/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.*;
import comsql.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author usuario
 */
public class CorporationJpaController {
    private static CorporationJpaController cjpa;

    private CorporationJpaController() {
        
    }

    private CorporationJpaController(Map config) {
        //emf = Persistence.createEntityManagerFactory("COMSQLPU", config);
    }

    public static CorporationJpaController getInstance() {
        if (cjpa == null) {
            cjpa = new CorporationJpaController();
        }
        return cjpa;
    }

    // private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(Corporation corporation) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(corporation);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Corporation corporation) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            corporation = em.merge(corporation);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = corporation.getId();
                if (findCorporation(id) == null) {
                    throw new NonexistentEntityException("The corporation with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Corporation corporation;
            try {
                corporation = em.getReference(Corporation.class, id);
                corporation.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The corporation with id " + id + " no longer exists.", enfe);
            }
            em.remove(corporation);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Corporation> findCorporationEntities() {
        return findCorporationEntities(true, -1, -1);
    }

    public List<Corporation> findCorporationEntities(int maxResults, int firstResult) {
        return findCorporationEntities(false, maxResults, firstResult);
    }

    private List<Corporation> findCorporationEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Corporation as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Corporation findCorporation(Integer id) {
        EntityManager em = getEntityManager();
        Corporation c;
        try {
            c = em.find(Corporation.class, id);
            em.refresh(c);
            return c;
        } finally {
            em.close();
        }
    }

    public int getCorporationCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from Corporation as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
