/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.*;
import comsql.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author usuario
 */
public class ReceptedPlotJpaController {
    private static ReceptedPlotJpaController jpa;

    private ReceptedPlotJpaController() {

    }

    public static ReceptedPlotJpaController getInstance() {
        if (jpa == null) {
            jpa = new ReceptedPlotJpaController();
        }
        return jpa;
    }

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(ReceptedPlot receptedPlot) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ReceptAccount receptaccountId = receptedPlot.getReceptaccountId();
            if (receptaccountId != null) {
                receptaccountId = em.getReference(receptaccountId.getClass(), receptaccountId.getId());
                receptedPlot.setReceptaccountId(receptaccountId);
            }
            em.persist(receptedPlot);
            if (receptaccountId != null) {
                receptaccountId.getReceptedPlotCollection().add(receptedPlot);
                receptaccountId = em.merge(receptaccountId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ReceptedPlot receptedPlot) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ReceptedPlot persistentReceptedPlot = em.find(ReceptedPlot.class, receptedPlot.getId());
            ReceptAccount receptaccountIdOld = persistentReceptedPlot.getReceptaccountId();
            ReceptAccount receptaccountIdNew = receptedPlot.getReceptaccountId();
            if (receptaccountIdNew != null) {
                receptaccountIdNew = em.getReference(receptaccountIdNew.getClass(), receptaccountIdNew.getId());
                receptedPlot.setReceptaccountId(receptaccountIdNew);
            }
            receptedPlot = em.merge(receptedPlot);
            if (receptaccountIdOld != null && !receptaccountIdOld.equals(receptaccountIdNew)) {
                receptaccountIdOld.getReceptedPlotCollection().remove(receptedPlot);
                receptaccountIdOld = em.merge(receptaccountIdOld);
            }
            if (receptaccountIdNew != null && !receptaccountIdNew.equals(receptaccountIdOld)) {
                receptaccountIdNew.getReceptedPlotCollection().add(receptedPlot);
                receptaccountIdNew = em.merge(receptaccountIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = receptedPlot.getId();
                if (findReceptedPlot(id) == null) {
                    throw new NonexistentEntityException("The receptedPlot with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ReceptedPlot receptedPlot;
            try {
                receptedPlot = em.getReference(ReceptedPlot.class, id);
                receptedPlot.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The receptedPlot with id " + id + " no longer exists.", enfe);
            }
            ReceptAccount receptaccountId = receptedPlot.getReceptaccountId();
            if (receptaccountId != null) {
                receptaccountId.getReceptedPlotCollection().remove(receptedPlot);
                receptaccountId = em.merge(receptaccountId);
            }
            em.remove(receptedPlot);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ReceptedPlot> findReceptedPlotEntities() {
        return findReceptedPlotEntities(true, -1, -1);
    }

    public List<ReceptedPlot> findReceptedPlotEntities(int maxResults, int firstResult) {
        return findReceptedPlotEntities(false, maxResults, firstResult);
    }

    private List<ReceptedPlot> findReceptedPlotEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from ReceptedPlot as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ReceptedPlot findReceptedPlot(Integer id) {
        EntityManager em = getEntityManager();
        ReceptedPlot r;
        try {
            r = em.find(ReceptedPlot.class, id);
            em.refresh(r);
            return r;
        } finally {
            em.close();
        }
    }

    public int getReceptedPlotCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from ReceptedPlot as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
