/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.JPA.exceptions.IllegalOrphanException;
import comsql.JPA.exceptions.NonexistentEntityException;
import comsql.Purchase;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import comsql.PurchaseType;
import comsql.Supplier;
import comsql.PayAccount;
import java.util.ArrayList;
import java.util.List;
import comsql.PurchaseProduct;

/**
 *
 * @author usuario
 */
public class PurchaseJpaController {
    private static PurchaseJpaController jpa;

    private PurchaseJpaController() {

    }

    public static PurchaseJpaController getInstance() {
        if (jpa == null) {
            jpa = new PurchaseJpaController();
        }
        return jpa;
    }

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(Purchase purchase) {
        if (purchase.getPayAccountCollection() == null) {
            purchase.setPayAccountCollection(new ArrayList<PayAccount>());
        }
        if (purchase.getPurchaseProductCollection() == null) {
            purchase.setPurchaseProductCollection(new ArrayList<PurchaseProduct>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PurchaseType purchasetypeId = purchase.getPurchasetypeId();
            if (purchasetypeId != null) {
                purchasetypeId = em.getReference(purchasetypeId.getClass(), purchasetypeId.getId());
                purchase.setPurchasetypeId(purchasetypeId);
            }
            Supplier supplierId = purchase.getSupplierId();
            if (supplierId != null) {
                supplierId = em.getReference(supplierId.getClass(), supplierId.getId());
                purchase.setSupplierId(supplierId);
            }
            List<PayAccount> attachedPayAccountCollection = new ArrayList<PayAccount>();
            for (PayAccount payAccountCollectionPayAccountToAttach : purchase.getPayAccountCollection()) {
                payAccountCollectionPayAccountToAttach = em.getReference(payAccountCollectionPayAccountToAttach.getClass(), payAccountCollectionPayAccountToAttach.getId());
                attachedPayAccountCollection.add(payAccountCollectionPayAccountToAttach);
            }
            purchase.setPayAccountCollection(attachedPayAccountCollection);
            List<PurchaseProduct> attachedPurchaseProductCollection = new ArrayList<PurchaseProduct>();
            for (PurchaseProduct purchaseProductCollectionPurchaseProductToAttach : purchase.getPurchaseProductCollection()) {
                purchaseProductCollectionPurchaseProductToAttach = em.getReference(purchaseProductCollectionPurchaseProductToAttach.getClass(), purchaseProductCollectionPurchaseProductToAttach.getPurchaseProductPK());
                attachedPurchaseProductCollection.add(purchaseProductCollectionPurchaseProductToAttach);
            }
            purchase.setPurchaseProductCollection(attachedPurchaseProductCollection);
            em.persist(purchase);
            if (purchasetypeId != null) {
                purchasetypeId.getPurchaseCollection().add(purchase);
                purchasetypeId = em.merge(purchasetypeId);
            }
            if (supplierId != null) {
                supplierId.getPurchaseCollection().add(purchase);
                supplierId = em.merge(supplierId);
            }
            for (PayAccount payAccountCollectionPayAccount : purchase.getPayAccountCollection()) {
                Purchase oldPurchaseIdOfPayAccountCollectionPayAccount = payAccountCollectionPayAccount.getPurchaseId();
                payAccountCollectionPayAccount.setPurchaseId(purchase);
                payAccountCollectionPayAccount = em.merge(payAccountCollectionPayAccount);
                if (oldPurchaseIdOfPayAccountCollectionPayAccount != null) {
                    oldPurchaseIdOfPayAccountCollectionPayAccount.getPayAccountCollection().remove(payAccountCollectionPayAccount);
                    oldPurchaseIdOfPayAccountCollectionPayAccount = em.merge(oldPurchaseIdOfPayAccountCollectionPayAccount);
                }
            }
            for (PurchaseProduct purchaseProductCollectionPurchaseProduct : purchase.getPurchaseProductCollection()) {
                Purchase oldPurchaseOfPurchaseProductCollectionPurchaseProduct = purchaseProductCollectionPurchaseProduct.getPurchase();
                purchaseProductCollectionPurchaseProduct.setPurchase(purchase);
                purchaseProductCollectionPurchaseProduct = em.merge(purchaseProductCollectionPurchaseProduct);
                if (oldPurchaseOfPurchaseProductCollectionPurchaseProduct != null) {
                    oldPurchaseOfPurchaseProductCollectionPurchaseProduct.getPurchaseProductCollection().remove(purchaseProductCollectionPurchaseProduct);
                    oldPurchaseOfPurchaseProductCollectionPurchaseProduct = em.merge(oldPurchaseOfPurchaseProductCollectionPurchaseProduct);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Purchase purchase) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Purchase persistentPurchase = em.find(Purchase.class, purchase.getId());
            PurchaseType purchasetypeIdOld = persistentPurchase.getPurchasetypeId();
            PurchaseType purchasetypeIdNew = purchase.getPurchasetypeId();
            Supplier supplierIdOld = persistentPurchase.getSupplierId();
            Supplier supplierIdNew = purchase.getSupplierId();
            List<PayAccount> payAccountCollectionOld = persistentPurchase.getPayAccountCollection();
            List<PayAccount> payAccountCollectionNew = purchase.getPayAccountCollection();
            List<PurchaseProduct> purchaseProductCollectionOld = persistentPurchase.getPurchaseProductCollection();
            List<PurchaseProduct> purchaseProductCollectionNew = purchase.getPurchaseProductCollection();
            List<String> illegalOrphanMessages = null;
            for (PurchaseProduct purchaseProductCollectionOldPurchaseProduct : purchaseProductCollectionOld) {
                if (!purchaseProductCollectionNew.contains(purchaseProductCollectionOldPurchaseProduct)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain PurchaseProduct " + purchaseProductCollectionOldPurchaseProduct + " since its purchase field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (purchasetypeIdNew != null) {
                purchasetypeIdNew = em.getReference(purchasetypeIdNew.getClass(), purchasetypeIdNew.getId());
                purchase.setPurchasetypeId(purchasetypeIdNew);
            }
            if (supplierIdNew != null) {
                supplierIdNew = em.getReference(supplierIdNew.getClass(), supplierIdNew.getId());
                purchase.setSupplierId(supplierIdNew);
            }
            List<PayAccount> attachedPayAccountCollectionNew = new ArrayList<PayAccount>();
            for (PayAccount payAccountCollectionNewPayAccountToAttach : payAccountCollectionNew) {
                payAccountCollectionNewPayAccountToAttach = em.getReference(payAccountCollectionNewPayAccountToAttach.getClass(), payAccountCollectionNewPayAccountToAttach.getId());
                attachedPayAccountCollectionNew.add(payAccountCollectionNewPayAccountToAttach);
            }
            payAccountCollectionNew = attachedPayAccountCollectionNew;
            purchase.setPayAccountCollection(payAccountCollectionNew);
            List<PurchaseProduct> attachedPurchaseProductCollectionNew = new ArrayList<PurchaseProduct>();
            for (PurchaseProduct purchaseProductCollectionNewPurchaseProductToAttach : purchaseProductCollectionNew) {
                purchaseProductCollectionNewPurchaseProductToAttach = em.getReference(purchaseProductCollectionNewPurchaseProductToAttach.getClass(), purchaseProductCollectionNewPurchaseProductToAttach.getPurchaseProductPK());
                attachedPurchaseProductCollectionNew.add(purchaseProductCollectionNewPurchaseProductToAttach);
            }
            purchaseProductCollectionNew = attachedPurchaseProductCollectionNew;
            purchase.setPurchaseProductCollection(purchaseProductCollectionNew);
            purchase = em.merge(purchase);
            if (purchasetypeIdOld != null && !purchasetypeIdOld.equals(purchasetypeIdNew)) {
                purchasetypeIdOld.getPurchaseCollection().remove(purchase);
                purchasetypeIdOld = em.merge(purchasetypeIdOld);
            }
            if (purchasetypeIdNew != null && !purchasetypeIdNew.equals(purchasetypeIdOld)) {
                purchasetypeIdNew.getPurchaseCollection().add(purchase);
                purchasetypeIdNew = em.merge(purchasetypeIdNew);
            }
            if (supplierIdOld != null && !supplierIdOld.equals(supplierIdNew)) {
                supplierIdOld.getPurchaseCollection().remove(purchase);
                supplierIdOld = em.merge(supplierIdOld);
            }
            if (supplierIdNew != null && !supplierIdNew.equals(supplierIdOld)) {
                supplierIdNew.getPurchaseCollection().add(purchase);
                supplierIdNew = em.merge(supplierIdNew);
            }
            for (PayAccount payAccountCollectionOldPayAccount : payAccountCollectionOld) {
                if (!payAccountCollectionNew.contains(payAccountCollectionOldPayAccount)) {
                    payAccountCollectionOldPayAccount.setPurchaseId(null);
                    payAccountCollectionOldPayAccount = em.merge(payAccountCollectionOldPayAccount);
                }
            }
            for (PayAccount payAccountCollectionNewPayAccount : payAccountCollectionNew) {
                if (!payAccountCollectionOld.contains(payAccountCollectionNewPayAccount)) {
                    Purchase oldPurchaseIdOfPayAccountCollectionNewPayAccount = payAccountCollectionNewPayAccount.getPurchaseId();
                    payAccountCollectionNewPayAccount.setPurchaseId(purchase);
                    payAccountCollectionNewPayAccount = em.merge(payAccountCollectionNewPayAccount);
                    if (oldPurchaseIdOfPayAccountCollectionNewPayAccount != null && !oldPurchaseIdOfPayAccountCollectionNewPayAccount.equals(purchase)) {
                        oldPurchaseIdOfPayAccountCollectionNewPayAccount.getPayAccountCollection().remove(payAccountCollectionNewPayAccount);
                        oldPurchaseIdOfPayAccountCollectionNewPayAccount = em.merge(oldPurchaseIdOfPayAccountCollectionNewPayAccount);
                    }
                }
            }
            for (PurchaseProduct purchaseProductCollectionNewPurchaseProduct : purchaseProductCollectionNew) {
                if (!purchaseProductCollectionOld.contains(purchaseProductCollectionNewPurchaseProduct)) {
                    Purchase oldPurchaseOfPurchaseProductCollectionNewPurchaseProduct = purchaseProductCollectionNewPurchaseProduct.getPurchase();
                    purchaseProductCollectionNewPurchaseProduct.setPurchase(purchase);
                    purchaseProductCollectionNewPurchaseProduct = em.merge(purchaseProductCollectionNewPurchaseProduct);
                    if (oldPurchaseOfPurchaseProductCollectionNewPurchaseProduct != null && !oldPurchaseOfPurchaseProductCollectionNewPurchaseProduct.equals(purchase)) {
                        oldPurchaseOfPurchaseProductCollectionNewPurchaseProduct.getPurchaseProductCollection().remove(purchaseProductCollectionNewPurchaseProduct);
                        oldPurchaseOfPurchaseProductCollectionNewPurchaseProduct = em.merge(oldPurchaseOfPurchaseProductCollectionNewPurchaseProduct);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = purchase.getId();
                if (findPurchase(id) == null) {
                    throw new NonexistentEntityException("The purchase with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Purchase purchase;
            try {
                purchase = em.getReference(Purchase.class, id);
                purchase.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The purchase with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<PurchaseProduct> purchaseProductCollectionOrphanCheck = purchase.getPurchaseProductCollection();
            for (PurchaseProduct purchaseProductCollectionOrphanCheckPurchaseProduct : purchaseProductCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Purchase (" + purchase + ") cannot be destroyed since the PurchaseProduct " + purchaseProductCollectionOrphanCheckPurchaseProduct + " in its purchaseProductCollection field has a non-nullable purchase field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            PurchaseType purchasetypeId = purchase.getPurchasetypeId();
            if (purchasetypeId != null) {
                purchasetypeId.getPurchaseCollection().remove(purchase);
                purchasetypeId = em.merge(purchasetypeId);
            }
            Supplier supplierId = purchase.getSupplierId();
            if (supplierId != null) {
                supplierId.getPurchaseCollection().remove(purchase);
                supplierId = em.merge(supplierId);
            }
            List<PayAccount> payAccountCollection = purchase.getPayAccountCollection();
            for (PayAccount payAccountCollectionPayAccount : payAccountCollection) {
                payAccountCollectionPayAccount.setPurchaseId(null);
                payAccountCollectionPayAccount = em.merge(payAccountCollectionPayAccount);
            }
            em.remove(purchase);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Purchase> findPurchaseEntities() {
        return findPurchaseEntities(true, -1, -1);
    }

    public List<Purchase> findPurchaseEntities(int maxResults, int firstResult) {
        return findPurchaseEntities(false, maxResults, firstResult);
    }

    private List<Purchase> findPurchaseEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Purchase as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Purchase findPurchase(Integer id) {
        EntityManager em = getEntityManager();
        Purchase p;
        try {
            p = em.find(Purchase.class, id);
            em.refresh(p);
            return p;
        } finally {
            em.close();
        }
    }

    public int getPurchaseCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from Purchase as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
