/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.exceptions.NonexistentEntityException;
import comsql.ReceivingType;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import comsql.PayedPlot;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author usuario
 */
public class ReceivingTypeJpaController {
    private static ReceivingTypeJpaController jpa;

    private ReceivingTypeJpaController() {

    }

    public static ReceivingTypeJpaController getInstance() {
        if (jpa == null) {
            jpa = new ReceivingTypeJpaController();
        }
        return jpa;
    }

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(ReceivingType receivingType) {
        if (receivingType.getPayedPlotCollection() == null) {
            receivingType.setPayedPlotCollection(new ArrayList<PayedPlot>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<PayedPlot> attachedPayedPlotCollection = new ArrayList<PayedPlot>();
            for (PayedPlot payedPlotCollectionPayedPlotToAttach : receivingType.getPayedPlotCollection()) {
                payedPlotCollectionPayedPlotToAttach = em.getReference(payedPlotCollectionPayedPlotToAttach.getClass(), payedPlotCollectionPayedPlotToAttach.getId());
                attachedPayedPlotCollection.add(payedPlotCollectionPayedPlotToAttach);
            }
            receivingType.setPayedPlotCollection(attachedPayedPlotCollection);
            em.persist(receivingType);
            for (PayedPlot payedPlotCollectionPayedPlot : receivingType.getPayedPlotCollection()) {
                ReceivingType oldReceivingtypeIdOfPayedPlotCollectionPayedPlot = payedPlotCollectionPayedPlot.getReceivingtypeId();
                payedPlotCollectionPayedPlot.setReceivingtypeId(receivingType);
                payedPlotCollectionPayedPlot = em.merge(payedPlotCollectionPayedPlot);
                if (oldReceivingtypeIdOfPayedPlotCollectionPayedPlot != null) {
                    oldReceivingtypeIdOfPayedPlotCollectionPayedPlot.getPayedPlotCollection().remove(payedPlotCollectionPayedPlot);
                    oldReceivingtypeIdOfPayedPlotCollectionPayedPlot = em.merge(oldReceivingtypeIdOfPayedPlotCollectionPayedPlot);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ReceivingType receivingType) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ReceivingType persistentReceivingType = em.find(ReceivingType.class, receivingType.getId());
            List<PayedPlot> payedPlotCollectionOld = persistentReceivingType.getPayedPlotCollection();
            List<PayedPlot> payedPlotCollectionNew = receivingType.getPayedPlotCollection();
            List<PayedPlot> attachedPayedPlotCollectionNew = new ArrayList<PayedPlot>();
            for (PayedPlot payedPlotCollectionNewPayedPlotToAttach : payedPlotCollectionNew) {
                payedPlotCollectionNewPayedPlotToAttach = em.getReference(payedPlotCollectionNewPayedPlotToAttach.getClass(), payedPlotCollectionNewPayedPlotToAttach.getId());
                attachedPayedPlotCollectionNew.add(payedPlotCollectionNewPayedPlotToAttach);
            }
            payedPlotCollectionNew = attachedPayedPlotCollectionNew;
            receivingType.setPayedPlotCollection(payedPlotCollectionNew);
            receivingType = em.merge(receivingType);
            for (PayedPlot payedPlotCollectionOldPayedPlot : payedPlotCollectionOld) {
                if (!payedPlotCollectionNew.contains(payedPlotCollectionOldPayedPlot)) {
                    payedPlotCollectionOldPayedPlot.setReceivingtypeId(null);
                    payedPlotCollectionOldPayedPlot = em.merge(payedPlotCollectionOldPayedPlot);
                }
            }
            for (PayedPlot payedPlotCollectionNewPayedPlot : payedPlotCollectionNew) {
                if (!payedPlotCollectionOld.contains(payedPlotCollectionNewPayedPlot)) {
                    ReceivingType oldReceivingtypeIdOfPayedPlotCollectionNewPayedPlot = payedPlotCollectionNewPayedPlot.getReceivingtypeId();
                    payedPlotCollectionNewPayedPlot.setReceivingtypeId(receivingType);
                    payedPlotCollectionNewPayedPlot = em.merge(payedPlotCollectionNewPayedPlot);
                    if (oldReceivingtypeIdOfPayedPlotCollectionNewPayedPlot != null && !oldReceivingtypeIdOfPayedPlotCollectionNewPayedPlot.equals(receivingType)) {
                        oldReceivingtypeIdOfPayedPlotCollectionNewPayedPlot.getPayedPlotCollection().remove(payedPlotCollectionNewPayedPlot);
                        oldReceivingtypeIdOfPayedPlotCollectionNewPayedPlot = em.merge(oldReceivingtypeIdOfPayedPlotCollectionNewPayedPlot);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = receivingType.getId();
                if (findReceivingType(id) == null) {
                    throw new NonexistentEntityException("The receivingType with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ReceivingType receivingType;
            try {
                receivingType = em.getReference(ReceivingType.class, id);
                receivingType.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The receivingType with id " + id + " no longer exists.", enfe);
            }
            List<PayedPlot> payedPlotCollection = receivingType.getPayedPlotCollection();
            for (PayedPlot payedPlotCollectionPayedPlot : payedPlotCollection) {
                payedPlotCollectionPayedPlot.setReceivingtypeId(null);
                payedPlotCollectionPayedPlot = em.merge(payedPlotCollectionPayedPlot);
            }
            em.remove(receivingType);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ReceivingType> findReceivingTypeEntities() {
        return findReceivingTypeEntities(true, -1, -1);
    }

    public List<ReceivingType> findReceivingTypeEntities(int maxResults, int firstResult) {
        return findReceivingTypeEntities(false, maxResults, firstResult);
    }

    private List<ReceivingType> findReceivingTypeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from ReceivingType as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ReceivingType findReceivingType(Integer id) {
        EntityManager em = getEntityManager();
        ReceivingType rt;
        try {
            rt = em.find(ReceivingType.class, id);
            em.refresh(rt);
            return rt;
        } finally {
            em.close();
        }
    }

    public int getReceivingTypeCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from ReceivingType as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
