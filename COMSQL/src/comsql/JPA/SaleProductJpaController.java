/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.*;
import comsql.exceptions.NonexistentEntityException;
import comsql.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author usuario
 */
public class SaleProductJpaController {
    private static SaleProductJpaController pjpa;

    private SaleProductJpaController() {

    }

    public static SaleProductJpaController getInstance() {
        if (pjpa == null) {
            pjpa = new SaleProductJpaController();
        }
        return pjpa;
    }
    //private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(SaleProduct saleProduct) throws PreexistingEntityException, Exception {
        if (saleProduct.getSaleProductPK() == null) {
            saleProduct.setSaleProductPK(new SaleProductPK());
        }
        saleProduct.getSaleProductPK().setSaleId(saleProduct.getSale().getId());
        saleProduct.getSaleProductPK().setProductId(saleProduct.getProduct().getId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Product product = saleProduct.getProduct();
            if (product != null) {
                product = em.getReference(product.getClass(), product.getId());
                saleProduct.setProduct(product);
            }
            Sale sale = saleProduct.getSale();
            if (sale != null) {
                sale = em.getReference(sale.getClass(), sale.getId());
                saleProduct.setSale(sale);
            }
            em.persist(saleProduct);
            if (product != null) {
                product.getSaleProductCollection().add(saleProduct);
                product = em.merge(product);
            }
            if (sale != null) {
                sale.getSaleProductCollection().add(saleProduct);
                sale = em.merge(sale);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findSaleProduct(saleProduct.getSaleProductPK()) != null) {
                throw new PreexistingEntityException("SaleProduct " + saleProduct + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SaleProduct saleProduct) throws NonexistentEntityException, Exception {
        saleProduct.getSaleProductPK().setSaleId(saleProduct.getSale().getId());
        saleProduct.getSaleProductPK().setProductId(saleProduct.getProduct().getId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SaleProduct persistentSaleProduct = em.find(SaleProduct.class, saleProduct.getSaleProductPK());
            Product productOld = persistentSaleProduct.getProduct();
            Product productNew = saleProduct.getProduct();
            Sale saleOld = persistentSaleProduct.getSale();
            Sale saleNew = saleProduct.getSale();
            if (productNew != null) {
                productNew = em.getReference(productNew.getClass(), productNew.getId());
                saleProduct.setProduct(productNew);
            }
            if (saleNew != null) {
                saleNew = em.getReference(saleNew.getClass(), saleNew.getId());
                saleProduct.setSale(saleNew);
            }
            saleProduct = em.merge(saleProduct);
            if (productOld != null && !productOld.equals(productNew)) {
                productOld.getSaleProductCollection().remove(saleProduct);
                productOld = em.merge(productOld);
            }
            if (productNew != null && !productNew.equals(productOld)) {
                productNew.getSaleProductCollection().add(saleProduct);
                productNew = em.merge(productNew);
            }
            if (saleOld != null && !saleOld.equals(saleNew)) {
                saleOld.getSaleProductCollection().remove(saleProduct);
                saleOld = em.merge(saleOld);
            }
            if (saleNew != null && !saleNew.equals(saleOld)) {
                saleNew.getSaleProductCollection().add(saleProduct);
                saleNew = em.merge(saleNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                SaleProductPK id = saleProduct.getSaleProductPK();
                if (findSaleProduct(id) == null) {
                    throw new NonexistentEntityException("The saleProduct with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(SaleProductPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SaleProduct saleProduct;
            try {
                saleProduct = em.getReference(SaleProduct.class, id);
                saleProduct.getSaleProductPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The saleProduct with id " + id + " no longer exists.", enfe);
            }
            Product product = saleProduct.getProduct();
            if (product != null) {
                product.getSaleProductCollection().remove(saleProduct);
                product = em.merge(product);
            }
            Sale sale = saleProduct.getSale();
            if (sale != null) {
                sale.getSaleProductCollection().remove(saleProduct);
                sale = em.merge(sale);
            }
            em.remove(saleProduct);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SaleProduct> findSaleProductEntities() {
        return findSaleProductEntities(true, -1, -1);
    }

    public List<SaleProduct> findSaleProductEntities(int maxResults, int firstResult) {
        return findSaleProductEntities(false, maxResults, firstResult);
    }

    private List<SaleProduct> findSaleProductEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from SaleProduct as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SaleProduct findSaleProduct(SaleProductPK id) {
        EntityManager em = getEntityManager();
        SaleProduct s;
        try {
            s = em.find(SaleProduct.class, id);
            em.refresh(s);
            return s;
        } finally {
            em.close();
        }
    }

    public int getSaleProductCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from SaleProduct as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
