/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.JPA.exceptions.NonexistentEntityException;
import comsql.PayedPlot;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import comsql.PayAccount;
import oracle.toplink.essentials.config.HintValues;
import oracle.toplink.essentials.config.TopLinkQueryHints;

/**
 *
 * @author usuario
 */
public class PayedPlotJpaController {
    private static PayedPlotJpaController jpa;

    private PayedPlotJpaController() {

    }

    public static PayedPlotJpaController getInstance() {
        if (jpa == null) {
            jpa = new PayedPlotJpaController();
        }
        return jpa;
    }

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(PayedPlot payedPlot) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PayAccount payaccountId = payedPlot.getPayaccountId();
            if (payaccountId != null) {
                payaccountId = em.getReference(payaccountId.getClass(), payaccountId.getId());
                payedPlot.setPayaccountId(payaccountId);
            }
            em.persist(payedPlot);
            if (payaccountId != null) {
                payaccountId.getPayedPlotCollection().add(payedPlot);
                payaccountId = em.merge(payaccountId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PayedPlot payedPlot) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PayedPlot persistentPayedPlot = em.find(PayedPlot.class, payedPlot.getId());
            PayAccount payaccountIdOld = persistentPayedPlot.getPayaccountId();
            PayAccount payaccountIdNew = payedPlot.getPayaccountId();
            if (payaccountIdNew != null) {
                payaccountIdNew = em.getReference(payaccountIdNew.getClass(), payaccountIdNew.getId());
                payedPlot.setPayaccountId(payaccountIdNew);
            }
            payedPlot = em.merge(payedPlot);
            if (payaccountIdOld != null && !payaccountIdOld.equals(payaccountIdNew)) {
                payaccountIdOld.getPayedPlotCollection().remove(payedPlot);
                payaccountIdOld = em.merge(payaccountIdOld);
            }
            if (payaccountIdNew != null && !payaccountIdNew.equals(payaccountIdOld)) {
                payaccountIdNew.getPayedPlotCollection().add(payedPlot);
                payaccountIdNew = em.merge(payaccountIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = payedPlot.getId();
                if (findPayedPlot(id) == null) {
                    throw new NonexistentEntityException("The payedPlot with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PayedPlot payedPlot;
            try {
                payedPlot = em.getReference(PayedPlot.class, id);
                payedPlot.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The payedPlot with id " + id + " no longer exists.", enfe);
            }
            PayAccount payaccountId = payedPlot.getPayaccountId();
            if (payaccountId != null) {
                payaccountId.getPayedPlotCollection().remove(payedPlot);
                payaccountId = em.merge(payaccountId);
            }
            em.remove(payedPlot);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PayedPlot> findPayedPlotEntities() {
        return findPayedPlotEntities(true, -1, -1);
    }

    public List<PayedPlot> findPayedPlotEntities(int maxResults, int firstResult) {
        return findPayedPlotEntities(false, maxResults, firstResult);
    }

    private List<PayedPlot> findPayedPlotEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from PayedPlot as o");
            q.setHint(TopLinkQueryHints.REFRESH, HintValues.TRUE);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PayedPlot findPayedPlot(Integer id) {
        EntityManager em = getEntityManager();
        PayedPlot p;
        try {
            p = em.find(PayedPlot.class, id);
            em.refresh(p);
            return p;
        } finally {
            em.close();
        }
    }

    public int getPayedPlotCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from PayedPlot as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
