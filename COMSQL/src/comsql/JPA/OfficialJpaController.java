/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.*;
import comsql.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author usuario
 */
public class OfficialJpaController {
    private static OfficialJpaController ojpac;

    public OfficialJpaController() {

    }

    public static OfficialJpaController getInstance () {
        if (ojpac == null) {
            ojpac = new OfficialJpaController();
        }
        return ojpac;
    }
    
    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(Official official) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Address addressId = official.getAddressId();
            if (addressId != null) {
                addressId = em.getReference(addressId.getClass(), addressId.getId());
                official.setAddressId(addressId);
            }
            em.persist(official);
            if (addressId != null) {
                addressId.getOfficialCollection().add(official);
                addressId = em.merge(addressId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Official official) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Official persistentOfficial = em.find(Official.class, official.getId());
            Address addressIdOld = persistentOfficial.getAddressId();
            Address addressIdNew = official.getAddressId();
            if (addressIdNew != null) {
                addressIdNew = em.getReference(addressIdNew.getClass(), addressIdNew.getId());
                official.setAddressId(addressIdNew);
            }
            official = em.merge(official);
            if (addressIdOld != null && !addressIdOld.equals(addressIdNew)) {
                addressIdOld.getOfficialCollection().remove(official);
                addressIdOld = em.merge(addressIdOld);
            }
            if (addressIdNew != null && !addressIdNew.equals(addressIdOld)) {
                addressIdNew.getOfficialCollection().add(official);
                addressIdNew = em.merge(addressIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = official.getId();
                if (findOfficial(id) == null) {
                    throw new NonexistentEntityException("The official with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Official official;
            try {
                official = em.getReference(Official.class, id);
                official.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The official with id " + id + " no longer exists.", enfe);
            }
            Address addressId = official.getAddressId();
            if (addressId != null) {
                addressId.getOfficialCollection().remove(official);
                addressId = em.merge(addressId);
            }
            em.remove(official);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Official> findOfficialEntities() {
        return findOfficialEntities(true, -1, -1);
    }

    public List<Official> findOfficialEntities(int maxResults, int firstResult) {
        return findOfficialEntities(false, maxResults, firstResult);
    }

    private List<Official> findOfficialEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Official as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Official findOfficial(Integer id) {
        EntityManager em = getEntityManager();
        Official o;
        try {
            o = em.find(Official.class, id);
            em.refresh(o);
            return o;
        } finally {
            em.close();
        }
    }

    public int getOfficialCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from Official as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
