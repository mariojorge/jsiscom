/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.*;
import comsql.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author usuario
 */
public class ReceptPlotJpaController {
    private static ReceptPlotJpaController jpa;

    private ReceptPlotJpaController() {

    }

    public static ReceptPlotJpaController getInstance() {
        if (jpa == null) {
            jpa = new ReceptPlotJpaController();
        }
        return jpa;
    }

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(ReceptPlot receptPlot) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ReceptAccount receptaccountId = receptPlot.getReceptaccountId();
            if (receptaccountId != null) {
                receptaccountId = em.getReference(receptaccountId.getClass(), receptaccountId.getId());
                receptPlot.setReceptaccountId(receptaccountId);
            }
            em.persist(receptPlot);
            if (receptaccountId != null) {
                receptaccountId.getReceptPlotCollection().add(receptPlot);
                receptaccountId = em.merge(receptaccountId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ReceptPlot receptPlot) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ReceptPlot persistentReceptPlot = em.find(ReceptPlot.class, receptPlot.getId());
            ReceptAccount receptaccountIdOld = persistentReceptPlot.getReceptaccountId();
            ReceptAccount receptaccountIdNew = receptPlot.getReceptaccountId();
            if (receptaccountIdNew != null) {
                receptaccountIdNew = em.getReference(receptaccountIdNew.getClass(), receptaccountIdNew.getId());
                receptPlot.setReceptaccountId(receptaccountIdNew);
            }
            receptPlot = em.merge(receptPlot);
            if (receptaccountIdOld != null && !receptaccountIdOld.equals(receptaccountIdNew)) {
                receptaccountIdOld.getReceptPlotCollection().remove(receptPlot);
                receptaccountIdOld = em.merge(receptaccountIdOld);
            }
            if (receptaccountIdNew != null && !receptaccountIdNew.equals(receptaccountIdOld)) {
                receptaccountIdNew.getReceptPlotCollection().add(receptPlot);
                receptaccountIdNew = em.merge(receptaccountIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = receptPlot.getId();
                if (findReceptPlot(id) == null) {
                    throw new NonexistentEntityException("The receptPlot with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ReceptPlot receptPlot;
            try {
                receptPlot = em.getReference(ReceptPlot.class, id);
                receptPlot.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The receptPlot with id " + id + " no longer exists.", enfe);
            }
            ReceptAccount receptaccountId = receptPlot.getReceptaccountId();
            if (receptaccountId != null) {
                receptaccountId.getReceptPlotCollection().remove(receptPlot);
                receptaccountId = em.merge(receptaccountId);
            }
            em.remove(receptPlot);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ReceptPlot> findReceptPlotEntities() {
        return findReceptPlotEntities(true, -1, -1);
    }

    public List<ReceptPlot> findReceptPlotEntities(int maxResults, int firstResult) {
        return findReceptPlotEntities(false, maxResults, firstResult);
    }

    private List<ReceptPlot> findReceptPlotEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from ReceptPlot as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ReceptPlot findReceptPlot(Integer id) {
        EntityManager em = getEntityManager();
        ReceptPlot r;
        try {
            r = em.find(ReceptPlot.class, id);
            em.refresh(r);
            return r;
        } finally {
            em.close();
        }
    }

    public int getReceptPlotCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from ReceptPlot as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
