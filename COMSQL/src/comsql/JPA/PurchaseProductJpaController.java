/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.*;
import comsql.exceptions.NonexistentEntityException;
import comsql.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author usuario
 */
public class PurchaseProductJpaController {
    private static PurchaseProductJpaController ppjpa;

    private PurchaseProductJpaController() {
        //emf = Persistence.createEntityManagerFactory("COMSQLPU");
    }

    public static PurchaseProductJpaController getInstance() {
        if (ppjpa == null) {
            ppjpa = new PurchaseProductJpaController();
        }
        return ppjpa;
    }
    //private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(PurchaseProduct purchaseProduct) throws PreexistingEntityException, Exception {
        if (purchaseProduct.getPurchaseProductPK() == null) {
            purchaseProduct.setPurchaseProductPK(new PurchaseProductPK());
        }
        purchaseProduct.getPurchaseProductPK().setPurchaseId(purchaseProduct.getPurchase().getId());
        purchaseProduct.getPurchaseProductPK().setProductId(purchaseProduct.getProduct().getId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Product product = purchaseProduct.getProduct();
            if (product != null) {
                product = em.getReference(product.getClass(), product.getId());
                purchaseProduct.setProduct(product);
            }
            Purchase purchase = purchaseProduct.getPurchase();
            if (purchase != null) {
                purchase = em.getReference(purchase.getClass(), purchase.getId());
                purchaseProduct.setPurchase(purchase);
            }
            em.persist(purchaseProduct);
            if (product != null) {
                product.getPurchaseProductCollection().add(purchaseProduct);
                product = em.merge(product);
            }
            if (purchase != null) {
                purchase.getPurchaseProductCollection().add(purchaseProduct);
                purchase = em.merge(purchase);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPurchaseProduct(purchaseProduct.getPurchaseProductPK()) != null) {
                throw new PreexistingEntityException("PurchaseProduct " + purchaseProduct + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PurchaseProduct purchaseProduct) throws NonexistentEntityException, Exception {
        purchaseProduct.getPurchaseProductPK().setPurchaseId(purchaseProduct.getPurchase().getId());
        purchaseProduct.getPurchaseProductPK().setProductId(purchaseProduct.getProduct().getId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PurchaseProduct persistentPurchaseProduct = em.find(PurchaseProduct.class, purchaseProduct.getPurchaseProductPK());
            Product productOld = persistentPurchaseProduct.getProduct();
            Product productNew = purchaseProduct.getProduct();
            Purchase purchaseOld = persistentPurchaseProduct.getPurchase();
            Purchase purchaseNew = purchaseProduct.getPurchase();
            if (productNew != null) {
                productNew = em.getReference(productNew.getClass(), productNew.getId());
                purchaseProduct.setProduct(productNew);
            }
            if (purchaseNew != null) {
                purchaseNew = em.getReference(purchaseNew.getClass(), purchaseNew.getId());
                purchaseProduct.setPurchase(purchaseNew);
            }
            purchaseProduct = em.merge(purchaseProduct);
            if (productOld != null && !productOld.equals(productNew)) {
                productOld.getPurchaseProductCollection().remove(purchaseProduct);
                productOld = em.merge(productOld);
            }
            if (productNew != null && !productNew.equals(productOld)) {
                productNew.getPurchaseProductCollection().add(purchaseProduct);
                productNew = em.merge(productNew);
            }
            if (purchaseOld != null && !purchaseOld.equals(purchaseNew)) {
                purchaseOld.getPurchaseProductCollection().remove(purchaseProduct);
                purchaseOld = em.merge(purchaseOld);
            }
            if (purchaseNew != null && !purchaseNew.equals(purchaseOld)) {
                purchaseNew.getPurchaseProductCollection().add(purchaseProduct);
                purchaseNew = em.merge(purchaseNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                PurchaseProductPK id = purchaseProduct.getPurchaseProductPK();
                if (findPurchaseProduct(id) == null) {
                    throw new NonexistentEntityException("The purchaseProduct with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(PurchaseProductPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PurchaseProduct purchaseProduct;
            try {
                purchaseProduct = em.getReference(PurchaseProduct.class, id);
                purchaseProduct.getPurchaseProductPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The purchaseProduct with id " + id + " no longer exists.", enfe);
            }
            Product product = purchaseProduct.getProduct();
            if (product != null) {
                product.getPurchaseProductCollection().remove(purchaseProduct);
                product = em.merge(product);
            }
            Purchase purchase = purchaseProduct.getPurchase();
            if (purchase != null) {
                purchase.getPurchaseProductCollection().remove(purchaseProduct);
                purchase = em.merge(purchase);
            }
            em.remove(purchaseProduct);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PurchaseProduct> findPurchaseProductEntities() {
        return findPurchaseProductEntities(true, -1, -1);
    }

    public List<PurchaseProduct> findPurchaseProductEntities(int maxResults, int firstResult) {
        return findPurchaseProductEntities(false, maxResults, firstResult);
    }

    private List<PurchaseProduct> findPurchaseProductEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from PurchaseProduct as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PurchaseProduct findPurchaseProduct(PurchaseProductPK id) {
        EntityManager em = getEntityManager();
        PurchaseProduct p;
        try {
            p = em.find(PurchaseProduct.class, id);
            em.refresh(p);
            return p;
        } finally {
            em.close();
        }
    }

    public int getPurchaseProductCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from PurchaseProduct as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
