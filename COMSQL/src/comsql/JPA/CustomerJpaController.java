/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.Customer;
import comsql.JPA.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import comsql.Address;
import comsql.Sale;
import java.util.ArrayList;
import java.util.List;
import comsql.ReceptAccount;

/**
 *
 * @author usuario
 */
public class CustomerJpaController {
    private static CustomerJpaController jpa;

    private CustomerJpaController() {

    }

    public static CustomerJpaController getInstance() {
        if (jpa == null) {
            jpa = new CustomerJpaController();
        }
        return jpa;
    }

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(Customer customer) {
        if (customer.getSaleCollection() == null) {
            customer.setSaleCollection(new ArrayList<Sale>());
        }
        if (customer.getReceptAccountCollection() == null) {
            customer.setReceptAccountCollection(new ArrayList<ReceptAccount>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Address addressId = customer.getAddressId();
            if (addressId != null) {
                addressId = em.getReference(addressId.getClass(), addressId.getId());
                customer.setAddressId(addressId);
            }
            List<Sale> attachedSaleCollection = new ArrayList<Sale>();
            for (Sale saleCollectionSaleToAttach : customer.getSaleCollection()) {
                saleCollectionSaleToAttach = em.getReference(saleCollectionSaleToAttach.getClass(), saleCollectionSaleToAttach.getId());
                attachedSaleCollection.add(saleCollectionSaleToAttach);
            }
            customer.setSaleCollection(attachedSaleCollection);
            List<ReceptAccount> attachedReceptAccountCollection = new ArrayList<ReceptAccount>();
            for (ReceptAccount receptAccountCollectionReceptAccountToAttach : customer.getReceptAccountCollection()) {
                receptAccountCollectionReceptAccountToAttach = em.getReference(receptAccountCollectionReceptAccountToAttach.getClass(), receptAccountCollectionReceptAccountToAttach.getId());
                attachedReceptAccountCollection.add(receptAccountCollectionReceptAccountToAttach);
            }
            customer.setReceptAccountCollection(attachedReceptAccountCollection);
            em.persist(customer);
            if (addressId != null) {
                addressId.getCustomerCollection().add(customer);
                addressId = em.merge(addressId);
            }
            for (Sale saleCollectionSale : customer.getSaleCollection()) {
                Customer oldCustomerIdOfSaleCollectionSale = saleCollectionSale.getCustomerId();
                saleCollectionSale.setCustomerId(customer);
                saleCollectionSale = em.merge(saleCollectionSale);
                if (oldCustomerIdOfSaleCollectionSale != null) {
                    oldCustomerIdOfSaleCollectionSale.getSaleCollection().remove(saleCollectionSale);
                    oldCustomerIdOfSaleCollectionSale = em.merge(oldCustomerIdOfSaleCollectionSale);
                }
            }
            for (ReceptAccount receptAccountCollectionReceptAccount : customer.getReceptAccountCollection()) {
                Customer oldCustomerIdOfReceptAccountCollectionReceptAccount = receptAccountCollectionReceptAccount.getCustomerId();
                receptAccountCollectionReceptAccount.setCustomerId(customer);
                receptAccountCollectionReceptAccount = em.merge(receptAccountCollectionReceptAccount);
                if (oldCustomerIdOfReceptAccountCollectionReceptAccount != null) {
                    oldCustomerIdOfReceptAccountCollectionReceptAccount.getReceptAccountCollection().remove(receptAccountCollectionReceptAccount);
                    oldCustomerIdOfReceptAccountCollectionReceptAccount = em.merge(oldCustomerIdOfReceptAccountCollectionReceptAccount);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Customer customer) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Customer persistentCustomer = em.find(Customer.class, customer.getId());
            Address addressIdOld = persistentCustomer.getAddressId();
            Address addressIdNew = customer.getAddressId();
            List<Sale> saleCollectionOld = persistentCustomer.getSaleCollection();
            List<Sale> saleCollectionNew = customer.getSaleCollection();
            List<ReceptAccount> receptAccountCollectionOld = persistentCustomer.getReceptAccountCollection();
            List<ReceptAccount> receptAccountCollectionNew = customer.getReceptAccountCollection();
            if (addressIdNew != null) {
                addressIdNew = em.getReference(addressIdNew.getClass(), addressIdNew.getId());
                customer.setAddressId(addressIdNew);
            }
            List<Sale> attachedSaleCollectionNew = new ArrayList<Sale>();
            for (Sale saleCollectionNewSaleToAttach : saleCollectionNew) {
                saleCollectionNewSaleToAttach = em.getReference(saleCollectionNewSaleToAttach.getClass(), saleCollectionNewSaleToAttach.getId());
                attachedSaleCollectionNew.add(saleCollectionNewSaleToAttach);
            }
            saleCollectionNew = attachedSaleCollectionNew;
            customer.setSaleCollection(saleCollectionNew);
            List<ReceptAccount> attachedReceptAccountCollectionNew = new ArrayList<ReceptAccount>();
            for (ReceptAccount receptAccountCollectionNewReceptAccountToAttach : receptAccountCollectionNew) {
                receptAccountCollectionNewReceptAccountToAttach = em.getReference(receptAccountCollectionNewReceptAccountToAttach.getClass(), receptAccountCollectionNewReceptAccountToAttach.getId());
                attachedReceptAccountCollectionNew.add(receptAccountCollectionNewReceptAccountToAttach);
            }
            receptAccountCollectionNew = attachedReceptAccountCollectionNew;
            customer.setReceptAccountCollection(receptAccountCollectionNew);
            customer = em.merge(customer);
            if (addressIdOld != null && !addressIdOld.equals(addressIdNew)) {
                addressIdOld.getCustomerCollection().remove(customer);
                addressIdOld = em.merge(addressIdOld);
            }
            if (addressIdNew != null && !addressIdNew.equals(addressIdOld)) {
                addressIdNew.getCustomerCollection().add(customer);
                addressIdNew = em.merge(addressIdNew);
            }
            for (Sale saleCollectionOldSale : saleCollectionOld) {
                if (!saleCollectionNew.contains(saleCollectionOldSale)) {
                    saleCollectionOldSale.setCustomerId(null);
                    saleCollectionOldSale = em.merge(saleCollectionOldSale);
                }
            }
            for (Sale saleCollectionNewSale : saleCollectionNew) {
                if (!saleCollectionOld.contains(saleCollectionNewSale)) {
                    Customer oldCustomerIdOfSaleCollectionNewSale = saleCollectionNewSale.getCustomerId();
                    saleCollectionNewSale.setCustomerId(customer);
                    saleCollectionNewSale = em.merge(saleCollectionNewSale);
                    if (oldCustomerIdOfSaleCollectionNewSale != null && !oldCustomerIdOfSaleCollectionNewSale.equals(customer)) {
                        oldCustomerIdOfSaleCollectionNewSale.getSaleCollection().remove(saleCollectionNewSale);
                        oldCustomerIdOfSaleCollectionNewSale = em.merge(oldCustomerIdOfSaleCollectionNewSale);
                    }
                }
            }
            for (ReceptAccount receptAccountCollectionOldReceptAccount : receptAccountCollectionOld) {
                if (!receptAccountCollectionNew.contains(receptAccountCollectionOldReceptAccount)) {
                    receptAccountCollectionOldReceptAccount.setCustomerId(null);
                    receptAccountCollectionOldReceptAccount = em.merge(receptAccountCollectionOldReceptAccount);
                }
            }
            for (ReceptAccount receptAccountCollectionNewReceptAccount : receptAccountCollectionNew) {
                if (!receptAccountCollectionOld.contains(receptAccountCollectionNewReceptAccount)) {
                    Customer oldCustomerIdOfReceptAccountCollectionNewReceptAccount = receptAccountCollectionNewReceptAccount.getCustomerId();
                    receptAccountCollectionNewReceptAccount.setCustomerId(customer);
                    receptAccountCollectionNewReceptAccount = em.merge(receptAccountCollectionNewReceptAccount);
                    if (oldCustomerIdOfReceptAccountCollectionNewReceptAccount != null && !oldCustomerIdOfReceptAccountCollectionNewReceptAccount.equals(customer)) {
                        oldCustomerIdOfReceptAccountCollectionNewReceptAccount.getReceptAccountCollection().remove(receptAccountCollectionNewReceptAccount);
                        oldCustomerIdOfReceptAccountCollectionNewReceptAccount = em.merge(oldCustomerIdOfReceptAccountCollectionNewReceptAccount);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = customer.getId();
                if (findCustomer(id) == null) {
                    throw new NonexistentEntityException("The customer with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Customer customer;
            try {
                customer = em.getReference(Customer.class, id);
                customer.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The customer with id " + id + " no longer exists.", enfe);
            }
            Address addressId = customer.getAddressId();
            if (addressId != null) {
                addressId.getCustomerCollection().remove(customer);
                addressId = em.merge(addressId);
            }
            List<Sale> saleCollection = customer.getSaleCollection();
            for (Sale saleCollectionSale : saleCollection) {
                saleCollectionSale.setCustomerId(null);
                saleCollectionSale = em.merge(saleCollectionSale);
            }
            List<ReceptAccount> receptAccountCollection = customer.getReceptAccountCollection();
            for (ReceptAccount receptAccountCollectionReceptAccount : receptAccountCollection) {
                receptAccountCollectionReceptAccount.setCustomerId(null);
                receptAccountCollectionReceptAccount = em.merge(receptAccountCollectionReceptAccount);
            }
            em.remove(customer);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Customer> findCustomerEntities() {
        return findCustomerEntities(true, -1, -1);
    }

    public List<Customer> findCustomerEntities(int maxResults, int firstResult) {
        return findCustomerEntities(false, maxResults, firstResult);
    }

    private List<Customer> findCustomerEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Customer as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Customer findCustomer(Integer id) {
        EntityManager em = getEntityManager();
        Customer c;
        try {
            c = em.find(Customer.class, id);
            em.refresh(c);
            return c;
        } finally {
            em.close();
        }
    }

    public int getCustomerCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from Customer as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
