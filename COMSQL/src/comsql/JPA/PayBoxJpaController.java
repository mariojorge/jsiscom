/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.*;
import util.DbConfig;
import comsql.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author usuario
 */
public class PayBoxJpaController {
    private static PayBoxJpaController pbjpa;

    public PayBoxJpaController() {
        emf = Persistence.createEntityManagerFactory("COMSQLPU");
    }

    public PayBoxJpaController(Map config) {
        emf = Persistence.createEntityManagerFactory("COMSQLPU", config);
    }
    
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PayBox payBox) {
        if (payBox.getPBOpenCollection() == null) {
            payBox.setPBOpenCollection(new ArrayList<PBOpen>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<PBOpen> attachedPBOpenCollection = new ArrayList<PBOpen>();
            for (PBOpen PBOpenCollectionPBOpenToAttach : payBox.getPBOpenCollection()) {
                PBOpenCollectionPBOpenToAttach = em.getReference(PBOpenCollectionPBOpenToAttach.getClass(), PBOpenCollectionPBOpenToAttach.getId());
                attachedPBOpenCollection.add(PBOpenCollectionPBOpenToAttach);
            }
            payBox.setPBOpenCollection(attachedPBOpenCollection);
            em.persist(payBox);
            for (PBOpen PBOpenCollectionPBOpen : payBox.getPBOpenCollection()) {
                PayBox oldPbIdOfPBOpenCollectionPBOpen = PBOpenCollectionPBOpen.getPbId();
                PBOpenCollectionPBOpen.setPbId(payBox);
                PBOpenCollectionPBOpen = em.merge(PBOpenCollectionPBOpen);
                if (oldPbIdOfPBOpenCollectionPBOpen != null) {
                    oldPbIdOfPBOpenCollectionPBOpen.getPBOpenCollection().remove(PBOpenCollectionPBOpen);
                    oldPbIdOfPBOpenCollectionPBOpen = em.merge(oldPbIdOfPBOpenCollectionPBOpen);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PayBox payBox) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PayBox persistentPayBox = em.find(PayBox.class, payBox.getId());
            List<PBOpen> PBOpenCollectionOld = persistentPayBox.getPBOpenCollection();
            List<PBOpen> PBOpenCollectionNew = payBox.getPBOpenCollection();
            List<PBOpen> attachedPBOpenCollectionNew = new ArrayList<PBOpen>();
            for (PBOpen PBOpenCollectionNewPBOpenToAttach : PBOpenCollectionNew) {
                PBOpenCollectionNewPBOpenToAttach = em.getReference(PBOpenCollectionNewPBOpenToAttach.getClass(), PBOpenCollectionNewPBOpenToAttach.getId());
                attachedPBOpenCollectionNew.add(PBOpenCollectionNewPBOpenToAttach);
            }
            PBOpenCollectionNew = attachedPBOpenCollectionNew;
            payBox.setPBOpenCollection(PBOpenCollectionNew);
            payBox = em.merge(payBox);
            for (PBOpen PBOpenCollectionOldPBOpen : PBOpenCollectionOld) {
                if (!PBOpenCollectionNew.contains(PBOpenCollectionOldPBOpen)) {
                    PBOpenCollectionOldPBOpen.setPbId(null);
                    PBOpenCollectionOldPBOpen = em.merge(PBOpenCollectionOldPBOpen);
                }
            }
            for (PBOpen PBOpenCollectionNewPBOpen : PBOpenCollectionNew) {
                if (!PBOpenCollectionOld.contains(PBOpenCollectionNewPBOpen)) {
                    PayBox oldPbIdOfPBOpenCollectionNewPBOpen = PBOpenCollectionNewPBOpen.getPbId();
                    PBOpenCollectionNewPBOpen.setPbId(payBox);
                    PBOpenCollectionNewPBOpen = em.merge(PBOpenCollectionNewPBOpen);
                    if (oldPbIdOfPBOpenCollectionNewPBOpen != null && !oldPbIdOfPBOpenCollectionNewPBOpen.equals(payBox)) {
                        oldPbIdOfPBOpenCollectionNewPBOpen.getPBOpenCollection().remove(PBOpenCollectionNewPBOpen);
                        oldPbIdOfPBOpenCollectionNewPBOpen = em.merge(oldPbIdOfPBOpenCollectionNewPBOpen);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = payBox.getId();
                if (findPayBox(id) == null) {
                    throw new NonexistentEntityException("The payBox with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PayBox payBox;
            try {
                payBox = em.getReference(PayBox.class, id);
                payBox.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The payBox with id " + id + " no longer exists.", enfe);
            }
            List<PBOpen> PBOpenCollection = payBox.getPBOpenCollection();
            for (PBOpen PBOpenCollectionPBOpen : PBOpenCollection) {
                PBOpenCollectionPBOpen.setPbId(null);
                PBOpenCollectionPBOpen = em.merge(PBOpenCollectionPBOpen);
            }
            em.remove(payBox);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PayBox> findPayBoxEntities() {
        return findPayBoxEntities(true, -1, -1);
    }

    public List<PayBox> findPayBoxEntities(int maxResults, int firstResult) {
        return findPayBoxEntities(false, maxResults, firstResult);
    }

    private List<PayBox> findPayBoxEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from PayBox as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PayBox findPayBox(Integer id) {
        EntityManager em = getEntityManager();
        PayBox p;
        try {
            p = em.find(PayBox.class, id);
            em.refresh(p);
            return p;
        } finally {
            em.close();
        }
    }

    public int getPayBoxCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from PayBox as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public static PayBoxJpaController getInstance() {
        if (pbjpa == null) {
            pbjpa = new PayBoxJpaController(DbConfig.getMap());
        }
        return pbjpa;
    }

}
