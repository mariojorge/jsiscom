/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.*;
import util.DbConfig;
import comsql.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;

/**
 *
 * @author usuario
 */
public class PBMoveJpaController {
    private static PBMoveJpaController pbmjpa;

    public PBMoveJpaController() {
        emf = Persistence.createEntityManagerFactory("COMSQLPU");
    }

    public PBMoveJpaController(Map config) {
        emf = Persistence.createEntityManagerFactory("COMSQLPU", config);
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PBMove PBMove) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PBOpen pbopenId = PBMove.getPbopenId();
            if (pbopenId != null) {
                pbopenId = em.getReference(pbopenId.getClass(), pbopenId.getId());
                PBMove.setPbopenId(pbopenId);
            }
            em.persist(PBMove);
            if (pbopenId != null) {
                pbopenId.getPBMoveCollection().add(PBMove);
                pbopenId = em.merge(pbopenId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PBMove PBMove) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PBMove persistentPBMove = em.find(PBMove.class, PBMove.getId());
            PBOpen pbopenIdOld = persistentPBMove.getPbopenId();
            PBOpen pbopenIdNew = PBMove.getPbopenId();
            if (pbopenIdNew != null) {
                pbopenIdNew = em.getReference(pbopenIdNew.getClass(), pbopenIdNew.getId());
                PBMove.setPbopenId(pbopenIdNew);
            }
            PBMove = em.merge(PBMove);
            if (pbopenIdOld != null && !pbopenIdOld.equals(pbopenIdNew)) {
                pbopenIdOld.getPBMoveCollection().remove(PBMove);
                pbopenIdOld = em.merge(pbopenIdOld);
            }
            if (pbopenIdNew != null && !pbopenIdNew.equals(pbopenIdOld)) {
                pbopenIdNew.getPBMoveCollection().add(PBMove);
                pbopenIdNew = em.merge(pbopenIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = PBMove.getId();
                if (findPBMove(id) == null) {
                    throw new NonexistentEntityException("The pBMove with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PBMove PBMove;
            try {
                PBMove = em.getReference(PBMove.class, id);
                PBMove.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The PBMove with id " + id + " no longer exists.", enfe);
            }
            PBOpen pbopenId = PBMove.getPbopenId();
            if (pbopenId != null) {
                pbopenId.getPBMoveCollection().remove(PBMove);
                pbopenId = em.merge(pbopenId);
            }
            em.remove(PBMove);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PBMove> findPBMoveEntities() {
        return findPBMoveEntities(true, -1, -1);
    }

    public List<PBMove> findPBMoveEntities(int maxResults, int firstResult) {
        return findPBMoveEntities(false, maxResults, firstResult);
    }

    private List<PBMove> findPBMoveEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from PBMove as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PBMove findPBMove(Integer id) {
        EntityManager em = getEntityManager();
        PBMove p;
        try {
            p = em.find(PBMove.class, id);
            em.refresh(p);
            return p;
        } finally {
            em.close();
        }
    }

    public int getPBMoveCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from PBMove as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public static PBMoveJpaController getInstance() {
        if (pbmjpa == null) {
            pbmjpa = new PBMoveJpaController(DbConfig.getMap());
        }
        return pbmjpa;
    }

}
