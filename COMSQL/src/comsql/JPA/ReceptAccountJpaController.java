/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.JPA.exceptions.IllegalOrphanException;
import comsql.JPA.exceptions.NonexistentEntityException;
import comsql.ReceptAccount;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import comsql.Customer;
import comsql.Sale;
import comsql.ReceptPlot;
import java.util.ArrayList;
import java.util.List;
import comsql.ReceptedPlot;

/**
 *
 * @author usuario
 */
public class ReceptAccountJpaController {
    private static ReceptAccountJpaController jpa;

    private ReceptAccountJpaController() {

    }

    public static ReceptAccountJpaController getInstance() {
        if (jpa == null) {
            jpa = new ReceptAccountJpaController();
        }
        return jpa;
    }

    public EntityManager getEntityManager() {
        return EntityManagerProvider.getEntityManagerFactory();
    }

    public void create(ReceptAccount receptAccount) {
        if (receptAccount.getReceptPlotCollection() == null) {
            receptAccount.setReceptPlotCollection(new ArrayList<ReceptPlot>());
        }
        if (receptAccount.getReceptedPlotCollection() == null) {
            receptAccount.setReceptedPlotCollection(new ArrayList<ReceptedPlot>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Customer customerId = receptAccount.getCustomerId();
            if (customerId != null) {
                customerId = em.getReference(customerId.getClass(), customerId.getId());
                receptAccount.setCustomerId(customerId);
            }
            Sale saleId = receptAccount.getSaleId();
            if (saleId != null) {
                saleId = em.getReference(saleId.getClass(), saleId.getId());
                receptAccount.setSaleId(saleId);
            }
            List<ReceptPlot> attachedReceptPlotCollection = new ArrayList<ReceptPlot>();
            for (ReceptPlot receptPlotCollectionReceptPlotToAttach : receptAccount.getReceptPlotCollection()) {
                receptPlotCollectionReceptPlotToAttach = em.getReference(receptPlotCollectionReceptPlotToAttach.getClass(), receptPlotCollectionReceptPlotToAttach.getId());
                attachedReceptPlotCollection.add(receptPlotCollectionReceptPlotToAttach);
            }
            receptAccount.setReceptPlotCollection(attachedReceptPlotCollection);
            List<ReceptedPlot> attachedReceptedPlotCollection = new ArrayList<ReceptedPlot>();
            for (ReceptedPlot receptedPlotCollectionReceptedPlotToAttach : receptAccount.getReceptedPlotCollection()) {
                receptedPlotCollectionReceptedPlotToAttach = em.getReference(receptedPlotCollectionReceptedPlotToAttach.getClass(), receptedPlotCollectionReceptedPlotToAttach.getId());
                attachedReceptedPlotCollection.add(receptedPlotCollectionReceptedPlotToAttach);
            }
            receptAccount.setReceptedPlotCollection(attachedReceptedPlotCollection);
            em.persist(receptAccount);
            if (customerId != null) {
                customerId.getReceptAccountCollection().add(receptAccount);
                customerId = em.merge(customerId);
            }
            if (saleId != null) {
                saleId.getReceptAccountCollection().add(receptAccount);
                saleId = em.merge(saleId);
            }
            for (ReceptPlot receptPlotCollectionReceptPlot : receptAccount.getReceptPlotCollection()) {
                ReceptAccount oldReceptaccountIdOfReceptPlotCollectionReceptPlot = receptPlotCollectionReceptPlot.getReceptaccountId();
                receptPlotCollectionReceptPlot.setReceptaccountId(receptAccount);
                receptPlotCollectionReceptPlot = em.merge(receptPlotCollectionReceptPlot);
                if (oldReceptaccountIdOfReceptPlotCollectionReceptPlot != null) {
                    oldReceptaccountIdOfReceptPlotCollectionReceptPlot.getReceptPlotCollection().remove(receptPlotCollectionReceptPlot);
                    oldReceptaccountIdOfReceptPlotCollectionReceptPlot = em.merge(oldReceptaccountIdOfReceptPlotCollectionReceptPlot);
                }
            }
            for (ReceptedPlot receptedPlotCollectionReceptedPlot : receptAccount.getReceptedPlotCollection()) {
                ReceptAccount oldReceptaccountIdOfReceptedPlotCollectionReceptedPlot = receptedPlotCollectionReceptedPlot.getReceptaccountId();
                receptedPlotCollectionReceptedPlot.setReceptaccountId(receptAccount);
                receptedPlotCollectionReceptedPlot = em.merge(receptedPlotCollectionReceptedPlot);
                if (oldReceptaccountIdOfReceptedPlotCollectionReceptedPlot != null) {
                    oldReceptaccountIdOfReceptedPlotCollectionReceptedPlot.getReceptedPlotCollection().remove(receptedPlotCollectionReceptedPlot);
                    oldReceptaccountIdOfReceptedPlotCollectionReceptedPlot = em.merge(oldReceptaccountIdOfReceptedPlotCollectionReceptedPlot);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ReceptAccount receptAccount) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ReceptAccount persistentReceptAccount = em.find(ReceptAccount.class, receptAccount.getId());
            Customer customerIdOld = persistentReceptAccount.getCustomerId();
            Customer customerIdNew = receptAccount.getCustomerId();
            Sale saleIdOld = persistentReceptAccount.getSaleId();
            Sale saleIdNew = receptAccount.getSaleId();
            List<ReceptPlot> receptPlotCollectionOld = persistentReceptAccount.getReceptPlotCollection();
            List<ReceptPlot> receptPlotCollectionNew = receptAccount.getReceptPlotCollection();
            List<ReceptedPlot> receptedPlotCollectionOld = persistentReceptAccount.getReceptedPlotCollection();
            List<ReceptedPlot> receptedPlotCollectionNew = receptAccount.getReceptedPlotCollection();
            List<String> illegalOrphanMessages = null;
            for (ReceptPlot receptPlotCollectionOldReceptPlot : receptPlotCollectionOld) {
                if (!receptPlotCollectionNew.contains(receptPlotCollectionOldReceptPlot)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ReceptPlot " + receptPlotCollectionOldReceptPlot + " since its receptaccountId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (customerIdNew != null) {
                customerIdNew = em.getReference(customerIdNew.getClass(), customerIdNew.getId());
                receptAccount.setCustomerId(customerIdNew);
            }
            if (saleIdNew != null) {
                saleIdNew = em.getReference(saleIdNew.getClass(), saleIdNew.getId());
                receptAccount.setSaleId(saleIdNew);
            }
            List<ReceptPlot> attachedReceptPlotCollectionNew = new ArrayList<ReceptPlot>();
            for (ReceptPlot receptPlotCollectionNewReceptPlotToAttach : receptPlotCollectionNew) {
                receptPlotCollectionNewReceptPlotToAttach = em.getReference(receptPlotCollectionNewReceptPlotToAttach.getClass(), receptPlotCollectionNewReceptPlotToAttach.getId());
                attachedReceptPlotCollectionNew.add(receptPlotCollectionNewReceptPlotToAttach);
            }
            receptPlotCollectionNew = attachedReceptPlotCollectionNew;
            receptAccount.setReceptPlotCollection(receptPlotCollectionNew);
            List<ReceptedPlot> attachedReceptedPlotCollectionNew = new ArrayList<ReceptedPlot>();
            for (ReceptedPlot receptedPlotCollectionNewReceptedPlotToAttach : receptedPlotCollectionNew) {
                receptedPlotCollectionNewReceptedPlotToAttach = em.getReference(receptedPlotCollectionNewReceptedPlotToAttach.getClass(), receptedPlotCollectionNewReceptedPlotToAttach.getId());
                attachedReceptedPlotCollectionNew.add(receptedPlotCollectionNewReceptedPlotToAttach);
            }
            receptedPlotCollectionNew = attachedReceptedPlotCollectionNew;
            receptAccount.setReceptedPlotCollection(receptedPlotCollectionNew);
            receptAccount = em.merge(receptAccount);
            if (customerIdOld != null && !customerIdOld.equals(customerIdNew)) {
                customerIdOld.getReceptAccountCollection().remove(receptAccount);
                customerIdOld = em.merge(customerIdOld);
            }
            if (customerIdNew != null && !customerIdNew.equals(customerIdOld)) {
                customerIdNew.getReceptAccountCollection().add(receptAccount);
                customerIdNew = em.merge(customerIdNew);
            }
            if (saleIdOld != null && !saleIdOld.equals(saleIdNew)) {
                saleIdOld.getReceptAccountCollection().remove(receptAccount);
                saleIdOld = em.merge(saleIdOld);
            }
            if (saleIdNew != null && !saleIdNew.equals(saleIdOld)) {
                saleIdNew.getReceptAccountCollection().add(receptAccount);
                saleIdNew = em.merge(saleIdNew);
            }
            for (ReceptPlot receptPlotCollectionNewReceptPlot : receptPlotCollectionNew) {
                if (!receptPlotCollectionOld.contains(receptPlotCollectionNewReceptPlot)) {
                    ReceptAccount oldReceptaccountIdOfReceptPlotCollectionNewReceptPlot = receptPlotCollectionNewReceptPlot.getReceptaccountId();
                    receptPlotCollectionNewReceptPlot.setReceptaccountId(receptAccount);
                    receptPlotCollectionNewReceptPlot = em.merge(receptPlotCollectionNewReceptPlot);
                    if (oldReceptaccountIdOfReceptPlotCollectionNewReceptPlot != null && !oldReceptaccountIdOfReceptPlotCollectionNewReceptPlot.equals(receptAccount)) {
                        oldReceptaccountIdOfReceptPlotCollectionNewReceptPlot.getReceptPlotCollection().remove(receptPlotCollectionNewReceptPlot);
                        oldReceptaccountIdOfReceptPlotCollectionNewReceptPlot = em.merge(oldReceptaccountIdOfReceptPlotCollectionNewReceptPlot);
                    }
                }
            }
            for (ReceptedPlot receptedPlotCollectionOldReceptedPlot : receptedPlotCollectionOld) {
                if (!receptedPlotCollectionNew.contains(receptedPlotCollectionOldReceptedPlot)) {
                    receptedPlotCollectionOldReceptedPlot.setReceptaccountId(null);
                    receptedPlotCollectionOldReceptedPlot = em.merge(receptedPlotCollectionOldReceptedPlot);
                }
            }
            for (ReceptedPlot receptedPlotCollectionNewReceptedPlot : receptedPlotCollectionNew) {
                if (!receptedPlotCollectionOld.contains(receptedPlotCollectionNewReceptedPlot)) {
                    ReceptAccount oldReceptaccountIdOfReceptedPlotCollectionNewReceptedPlot = receptedPlotCollectionNewReceptedPlot.getReceptaccountId();
                    receptedPlotCollectionNewReceptedPlot.setReceptaccountId(receptAccount);
                    receptedPlotCollectionNewReceptedPlot = em.merge(receptedPlotCollectionNewReceptedPlot);
                    if (oldReceptaccountIdOfReceptedPlotCollectionNewReceptedPlot != null && !oldReceptaccountIdOfReceptedPlotCollectionNewReceptedPlot.equals(receptAccount)) {
                        oldReceptaccountIdOfReceptedPlotCollectionNewReceptedPlot.getReceptedPlotCollection().remove(receptedPlotCollectionNewReceptedPlot);
                        oldReceptaccountIdOfReceptedPlotCollectionNewReceptedPlot = em.merge(oldReceptaccountIdOfReceptedPlotCollectionNewReceptedPlot);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = receptAccount.getId();
                if (findReceptAccount(id) == null) {
                    throw new NonexistentEntityException("The receptAccount with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ReceptAccount receptAccount;
            try {
                receptAccount = em.getReference(ReceptAccount.class, id);
                receptAccount.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The receptAccount with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<ReceptPlot> receptPlotCollectionOrphanCheck = receptAccount.getReceptPlotCollection();
            for (ReceptPlot receptPlotCollectionOrphanCheckReceptPlot : receptPlotCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This ReceptAccount (" + receptAccount + ") cannot be destroyed since the ReceptPlot " + receptPlotCollectionOrphanCheckReceptPlot + " in its receptPlotCollection field has a non-nullable receptaccountId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Customer customerId = receptAccount.getCustomerId();
            if (customerId != null) {
                customerId.getReceptAccountCollection().remove(receptAccount);
                customerId = em.merge(customerId);
            }
            Sale saleId = receptAccount.getSaleId();
            if (saleId != null) {
                saleId.getReceptAccountCollection().remove(receptAccount);
                saleId = em.merge(saleId);
            }
            List<ReceptedPlot> receptedPlotCollection = receptAccount.getReceptedPlotCollection();
            for (ReceptedPlot receptedPlotCollectionReceptedPlot : receptedPlotCollection) {
                receptedPlotCollectionReceptedPlot.setReceptaccountId(null);
                receptedPlotCollectionReceptedPlot = em.merge(receptedPlotCollectionReceptedPlot);
            }
            em.remove(receptAccount);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ReceptAccount> findReceptAccountEntities() {
        return findReceptAccountEntities(true, -1, -1);
    }

    public List<ReceptAccount> findReceptAccountEntities(int maxResults, int firstResult) {
        return findReceptAccountEntities(false, maxResults, firstResult);
    }

    private List<ReceptAccount> findReceptAccountEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from ReceptAccount as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ReceptAccount findReceptAccount(Integer id) {
        EntityManager em = getEntityManager();
        ReceptAccount r;
        try {
            r = em.find(ReceptAccount.class, id);
            em.refresh(r);
            return r;
        } finally {
            em.close();
        }
    }

    public int getReceptAccountCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from ReceptAccount as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
