/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql.JPA;

import comsql.*;
import util.DbConfig;
import comsql.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author usuario
 */
public class PBOpenJpaController {
    private static PBOpenJpaController pbojpa;

    public PBOpenJpaController() {
        emf = Persistence.createEntityManagerFactory("COMSQLPU");
    }
    
    public PBOpenJpaController(Map config) {
        emf = Persistence.createEntityManagerFactory("COMSQLPU", config);
    }

    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PBOpen PBOpen) {
        if (PBOpen.getPBMoveCollection() == null) {
            PBOpen.setPBMoveCollection(new ArrayList<PBMove>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PayBox pbId = PBOpen.getPbId();
            if (pbId != null) {
                pbId = em.getReference(pbId.getClass(), pbId.getId());
                PBOpen.setPbId(pbId);
            }
            List<PBMove> attachedPBMoveCollection = new ArrayList<PBMove>();
            for (PBMove PBMoveCollectionPBMoveToAttach : PBOpen.getPBMoveCollection()) {
                PBMoveCollectionPBMoveToAttach = em.getReference(PBMoveCollectionPBMoveToAttach.getClass(), PBMoveCollectionPBMoveToAttach.getId());
                attachedPBMoveCollection.add(PBMoveCollectionPBMoveToAttach);
            }
            PBOpen.setPBMoveCollection(attachedPBMoveCollection);
            em.persist(PBOpen);
            if (pbId != null) {
                pbId.getPBOpenCollection().add(PBOpen);
                pbId = em.merge(pbId);
            }
            for (PBMove PBMoveCollectionPBMove : PBOpen.getPBMoveCollection()) {
                PBOpen oldPbopenIdOfPBMoveCollectionPBMove = PBMoveCollectionPBMove.getPbopenId();
                PBMoveCollectionPBMove.setPbopenId(PBOpen);
                PBMoveCollectionPBMove = em.merge(PBMoveCollectionPBMove);
                if (oldPbopenIdOfPBMoveCollectionPBMove != null) {
                    oldPbopenIdOfPBMoveCollectionPBMove.getPBMoveCollection().remove(PBMoveCollectionPBMove);
                    oldPbopenIdOfPBMoveCollectionPBMove = em.merge(oldPbopenIdOfPBMoveCollectionPBMove);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PBOpen PBOpen) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PBOpen persistentPBOpen = em.find(PBOpen.class, PBOpen.getId());
            PayBox pbIdOld = persistentPBOpen.getPbId();
            PayBox pbIdNew = PBOpen.getPbId();
            List<PBMove> PBMoveCollectionOld = persistentPBOpen.getPBMoveCollection();
            List<PBMove> PBMoveCollectionNew = PBOpen.getPBMoveCollection();
            if (pbIdNew != null) {
                pbIdNew = em.getReference(pbIdNew.getClass(), pbIdNew.getId());
                PBOpen.setPbId(pbIdNew);
            }
            List<PBMove> attachedPBMoveCollectionNew = new ArrayList<PBMove>();
            for (PBMove PBMoveCollectionNewPBMoveToAttach : PBMoveCollectionNew) {
                PBMoveCollectionNewPBMoveToAttach = em.getReference(PBMoveCollectionNewPBMoveToAttach.getClass(), PBMoveCollectionNewPBMoveToAttach.getId());
                attachedPBMoveCollectionNew.add(PBMoveCollectionNewPBMoveToAttach);
            }
            PBMoveCollectionNew = attachedPBMoveCollectionNew;
            PBOpen.setPBMoveCollection(PBMoveCollectionNew);
            PBOpen = em.merge(PBOpen);
            if (pbIdOld != null && !pbIdOld.equals(pbIdNew)) {
                pbIdOld.getPBOpenCollection().remove(PBOpen);
                pbIdOld = em.merge(pbIdOld);
            }
            if (pbIdNew != null && !pbIdNew.equals(pbIdOld)) {
                pbIdNew.getPBOpenCollection().add(PBOpen);
                pbIdNew = em.merge(pbIdNew);
            }
            for (PBMove PBMoveCollectionOldPBMove : PBMoveCollectionOld) {
                if (!PBMoveCollectionNew.contains(PBMoveCollectionOldPBMove)) {
                    PBMoveCollectionOldPBMove.setPbopenId(null);
                    PBMoveCollectionOldPBMove = em.merge(PBMoveCollectionOldPBMove);
                }
            }
            for (PBMove PBMoveCollectionNewPBMove : PBMoveCollectionNew) {
                if (!PBMoveCollectionOld.contains(PBMoveCollectionNewPBMove)) {
                    PBOpen oldPbopenIdOfPBMoveCollectionNewPBMove = PBMoveCollectionNewPBMove.getPbopenId();
                    PBMoveCollectionNewPBMove.setPbopenId(PBOpen);
                    PBMoveCollectionNewPBMove = em.merge(PBMoveCollectionNewPBMove);
                    if (oldPbopenIdOfPBMoveCollectionNewPBMove != null && !oldPbopenIdOfPBMoveCollectionNewPBMove.equals(PBOpen)) {
                        oldPbopenIdOfPBMoveCollectionNewPBMove.getPBMoveCollection().remove(PBMoveCollectionNewPBMove);
                        oldPbopenIdOfPBMoveCollectionNewPBMove = em.merge(oldPbopenIdOfPBMoveCollectionNewPBMove);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = PBOpen.getId();
                if (findPBOpen(id) == null) {
                    throw new NonexistentEntityException("The pBOpen with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PBOpen PBOpen;
            try {
                PBOpen = em.getReference(PBOpen.class, id);
                PBOpen.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The PBOpen with id " + id + " no longer exists.", enfe);
            }
            PayBox pbId = PBOpen.getPbId();
            if (pbId != null) {
                pbId.getPBOpenCollection().remove(PBOpen);
                pbId = em.merge(pbId);
            }
            List<PBMove> PBMoveCollection = PBOpen.getPBMoveCollection();
            for (PBMove PBMoveCollectionPBMove : PBMoveCollection) {
                PBMoveCollectionPBMove.setPbopenId(null);
                PBMoveCollectionPBMove = em.merge(PBMoveCollectionPBMove);
            }
            em.remove(PBOpen);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PBOpen> findPBOpenEntities() {
        return findPBOpenEntities(true, -1, -1);
    }

    public List<PBOpen> findPBOpenEntities(int maxResults, int firstResult) {
        return findPBOpenEntities(false, maxResults, firstResult);
    }

    private List<PBOpen> findPBOpenEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from PBOpen as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            q.setHint("toplink.refresh", "true");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PBOpen findPBOpen(Integer id) {
        EntityManager em = getEntityManager();
        PBOpen p;
        try {
            p = em.find(PBOpen.class, id);
            em.refresh(p);
            return p;
        } finally {
            em.close();
        }
    }

    public int getPBOpenCount() {
        EntityManager em = getEntityManager();
        try {
            return ((Long) em.createQuery("select count(o) from PBOpen as o").getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public static PBOpenJpaController getInstance() {
        if (pbojpa == null) {
            pbojpa = new PBOpenJpaController(DbConfig.getMap());
        }
        return pbojpa;
    }
}
