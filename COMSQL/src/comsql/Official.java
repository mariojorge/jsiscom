/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "official")
@NamedQueries({@NamedQuery(name = "Official.findAll", query = "SELECT o FROM Official o"), @NamedQuery(name = "Official.findById", query = "SELECT o FROM Official o WHERE o.id = :id"), @NamedQuery(name = "Official.findByName", query = "SELECT o FROM Official o WHERE o.name = :name"), @NamedQuery(name = "Official.findByCpf", query = "SELECT o FROM Official o WHERE o.cpf = :cpf"), @NamedQuery(name = "Official.findByRg", query = "SELECT o FROM Official o WHERE o.rg = :rg"), @NamedQuery(name = "Official.findByCharge", query = "SELECT o FROM Official o WHERE o.charge = :charge"), @NamedQuery(name = "Official.findByAdmission", query = "SELECT o FROM Official o WHERE o.admission = :admission"), @NamedQuery(name = "Official.findBySalary", query = "SELECT o FROM Official o WHERE o.salary = :salary"), @NamedQuery(name = "Official.findByPayday", query = "SELECT o FROM Official o WHERE o.payday = :payday")})
public class Official implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "cpf")
    private String cpf;
    @Column(name = "rg")
    private String rg;
    @Column(name = "charge")
    private String charge;
    @Column(name = "admission")
    @Temporal(TemporalType.DATE)
    private Date admission;
    @Column(name = "salary")
    private Double salary;
    @Column(name = "payday")
    private String payday;
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    @ManyToOne
    private Address addressId;

    public Official() {
    }

    public Official(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        Integer oldId = this.id;
        this.id = id;
        changeSupport.firePropertyChange("id", oldId, id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldName = this.name;
        this.name = name;
        changeSupport.firePropertyChange("name", oldName, name);
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        String oldCpf = this.cpf;
        this.cpf = cpf;
        changeSupport.firePropertyChange("cpf", oldCpf, cpf);
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        String oldRg = this.rg;
        this.rg = rg;
        changeSupport.firePropertyChange("rg", oldRg, rg);
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        String oldCharge = this.charge;
        this.charge = charge;
        changeSupport.firePropertyChange("charge", oldCharge, charge);
    }

    public Date getAdmission() {
        return admission;
    }

    public void setAdmission(Date admission) {
        Date oldAdmission = this.admission;
        this.admission = admission;
        changeSupport.firePropertyChange("admission", oldAdmission, admission);
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        Double oldSalary = this.salary;
        this.salary = salary;
        changeSupport.firePropertyChange("salary", oldSalary, salary);
    }

    public String getPayday() {
        return payday;
    }

    public void setPayday(String payday) {
        String oldPayday = this.payday;
        this.payday = payday;
        changeSupport.firePropertyChange("payday", oldPayday, payday);
    }

    public Address getAddressId() {
        return addressId;
    }

    public void setAddressId(Address addressId) {
        Address oldAddressId = this.addressId;
        this.addressId = addressId;
        changeSupport.firePropertyChange("addressId", oldAddressId, addressId);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Official)) {
            return false;
        }
        Official other = (Official) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "comsql.Official[id=" + id + "]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

}
