/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author usuario
 */
@Embeddable
public class PurchaseProductPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "purchase_id")
    private int purchaseId;
    @Basic(optional = false)
    @Column(name = "product_id")
    private int productId;

    public PurchaseProductPK() {
    }

    public PurchaseProductPK(int purchaseId, int productId) {
        this.purchaseId = purchaseId;
        this.productId = productId;
    }

    public int getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(int purchaseId) {
        this.purchaseId = purchaseId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) purchaseId;
        hash += (int) productId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PurchaseProductPK)) {
            return false;
        }
        PurchaseProductPK other = (PurchaseProductPK) object;
        if (this.purchaseId != other.purchaseId) {
            return false;
        }
        if (this.productId != other.productId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "comsql.PurchaseProductPK[purchaseId=" + purchaseId + ", productId=" + productId + "]";
    }

}
