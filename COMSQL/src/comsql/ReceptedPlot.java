/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "receptedplot")
@NamedQueries({@NamedQuery(name = "ReceptedPlot.findAll", query = "SELECT r FROM ReceptedPlot r"), @NamedQuery(name = "ReceptedPlot.findById", query = "SELECT r FROM ReceptedPlot r WHERE r.id = :id"), @NamedQuery(name = "ReceptedPlot.findByDate", query = "SELECT r FROM ReceptedPlot r WHERE r.date = :date"), @NamedQuery(name = "ReceptedPlot.findByAmount", query = "SELECT r FROM ReceptedPlot r WHERE r.amount = :amount")})
public class ReceptedPlot implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Column(name = "amount")
    private Double amount;
    @Column(name = "number")
    private String number;
    @JoinColumn(name = "receptaccount_id", referencedColumnName = "id")
    @ManyToOne
    private ReceptAccount receptaccountId;
    @JoinColumn(name = "receivingtype_id", referencedColumnName = "id")
    @ManyToOne
    private ReceivingType receivingtypeId;

    public ReceptedPlot() {
    }

    public ReceptedPlot(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public ReceptAccount getReceptaccountId() {
        return receptaccountId;
    }

    public void setReceptaccountId(ReceptAccount receptaccountId) {
        this.receptaccountId = receptaccountId;
    }

    public ReceivingType getReceivingtypeId() {
        return receivingtypeId;
    }

    public void setReceivingtypeId(ReceivingType receivingtypeId) {
        this.receivingtypeId = receivingtypeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReceptedPlot)) {
            return false;
        }
        ReceptedPlot other = (ReceptedPlot) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "comsql.ReceptedPlot[id=" + id + "]";
    }

}
