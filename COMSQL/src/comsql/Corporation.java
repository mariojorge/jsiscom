/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "corporation")
@NamedQueries({@NamedQuery(name = "Corporation.findAll", query = "SELECT c FROM Corporation c"), @NamedQuery(name = "Corporation.findById", query = "SELECT c FROM Corporation c WHERE c.id = :id"), @NamedQuery(name = "Corporation.findByName", query = "SELECT c FROM Corporation c WHERE c.name = :name"), @NamedQuery(name = "Corporation.findByFantasyname", query = "SELECT c FROM Corporation c WHERE c.fantasyname = :fantasyname"), @NamedQuery(name = "Corporation.findByCnpj", query = "SELECT c FROM Corporation c WHERE c.cnpj = :cnpj"), @NamedQuery(name = "Corporation.findByIe", query = "SELECT c FROM Corporation c WHERE c.ie = :ie"), @NamedQuery(name = "Corporation.findByPhone", query = "SELECT c FROM Corporation c WHERE c.phone = :phone"), @NamedQuery(name = "Corporation.findByFax", query = "SELECT c FROM Corporation c WHERE c.fax = :fax"), @NamedQuery(name = "Corporation.findByEmail", query = "SELECT c FROM Corporation c WHERE c.email = :email"), @NamedQuery(name = "Corporation.findByLogo", query = "SELECT c FROM Corporation c WHERE c.logo = :logo")})
public class Corporation implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "fantasyname")
    private String fantasyname;
    @Column(name = "cnpj")
    private String cnpj;
    @Column(name = "ie")
    private String ie;
    @Column(name = "phone")
    private String phone;
    @Column(name = "fax")
    private String fax;
    @Column(name = "email")
    private String email;
    @Column(name = "homepage")
    private String homepage;
    @Column(name = "logo")
    private String logo;
    @Column(name = "lastaccess")
    @Temporal(TemporalType.DATE)
    private Date lastaccess;
    @Column(name = "pass")
    private String pass;
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    @ManyToOne
    private Address addressId;

    public Corporation() {
    }

    public Corporation(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFantasyname() {
        return fantasyname;
    }

    public void setFantasyname(String fantasyname) {
        this.fantasyname = fantasyname;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Date getLastaccess() {
        return lastaccess;
    }

    public void setLastaccess(Date lastaccess) {
        this.lastaccess = lastaccess;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Address getAddressId() {
        return addressId;
    }

    public void setAddressId(Address addressId) {
        this.addressId = addressId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Corporation)) {
            return false;
        }
        Corporation other = (Corporation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "comsql.Corporation[id=" + id + "]";
    }

}
