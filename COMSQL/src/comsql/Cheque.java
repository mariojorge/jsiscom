/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "cheque")
@NamedQueries({@NamedQuery(name = "Cheque.findAll", query = "SELECT c FROM Cheque c"), @NamedQuery(name = "Cheque.findById", query = "SELECT c FROM Cheque c WHERE c.id = :id"), @NamedQuery(name = "Cheque.findByEmitter", query = "SELECT c FROM Cheque c WHERE c.emitter = :emitter"), @NamedQuery(name = "Cheque.findByPhone", query = "SELECT c FROM Cheque c WHERE c.phone = :phone"), @NamedQuery(name = "Cheque.findByForday", query = "SELECT c FROM Cheque c WHERE c.forday = :forday"), @NamedQuery(name = "Cheque.findByNumber", query = "SELECT c FROM Cheque c WHERE c.number = :number"), @NamedQuery(name = "Cheque.findByAgency", query = "SELECT c FROM Cheque c WHERE c.agency = :agency"), @NamedQuery(name = "Cheque.findByBank", query = "SELECT c FROM Cheque c WHERE c.bank = :bank"), @NamedQuery(name = "Cheque.findByAmount", query = "SELECT c FROM Cheque c WHERE c.amount = :amount"), @NamedQuery(name = "Cheque.findByDate", query = "SELECT c FROM Cheque c WHERE c.date = :date")})
public class Cheque implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "emitter")
    private String emitter;
    @Column(name = "phone")
    private String phone;
    @Column(name = "forday")
    @Temporal(TemporalType.DATE)
    private Date forday;
    @Column(name = "number")
    private String number;
    @Column(name = "agency")
    private String agency;
    @Column(name = "bank")
    private String bank;
    @Column(name = "amount")
    private Double amount;
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;

    public Cheque() {
    }

    public Cheque(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmitter() {
        return emitter;
    }

    public void setEmitter(String emitter) {
        this.emitter = emitter;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getForday() {
        return forday;
    }

    public void setForday(Date forday) {
        this.forday = forday;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cheque)) {
            return false;
        }
        Cheque other = (Cheque) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "comsql.Cheque[id=" + id + "]";
    }

}
