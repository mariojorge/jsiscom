/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "pbmove")
@NamedQueries({@NamedQuery(name = "PBMove.findAll", query = "SELECT p FROM PBMove p"), @NamedQuery(name = "PBMove.findById", query = "SELECT p FROM PBMove p WHERE p.id = :id"), @NamedQuery(name = "PBMove.findByMovimentdate", query = "SELECT p FROM PBMove p WHERE p.movimentdate = :movimentdate"), @NamedQuery(name = "PBMove.findByDescription", query = "SELECT p FROM PBMove p WHERE p.description = :description"), @NamedQuery(name = "PBMove.findByDocnumber", query = "SELECT p FROM PBMove p WHERE p.docnumber = :docnumber"), @NamedQuery(name = "PBMove.findByAmount", query = "SELECT p FROM PBMove p WHERE p.amount = :amount"), @NamedQuery(name = "PBMove.findByObservation", query = "SELECT p FROM PBMove p WHERE p.observation = :observation")})
public class PBMove implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "movimentdate")
    @Temporal(TemporalType.DATE)
    private Date movimentdate;
    @Column(name = "movimenttime")
    @Temporal(TemporalType.TIME)
    private Date movimenttime;
    @Column(name = "description")
    private String description;
    @Column(name = "docnumber")
    private String docnumber;
    @Column(name = "amount")
    private Double amount;
    @Column(name = "observation")
    private String observation;
    @JoinColumn(name = "pbopen_id", referencedColumnName = "id")
    @ManyToOne
    private PBOpen pbopenId;

    public Date getMovimenttime() {
        return movimenttime;
    }

    public void setMovimenttime(Date movimenttime) {
        this.movimenttime = movimenttime;
    }

    public PBMove() {
    }

    public PBMove(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getMovimentdate() {
        return movimentdate;
    }

    public void setMovimentdate(Date movimentdate) {
        this.movimentdate = movimentdate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if (description.length() > 40)
            this.description = description.substring(0, 39);
        else
            this.description = description;
    }

    public String getDocnumber() {
        return docnumber;
    }

    public void setDocnumber(String docnumber) {
        this.docnumber = docnumber;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public PBOpen getPbopenId() {
        return pbopenId;
    }

    public void setPbopenId(PBOpen pbopenId) {
        this.pbopenId = pbopenId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PBMove)) {
            return false;
        }
        PBMove other = (PBMove) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "comsql.PBMove[id=" + id + "]";
    }

}
