/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import comsql.JPA.ProductJpaController;
import comsql.JPA.SaleJpaController;
import comsql.JPA.SaleProductJpaController;
import comsql.exceptions.IllegalOrphanException;
import comsql.exceptions.NonexistentEntityException;
import comsql.exceptions.PreexistingEntityException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Query;

/**
 *
 * @author usuario
 */
public class SaleFunctions {
    private static SaleFunctions sf;

    private SaleFunctions() {

    }

    public static SaleFunctions getInstance() {
        if (sf == null) {
            sf = new SaleFunctions();
        }
        return sf;
    }

    public Boolean Create(Sale sale, List<SaleProduct> SalesProducts, Double inPlotValue) {
        Boolean isSaledProduct = false;
        try {
            SaleJpaController.getInstance().create(sale);
            // Se compra for a vista lançar no caixa.
            if (sale.getSaletypeId().getId() == 1)
                PayBoxFunctions.getInstance().toRecept(new Date(), new Date(), "VENDA A VISTA - CÓD.: " + String.valueOf(sale.getId()), String.valueOf(sale.getId()), sale.getTotal(), "");
            if (sale.getSaletypeId().getId() == 2)
                PayBoxFunctions.getInstance().toRecept(new Date(), new Date(), "RECEBIMENTO - CÓD.: " + String.valueOf(sale.getId()) + " - PAR.: Entrada", String.valueOf(sale.getId()), inPlotValue, "");
            LogFunctions.getInstance().Insert("Efetuou a venda No: " + String.valueOf(sale.getId()));
        } catch (Exception e) {
            return false;
        }
        for (int i=0; i<SalesProducts.size(); i++) {
            SalesProducts.get(i).setSale(sale);
            try {
                SaleProductJpaController.getInstance().create(SalesProducts.get(i));
                isSaledProduct = true;
            } catch (PreexistingEntityException ex) {
                Logger.getLogger(SaleFunctions.class.getName()).log(Level.SEVERE, null, ex);
                isSaledProduct = false;
            } catch (Exception ex) {
                Logger.getLogger(SaleFunctions.class.getName()).log(Level.SEVERE, null, ex);
                isSaledProduct = false;
            }
            if (isSaledProduct) {
                Product product = SalesProducts.get(i).getProduct();
                int quantity = product.getQuantity() - SalesProducts.get(i).getQuantity();
                product.setQuantity(quantity);
                try {
                    ProductJpaController.getInstance().edit(product);
                } catch (IllegalOrphanException ex) {
                    Logger.getLogger(SaleFunctions.class.getName()).log(Level.SEVERE, null, ex);
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(SaleFunctions.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(SaleFunctions.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            isSaledProduct = false;
        }
        return true;
    }

    public Sale getSale(int id) {
        return SaleJpaController.getInstance().findSale(id);
    }

    public List findSales() {
        return SaleJpaController.getInstance().findSaleEntities();
    }

    public List findSales(Date dInicial, Date dFinal) {
        String strQuery = null;
        strQuery = "select s from Sale as s WHERE s.date BETWEEN :initial AND :final ORDER BY s.date DESC";
        Query query = SaleJpaController.getInstance().getEntityManager().createQuery(strQuery);
        query.setParameter("initial", dInicial);
        query.setParameter("final", dFinal);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findSales(Date dInicial, Date dFinal, Customer customer) {
        String strQuery = null;
        strQuery = "select s from Sale as s WHERE s.customerId = :customer AND s.date BETWEEN :initial AND :final ORDER BY s.id DESC";
        Query query = SaleJpaController.getInstance().getEntityManager().createQuery(strQuery);
        query.setParameter("initial", dInicial);
        query.setParameter("final", dFinal);
        query.setParameter("customer", customer);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public Boolean destroy(Sale sale) {
        Boolean wasDestroyed = true;
        if (sale.getSaleProductCollection() != null) {
            for (int i=0; i<sale.getSaleProductCollection().size(); i++) {
                int productId = sale.getSaleProductCollection().get(i).getProduct().getId();
                int quantity = sale.getSaleProductCollection().get(i).getQuantity();
                try {
                    SaleProductJpaController.getInstance().destroy(sale.getSaleProductCollection().get(i).getSaleProductPK());
                } catch (NonexistentEntityException ex) {
                    wasDestroyed = false;
                }
                if (wasDestroyed)
                    ProductFunctions.getInstance().UpdateQuantity(ProductFunctions.getInstance().getProduct(productId), quantity);
            }
            wasDestroyed = true;
        }
        // Lança o extorno no caixa se venda a vista
        if (sale.getSaletypeId().getId() == 1)
            PayBoxFunctions.getInstance().toRecept(new Date(), new Date(), "EXTORNO DE VENDA A VISTA - CÓD.: " + String.valueOf(sale.getId()), String.valueOf(sale.getId()), -(sale.getTotal()), "");
        try {
            SaleJpaController.getInstance().destroy(sale.getId());
        } catch (comsql.JPA.exceptions.IllegalOrphanException ex) {
            return false;
        } catch (comsql.JPA.exceptions.NonexistentEntityException ex) {
            return false;
        }
        return true;
    }
}
