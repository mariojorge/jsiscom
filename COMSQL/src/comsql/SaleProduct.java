/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author usuario
 */
@Entity
@Table(name = "sale_product")
@NamedQueries({@NamedQuery(name = "SaleProduct.findAll", query = "SELECT s FROM SaleProduct s"), @NamedQuery(name = "SaleProduct.findBySaleId", query = "SELECT s FROM SaleProduct s WHERE s.saleProductPK.saleId = :saleId"), @NamedQuery(name = "SaleProduct.findByProductId", query = "SELECT s FROM SaleProduct s WHERE s.saleProductPK.productId = :productId"), @NamedQuery(name = "SaleProduct.findByQuantity", query = "SELECT s FROM SaleProduct s WHERE s.quantity = :quantity"), @NamedQuery(name = "SaleProduct.findByPrice", query = "SELECT s FROM SaleProduct s WHERE s.price = :price")})
public class SaleProduct implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SaleProductPK saleProductPK;
    @Column(name = "quantity")
    private Integer quantity;
    @Column(name = "price")
    private Double price;
    @JoinColumn(name = "product_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Product product;
    @JoinColumn(name = "sale_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Sale sale;

    public SaleProduct() {
    }

    public SaleProduct(SaleProductPK saleProductPK) {
        this.saleProductPK = saleProductPK;
    }

    public SaleProduct(int saleId, int productId) {
        this.saleProductPK = new SaleProductPK(saleId, productId);
    }

    public SaleProductPK getSaleProductPK() {
        return saleProductPK;
    }

    public void setSaleProductPK(SaleProductPK saleProductPK) {
        this.saleProductPK = saleProductPK;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Double getTotal() {
        return this.getQuantity() * this.getPrice();
    }

    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (saleProductPK != null ? saleProductPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SaleProduct)) {
            return false;
        }
        SaleProduct other = (SaleProduct) object;
        if ((this.saleProductPK == null && other.saleProductPK != null) || (this.saleProductPK != null && !this.saleProductPK.equals(other.saleProductPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "comsql.SaleProduct[saleProductPK=" + saleProductPK + "]";
    }

}
