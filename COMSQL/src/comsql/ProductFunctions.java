/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package comsql;

import comsql.JPA.ManufacturerJpaController;
import comsql.JPA.ProductJpaController;
import comsql.JPA.PurchaseProductJpaController;
import comsql.JPA.UnitJpaController;
import comsql.exceptions.IllegalOrphanException;
import comsql.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Query;

/**
 *
 * @author usuario
 */
public class ProductFunctions {
    private static ProductFunctions pf;
    private ProductJpaController pjpa;

    private ProductFunctions() {
        pjpa = ProductJpaController.getInstance();
    }

    public static ProductFunctions getInstance() {
        if (pf == null) {
            pf = new ProductFunctions();
        }
        return pf;
    }

    public Boolean Create(Product p) {
        try {
            pjpa.create(p);
            LogFunctions.getInstance().Insert("Cadastrou o produto \"" + p.getName() +
                    "\" de código \"" + p.getId() + "\"");
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public Boolean Edit(Product p ) {
        try {
            pjpa.edit(p);
            LogFunctions.getInstance().Insert("Editou o produto \"" + p.getName() +
                    "\" de código \"" + p.getId() + "\"");
        } catch (IllegalOrphanException ex) {
            ex.printStackTrace();
        } catch (NonexistentEntityException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public Boolean Destroy(int productId) {
        String productName = ProductFunctions.getInstance().getProduct(productId).getName();
        try {
            pjpa.destroy(productId);
            LogFunctions.getInstance().Insert("Apagou o produto \"" + productName +
                    "\" de código \"" + productId + "\"");
        } catch (IllegalOrphanException ex) {
            return false;
        } catch (NonexistentEntityException ex) {
            return false;
        }
        return true;
    }
    
    public Product getProduct(int i) {
        return pjpa.findProduct(i);
    }

    public Product SearchByBarCode(String barcode) {
        String strQuery = null;
        strQuery = "select p from Product as p WHERE p.barcode = :barcode";
        Query query = pjpa.getEntityManager().createQuery(strQuery);
        query.setParameter("barcode", barcode);
        query.setMaxResults(1);
        query.setHint("toplink.refresh", "true");
        return (Product) query.getSingleResult();
    }

    public List SearchProducts() {
        String strQuery = null;
        strQuery = "select p from Product as p ORDER BY p.name";
        Query query = pjpa.getEntityManager().createQuery(strQuery);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List SearchProductsByName(String name) {
        String strQuery = null;
        strQuery = "select p from Product as p WHERE p.name LIKE :name";
        Query query = pjpa.getEntityManager().createQuery(strQuery);
        query.setParameter("name", '%' + name + '%');
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List SearchProductsById(int id) {
        String strQuery = null;
        strQuery = "SELECT p FROM Product as p WHERE p.id = :id";
        Query query = pjpa.getEntityManager().createQuery(strQuery);
        query.setParameter("id", id);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List SearchProductsByBarCode(String barcode) {
        String strQuery = null;
        strQuery = "SELECT p FROM Product as p WHERE p.barcode = :barcode";
        Query query = pjpa.getEntityManager().createQuery(strQuery);
        query.setParameter("barcode", barcode);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List SearchProductByUni(String unit) {
        String strQuery = null;
        strQuery = "select p from Product as p WHERE p.unit LIKE :unit";
        Query query = pjpa.getEntityManager().createQuery(strQuery);
        query.setParameter("unit", '%' + unit + '%');
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List SearchProductByManufacturer(String manufacturer) {
        String strQuery = null;
        strQuery = "select p from Product as p WHERE p.manufacturer LIKE :manufacturer";
        Query query = pjpa.getEntityManager().createQuery(strQuery);
        query.setParameter("manufacturer", '%' + manufacturer + '%');
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List SearchProducts(String where, String Value) {
        String strQuery = null;
        int id = 0;
        
        if (where.compareTo("id") == 0) {
             try {
                 id = Integer.valueOf(Value);
             } catch (Exception e) {
                 id = -1;
             }
             strQuery = "select p from Product as p WHERE p.id = :value";
        }

        if (where.compareTo("name") == 0) {
             strQuery = "select p from Product as p WHERE p.name LIKE :value";
        }

        if (where.compareTo("barcode") == 0) {
             strQuery = "select p from Product as p WHERE p.barcode = :value";
        }

        Query query = pjpa.getEntityManager().createQuery(strQuery);

        if (where.compareTo("id") == 0)
            query.setParameter("value", id);
        else if (where.compareTo("barcode") == 0)
            query.setParameter("value", Value);
        else
            query.setParameter("value", "%" + Value + "%");

        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public List findUnits() {
        String strQuery = null;
        strQuery = "select u from Unit as u ORDER BY u.description";
        Query query = pjpa.getEntityManager().createQuery(strQuery);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public Boolean CreateUnit(String description) {
        Unit u = new Unit();
        u.setDescription(description);
        try {
            UnitJpaController.getInstance().create(u);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public List findManufacturers() {
        String strQuery = null;
        strQuery = "SELECT m FROM Manufacturer m ORDER BY m.description";
        Query query = pjpa.getEntityManager().createQuery(strQuery);
        query.setHint("toplink.refresh", "true");
        return query.getResultList();
    }

    public Boolean CreateManufacturer(String description) {
        Manufacturer m = new Manufacturer();
        m.setDescription(description);
        try {
            ManufacturerJpaController.getInstance().create(m);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /** Type: 0 - BARCODE    1- NAME  */
    public Boolean VerifyIfProductExist(String value) {
        if (value.isEmpty())
            return false;
        List<Product> list1 = SearchProductsByBarCode(value);
        if (list1.size() > 0)
            return true;
        return false;
    }

    public Boolean deletePurchaseProduct(PurchaseProductPK id) {
        int productId = id.getProductId();
        try {
            PurchaseProductJpaController.getInstance().destroy(id);
        } catch (NonexistentEntityException ex) {
            return false;
        }
        return true;
    }

    public Boolean deleteAllPurchaseProduct(Product p) {
        for (int i=0; i<p.getPurchaseProductCollection().size(); i++) {
            try {
                PurchaseProductJpaController.getInstance().destroy(p.getPurchaseProductCollection().get(i).getPurchaseProductPK());
            } catch (NonexistentEntityException ex) {
                return false;
            }
        }
        return true;
    }

    public Boolean UpdateQuantity(Product p, int quantity) {
        p.setQuantity(p.getQuantity() + quantity);
        try {
            ProductJpaController.getInstance().edit(p);
        } catch (IllegalOrphanException ex) {
            Logger.getLogger(ProductFunctions.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(ProductFunctions.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (Exception ex) {
            Logger.getLogger(ProductFunctions.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
}
