package util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 
 * @author thomasmodeneis 2008.
 *
 */
public class Calendario {
	
	 //Retorna a instancia unica da classe Singleton
    public static Calendario getInstance() {
        return CalendarioHolder.instance;
    }

    //Classe auxiliar para criacao da instancia. Evita problemas de sincronizacao de threads.
    private static class CalendarioHolder {
           private static Calendario instance = new Calendario();
    }
	
	//************** CONSTRUTORES *******************//
	
	/**
	 * Cria um calendar usando a data atual do sistema, popula todos os valores possiveis.	 
	 */
	public Calendario(){
		Calendar calendar = getCalendar();
		calendar.setTimeInMillis(System.currentTimeMillis());
		setter(calendar);this.dataFormatada = format(calendar, null);
	}
	
	/**
	 * usa um calendar que j� possua uma data, popula todos os valores possiveis.
	 * @param calendar
	 */
	public Calendario(Calendar calendar){
		setter(calendar);this.dataFormatada = format(calendar, null);
	}
	
	/**
	 * cria um calendar com o tempo passado como argumento, 
	 * popula todos os valores possiveis.	 
	 * @param currentTimeMillis
	 */
	public Calendario(long currentTimeMillis){			
		Calendar calendar = getCalendar();
		calendar.setTimeInMillis(currentTimeMillis);
		setter(calendar);this.dataFormatada = format(calendar, null);
	}
	
	/**
	 * cria um calendar com o tempo passado como argumento,
	 * e dateFormatted de acordo com @param mask.
	 * @param currentTimeMillis
	 * @param mask
	 */
	public Calendario(long currentTimeMillis, String mask){		
		Calendar calendar = getCalendar();
		calendar.setTimeInMillis(currentTimeMillis);
		setter(calendar);this.dataFormatada = format(calendar, mask);
	}
	
	
	//********************* METODOS UTEIS *************************//
	/**
	 * 
	 * @return (Calendar) GregorianCalendar;
	 */
	public static Calendar getCalendar(){		
		return (Calendar) new GregorianCalendar(); 
	}	
	
	/**
	 * Retorna calendario ocidental
	 * @return
	 */
	public static Calendar getOcidentalCalendar() {
        Calendar c = getCalendar();
		c.setFirstDayOfWeek(Calendar.MONDAY);
        c.setMinimalDaysInFirstWeek(4);
        return c;
    }

	/**
	 * "Reseta" a data para hora=0, minuto=0, segundo=0, milli=0 e AM
	 * @param d Data a ser "resetada"
	 */
	public static Date resetTime(Date d) {
		if(d==null){return null;}
		Calendar c = getCalendar();
		c.setTime(d);
		c.set(Calendar.HOUR, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		c.set(Calendar.AM_PM, Calendar.AM);
		return c.getTime();
	}

	/**
     * Remove o Milisegundo da data 
     * @return
     */
    public static Date resetMs(Date d) {   
    	if(d==null){return null;}
        Calendar c = getCalendar();
        c.setTime(d);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }
    
    /**
     * retorna a data: ano, mes, dia, 23, 59, 59
     * @param dataFim
     * @param mask
     * @return
     */
	public static Date getDate235959(Date data, String mask) {	
		if(data==null){return null;}
		String[] dataAux = format(data,null).split("/");		
		int dia = Integer.parseInt(dataAux[0]);
		int mes = Integer.parseInt(dataAux[1]);
		int ano = Integer.parseInt(dataAux[2]);		
		Calendar c = Calendar.getInstance();
		c.set(ano, mes, dia,23,59,59);
		return c.getTime();
	}

	/**
	 * Retorna dia relativo ao calendario 
	 * segunda dia 1;
	 * terca dia 2;
	 * quarta dia 3;
	 * quinta dia 4;
	 * sexta dia 5;
	 * sabado dia 6;
	 * domingo dia 7; 	 
	 */
	public static Integer diaRelativo(Date data){
		if(data==null){return null;}
		Calendar c = Calendar.getInstance();		
		c.setTime(data);		
		switch (c.get(Calendar.DAY_OF_WEEK)) {
			case 1:	return 7;				
			case 2:	return 1;				
			case 3:	return 2;				
			case 4:	return 3;				
			case 5:	return 4;				
			case 6:	return 5;				
			case 7:	return 6;					
		}return null;
	}
	
	/**
	 * formata datas a partir de 1 calendario e mask
	 * @param calendar
	 * @param mask
	 * @return
	 */
	public static String format(Calendar calendar, String mask){	
		if(calendar==null){return null;}
		return new SimpleDateFormat(mask == null ? DDMMYYYY : mask).format(new Date(calendar.getTimeInMillis()));
	}
	/**
	 * formata datas a partir de uma data e mask, padrao dd/mm/yyyy
	 * @param data
	 * @return
	 */
	public static String format(Date data, String mask){	
		if(data==null){return null;}
		return new SimpleDateFormat(mask == null ? DDMMYYYY : mask).format(data);
	}	
			
	public static final String DDMMYYYY = "dd/MM/yyyy";
	public static final String DDMMYYYYHH = "dd/MM/yyyy:HH";
	public static final String DDMMYYYYHHMM = "dd/MM/yyyy:HH:mm";
	public static final String DDMMYYYYHHMMSS = "dd/MM/yyyy:HH:mm:ss";
	
	public static final String MMDDYYYY = "MM/dd/yyyy";
	public static final String MMDDYYYYHH = "MM/dd/yyyy:HH:ss";
	public static final String MMDDYYYYHHMM = "MM/dd/yyyy:HH:mm";
	public static final String MMDDYYYYHHMMSS = "MM/dd/yyyy:HH:mm:ss";
	
	public static final String HHMM = "HH:mm";
	public static final String HHMMSS = "HH:mm:ss";
	
	private int ANO;
	private int MES;
	private int SEMANA_DO_ANO;
	private int SEMANA_DO_MES;
	private int DATA;
	private int DIA_DO_MES;
	private int DIA_DO_ANO;
	private int DIA_DA_SEMANA;
	private int DIA_DA_SEMANA_NO_MES;
	private int AM_PM;
	private int HORA;
	private int HORA_DO_DIA;
	private int MINUTO;
	private int SEGUNDO;
	private int MILLISEGUNDO;
	private int ZONE_OFFSET;
	private int DST_OFFSET;	
	
	private String dataFormatada;
	

	private void setter(Calendar calendar){
		this.ANO=calendar.get(Calendar.YEAR);
		this.MES=calendar.get(Calendar.MONTH);
		this.SEMANA_DO_ANO=calendar.get(Calendar.WEEK_OF_YEAR);
		this.SEMANA_DO_MES=calendar.get(Calendar.WEEK_OF_MONTH);
		this.DATA=calendar.get(Calendar.DATE);
		this.DIA_DO_MES=calendar.get(Calendar.DAY_OF_MONTH);		 
		this.DIA_DO_ANO=calendar.get(Calendar.DAY_OF_YEAR);
		this.DIA_DA_SEMANA=calendar.get(Calendar.DAY_OF_WEEK);
		this.DIA_DA_SEMANA_NO_MES=calendar.get(Calendar.DAY_OF_WEEK_IN_MONTH);
		this.AM_PM=calendar.get(Calendar.AM_PM);
		this.HORA=calendar.get(Calendar.HOUR);
		this.HORA_DO_DIA=calendar.get(Calendar.HOUR_OF_DAY);
		this.MINUTO=calendar.get(Calendar.MINUTE);
		this.SEGUNDO=calendar.get(Calendar.SECOND);
		this.MILLISEGUNDO=calendar.get(Calendar.MILLISECOND);
		this.ZONE_OFFSET=calendar.get(Calendar.ZONE_OFFSET);
		this.DST_OFFSET=calendar.get(Calendar.DST_OFFSET);
	}
	public int getANO() {
		return ANO;
	}
	public void setANO(int ano) {
		ANO = ano;
	}
	public int getMES() {
		return MES;
	}
	public void setMES(int mes) {
		MES = mes;
	}
	public int getSEMANA_DO_ANO() {
		return SEMANA_DO_ANO;
	}
	public void setSEMANA_DO_ANO(int semana_do_ano) {
		SEMANA_DO_ANO = semana_do_ano;
	}
	public int getSEMANA_DO_MES() {
		return SEMANA_DO_MES;
	}
	public void setSEMANA_DO_MES(int semana_do_mes) {
		SEMANA_DO_MES = semana_do_mes;
	}
	public int getDATA() {
		return DATA;
	}
	public void setDATA(int data) {
		DATA = data;
	}
	public int getDIA_DO_MES() {
		return DIA_DO_MES;
	}
	public void setDIA_DO_MES(int dia_do_mes) {
		DIA_DO_MES = dia_do_mes;
	}
	public int getDIA_DO_ANO() {
		return DIA_DO_ANO;
	}
	public void setDIA_DO_ANO(int dia_do_ano) {
		DIA_DO_ANO = dia_do_ano;
	}
	public int getDIA_DA_SEMANA() {
		return DIA_DA_SEMANA;
	}
	public void setDIA_DA_SEMANA(int dia_da_semana) {
		DIA_DA_SEMANA = dia_da_semana;
	}
	public int getDIA_DA_SEMANA_NO_MES() {
		return DIA_DA_SEMANA_NO_MES;
	}
	public void setDIA_DA_SEMANA_NO_MES(int dia_da_semana_no_mes) {
		DIA_DA_SEMANA_NO_MES = dia_da_semana_no_mes;
	}
	public int getAM_PM() {
		return AM_PM;
	}
	public void setAM_PM(int am_pm) {
		AM_PM = am_pm;
	}
	public int getHORA() {
		return HORA;
	}
	public void setHORA(int hora) {
		HORA = hora;
	}
	public int getHORA_DO_DIA() {
		return HORA_DO_DIA;
	}
	public void setHORA_DO_DIA(int hora_do_dia) {
		HORA_DO_DIA = hora_do_dia;
	}
	public int getMINUTO() {
		return MINUTO;
	}
	public void setMINUTO(int minuto) {
		MINUTO = minuto;
	}
	public int getSEGUNDO() {
		return SEGUNDO;
	}
	public void setSEGUNDO(int segundo) {
		SEGUNDO = segundo;
	}
	public int getMILLISEGUNDO() {
		return MILLISEGUNDO;
	}
	public void setMILLISEGUNDO(int millisegundo) {
		MILLISEGUNDO = millisegundo;
	}
	public int getZONE_OFFSET() {
		return ZONE_OFFSET;
	}
	public void setZONE_OFFSET(int zone_offset) {
		ZONE_OFFSET = zone_offset;
	}
	public int getDST_OFFSET() {
		return DST_OFFSET;
	}
	public void setDST_OFFSET(int dst_offset) {
		DST_OFFSET = dst_offset;
	}
	public String getDataFormatada() {
		return dataFormatada;
	}
	public void setDataFormatada(String dataFormatada) {
		this.dataFormatada = dataFormatada;
	}
	
	@Override
	public String toString() {		
		String toString = 		
		 "ANO: " + getANO()+"\n"		
		+"MES: " + getMES()+"\n"
		+"SEMANA_DO_ANO: "+getSEMANA_DO_ANO()+"\n"
		+"SEMANA_DO_MES: "+getSEMANA_DO_MES()+"\n"
		+"DATA: " + getDATA()+"\n"
		+"DIA_DO_MES: "+getDIA_DO_MES()+"\n"
		+"DIA_DO_ANO: " + getDIA_DO_ANO()+"\n"
		+"DIA_DA_SEMANA: " +getDIA_DA_SEMANA()+"\n"
		+"DIA_DA_SEMANA_NO_MES: "+getDIA_DA_SEMANA_NO_MES()+"\n"
		+"AM_PM: " + getAM_PM()+"\n"
		+"HORA: " + getHORA()+"\n"
		+"HORA_DO_DIA: "+getHORA_DO_DIA()+"\n"
		+"MINUTO: " + getMINUTO()+"\n"
		+"SEGUNDO: " +getSEGUNDO()+"\n"
		+"MILLISEGUNDO: " +getMILLISEGUNDO()+"\n"
		+"ZONE_OFFSET: "+(getZONE_OFFSET()) / (60 * 60 * 1000)+"\n"
		+"DST_OFFSET: "+ (getDST_OFFSET()) / (60 * 60 * 1000)+"\n"
		+"DATA FORMATADA: "+getDataFormatada()+"\n";	
		return toString;
	}
}
