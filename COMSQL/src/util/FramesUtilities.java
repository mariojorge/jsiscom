/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

/**
 *
 * @author usuario
 */
public class FramesUtilities {

    public static void focusLostTextField(JFormattedTextField field) {
        String s = field.getText();
        if (s.compareTo("") == 0) {
            field.setText("0");
        }
        field.setText(s.replace(".", ","));
        try {
            field.commitEdit();
        } catch (Exception e) {
            field.setValue(0);
        }
        try {
            field.setFormatterFactory(new DefaultFormatterFactory(new NumberFormatter(NumberFormat.getCurrencyInstance())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        Number d = (Number) field.getValue();
        Double i = d.doubleValue();
        if (i <= 0) {
            i = i * -1;
            if (i == -0)
                i = 0.0;
            field.setValue(i);
        }
    }

    public static void focusLostTextField(JTextField field) {
        String j = field.getText();
        j = j.replace(".", ",");
        Number n = null;
        try {
            n = NumberFormat.getNumberInstance(Locale.getDefault()).parse(j);
        } catch (ParseException ex) {
            n = 0;
        }
        Double d = n.doubleValue();
        if (d <= 0) {
            d = d * -1;
            if (d == -0)
                d = 0.0;
        }
        field.setText(NumberFormat.getCurrencyInstance(Locale.getDefault()).format(d));
    }

    public static void focusGainedTextField(JFormattedTextField field) {
        field.setFormatterFactory(new DefaultFormatterFactory(new NumberFormatter(NumberFormat.getNumberInstance(Locale.getDefault()))));
    }

    public static void focusGainedTextField(JTextField field) {
        String s = field.getText();
        if (!s.isEmpty()) {
            Number n = null;
            Double d = null;

            s = s.substring(3, s.length());
            try {
                n = NumberFormat.getNumberInstance(Locale.getDefault()).parse(s);
            } catch (ParseException ex) {
                Logger.getLogger(FramesUtilities.class.getName()).log(Level.SEVERE, null, ex);
            }
            d = n.doubleValue();
            s = String.valueOf(NumberFormat.getNumberInstance(Locale.getDefault()).format(d));
            s = s.replace(".", ",");
        } else {
            s = "";
        }
        field.setText(s);
        field.selectAll();
    }

}
