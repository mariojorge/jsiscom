/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import comsql.CorporationFunctions;
import comsql.PayBoxFunctions;
import java.awt.Dialog.ModalExclusionType;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author usuario
 */
public class PrintReport {
    private Connection connection  = null;
    private String url;
    private String user;
    private String password;
    private Boolean isGenerated = false;
    private JasperPrint jasperPrint;
    
    public PrintReport() {
        Properties p = new Properties();
        FileInputStream f = null;
        try {
            f = new FileInputStream("database.properties");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PrintReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            p.load(f);
        } catch (IOException ex) {
            Logger.getLogger(PrintReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        url = p.getProperty("url");
        user = p.getProperty("user");
        password = p.getProperty("password");
    }

    private Connection getConnection() throws ClassNotFoundException, SQLException {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            connection = (Connection) DriverManager.getConnection(url, user, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    public void loadingReport(String query, String file) {
        Map parameters = new HashMap();
        parameters.put("ENTERPRISE_NAME", CorporationFunctions.getInstance().getCorporation().getName());
        Connection con = null;
        try {
            con = getConnection();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(PrintReport.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(PrintReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        Statement stm = null;
        try {
            stm = (Statement) con.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(PrintReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        ResultSet rs = null;
        try {
            rs = stm.executeQuery(query);
        } catch (SQLException ex) {
            Logger.getLogger(PrintReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);
        file = "reports/" + file + ".jasper";
        try {
            jasperPrint = JasperFillManager.fillReport(file, parameters, jrRS);
        } catch (JRException ex) {
            Logger.getLogger(PrintReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        isGenerated = true;
        try {
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(PrintReport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void showReport() {
        JasperViewer jv = new JasperViewer(jasperPrint, false);
        jv.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
        jv.setTitle("Visualizar Relatório");
        jv.setVisible(true);
    }

    public Boolean isGenerated() {
        if (isGenerated)
            return true;
        else
            return false;
    }

}