/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;

/**
 *
 * @author usuario
 */
public class MyTryIcon {
    private static SystemTray tray;
    private static TrayIcon trayIcon;
    private static Image image;

    public MyTryIcon() {
        if (SystemTray.isSupported()) {
            tray = SystemTray.getSystemTray();
            image = Toolkit.getDefaultToolkit().getImage("imagens/bar-chart-32x32.png");
            trayIcon = new TrayIcon(image);
            trayIcon.setImageAutoSize(true);
            try {
                tray.add(trayIcon);
            } catch (AWTException e) {

            }
        }
    }

    public static void showMessage(String caption, String message) {
        if (trayIcon != null)
            trayIcon.displayMessage(caption, message, MessageType.NONE);
    }

    public static void showInfoMessage(String caption, String message) {
        if (trayIcon != null)
            trayIcon.displayMessage(caption, message, MessageType.INFO);
    }

    public static void showErrorMessage(String caption, String message) {
        if (trayIcon != null)
            trayIcon.displayMessage(caption, message, MessageType.ERROR);
    }

    public static void showWarningMessage(String caption, String message) {
        if (trayIcon != null)
            trayIcon.displayMessage(caption, message, MessageType.WARNING);
    }

}
