/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import comsql.User;
import frames.User.UserGetPermissionDialog;
import javax.swing.JOptionPane;

/**
 *
 * @author usuario
 */
public class VerifyPermission {

    private VerifyPermission() {

    }
    
    public static Boolean hasPermissionToDelete() {
        boolean canExecute = false;
        if (LoggedUser.isAdministrator()) {
            canExecute = true;
        } else {
            if (VerifyPermission.hasPermission())
                canExecute = true;
        }
        return canExecute;        
    }

    public static Boolean hasPermissionToOpenDialog() {
        return VerifyPermission.hasPermissionToDelete();
    }

    public static Boolean hasPermission() {
        // Verifica se o usuario esta logado
        if (LoggedUser.isLogged()) {
            if (LoggedUser.getLoggedUser().getUserLevel() == 1 ||
                    LoggedUser.getLoggedUser().getUser().compareTo("SUPORTE") == 0) {
                return true;
            } else {
                UserGetPermissionDialog getPermissionDialog = new UserGetPermissionDialog(null, true);
                getPermissionDialog.setLocationRelativeTo(null);
                getPermissionDialog.setVisible(true);
                User u = getPermissionDialog.getLoggedUser();
                if (u != null) {
                    if (hasPermission(u))
                        return true;
                }
            }
        }
        JOptionPane.showMessageDialog(null, "USUÁRIO SEM AUTORIZAÇÃO!", "Mensagem", 2);
        return false;
    }

    private static Boolean hasPermission(User u) {
        // Verifica se o usuario esta logado
        if (u.getUserLevel() == 1 || u.getUser().compareTo("SUPORTE") == 0) {
                return true;
        }
        return false;
    }
}
