/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import comsql.User;

/**
 *
 * @author usuario
 */
public class LoggedUser {
    private static User loggedUser = null;
    private static LoggedUser instance;

    private LoggedUser() {

    }

    public static LoggedUser getInstance() {
        if (instance == null) {
            instance = new LoggedUser();
        }
        return instance;
    }

    public static User getLoggedUser() {
        if (loggedUser != null) {
            return loggedUser;
        }
        return null;
    }

    public static void setLoggedUser(User u) {
        LoggedUser.loggedUser = u;
    }

    public static Boolean isLogged() {
        if (LoggedUser.getLoggedUser() != null)
            return true;
        return false;
    }

    public static Boolean isAdministrator() {
        if (isLogged()) {
            if (getLoggedUser().getUserLevel() == 1)
                return true;
        }
        return false;
    }
}
