package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;


public class CpuSerial {
    /**
     * Get the CPU serial
     *
     * @return the value of string
     */
    public static String getCPUSerial() {
        String result = "";
        String SO = (String) System.getProperties().get("os.name");
        if (SO.contains("Windows")) {
            try {
                File file = File.createTempFile("tmp", ".vbs");
                file.deleteOnExit();
                FileWriter fw = new java.io.FileWriter(file);

                String vbs =
                "On Error Resume Next \r\n\r\n" +
                "strComputer = \".\"  \r\n" +
                "Set objWMIService = GetObject(\"winmgmts:\" _ \r\n" +
                "    & \"{impersonationLevel=impersonate}!\\\\\" & strComputer & \"\\root\\cimv2\") \r\n" +
                "Set colItems = objWMIService.ExecQuery(\"Select * from Win32_Processor\")  \r\n " +
                "For Each objItem in colItems\r\n " +
                "    Wscript.Echo objItem.ProcessorId  \r\n " +
                "    exit for  ' do the first cpu only! \r\n" +
                "Next                    ";


                fw.write(vbs);
                fw.close();
                Process p = Runtime.getRuntime().exec("cscript //NoLogo " + file.getPath());
                BufferedReader input =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));
                String line;
                while ((line = input.readLine()) != null) {
                    result += line;
                }
                input.close();
            } catch (Exception e) {

            }
            if (result.trim().length() < 1 || result == null) {
                result = "NO_CPU_ID";
            }
        } else {
            // CODIGO PARA DESCOBRIR CPU_SERIAL NO LINUX
        }
        return result.trim();
    }

}