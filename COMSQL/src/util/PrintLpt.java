/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class PrintLpt {
    private FileOutputStream fos = null;
    private PrintStream ps = null;
    private int colNumber = 38;
    private Boolean showNotepad = false;
    private final String file = "reports/printjob.txt";

    public PrintLpt() {
        try {
            fos = new FileOutputStream(file, false);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PrintLpt.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            ps = new PrintStream(fos);
        } catch (Exception e) {

        }
        showNotepad = true;
    }

    public PrintLpt(String port) {
        try {
            fos = new FileOutputStream(port);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PrintLpt.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            ps = new PrintStream(fos);
        } catch (Exception e) {

        }
    }
    
    public void ChangeColNumber(int number) {
        this.colNumber = number;
    }

    public void PrintLine(String s) {
        if (s.length() > this.colNumber)
            ps.println(s.substring(0, this.colNumber - 1));
        else
            ps.println(s);
    }
    
    public void Close() {
        if (showNotepad) {
            try {
                Runtime.getRuntime().exec("notepad.exe " + file);
            } catch (IOException ex) {
                Logger.getLogger(PrintLpt.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        ps.close();
        try {
            fos.close();
        } catch (IOException ex) {
            Logger.getLogger(PrintLpt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }



}
