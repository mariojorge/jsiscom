/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class DbConfig {
    // toplink.jdbc.user
    private static String user;
    // toplink.jdbc.password
    private static String password;
    // toplink.jdbc.url
    private static String url;
    private static Map config;
    
    private void DBConfig() {

    }

    public static Map getMap() {
        Properties p = new Properties();
        FileInputStream f = null;
        try {
            f = new FileInputStream("database.properties");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DbConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            p.load(f);
        } catch (IOException ex) {
            Logger.getLogger(DbConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
        url = p.getProperty("url");
        user = p.getProperty("user");
        password = p.getProperty("password");
        config = new HashMap();
        config.put("toplink.jdbc.user", user);
        config.put("toplink.jdbc.password", password);
        config.put("toplink.jdbc.url", url);
        return config;
    }

}
