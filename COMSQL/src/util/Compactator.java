/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

/**
 *
 * @author usuario
 */
import java.io.*;
import java.util.zip.*;

public class Compactator {

   // ------------------------------------------------ Constantes
   static final int TAMANHO_BUFFER = 2048; // 2kb

   public void compactar (String arqSaida) {
      int i, cont;
      byte[] dados = new byte[TAMANHO_BUFFER];
      String arquivos[];
      File f = null;
      BufferedInputStream origem = null;
      FileInputStream streamDeEntrada = null;
      FileOutputStream destino = null;
      ZipOutputStream saida = null;
      ZipEntry entry = null;

      try {
         destino = new FileOutputStream(arqSaida);
         saida = new ZipOutputStream(new BufferedOutputStream(destino));
         f = new File("."); // Todos os arquivos da pasta onde a classe está
         arquivos = f.list();

         for (i = 0; i < arquivos.length; i++) {
            File arquivo = new File(arquivos[i]);

            if (arquivo.isFile() && !(arquivo.getName()).equals(arqSaida)) {
               System.out.println("Compactando: " + arquivos[i]);

               streamDeEntrada = new FileInputStream(arquivo);
               origem = new BufferedInputStream(streamDeEntrada, TAMANHO_BUFFER);
               entry = new ZipEntry(arquivos[i]);
               saida.putNextEntry(entry);

               while((cont = origem.read(dados, 0, TAMANHO_BUFFER)) != -1) {
                  saida.write(dados, 0, cont);
               }

               origem.close();
            }
         }

         saida.close();

      } catch(Exception e) {
         e.printStackTrace();
      }
   }
}