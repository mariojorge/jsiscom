/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import javax.swing.text.*;
/**
 *
 * @author usuario
 */
public class FixedLengthDocument extends PlainDocument {
   /**
    * Tamanho máximo default do componente = 4000 caracteres
    */
   private int iMaxLength;
   /**
    * Cria o validador com o tamanho máximo de 4000
    */
   /**
    * Cria o validador com o tamanho definido no parâmetro
    *
    * @param tamanho Tamanho máximo da cadeia de caracteres
    */
   public FixedLengthDocument(int maxlen) {
      super();
      iMaxLength = maxlen;
   }

        /**
    * A cada tecla pressionada valida a tecla verifica se não está no máximo que o campo pode aguentar.
    *
    * @see javax.swing.text.Document#insertString(int, java.lang.String, javax.swing.text.AttributeSet)
    */
    @Override
   public void insertString(int offset, String str, AttributeSet attr)
       throws BadLocationException {
    if (str == null) return;

    if (iMaxLength <= 0)     // aceitara qualquer no. de caracteres
    {
        super.insertString(offset, str, attr);
        return;
    }

    int ilen = (getLength() + str.length());
    if (ilen <= iMaxLength)    // se o comprimento final for menor...
        super.insertString(offset, str, attr);   // ...aceita str
    else
    {
        if (getLength() == iMaxLength) return; // nada a fazer
        String newStr = str.substring(0, (iMaxLength - getLength()));

        super.insertString(offset, newStr, attr);
    }

   }
}
